#!ruby

require 'webrick'
include WEBrick

class WebgetsComponent < HTTPServlet::AbstractServlet
    attr_accessor :label
    
    def initialize(server, label)    
        super(server)
        @label = label
    end

    def to_s
        "component: "+label    
    end

    def do_GET(req, res)
        res.body = "<html><body>"+self.to_s()+"</body></html>"
        res['Content-Type'] = "text/html"
    end
end

class WebgetsContainer < WebgetsComponent
    attr_accessor :components
    
    def initialize(server, components)    
        super(server, components.length.to_s + " components")
        @components = components
    end
    
    def add(component)
        @components += [component]
    end
    
    def remove(component)
        @components.delete(component)
    end
    
    def each # iterate over the components
        @components.each { |component| yield component }
    end
    
    def do_GET(req, res)
        res.body = "<html><body>container: "+@label+" ( "
        self.each { |component| res.body+=component.to_s+", " }
        res.body += ")</body></html>"
        res['Content-Type'] = "text/html"
    end
end
