#!/usr/local/bin/ruby
require 'webrick'
require 'webgets'
include WEBrick

s = HTTPServer.new(
  :Port            => 2000,
  :DocumentRoot    => "/Public/Projects/Webgets/ruby"
)

## mount subdirectories
# s.mount("/ipr", HTTPServlet::FileHandler, "/proj/ipr/public_html")
# s.mount("/~gotoyuzo",
#        HTTPServlet::FileHandler, "/Public/Projects/Webgets/ruby",
#        true)  #<= allow to show directory index.

a_component = WebgetsComponent.new(s, "one component")
b_component = WebgetsComponent.new(s, "two component")

s.mount("/component", WebgetsComponent, "test component")
s.mount("/container", WebgetsContainer, [a_component, b_component])

trap("INT"){ s.shutdown }
s.start

