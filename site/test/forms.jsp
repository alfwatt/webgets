<%@page import="org.webgets.html.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:document id="test_forms" label="Webgets Forms Components">
<%
		LabeledColumns tools = new LabeledColumns( 300);
		test_forms.addComponent( tools);

		LabeledList leftInputs = new LabeledList( "Unregistred Inputs");
		tools.addComponent( new Form( leftInputs));		

		leftInputs.addComponent( Input.createResetInput( "Reset"));
		leftInputs.addComponent( Input.createSubmitInput( "Submit"));
		leftInputs.addComponent( Input.createHiddenInput( "hidden", "hidden-value"));
		leftInputs.addComponent( Input.createButtonInput( "button", "button-value"));
		leftInputs.addComponent( new Group( "radio", new Input[]
		{
			Input.createRadioInput( "radio", "radio-1", false),
			Input.createRadioInput( "radio", "radio-2", true),
			Input.createRadioInput( "radio", "radio-3", false)
		}));
		leftInputs.addComponent( new Group( "checkbox", new Input[]
		{
			Input.createCheckboxInput( "checkbox", "checkbox-1", true),
			Input.createCheckboxInput( "checkbox", "checkbox-2", true),
			Input.createCheckboxInput( "checkbox", "checkbox-3", false)
		}));
		leftInputs.addComponent( Input.createFileInput( "file", "file-name", "text/plain"));
		leftInputs.addComponent( Input.createImageInput( "image", "center", "../images/green.gif"));
		leftInputs.addComponent( Input.createPasswordInput( "password", "password-value", 20, 10));
		leftInputs.addComponent( Input.createTextInput( "text", "text-value", 20, 10));
		leftInputs.addComponent( new TextArea( "textarea", "textarea-value", 5, 30));
		
		
		LabeledList rightInputs = new LabeledList( "Registred Inputs");
		Form rightForm = new Form( rightInputs);
		tools.addComponent( rightForm);
		
		rightInputs.addComponent( Input.createResetInput( "Reset", rightForm));
		rightInputs.addComponent( Input.createSubmitInput( "Submit", rightForm));
		rightInputs.addComponent( Input.createHiddenInput( "hidden", "hidden-value", rightForm));
		rightInputs.addComponent( Input.createButtonInput( "button", "button-value", rightForm));
		rightInputs.addComponent( new Group( "radio", new Input[]
		{
			Input.createRadioInput( "radio", "radio-1", false, rightForm),
			Input.createRadioInput( "radio", "radio-2", true, rightForm),
			Input.createRadioInput( "radio", "radio-3", false, rightForm)
		}));
		rightInputs.addComponent( new Group( "checkbox", new Input[]
		{
			Input.createCheckboxInput( "checkbox", "checkbox-1", true, rightForm),
			Input.createCheckboxInput( "checkbox", "checkbox-2", true, rightForm),
			Input.createCheckboxInput( "checkbox", "checkbox-3", false, rightForm)
		}));
		rightInputs.addComponent( Input.createFileInput( "file", "file-name", 
		                                                "text/plain", rightForm));
		rightInputs.addComponent( Input.createImageInput( 
			"image", "center", "../images/green.gif", rightForm));
		rightInputs.addComponent( Input.createPasswordInput( "password", "password-value", 
		                                                     20, 10, rightForm));
		rightInputs.addComponent( Input.createTextInput( "text", "text-value", 
		                                                 20, 10, rightForm));
		rightInputs.addComponent( new TextArea( "textarea", "textarea-value", 5, 30, rightForm));
%>
</webgets:document>
