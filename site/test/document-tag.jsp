<%@ page language="java" import="org.webgets.*, org.webgets.html.*, org.webgets.webtop.*" %>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:document id="test_document" label="Webgets Tag Library Support">
<%
	// webgets code goes here, makes refrence to 'document' object
	Image.registerLocalPath( request, "../images/");
	test_document.addComponent( new ToggleContainer( new Align( 
		new Border( new P( "Webgets Tag Library Support")))));
%>
</webgets:document>
