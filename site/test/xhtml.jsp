<%@ page language="java" import="org.webgets.*, org.webgets.xhtml.*" %>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:component id="xhtml" classname="org.webgets.xhtml.XHTMLDocument">
<%
	xhtml.setLabel( "XHTML Test Document");
	Table table = new Table();
		Table.Row row = new Table.Row();
			Table.Data data = new Table.Data();
			   data.add( new Cdata( "Hello XHTML"));
			row.add( data);
		table.add( row);
	xhtml.add( table);
%>
</webgets:component>
