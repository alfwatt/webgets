<%@page import="org.webgets.webtop.*, org.w3c.dom.*, javax.xml.parsers.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>

<webgets:document id="test_xml" label="Webgets XML Tools">
<%
	TabGroup tabs = new TabGroup();
	org.webgets.html.Image.registerLocalPath( request, "../images/");
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder parser = factory.newDocumentBuilder();
	Document events = parser.parse("/Public/Webgets/doc/patterns/test-pattern.xml");
	BorderTreeListener target = new BorderTreeListener( "Selected Node");
	tabs.addComponent( new org.webgets.html.Columns( "Test Pattern", new org.webgets.Component[] { new DOMTree( events, target), target}));
	tabs.addComponent( new DOMBrowser( events));
	test_xml.addComponent( new org.webgets.html.Border( tabs));
%>
</webgets:document>
