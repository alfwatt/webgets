<%@ page language="java" import="org.webgets.*, org.webgets.html.*, org.webgets.webtop.*" %>
<%@ taglib uri="/webgets" prefix="webgets"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/bsf-1.0" prefix="bsf" %>
<html>
<head><title>BSF Jacl Scriptlet</title></head>
<body>

<bsf:scriptlet language="jacl">

  $out println "Hello world"

</bsf:scriptlet>

</body>
</html>
