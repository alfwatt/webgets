<%@page import="org.webgets.html.*"%>
<%
	Document html = new Document( "HTML Package");
	LabeledColumns htmlTabs = new LabeledColumns();	

	LabeledList components = new LabeledList( "Components");
				components.addComponent( new Align( new P( "Align"), Align.CENTER));
				components.addComponent( new Area( "Area", 10, 10));
				components.addComponent( new Block( "html <b>Block</b>"));
				components.addComponent( new Border( new P( "Border")));
				components.addComponent( new Style( "Style &amp; Font", 
					new Font( "sans-serif", "blue", "+2")));
				components.addComponent( new Image( "Image", "image.jpg"));
				components.addComponent( new Label( "Label", "labeled text"));
				components.addComponent( new Link( "http://www.webgets.org", "Webgets"));
				components.addComponent( new P( "P"));
				components.addComponent( new Pre( "PRE"));
				components.addComponent( new Form( new Select( "Select", "foo", 
					new String[] { "foo", "bar", "baz"}, new String[] { "Foo", "Bar", "Baz"})));
				components.addComponent( new ShortLabel( 
					"ShortLabel takes care of very long lable labels, this one will have to be shortened",
					"ShortLabel takes care of very long lable labels, this one will have to be shortened"));
				components.addComponent( new Label( "Spacer", new Spacer( 50, 50)));
				components.addComponent( new Form( new TextArea( "TextArea", "text area", 5, 10)));
				components.addComponent( new Label( "Thermometer", new Thermometer( 0.5f)));
				components.addComponent( new Warning( "Warning!"));
	htmlTabs.addComponent( components);

	LabeledList containers = new LabeledList( "Containers");
		Banner banner = new Banner( "Banner");
			banner.addComponent( new P( "Banner Item"));
			banner.addComponent( new P( "Banner Item"));
		containers.addComponent( banner);						

		BulletList blist = new BulletList( "Bullets");
		blist.addComponent( new Block( "Bullet!"));
		blist.addComponent( new Block( "Another Bullet!"));
		containers.addComponent( blist);
	
		Columns columns = new Columns( "Columns", 30);
		columns.addComponent( new Block( "Column One"));
		columns.addComponent( new Block( "Column Two"));
		containers.addComponent( columns);
	
		Flow flow = new Flow( "Flow");
		flow.addComponent( new Block( "text "));
		flow.addComponent( new Image( "Sloth!", "image.jpg", 24, 24));
		flow.addComponent( new Block( " Flow"));
		containers.addComponent( flow);
		
		containers.addComponent( new Form( new Input( Input.SUBMIT_TYPE, "Form")));
		
		Group group = new Group( "Group");
		group.addComponent( new P( "Grouped"));
		group.addComponent( new Image( "Grouped", "image.jpg", 20, 20));
		group.addComponent( new Style( "Grouped", Font.PLUS_1));
		containers.addComponent( group);
		
		LabeledColumns lcols = new LabeledColumns( "LabeledColumns", 30);
		lcols.addComponent( new Label( "Column One", "foo"));
		lcols.addComponent( new Label( "Column Two", "bar"));
		containers.addComponent( lcols);
	
		List list = new List( "List");
		list.addComponent( new Block( "Item One"));
		list.addComponent( new Block( "Item Two"));
		list.addComponent( new Block( "Item Three"));
		containers.addComponent( list);
		
		MenuList menu = new MenuList( "Menu List");
		menu.addComponent( new Block( "Item One"));
		menu.addComponent( new Block( "Item Two"));
		menu.addComponent( new Block( "Item Three"));
		containers.addComponent( menu);
		
		OrdinalList olist = new OrdinalList( "Ordinal List");
		olist.addComponent( new Block( "Item One"));
		olist.addComponent( new Block( "Item Two"));
		olist.addComponent( new Block( "Item Three"));
		containers.addComponent( olist);
		
		Table table = new Table( "Table", 3);
		table.addComponent( new Block( "Item One"));
		table.addComponent( new Block( "Item Two"));
		table.addComponent( new Block( "Item Three"));
		table.addComponent( new Block( "Item Four"));
		table.addComponent( new Block( "Item Five"));
		table.addComponent( new Block( "Item Six"));
		table.addComponent( new Block( "Item Seven"));
		containers.addComponent( table);
	htmlTabs.addComponent( containers);
	
		LabeledList decorators = new LabeledList( "Decorators");
			decorators.addComponent( new Style( "Style.B", Style.B));
			decorators.addComponent( new Style( "Style.BIG", Style.BIG));
			decorators.addComponent( new Style( "Style.I", Style.I));
			decorators.addComponent( new Style( "Style.SMALL", Style.SMALL));
			decorators.addComponent( new Style( "Style.STRIKE", Style.STRIKE));
			decorators.addComponent( new Style( "Style.TT", Style.TT));
			decorators.addComponent( new Style( "Style.U", Style.U));
			decorators.addComponent( new Style( "Style.SMALL", Style.SMALL));
			decorators.addComponent( new Style( "Font.MINUS_1", Font.MINUS_1));
			decorators.addComponent( new Style( "Font.MINUS_2", Font.MINUS_2));
			decorators.addComponent( new Style( "Font.PLUS_1", Font.PLUS_1));
			decorators.addComponent( new Style( "Font.PLUS_2", Font.PLUS_2));
			decorators.addComponent( new Style( "Font.PLUS_3", Font.PLUS_3));
			decorators.addComponent( new Style( "Font.PLUS_4", Font.PLUS_4));
			decorators.addComponent( new Style( "Font.SANS_MINUS_1", Font.SANS_MINUS_1));
			decorators.addComponent( new Style( "Font.SANS_MINUS_2", Font.SANS_MINUS_2));
			decorators.addComponent( new Style( "Font.SANS_PLUS_1", Font.SANS_PLUS_1));
			decorators.addComponent( new Style( "Font.SANS_PLUS_2", Font.SANS_PLUS_2));
			decorators.addComponent( new Style( "Font.SANS_PLUS_3", Font.SANS_PLUS_3));
			decorators.addComponent( new Style( "Font.SANS_PLUS_4", Font.SANS_PLUS_4));
			decorators.addComponent( new Style( "Font.SIZE_1", Font.SIZE_1));
			decorators.addComponent( new Style( "Font.SIZE_2", Font.SIZE_2));
			decorators.addComponent( new Style( "Font.SIZE_3", Font.SIZE_3));
			decorators.addComponent( new Style( "Font.SIZE_4", Font.SIZE_4));
			decorators.addComponent( new Style( "Font.SIZE_5", Font.SIZE_5));
			decorators.addComponent( new Style( "Font.SIZE_6", Font.SIZE_6));
			decorators.addComponent( new Style( "Font.SIZE_7", Font.SIZE_7));
			decorators.addComponent( new Style( "Font.SANS_SIZE_1", Font.SANS_SIZE_1));
			decorators.addComponent( new Style( "Font.SANS_SIZE_2", Font.SANS_SIZE_2));
			decorators.addComponent( new Style( "Font.SANS_SIZE_3", Font.SANS_SIZE_3));
			decorators.addComponent( new Style( "Font.SANS_SIZE_4", Font.SANS_SIZE_4));
			decorators.addComponent( new Style( "Font.SANS_SIZE_5", Font.SANS_SIZE_5));
			decorators.addComponent( new Style( "Font.SANS_SIZE_6", Font.SANS_SIZE_6));
			decorators.addComponent( new Style( "Font.SANS_SIZE_7", Font.SANS_SIZE_7));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_1", Font.SERIF_SIZE_1));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_2", Font.SERIF_SIZE_2));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_3", Font.SERIF_SIZE_3));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_4", Font.SERIF_SIZE_4));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_5", Font.SERIF_SIZE_5));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_6", Font.SERIF_SIZE_6));
			decorators.addComponent( new Style( "Font.SERIF_SIZE_7", Font.SERIF_SIZE_7));
		htmlTabs.addComponent( decorators);
		html.addComponent( htmlTabs);
		html.init( this.getServletConfig());
		html.service( request, response);
%>
