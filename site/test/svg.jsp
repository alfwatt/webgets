<%@ page language="java" import="org.webgets.*, org.webgets.svg.*" %><% // HACK this needs a real servlet...

SVGDocument svg = new SVGDocument();
svg.setLabel( "Hello SVG"); 

Group g = new Group();
g.setTransform( "rotate( 10)");

g.addComponent( new Rect( "10",  "10",  "red"));
g.addComponent( new Circle( "75", "75", "25", "pink"));

g.addComponent( new Rect( "120", "10",  "green"));
g.addComponent( new Circle( "175", "50", "20", "lightgreen"));

g.addComponent( new Rect( "10",  "120", "blue"));
g.addComponent( new Circle( "50", "180", "30", "lightblue"));

g.addComponent( new Rect( "120", "120", "grey"));
g.addComponent( new Circle( "170", "170", "45", "lightgrey"));
svg.addComponent( g);

svg.addComponent( new Text( "Hello SVG", "100", "100"));
svg.addComponent( new Grid());

/*	 
Rect rGreen = new Rect();
	 rGreen.setFill( "green");
     rGreen.setX( "120");
     rGreen.setY( "10");
     rGreen.setWidth( "100");
     rGreen.setHeight( "100");
svg.addComponent( rGreen);

Rect rBlue = new Rect();
	 rBlue.setFill( "blue");
	 rGreen.setFill( "green");
     rGreen.setX( "10");
     rGreen.setY( "120");
     rGreen.setWidth( "100");
     rGreen.setHeight( "100");
svg.addComponent( rBlue);

svg.addComponent( new Rect());
svg.addComponent( new Circle());
svg.addComponent( new Ellipse());
svg.addComponent( new Line());
svg.addComponent( new Polyline( "poly", "30,30 10,80 200,200 500,500"));
svg.addComponent( new Polygon( "gone", "100,100 150,150 100,200"));
Group g = new Group();
g.setTransform( "rotate(15)");
g.addComponent( new Rect());
g.addComponent( new Ellipse());
svg.addComponent( g);
Text hello = new Text( "Hello SVG");
hello.setX( "200");
hello.setY( "200");
hello.setFontSize( "48");
svg.addComponent( hello);
*/

svg.service( request, response);


%>
