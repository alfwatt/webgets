<%@ page language="java" import="org.webgets.*, org.webgets.html.*, org.webgets.webtop.*" %>
<%@ taglib uri="/webgets" prefix="webgets"%>
<html>
<head><title>Webgets Component Tag</title></head>
<body>

<webgets:component id="test_component_list" classname="org.webgets.html.List">
<%
	Image.registerLocalPath( request, "../images/");
	test_component_list.addComponent( new P( "Webgets Component Tag"));
	test_component_list.addComponent( new ToggleContainer( new Block( Integer.toString( test_component_list.hashCode())))); 
%>
</webgets:component>

<webgets:component id="test_component_border" classname="org.webgets.html.Border">
<%
	test_component_border.addComponent( new Block( "Foo!")); 
%>
</webgets:component>

</body>
</html>
