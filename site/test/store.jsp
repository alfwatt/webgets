<%@page import="org.webgets.html.*, org.webgets.webtop.*, org.webgets.store.*, org.webgets.demos.patterns.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:document id="test_store" label="Webgets ObjectStore Test">
<%
	IndexedStore serial = new SerialBeanStore( "/tmp/beans");
	Pattern created = (Pattern) serial.create( Pattern.class);
	created.setName( "Name");
	created.setContext( "Context");
	created.setProblem( "Problem");
	created.setSolution( "Solution");
	created.setAuthor( "author@site.com");
	String key = serial.insert( created);
	Pattern selected = (Pattern) serial.select( key);
	
	test_store.addComponent( new P( "SerialBeanStore Test"));
	test_store.addComponent( Browser.createBrowser( created, Pattern.BEAN_INFO));
	test_store.addComponent( new P( key));
	test_store.addComponent( Browser.createBrowser( selected, Pattern.BEAN_INFO));
		
	test_store.addComponent( new P( "Index List"));
	BulletList indexList = new BulletList();
	test_store.addComponent( indexList);		

	String[] indexes = serial.getIndexes();
	int index = 0;
	while ( index < indexes.length)
	{
		String indexName = indexes[index];
		indexList.addComponent( new org.webgets.StringComp( indexes[index++]));
		BulletList keyList = new BulletList();
		String[] keys = serial.getIndexKeys( indexName);
		int keyIndex = 0;
		while ( keyIndex < keys.length)
			keyList.addComponent( new org.webgets.StringComp( keys[keyIndex++]));
		indexList.addComponent( keyList);
	}
%>
</webgets:document>
