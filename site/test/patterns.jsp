<%@page import="org.webgets.html.*, org.webgets.webtop.*, org.webgets.demos.patterns.*, org.webgets.store.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:document id="test_patterns" label="Webgets Patterns Tools">
<%
	 ObjectStore serial = new SerialBeanStore( "/tmp/beans");
	 Pattern pn = new Pattern( "Test Case");

	 Columns tools = new Columns( 256);
	 tools.setGutter( 25);
	 test_patterns.addComponent( tools);
	 tools.addComponent( Editor.createEditor( pn, Pattern.BEAN_INFO, serial));
	 tools.addComponent( new Border( new PatternBrowser( pn)));
%>
</webgets:document>
