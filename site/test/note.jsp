<%@ page language="java" import="org.webgets.*, org.webgets.html.*, org.webgets.webtop.*" %>
<%@ taglib uri="/webgets" prefix="webgets"%>
<html>
<head><title>Webgets Component Tag</title></head>
<body>

<webgets:component id="test_note" classname="org.webgets.webtop.Note">
<%
	test_note.setText( "Hello Note");
%>
</webgets:component>

</body>
</html>
