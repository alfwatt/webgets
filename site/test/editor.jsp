<%@page import="org.webgets.html.*, org.webgets.webtop.*, org.webgets.demos.patterns.*, org.webgets.store.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:document id="test_editor" label="Webgets Editor Components">
<%
		ObjectStore serial = new SerialBeanStore( "/tmp/beans");
		Pattern pattern = new Pattern( "Test Pattern");

		TabGroup editors = new TabGroup();
		test_editor.addComponent( new Align( editors));
		editors.addComponent( new Label( "Pattern Only", 
			Editor.createEditor( pattern)));
		editors.addComponent( new Label( "Pattern With BeanInfo", 
			Editor.createEditor( pattern, Pattern.BEAN_INFO)));
		editors.addComponent( new Label( "Pattern With ObjectStore", 
			Editor.createEditor( pattern, serial)));
		editors.addComponent( new Label( "Pattern With BeanInfo and ObjectStore", 
			Editor.createEditor( pattern, Pattern.BEAN_INFO, serial)));
%>
</webgets:document>
