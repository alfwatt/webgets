<%@page import="org.webgets.html.*, org.webgets.scratch.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>
<webgets:document id="test_scratch" label="Webgets Java Scratch Pad">
<%
	Insets insets = new Insets( new JavaPad());
	insets.setLeft( "50");
	insets.setRight( "50");
	insets.setTop( "25");
	test_scratch.addComponent( insets);
%>
</webgets:document>
