﻿<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE taglib PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN"
        				"http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd" >

<taglib>
	<tlibversion>1.0</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>webgets</shortname>
	<uri>/webgets</uri>
	<info>tld for webgets document and component tags </info>

	<tag> 
		<name>component</name>
		<tagclass>org.webgets.tags.ComponentTag</tagclass>
		<teiclass>org.webgets.tags.ComponentTEI</teiclass>
		<bodycontent>JSP</bodycontent>
		<info>The component tag supports an id attribute to set it's
		name in the enclosed script and a label tag to set it's label,
		as well as a class tag to indicate the subclass of Component to
		create.</info>


		<attribute>
			<name>id</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>

		<attribute>
			<name>classname</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>

		<attribute>
			<name>label</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		
	</tag>

	<tag> 
		<name>document</name>
		<tagclass>org.webgets.tags.DocumentTag</tagclass>
		<teiclass>org.webgets.tags.DocumentTEI</teiclass>
		<bodycontent>JSP</bodycontent>

		<info>The document tag supports an id attribute to set it's
		name in the enclosed script and a label tag to set it's title.
		
		Note that the id is used with the session and should be treated
		as a namespace. This is handy, because you can have one document
		with two different population methods depending on wihch URL is
		accessed first.
		
		Style type and style href can be defined for the Document as well.
		</info>

		<attribute>
			<name>id</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>

		<attribute>
			<name>label</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		
		<attribute>
			<name>styletype</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		
		<attribute>
			<name>stylehref</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag> 
		<name>xhtml-document</name>
		<tagclass>org.webgets.tags.XHTMLDocumentTag</tagclass>
		<teiclass>org.webgets.tags.XHTMLDocumentTEI</teiclass>
		<bodycontent>JSP</bodycontent>

		<info>The XHTML Document tag supports an id attribute to set it's
		name in the enclosed script and a label tag to set it's title.
		
		Note that the id is used as the session key and should be treated
		as a namespace. This is usefull, because you can have one document
		with two different population methods depending on wihch URL is
		accessed first.
		
		Style type and style href can be defined for the Document as well.
		</info>

		<attribute>
			<name>id</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>

		<attribute>
			<name>label</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>webtop-document</name> 
		<tagclass>org.webgets.tags.WebtopDocumentTag</tagclass>
		<teiclass>org.webgets.tags.WebtopDocumentTEI</teiclass>
		<bodycontent>JSP</bodycontent>

		<info>The Webtop Document tag supports an id attribute to set it's
		name in the enclosed script and a label tag to set it's title.
		
		Note that the id is used as the session key and should be treated
		as a namespace. This is usefull, because you can have one document
		with two different population methods depending on wihch URL is
		accessed first.
		
		Style type and style href can be defined for the Document as well.
		</info>

		<attribute>
			<name>id</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>

		<attribute>
			<name>label</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

</taglib> 
