#!/bin/sh
#
# build-docs.sh

# run tidy to check for errors in the source document
# then run the XSL transform 
tidy -q -xml webgets-guide.xml > /dev/null && \
	xsltproc webgets-docs.xsl webgets-guide.xml | tidy -qi -asxml > webgets-guide.html
