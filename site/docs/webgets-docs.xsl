<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'
                xmlns="http://www.w3.org/TR/xhtml1/transitional"
                exclude-result-prefixes="#default">
	<xsl:import href="/sw/share/xml/xsl/html/docbook.xsl"/>
	<xsl:variable name="html.stylesheet">webgets-docs.css</xsl:variable>
</xsl:stylesheet>
