<%@page import="org.webgets.html.*, org.webgets.webtop.*, org.webgets.demos.patterns.*"%>
<%@ taglib uri="/webgets" prefix="webgets"%>

<webgets:document id="patterns_demo" label="Patterns Demo" stylehref="../../webgets-html.css">
<%
Columns tasks = new Columns( "Tasks", 200);
tasks.addComponent( Editor.createEditor( new PatternLanguage( "Webgets Patterns"), PatternLanguage.BEAN_INFO));
tasks.addComponent( Editor.createEditor( new Pattern( "Events"), Pattern.BEAN_INFO));
patterns_demo.addComponent( tasks);
%>
</webgets:document>
