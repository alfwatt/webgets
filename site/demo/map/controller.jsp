<%@page import="org.webgets.demos.map.*"%>

<%@ taglib uri="/webgets" prefix="webgets"%>

<webgets:document id="controller" label="Webgets"><%
	Map m = (Map) session.getValue( "org.webgets.demos.map");
	 controller.addComponent( new MapController( m));
	 controller.addComponent( org.webgets.webtop.Editor.createEditor( m.getGrid(), org.webgets.svg.Grid.BEAN_INFO));
	 controller.addComponent( org.webgets.html.Block.BR);
	 controller.addComponent( new org.webgets.html.Align( new org.webgets.html.Link( 
			"http://www.adobe.com/svg/viewer/install/", "Adobe SVG Viewer")));
 	 controller.addComponent( org.webgets.html.Block.BR);
%></webgets:document>
