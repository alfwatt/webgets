<%@ page language="java" import="org.webgets.demos.map.*, org.webgets.svg.*" %>

<% if ( session.getValue( "org.webgets.demos.map") == null)
{
	Map map = new Map( "map", new Image( "map.png", "396", "405"), new Grid( 30, 30));
	session.putValue( "org.webgets.demos.map", map); 
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><title>Map</title></head>
<frameset cols="30%,70%">
<frame src="<%=response.encodeURL( "controller.jsp")%>" name="controller" scrolling="no" frameborder="1" />
<frame src="<%=response.encodeURL( "map.jsp")%>" name="map" scrolling="yes" frameborder="1" />
</frameset>
</html> 
