Webgets is a component system for developing web-based applications on
the Java Servlet platform. It provides tools for working with Java
Beans, SVG Documents and creating consistent, reliable and highly
interactive web pages.
