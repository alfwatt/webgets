using System;
using System.Collections;

namespace Webgets
{
	public class Component
	{
		string MyLabel;

		public Component(string label)
		{
			MyLabel = label;
		}
   		
		public string Label
		{
			get { return MyLabel; }
			set { MyLabel = value; }
		}
	} 

	public class Container: Component
	{
		ArrayList MyComponents;
		
		public Container( string label) : base( label)
		{
		}
		
		public Container( string label, ArrayList components) : base( label)
		{
			MyComponents = components;
		}
		
		public ArrayList Components
		{
			get { return MyComponents; }
			set { MyComponents = value; }
		}
		
		public void addComponent( Component component)
		{
			MyComponents.Add(component);
		}

		public void removeComponent( Component component)
		{
			MyComponents.Remove(component);
		}
	}

	public class Test
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello Webgets");
		}
	}	

}
