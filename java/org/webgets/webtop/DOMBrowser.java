package org.webgets.webtop;

import org.w3c.dom.*;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

import org.w3c.dom.Document; /* incantation to hide org.webgets.html.Document */

public class DOMBrowser extends DelegatorAdapter
{
	private Document document;
	
	public DOMBrowser( Document document)
	{
		super( document.getDocumentElement().getNodeName());
		this.document = document;
		this.setDelegate( new LabeledColumns( 128));
		((Container) this.getDelegate()).addComponent( new NodeBrowser( document.getDocumentElement(), this));
	}

	void addNode( NodeBrowser add, int depth)
	{
		LabeledColumns delegate = (LabeledColumns) this.getDelegate();
		delegate.trimToIndex( depth);
		delegate.addComponent( add);
	}
	
	public String getLabel()
	{
		return this.document.getDocumentElement().getNodeName();
	}
	
	static class NodeBrowser extends ComponentAdapter
	{
		private DOMBrowser parent;
		private Node node;
		private int depth = 1;
	
		NodeBrowser( Node node, DOMBrowser parent)
		{
			super( node.getNodeName());
			this.node = node;
			this.parent = parent;
		}
		
		NodeBrowser( Node node, DOMBrowser parent, int depth)
		{	
			this( node, parent);
			this.depth = depth;
		}
		
		public String getLabel()
		{
			return this.node.getNodeName(); // + " " + Integer.toString( this.depth);
		}
		
		// http://lupus/webgets_dev/webtop.jsp;jsessionid=aaa6-7bVKyH85Q?path=#document
		public String getPath()
		{
			StringBuffer path = new StringBuffer( this.node.getNodeName());
			Node parent = this.node.getParentNode();
			while ( parent != null)
			{
				path.insert( 0, "/");
				if ( ! parent.getNodeName().equals( "#document"))
					path.insert( 0, parent.getNodeName());
				parent = parent.getParentNode();
			}
			return path.toString();
		}
		
		public void onRequest( RequestEvent request)
		{	
			super.onRequest( request);
			if ( request.isTargeted( this))
			{
				// get the nodes INDEX out of the request...
				int index = Integer.parseInt( request.getParameter( "index"));
				this.parent.addNode( 
					new NodeBrowser( this.node.getChildNodes().item( index), this.parent, this.depth+1),
					     this.depth);
			}
		}
		
		public void onDraw( TagWriter out)
		{
			super.onDraw( out);
			NamedNodeMap attrs = node.getAttributes();
			if ( attrs != null)
			{
				int aindex = 0;
				int alimit = attrs.getLength();
				while ( aindex < alimit)
				{
					Node attr = attrs.item( aindex++);
					out.tag( HTML.NOBR);
					out.print( attr.getNodeName());
					out.print( "=");
					out.print( attr.getNodeValue());
					out.closeTag();
					out.emptyTag( HTML.BR);
				}
			}
									
			NodeList children = node.getChildNodes();
			if ( children != null)
			{
				out.emptyTag( HTML.BR);
				int cindex = 0;
				int climit = children.getLength();
				while ( cindex < climit)
				{
					String text = children.item( cindex).getNodeName();
					if ( text.equals( "#text"))
					{
						String content = children.item( cindex).getNodeValue();
						if ( content != null && TagWriter.stripWhitespace( content).length() > 0)
						{
							out.print( content);
							out.emptyTag( HTML.BR);
						}
					}
					else
					{	

						out.tag( HTML.A, HTML.HREF, out.getRequestURL(), new String[] 
								{ RequestEvent.TARGET, this.getIdString(),
								"index", Integer.toString( cindex)});
							out.print( text);
						out.closeTag();
						out.emptyTag( HTML.BR);
					}
					cindex++;
				}
			}
		}
	}
}
