package org.webgets.webtop;

import org.webgets.*;
import org.webgets.html.*;
import org.webgets.util.*;

public class ScaledChartLine extends ChartLine
{
	private int max = 1;
	private int count = 0;

	public ScaledChartLine( String label, int count, int max, String todoColor)
	{
		super( label);
		this.setCount( count);
		this.setMax( max);
		this.setTodoColor( todoColor);
	}

	public ScaledChartLine( String label, int count, int max, String doneColor,  String todoColor)
	{
		super( label);
		this.setCount( count);
		this.setMax( max);
		this.setDoneColor( doneColor);
		this.setTodoColor( todoColor);
	}

	public void setCount( int count)
	{
		this.count = count;
		this.recalc();
	}

	public void setMax( int max)
	{
		if ( max == 0)
			throw new IllegalArgumentException( "ScaledChartLine.setMax( max) must not be zero, it's used as a divisor");
		this.max = max;
		this.recalc();
	}

	public void onDrawSummary( TagWriter out)
	{
		out.tag( TD);
			out.write( Integer.toString( this.count));
		out.closeTag();
	}

	private void recalc()
	{
		this.setPercent( (float)count/(float)max);
	}
}
