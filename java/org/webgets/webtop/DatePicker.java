package org.webgets.webtop;

import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.DateFormatSymbols;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

DatePicker supports the changing of dates via fill-in fields for
month, day and year.

*/
public class DatePicker extends ComponentAdapter implements HTML
{
	/**
	
	The DatePicker.DateChangeListener defines on method
	<code>onDateChange( Date now)</code> which is called when a new date
	is selected by the DatePicker.
	
	*/
	public interface DateChangeListener
	{
		void onDateChange( Date now);
	}

	protected static String DAY   = "day";
	protected static String MONTH = "month";
	protected static String YEAR  = "year";
	protected static String UNDO  = "Undo";
	protected static String SAVE  = "Save";

	protected static int[] MONTHS = new int[] 
	{ 
		Calendar.JANUARY, 	Calendar.FEBRUARY,	Calendar.MARCH,
		Calendar.APRIL, 	Calendar.MAY, 		Calendar.JUNE,
		Calendar.JULY,		Calendar.AUGUST,	Calendar.SEPTEMBER,
		Calendar.OCTOBER,	Calendar.NOVEMBER,	Calendar.DECEMBER
	};

	protected static int[] WEEKDAYS = new int[] 
	{ 
		Calendar.SUNDAY,
		Calendar.MONDAY, 
		Calendar.TUESDAY, 
		Calendar.WEDNESDAY,
		Calendar.THURSDAY,
		Calendar.FRIDAY,
		Calendar.SATURDAY
	};


	protected static int[] yearsAround( int year)
	{
		return new int[] 
		{
			year-5,
			year-4,
			year-3,
			year-2,
			year-1,
			year,
			year+1,
			year+2,
			year+3,
			year+4,
			year+5
		};
	}

	protected Calendar calendar = Calendar.getInstance();
	protected DateFormatSymbols symbols = new DateFormatSymbols();
	protected DateChangeListener listener = null;
	protected Image increment = Image.createLocalImage( "right.gif");
	protected Image decrement = Image.createLocalImage( "left.gif");

	public DatePicker()
	{
		super();
	}
	
	public DatePicker( Date now)
	{
		super();
		this.setDate( now);
	}
	
	public DatePicker( Date now, DateChangeListener listener)
	{
		this( now);
		this.setListener( listener);
	}
	
	public String getLabel()
	{
		return DateFormat.getDateInstance( DateFormat.FULL).format( this.calendar.getTime());
	}
	
	protected void updateDate()
	{
		Date now = this.calendar.getTime();
		if ( this.listener != null)
			this.listener.onDateChange( this.calendar.getTime());
	}
	
	public void setDate( Date now)
	{
		this.calendar.setTime( now);
		if ( this.listener != null)
			this.listener.onDateChange( this.calendar.getTime());
	}
	
	public Date getDate()
	{
		return this.calendar.getTime();
	}

	public void setListener( DateChangeListener listener)
	{
		this.listener = listener;
	}

	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
		if ( request.isTargeted( this))
		{
			if ( request.hasParameter( DAY))
				this.calendar.set( Calendar.DAY_OF_MONTH, 
					Integer.parseInt( request.getParameter( DAY)));
				
			if ( request.hasParameter( MONTH))
				this.calendar.set( Calendar.MONTH, 
					Integer.parseInt( request.getParameter( MONTH)));

			if ( request.hasParameter( YEAR))
				this.calendar.set( Calendar.YEAR, 
					Integer.parseInt( request.getParameter( YEAR)));
					
			this.updateDate();
		}	
	}
	

	private static String[]   DAY_SELECT_ATTRS = new String[] { NAME, DAY};
	private static String[] MONTH_SELECT_ATTRS = new String[] { NAME, MONTH};
	private static String[]  YEAR_SELECT_ATTRS = new String[] { NAME, YEAR};
	private static String[]        RESET_ATTRS = new String[] { TYPE, RESET,  VALUE, UNDO};
	private static String[]       SUBMIT_ATTRS = new String[] { TYPE, SUBMIT, VALUE, SAVE};

	private static String[] CONTROL_TR_ATTRS = new String[] { ALIGN, RIGHT};
	private static String[] CONTROL_TD_ATTRS = new String[] { COLSPAN, "3"};
	
	protected void onDrawDaySelect( TagWriter out)
	{
		out.tag( SELECT, DAY_SELECT_ATTRS);
		int firstDay = this.calendar.getActualMinimum( Calendar.DAY_OF_MONTH);
		int lastDay = this.calendar.getActualMaximum( Calendar.DAY_OF_MONTH);
		int day = firstDay;
		while ( day <= lastDay)
		{
			String dayString = Integer.toString( day);
			
			if ( day == this.calendar.get( Calendar.DAY_OF_MONTH))
				out.tag( OPTION, new String[] { 
					VALUE, dayString, SELECTED, null});
			else
				out.tag( OPTION, new String[] { VALUE, dayString});
				
				out.print( dayString);
			out.closeTag();
			day++;
		}
		out.closeTag();
	}
	
	protected void onDrawMonthSelect( TagWriter out)
	{
		out.tag( SELECT, MONTH_SELECT_ATTRS);
		String[] monthNames = symbols.getMonths();
		int mindex = 0;
		while ( mindex < MONTHS.length)
		{
			String monthString = Integer.toString( MONTHS[mindex]);
			String monthName = monthNames[MONTHS[mindex]];
			
			if ( MONTHS[mindex] == this.calendar.get( Calendar.MONTH))
				out.tag( OPTION, new String[] { 
					VALUE, monthString, SELECTED, null});
			else
				out.tag( OPTION, new String[] { VALUE, monthString});
				out.print( monthName);
			out.closeTag();
			
			mindex++;
		}
		out.closeTag();
	}
	
	protected void onDrawYearSelect( TagWriter out)
	{
		out.tag( SELECT, YEAR_SELECT_ATTRS);
		int[] years = DatePicker.yearsAround( this.calendar.get( Calendar.YEAR));
		int yindex = 0;
		while ( yindex < years.length)
		{
			String yearString = Integer.toString( years[yindex]);
			
			if ( years[yindex] == this.calendar.get( Calendar.YEAR))
				out.tag( OPTION, new String[] { 
					VALUE, yearString, SELECTED, null});
			else
				out.tag( OPTION, new String[] { VALUE, yearString});
				out.print( yearString);
			out.closeTag();
			
			yindex++;
		}
		out.closeTag();
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( TABLE);
			
			out.tag( FORM, new String[] { METHOD, HTTP.GET, 
										  ACTION, out.encodeURL( out.getRequestURL())});
				out.emptyTag( INPUT, new String[] { 
					TYPE, HIDDEN, 
					NAME, RequestEvent.TARGET, 
					VALUE, this.getIdString()});
			
			out.tag( TR, CONTROL_TR_ATTRS);
				out.tag( TD);
					this.onDrawDaySelect( out);
				out.closeTag();
				out.tag( TD);
					out.tag( A, HREF, out.getRequestURL(), new String[] { 
						RequestEvent.TARGET, this.getIdString(),
						DAY, Integer.toString( this.calendar.get( Calendar.DAY_OF_MONTH)-1)});
						this.decrement.onDraw( out);
					out.closeTag();
				out.closeTag();
				out.tag( TD);
					out.tag( A, HREF, out.getRequestURL(), new String[] { 
						RequestEvent.TARGET, this.getIdString(),
						DAY, Integer.toString( this.calendar.get( Calendar.DAY_OF_MONTH)+1)});
						this.increment.onDraw( out);
					out.closeTag();
				out.closeTag();
			out.closeTag();

			out.tag( TR, CONTROL_TR_ATTRS);
				out.tag( TD);
					this.onDrawMonthSelect( out);
				out.closeTag();
				out.tag( TD);
					out.tag( A, HREF, out.getRequestURL(), new String[] { 
						RequestEvent.TARGET, this.getIdString(),
						MONTH, Integer.toString( this.calendar.get( Calendar.MONTH)-1)});
						this.decrement.onDraw( out);
					out.closeTag();
				out.closeTag();
				out.tag( TD);
					out.tag( A, HREF, out.getRequestURL(), new String[] { 
						RequestEvent.TARGET, this.getIdString(),
						MONTH, Integer.toString( this.calendar.get( Calendar.MONTH)+1)});
						this.increment.onDraw( out);
					out.closeTag();
				out.closeTag();
			out.closeTag();

			out.tag( TR, CONTROL_TR_ATTRS);
				out.tag( TD);
					this.onDrawYearSelect( out);
				out.closeTag();
				out.tag( TD);
					out.tag( A, HREF, out.getRequestURL(), new String[] { 
						RequestEvent.TARGET, this.getIdString(),
						YEAR, Integer.toString( this.calendar.get( Calendar.YEAR)-1)});
						this.decrement.onDraw( out);
					out.closeTag();
				out.closeTag();
				out.tag( TD);
					out.tag( A, HREF, out.getRequestURL(), new String[] { 
						RequestEvent.TARGET, this.getIdString(),
						YEAR, Integer.toString( this.calendar.get( Calendar.YEAR)+1)});
						this.increment.onDraw( out);
					out.closeTag();
				out.closeTag();
			out.closeTag();
			
			out.tag( TR, CONTROL_TR_ATTRS);
				out.tag( TD, CONTROL_TD_ATTRS);
					out.emptyTag( INPUT, RESET_ATTRS);
					out.emptyTag( INPUT, SUBMIT_ATTRS);
				out.closeTag();
			out.closeTag();
			
			out.closeTag( FORM);
		out.closeTag( TABLE);
	}
}
