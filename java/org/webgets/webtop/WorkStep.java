package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;

/**

A WorkStep defines an individual step of a WorkFlow. Work Steps have an 
optional description and warning message. Before a WorkFlow steps past a
WorkStep it will call the abstract <code>isStepCompleted()</code> method
and display the description and warning messages if it returns false.

@see WorkFlow

*/
public abstract class WorkStep extends ContainerAdapter
{
	private String description = null;
	private String warning = null;

	//
	// Public Constructors

	public WorkStep( String label)
	{
		super( label);
	}

	public WorkStep( String label, Component proxy)
	{
		super( label, proxy);
	}
	
	public WorkStep( String label, Component[] components)
	{
		super( label, components);
	}

	//
	// Public Methods

	public void setDescription( String description)
	{
		this.description = description;
	}
	
	public String getDescription()
	{
		return this.description;
	}

	public void setCompletionWarning( String warning)
	{
		this.warning = warning;
	}
	
	public String getCompletionWarning()
	{
		return this.warning;
	}
	
	//
	// Abstract Methods
	
	/**

	@returns true if the WorkStep is complete, false to display the
	description and warning messages.
	
	*/
	public abstract boolean isStepComplete();

}

/* Love, Alf */
