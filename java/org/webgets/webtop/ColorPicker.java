package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

public class ColorPicker extends ComponentAdapter implements HTML
{
	private static String HEX = "hex";
	private static String NAME = "name";
	private static String[] SAFE_HEX = new String[] { "00", "33", "66", "99", "CC", "FF"};
	private static String[] TABLE_ATTRS = new String[] { BGCOLOR, WHITE};
	
	public static final String DEFAULT_COLOR = "gray";

	/**
	
	ColorChangeListener defines on method onColorChange( String newColor) which
	is called when a new color is selected by the ColorPicker.
	
	*/
	public interface ColorChangeListener
	{
		void onColorChange( String color);
	}

	private Image trans = Image.createLocalImage( "select", "trans.gif", 10, 10);
	private ColorChangeListener listener = null;
	
	public ColorPicker()
	{
		super( DEFAULT_COLOR);
	}
	
	public ColorPicker( String selected)
	{
		super( selected);
		this.setSelected( selected);
	}
	
	public ColorPicker( String selected, ColorChangeListener listener)
	{
		this( selected);
		this.listener = listener;
	}
	
	public String getSelected()
	{
		return this.getLabel();
	}
	
	public void setSelected( String selected)
	{
		this.setLabel( selected);

		if ( this.listener != null)
			this.listener.onColorChange( selected);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( TABLE, TABLE_ATTRS);
		
			final int width = 18;
			int counter = 0;

			// the web safe colors
			int rIndex = 0;
			while ( rIndex < SAFE_HEX.length)
			{
				int gIndex = 0;
				while ( gIndex < SAFE_HEX.length)
				{
					int bIndex = 0;
					while ( bIndex < SAFE_HEX.length)
					{
						if ( counter == 0)
							out.tag( TR);
							
						String safeColor = SAFE_HEX[rIndex] + SAFE_HEX[gIndex] + SAFE_HEX[bIndex];
						out.tag( TD, new String[] { BGCOLOR, "#" + safeColor});
							out.tag( A, HREF, out.getRequestURL(), new String[]
								{ RequestEvent.TARGET, this.getIdString(), 
								  HEX, safeColor});
								this.trans.setAlt( safeColor);
								this.trans.onDraw( out);
//								out.print( SAFE_HEX[rIndex]);
//								out.print( SAFE_HEX[gIndex]);
//								out.print( SAFE_HEX[bIndex]);
							out.closeTag();
						out.closeTag();
						
						if ( ++counter == width)
						{
							out.closeTag();
							counter = 0;
						}
						
						bIndex++;		
					}
					gIndex++;
				}
				rIndex++;
			}

			// pick from the color constants in COLORS
			out.tag( TR);
			int index = 0;
			while ( index < COLORS.length)
			{					
				out.tag( TD, new String[] { BGCOLOR, COLORS[index]});
					out.tag( A, HREF, out.getRequestURL(), new String[]
						{ RequestEvent.TARGET, this.getIdString(), 
						  NAME, COLORS[index]});
						this.trans.setAlt( COLORS[index]);
						this.trans.onDraw( out);
					out.closeTag();
				out.closeTag();
				
				index++;
			}
			out.closeTag();

			// the selected color
			out.tag( TR);
				out.tag( TD, new String[] { COLSPAN, Integer.toString( width-1), 
											ALIGN, RIGHT});
					out.print( this.getLabel());
				out.closeTag();
				out.tag( TD, new String[] { BGCOLOR, this.getLabel(), WIDTH, "10", HEIGHT, "10"});
				out.closeTag();
			out.closeTag();
		out.closeTag( TABLE);
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		if ( request.isTargeted( this))
			if ( request.hasParameter( NAME))
				this.setSelected( request.getParameter( NAME));
			else if ( request.hasParameter( HEX))
				this.setSelected( "#" + request.getParameter( HEX));
	}
}
