package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

public class WorkFlow extends ContainerAdapter
{
	private int step = 0;
	
	private RequestListener nextListener = new RequestListener()
	{
		public void onRequest( RequestEvent request) { nextStep(); }
	};
	
	private Button next = new Button( "Next", nextListener);
	private TextButton nextText = new TextButton( "Next", nextListener);
	
	private RequestListener backListener = new RequestListener()
	{
		public void onRequest( RequestEvent request) { backStep(); }
	};
	
	private Button back = new Button( "Back", backListener);
	 private TextButton backText = new TextButton( "Back", backListener);
	
	private Attributes navigationAttrs = new Attributes( HTML.ALIGN, HTML.CENTER);
	private Attributes labelAttrs = new Attributes( HTML.NOWRAP, null);
	private Attributes stepAttrs = new Attributes( HTML.COLSPAN, "3");
	private Attributes statusAttrs = new Attributes( HTML.ALIGN, HTML.CENTER);
	
	public WorkFlow( String label)
	{
		super( label);
	}
	
	public WorkFlow( String label, Component[] components)
	{
		super( label, components);
	}
	
	public int getStep()
	{	
		return this.step;
	}
	
	public void setStep( int step)
	{
		Check.isLegalArg( step >= 0 && step <= this.countSteps());
		this.step = step;
	}
	
	public int countSteps()
	{
		return this.getComponents().length;
	}
	
	public void nextStep()
	{
		if ( this.getStep() < this.countSteps())
			this.setStep( this.getStep() + 1);
	}
	
	public void backStep()
	{
		if ( this.getStep() > 0)
			this.setStep( this.getStep() - 1);
	}
	
	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		super.init( config);
		this.next.init( config);
		this.nextText.init( config);
		this.back.init( config);
		this.backText.init( config);
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
		if ( true) // TODO check for WorkStep completeness before letting the buttons fire
		{
			this.next.onRequest( request);
			this.nextText.onRequest( request);
			this.back.onRequest( request);
			this.backText.onRequest( request);
		}

		if ( this.getStep() == 0)
		{
			this.back.disable();
			this.backText.disable();
			this.backText.setLabel( "");
		}
		else
		{
			this.back.enable();
			this.backText.enable();
			this.backText.setLabel( this.getComponents()[this.getStep()-1].getLabel());
		}
		
		if ( this.getStep() == this.countSteps() - 1)
		{
			this.next.disable();
			this.nextText.disable();
			this.nextText.setLabel( "");
		}
		else
		{
			this.next.enable();
			this.nextText.enable();
			this.nextText.setLabel( this.getComponents()[this.getStep()+1].getLabel());
		}
	}
	
	/*
	
	<table>
		<tr align="center">
			<td nowrap align="right">
				<nobr>
					this.getLabel(): step.getLabel()
				</nobr>
			</td>
		</tr>
		<tr>
			<td>step.onDraw()</td>
		</tr>
	</table>
	
	*/
	public void onDraw( TagWriter out)
	{
		super.traceDraw();
		Component step = this.getComponents()[this.step];
		out.tag( HTML.TABLE);
			out.tag( HTML.TR, this.navigationAttrs);
				out.tag( HTML.TD);
					this.back.onDraw( out);
				out.closeTag();
				out.tag( HTML.TD, this.labelAttrs);
					Font.PLUS_2.onDraw( out, this.getLabel() + ": " + step.getLabel());
				out.closeTag();
				out.tag( HTML.TD);
					this.next.onDraw( out);
				out.closeTag();
			out.closeTag();
				
			out.tag( HTML.TR);
				out.tag( HTML.TD, this.stepAttrs);
					step.onDraw( out);
				out.closeTag();
			out.closeTag();
			
			out.tag( HTML.TR, this.statusAttrs);
				out.tag( HTML.TD);
					this.backText.onDraw( out);
				out.closeTag();
				out.tag( HTML.TD);
					out.print( this.getLabel() + ": " + Integer.toString( this.getStep() + 1)
						+ " of " + Integer.toString( this.countSteps()));
				out.closeTag();
				out.tag( HTML.TD);
					this.nextText.onDraw( out);
				out.closeTag();
			out.closeTag();
		out.closeTag( HTML.TABLE);
		super.traceEndDraw();
	}
}
