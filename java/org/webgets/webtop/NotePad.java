package org.webgets.webtop;

import java.io.File;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

A Notepad is a Container that holds and displays a list of notes, allows for the
creation of new notes and manages the storage of notes on disk.



*/
public class NotePad extends ContainerAdapter implements HTML
{
	private static final String CREATE = "create";
	private static final int NONE_SELECTED = -1;
	
	private File pad = null;
	private Input idInput = Input.createIdInput( this);
	private Image select = Image.createLocalImage( "right.gif");
	private Image deselect = Image.createLocalImage( "down.gif");
	private Image delete = Image.createLocalImage( "close.gif");
	private int selected = NONE_SELECTED;
		
	public NotePad( File pad)
	{
		super( pad.toString());
		this.setPad( pad);
	}
	
	public NotePad( String label)
	{
		super( label);
	}

	public NotePad( String label, File pad)
	{
		super( label);
		this.setPad( pad);
	}

	public File getPad()
	{
		return this.pad;
	}
	
	public void setPad( File pad)
	{
		this.pad = pad;
	}
	
	public void addComponent( Component add)
	{
		if ( ! ( add instanceof Note))
			throw new IllegalArgumentException( "only Notes can be added to NotePads");
		super.addComponent( add);
	}

	public void addComponent( Component add, int where)
	{
		if ( ! ( add instanceof Note))
			throw new IllegalArgumentException( "only Notes can be added to NotePads");
		super.addComponent( add);
	}

	private void setSelected( int selected)
	{
		if ( selected == this.selected)
			this.selected = NONE_SELECTED;
		else
			this.selected = selected;
	}
	
	private int getSelected()
	{
		return this.selected;
	}
	
	private void createNote( String label)
	{
		this.addComponent( new Note( label));
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
		if ( request.isTargeted( this))
		{
			if ( request.hasParameter( CREATE))
				this.createNote( request.getParameter( CREATE));
				
			if ( request.hasParameter( SELECT))
				this.setSelected( Integer.parseInt( request.getParameter( SELECT)));
			
			request.stopProcessing( this);
		}
	}
	
	private static String[] ALIGN_CENTER = new String[] { ALIGN, CENTER};
	private static String[] SPAN_4 = new String[] { COLSPAN, "4"};
	
	public void onDraw( TagWriter out)
	{
		super.traceDraw();
		out.tag( TABLE);
			out.tag( TR);
				out.tag( TD, SPAN_4);
					Font.SIZE_5.onDraw( out, this.getLabel());
				out.closeTag();
			out.closeTag();

			out.tag( TR);
				out.tag( TD, SPAN_4);
					out.tag( FORM, new String[] { METHOD, HTTP.GET, 
												  ACTION, out.encodeURL( out.getRequestURL())});
						out.tag( NOBR);
							this.idInput.onDraw( out);
							out.emptyTag( INPUT, new String[] { TYPE, TEXT, NAME, CREATE});
							out.emptyTag( INPUT, new String[] { TYPE, SUBMIT, VALUE, "Create"});
						out.closeTag();
					out.closeTag();
				out.closeTag();
			out.closeTag();
			
			out.tag( TR);
				out.tag( TD);
				out.closeTag();
				out.tag( TH);
					out.print( "Label");
				out.closeTag();
				out.tag( TH);
					out.print( "Date");
				out.closeTag();
				out.tag( TH);
				out.closeTag();
			out.closeTag();
						
			Component[] notes = this.getComponents();
			int index = 0;
			if ( notes.length == 0)
			{
				out.tag( TR, ALIGN_CENTER);
					out.tag( TD, SPAN_4);
						out.print( "Empty Note Pad");
					out.closeTag();
				out.closeTag();
			}
			else while (index < notes.length)
			{
				Note note = (Note) notes[index];
				out.tag( TR);
					out.tag( TD);
						out.tag( A, HREF, out.getRequestURL(), new String[] { 
							RequestEvent.TARGET, this.getIdString(),
							SELECT, Integer.toString( index)});
						if ( index != this.getSelected())
							this.select.onDraw( out);
						else
							this.deselect.onDraw( out);
						out.closeTag();
					out.closeTag();
					
					out.tag( TD);
						out.print( note.getLabel());
					out.closeTag();
										
					out.tag( TD);
						this.delete.onDraw( out);
					out.closeTag();
				out.closeTag( TR);
				
				index++;
			}

			if ( this.getSelected() != NONE_SELECTED)
			{
				out.tag( TR);
					out.tag( TD, SPAN_4);
						notes[this.getSelected()].onDraw( out);
					out.closeTag();		
				out.closeTag();	
			}
	
		out.closeTag( TABLE);
		super.traceEndDraw();
	}
}
