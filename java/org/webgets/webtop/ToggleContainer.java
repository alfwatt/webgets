package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

public class ToggleContainer extends ContainerAdapter
{
	private static String[] TOGGLE_TABLE_ATTRS = new String[] { 
		HTML.CELLSPACING, "0", HTML.CELLPADDING, "1"};
	private static String[] TOGGLE_TR_ATTRS = new String[] { HTML.VALIGN, HTML.TOP};



	private Toggle toggle = new Toggle();
	
    public ToggleContainer()
    {
        super();
    }
    
    public ToggleContainer( String label) 
    {
        super( label);
    }
    
    public ToggleContainer( Component delegate)
    {
    	super( delegate);
    }
    
    public ToggleContainer( String label, Component add)
    {
		super( label, add);
    }
    
    public ToggleContainer( String label, Component[] add)
    {
    	super( label, add);
    }
    
    public ToggleContainer( String label, Image selected, Image unselected)
    {
		super( label);
		this.toggle.setSelectedImage( selected);
		this.toggle.setUnselectedImage( unselected);    
    }
    
    public Toggle getToggle()
    {
    	return this.toggle;
    }
    
    public void onDraw( TagWriter out)
    {
		if ( this.toggle.isSelected())
		{
			out.tag( HTML.TABLE, TOGGLE_TABLE_ATTRS);
				out.tag( HTML.TR, TOGGLE_TR_ATTRS);
					out.tag( HTML.TD);
						this.toggle.onDraw( out);
					out.closeTag();
					out.tag( HTML.TD);
						super.onDraw( out);
					out.closeTag();
				out.closeTag();
			out.closeTag( HTML.TABLE);
		}		
		else
		{
			super.traceDraw();
			this.toggle.onDraw( out);
			super.traceEndDraw();
		}
    }
    
    public void onRequest( RequestEvent out)
    {
    	this.toggle.onRequest( out);
    	super.onRequest( out);
    }
}
