package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

BlockText generates 3x3 Character

*/
public class BlockText 
{
	private static final int blockWidth = 3;
	private static final int blockHeight = 3;
	private static final boolean fg = true;
	private static final boolean bg = false;

	private static final boolean[] A = new boolean[] { 	bg, fg, bg, 
														fg, fg, fg, 
														fg, bg, fg };

	private static final boolean[] B = new boolean[] {	fg, bg, bg,
														fg, fg, fg, 
														fg, fg, fg };

	private static final boolean[] C = new boolean[] {	fg, fg, fg,
														fg, bg, bg, 
														fg, fg, fg };

	private static final boolean[] D = new boolean[] {	fg, fg, bg,
														fg, bg, fg, 
														fg, fg, bg };

	private static final boolean[] E = new boolean[] {	fg, fg, fg, 
														fg, fg, bg, 
														fg, fg, fg };

	private static final boolean[] F = new boolean[] {	fg, fg, fg, 
														fg, fg, bg, 
														fg, bg, bg };

	private static final boolean[] G = new boolean[] {	bg, fg, fg, 
														bg, bg, fg, 
														bg, fg, fg };

	private static final boolean[] H = new boolean[] {	fg, bg, fg,
														fg, fg, fg, 
														fg, bg, fg };

	private static final boolean[] I = new boolean[] {	bg, fg, bg,
														bg, fg, bg, 
														bg, fg, bg };

	private static final boolean[] J = new boolean[] {	bg, bg, fg,
														fg, bg, fg, 
														fg, fg, fg };

	private static final boolean[] K = new boolean[] {	fg, bg, fg,
														fg, fg, bg, 
														fg, bg, fg };

	private static final boolean[] L = new boolean[] {	fg, bg, bg,
														fg, bg, bg, 
														fg, fg, fg };

	private static final boolean[] M = new boolean[] {	fg, fg, fg,
														fg, fg, fg, 
														fg, bg, fg };

	private static final boolean[] N = new boolean[] {	fg, bg, fg,
														fg, bg, fg, 
														fg, bg, fg };

	private static final boolean[] O = new boolean[] {	fg, fg, fg,
														fg, bg, fg, 
														fg, fg, fg };

	private static final boolean[] P = new boolean[] {	fg, fg, bg,
														fg, fg, bg, 
														fg, bg, bg };

	private static final boolean[] Q = new boolean[] {	fg, fg, bg,
														fg, fg, bg, 
														bg, fg, fg };

	private static final boolean[] R = new boolean[] {	fg, fg, fg,
														fg, bg, bg, 
														fg, bg, bg };

	private static final boolean[] S = new boolean[] {	bg, fg, fg,
														bg, fg, bg, 
														fg, fg, bg };

	private static final boolean[] T = new boolean[] {	fg, fg, fg,
														bg, fg, bg, 
														bg, fg, bg };

	private static final boolean[] U = new boolean[] {	fg, bg, fg,
														fg, bg, fg, 
														fg, fg, fg };

	private static final boolean[] V = new boolean[] {	fg, bg, fg,
														fg, bg, fg, 
														bg, fg, bg };

	private static final boolean[] W = new boolean[] {	fg, bg, fg,
														fg, fg, fg, 
														fg, fg, fg };

	private static final boolean[] X = new boolean[] {	fg, bg, fg,
														bg, fg, bg, 
														fg, bg, fg };

	private static final boolean[] Y = new boolean[] {	fg, bg, fg, 
														bg, fg, bg, 
														bg, fg, bg };

	private static final boolean[] Z = new boolean[] {	fg, fg, bg, 
														bg, fg, bg, 
														bg, fg, fg };

	private static final boolean[] ZERO = new boolean[] {	fg, fg, fg,
															fg, bg, fg, 
															fg, fg, fg };

	private static final boolean[] ONE = new boolean[] {	fg, fg, bg,
															bg, fg, bg, 
															bg, fg, bg };

	private static final boolean[] TWO = new boolean[] {	fg, fg, bg, 
															bg, fg, bg, 
															bg, fg, fg };

	private static final boolean[] THREE = new boolean[] {	fg, fg, fg,
															bg, fg, fg, 
															fg, fg, fg };

	private static final boolean[] FOUR = new boolean[] {	fg, fg, bg,
															fg, fg, fg, 
															bg, fg, bg };

	private static final boolean[] FIVE = new boolean[] {	fg, fg, bg,
															fg, fg, fg, 
															bg, fg, fg };

	private static final boolean[] SIX = new boolean[] {	fg, fg, fg,
															fg, fg, bg, 
															fg, fg, bg };

	private static final boolean[] SEVEN = new boolean[] {	fg, fg, fg,
															bg, bg, fg, 
															bg, bg, fg };

	private static final boolean[] EIGHT = new boolean[] {	fg, fg, fg,
															bg, fg, bg, 
															fg, fg, fg };

	private static final boolean[] NINE = new boolean[] {	fg, fg, fg, 
															fg, fg, fg, 
															bg, bg, fg };

	private static final boolean[] SPACE = new boolean[] {	bg, bg, bg,
															bg, bg, bg, 
															bg, bg, bg };

	private static final boolean[] DOT = new boolean[] {	bg, bg, bg,
															bg, fg, bg, 
															bg, bg, bg };

	private static final boolean[] QUES = new boolean[] {	bg, bg, bg,
															bg, bg, bg, 
															bg, bg, bg };

	public static Component createBlockText( String text, Component fgComp, Component bgComp)
	{
		text = text.trim();
		int textWidth = ( text.length() * ( blockWidth+1))-1; // TODO padding
		Table block = new Table( text, textWidth); 

		/* iterate over block height (rows), then by text, then by block width (cols)*/
		int rowIndex = 0;
		while ( rowIndex < blockHeight)
		{
			int textIndex = 0;
			int textLength = text.length();
			while ( textIndex < textLength)
			{
				char textChar = text.charAt( textIndex);
				boolean[] textArray = getBlock( textChar);

				int colIndex = 0;
				while ( colIndex < blockWidth)
				{
					int groundIndex = (rowIndex * blockWidth) + colIndex;
					boolean ground = textArray[ groundIndex];
					block.addComponent( ground ? fgComp : bgComp);
					colIndex++;

					if ( colIndex == blockWidth && ( textIndex+1 < blockHeight))
						block.addComponent( bgComp);
				}
				textIndex++;
			}
			rowIndex++;
		}

		return block;
	}

	public static final boolean[] getBlock( char block)
	{
		block = Character.toLowerCase( block);

		switch ( block)
		{
			case 'a': return A;
			case 'b': return B;
			case 'c': return C;
			case 'd': return D;
			case 'e': return E;
			case 'f': return F;
			case 'g': return G;
			case 'h': return H;
			case 'i': return I;
			case 'j': return J;
			case 'k': return K;
			case 'l': return L;
			case 'm': return M;
			case 'n': return N;
			case 'o': return O;
			case 'p': return P;
			case 'q': return Q;
			case 'r': return R;
			case 's': return S;
			case 't': return T;
			case 'u': return U;
			case 'v': return V;
			case 'w': return W;
			case 'x': return X;
			case 'y': return Y;
			case 'z': return Z;

			case '0': return ZERO;
			case '1': return ONE;
			case '2': return TWO;
			case '3': return THREE;
			case '4': return FOUR;
			case '5': return FIVE;
			case '6': return SIX;
			case '7': return SEVEN;
			case '8': return EIGHT;
			case '9': return NINE;

			case ' ': return SPACE;
			case '?': return QUES;

			default: return DOT;
		}
	}

	private BlockText()
	{
		super();
	}

}

