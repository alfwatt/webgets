package org.webgets.webtop;

import java.text.*;
import java.beans.*;
import java.lang.reflect.*;

import org.webgets.*;
import org.webgets.html.*;
import org.webgets.text.*;
import org.webgets.util.*;
import org.webgets.util.beans.*;

/**

The Browser views java beans.

*/
public class Browser extends ComponentAdapter
{
	public static Browser createBrowser( Object browse) throws IntrospectionException
	{
		return new Browser( browse);
	}
	
	public static Browser createBrowser( Object browse, BeanInfo info)
	{
		return new Browser( browse, info);
	}

	private Object bean = null;
	private BeanInfo info = null;
	private Component delegate = null;
	
	protected Browser()
	{
		super();
	}

	/** create a bean editor, uses java.beans.Introspector to create the BeanInfo */
	public Browser( Object bean) throws IntrospectionException
	{
		Check.isNotNull( bean);
		this.bean = bean;
		this.info = Introspector.getBeanInfo( bean.getClass(), Object.class);
	}

	/** create a bean editor, users provides the bean info class */
	public Browser( Object bean, BeanInfo info)
	{
		Check.isNotNull( bean);
		Check.isNotNull( info);
		this.bean = bean;
		this.info = info;
	}

	public BeanInfo getBeanInfo()
	{
		try
		{
			if ( this.info == null && this.bean != null)
				this.info = Introspector.getBeanInfo( this.bean.getClass());
		}
		catch ( IntrospectionException warn)
		{
			Log.printWarn( warn.toString());
		}

		return this.info;
	}
	
	private void build()
	{
		List body = new MenuList( this.getLabel());
		LabeledList propList = new LabeledList( this.getLabel());
		body.addComponent( propList);
		
		// get the property descriptors
		PropertyDescriptor[] props = this.getBeanInfo().getPropertyDescriptors();

		int index = 0;
		int limit = props.length;
		while ( index < limit)
		{
			PropertyDescriptor prop = props[index++];
			Class type = prop.getPropertyType();
			String name = prop.getDisplayName();
			String value = null;

			try
			{
				if ( type.isArray())
				{
					BulletList array = new BulletList();
					Object[] values = BeanTools.getPropertyValueAsArray( prop, this.bean);
					int arrayIndex = 0;
					while ( arrayIndex < values.length)
						array.addComponent( new StringComp( values[arrayIndex++]));
					propList.addComponent( new Label( name, array));
				}
				else if ( type == String.class)
				{
					value = BeanTools.getPropertyValueAsString( prop, this.bean);
					propList.addComponent( new Label( name, value));
				}			
				else
				{
					value = BeanTools.getPropertyValueAsString( prop, this.bean);
					propList.addComponent( new Label( name, TagWriter.trimString( value)));
				}
			}
			catch ( Exception cantRead)
			{
				Log.printWarn( "BeanEditor.buildForm() attempting to read property nane=" + name 
					+ " value=" + value, cantRead);
					
				if ( Debug.DEBUG)
					value = cantRead.toString();
				else
					value = "";
			}
			
		}
		
		this.delegate = body;

		// XXX HACK stuffed exception
		try
		{
			this.delegate.init( this.getServletConfig());
		}
		catch ( javax.servlet.ServletException stuff)
		{
			Log.printWarn( "Browser.buildForm()", stuff);
		}
	}
	
	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		super.init( config);
		this.build();
	}

	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		this.build();
		this.delegate.onRequest( request);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		this.delegate.onDraw( out);
	}

	public String getLabel()
	{
		return this.getBeanInfo().getBeanDescriptor().getDisplayName() + ": " + this.bean.toString();
	}

}
