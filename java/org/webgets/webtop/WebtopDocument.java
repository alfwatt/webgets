package org.webgets.webtop;

import org.webgets.*;
import org.webgets.xhtml.*;

public class WebtopDocument extends XHTMLDocument
{
	public WebtopDocument()
	{	
		super();
	}
	
	public WebtopDocument( String label)
	{
		super( label);
	}
	
	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		/* Add the Controls */
		super.init( config);
	}
}
