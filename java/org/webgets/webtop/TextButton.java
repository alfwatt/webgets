package org.webgets.webtop;

import org.webgets.util.*;
import org.webgets.html.HTML;

public class TextButton extends Button
{
	public TextButton()
	{
		super();
	}
	
	public TextButton( String label, RequestListener listener)
	{
		super( label, listener);
	}
	
	public TextButton( String label, String url)
	{
		super( label, url);
	}
	
	/*
	
	<form method="get" action="/page.jsp">
		<input type="submit" name="Prev" value="Prev">
		<input type="submit" name="Next" value="Next" disabled>
	</form> 
	
	*/
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		String actionURL = this.hasListener() ? out.getRequestURL() : this.getURL();
	
		if ( this.isEnabled())		
			if ( this.isLocal())
				out.tag( HTML.A, HTML.HREF, actionURL, new String[] { 
					RequestEvent.TARGET, this.getIdString()});
			else
				out.tag( HTML.A, HTML.HREF, actionURL); 
			
		out.print( this.getLabel());

		if ( this.isEnabled())
			out.closeTag( HTML.A);
	}
}
	
