package org.webgets.webtop;

import java.beans.BeanInfo;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.store.*;

/**

Editor is an abstract base class for Webgets Components which edit
objects.

Editor has two protected constructors which subclasses can use to set
properties at init time, static methods are provided to create editors
for use. This allows the editor class to lookup the appropriate editor
for a particaular class in a registry.

*/
public abstract class Editor extends ComponentAdapter
{

	private static EditorRegistry REGISTRY = new EditorRegistry();

	//
	// Class Methods

	/** registers a subclass of Editor */
	public static void registerEditor( Class editie, Class editor)
	{
		REGISTRY.registerEditor( editie, editor);
	}
	
	public static void deregisterEditor( Class editie)
	{
		REGISTRY.deregisterEditor( editie);
	}

	/** @returns a new BeanEditor */
	public static Editor createEditor( Object object) throws Exception
	{
		return REGISTRY.createEditor( object);
	}
	
	/** @returns a new BeanEditor */
	public static Editor createEditor( Object object, ObjectStore store)
		throws java.beans.IntrospectionException
	{
		return new BeanEditor( object, store);
	}

	/** @returns a new BeanEditor with the object and info provided */
	public static Editor createEditor( Object object, BeanInfo info)
	{
		return new BeanEditor( object, info);
	}

	/** @returns a new BeanEditor with the object, info and store provided */
	public static Editor createEditor( Object object, BeanInfo info, ObjectStore store)
	{
		return new BeanEditor( object, info , store);
	}

	static
	{
		REGISTRY.registerEditor( Object.class, BeanEditor.class);
	}

	//
	// Instance Members

	private Object object = null;
	private ObjectStore store = null;

	private boolean helping = false;
	private boolean modified = false;
	private StoreException storeException = null;

	//
	// Constructors
	
	protected Editor()
	{
		super();
	}
	
	/** @param object the Object to edit */
	protected Editor( Object object)
	{
		super();
		this.setObject( object);
	}

	/** 
	@param object the Object to edit
	@param store the ObjectStore to save Objects in
	*/
	protected Editor( Object object, ObjectStore store)
	{
		this( object);
		this.setStore( store);
	}

	//
	// Instance Methods

	/** @returns the Object to be edited */
	public Object getObject()
	{
		return this.object;
	}
	
	/** @param object the Object to be edited */
	public void setObject( Object object)
	{
		this.object = object;
	}
	
	/** @returns the ObjectStore to save objects in */
	public ObjectStore getStore()
	{
		return this.store;
	}
	
	/** @param the ObjectStore to save object in */
	public void setStore( ObjectStore store)
	{
		Check.isNotNull( store);
		this.store = store;
	}
	
	/** @returns true if this Editor has an ObjectStore */
	public boolean hasStore()
	{
		return this.store != null;
	}
	
	/** @param storeException should be set if the ObjectStore thows an
	exception during any store operation. The exception may then be
	reported to the user. */
	public void setStoreException( StoreException storeException)
	{
		this.storeException = storeException;
	}
	
	/** @returns a StoreException previously encountered by the editor,
	for display */
	public StoreException getStoreException()
	{
		return this.storeException;
	}
	
	/** @reutrns true if the Editor has previoulsy encountered a
	StoreException */
	public boolean hasStoreException()
	{
		return this.storeException != null;
	}
	
	/** clears a previous StoreException, once it has been reported to
	the user or corrected */
	public void clearStoreException()
	{
		this.storeException = null;
	}

	/** @param helping true if the editor is displaying help */
	public void setHelping( boolean helping)
	{
		this.helping = helping;
	}
	
	/** @returns true if the editor is displaying help */
	public boolean isHelping()
	{
		return this.helping;
	}
	
	/** @param modified is true if the Editor has modified it's Object */
	public void setModified( boolean modified)
	{
		this.modified = modified;
	}
	
	/** @returns true if the Editor has  modified it's Object */
	public boolean isModified()
	{
		return this.modified;
	}
	
	//
	// Interface Methods

	public String getLabel()
	{
		return this.object.toString();
	}
}
