package org.webgets.webtop;

import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.DateFormatSymbols;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

DatePicker supports the picking of dates via an on-screen calendar.

*/

public class CalendarDatePicker extends DatePicker implements HTML
{
	private static String[]       TABLE_ATTRS = new String[] { CELLSPACING, "0", CELLPADDING, "1"};
	private static String[]  CONTROL_TR_ATTRS = new String[] { ALIGN, RIGHT};
	private static String[]  CONTROL_TD_ATTRS = new String[] { COLSPAN, "7"};
	private static String[] CALENDAR_TR_ATTRS = new String[] { ALIGN, CENTER};
	private static String[]    GO_INPUT_ATTRS = new String[] { TYPE, SUBMIT, VALUE, "Go"};
	
	private Attributes disabledAttrs = new Attributes( CLASS, CSS_DISABLED);
	private Attributes lightblueAttrs = new Attributes( CLASS, CSS_LIGHT_BLUE);
	private Attributes selectedAttrs = new Attributes( CLASS, CSS_SELECTED);

	public void onDraw( TagWriter out)
	{
		super.traceDraw();
		
		Calendar day = (Calendar) this.calendar.clone();
		day.set( Calendar.WEEK_OF_MONTH, 1);
		day.set( Calendar.DAY_OF_WEEK, day.getFirstDayOfWeek()-1);
		day.getTime(); // force a compute
		
		out.tag( TABLE, TABLE_ATTRS);

			out.tag( TR, CONTROL_TR_ATTRS);
				out.tag( FORM, new String[] { METHOD, HTTP.GET, 
											  ACTION, out.encodeURL( out.getRequestURL())});
					out.emptyTag( INPUT, new String[] { 
						TYPE, HIDDEN, 
						NAME, RequestEvent.TARGET, 
						VALUE, this.getIdString()});
			

					out.tag( TD, CONTROL_TD_ATTRS);
						super.onDrawMonthSelect( out);
						super.onDrawYearSelect( out);
						out.emptyTag( INPUT, GO_INPUT_ATTRS);
					out.closeTag();
				out.closeTag();
			out.closeTag();
		
			out.tag( TR);
				String[] shortDayNames = symbols.getShortWeekdays();
				int dindex = day.getFirstDayOfWeek();
				while ( dindex < shortDayNames.length)
				{
					out.tag( TH);
						out.print( shortDayNames[dindex]);
					out.closeTag();
					dindex++;
				}
			out.closeTag();
			
			int week = 1;
			while ( week++ <= this.calendar.getActualMaximum( Calendar.WEEK_OF_MONTH))
			{
				out.tag( TR, CALENDAR_TR_ATTRS);
					int dayOfWeek = 1;
					while ( dayOfWeek++ <= 7)
					{
						day.add( Calendar.DAY_OF_MONTH, 1);
						if ( day.get( Calendar.MONTH) != this.calendar.get( Calendar.MONTH))
							out.tag( TD, this.disabledAttrs);
						else if ( day.equals( this.calendar))
							out.tag( TD, this.selectedAttrs);
						else
							out.tag( TD);
						
//						out.emptyTag( INPUT, new String[] { TYPE, SUBMIT, NAME, "day", 
//							VALUE, Integer.toString( day.get(Calendar.DAY_OF_MONTH))});
						out.tag( A, HREF, out.getRequestURL(), new String[] { 
							RequestEvent.TARGET, this.getIdString(),
							YEAR, Integer.toString( day.get( Calendar.YEAR)),
							MONTH, Integer.toString( day.get( Calendar.MONTH)),
							DAY, Integer.toString( day.get( Calendar.DAY_OF_MONTH))});
							out.print( day.get( Calendar.DAY_OF_MONTH));
						out.closeTag();
						out.closeTag();
					}
				out.closeTag();
			}
		
		/*
			out.tag( TR, CONTROL_TR_ATTRS);
				out.tag( FORM, new String[] { METHOD, HTTP.GET, 
											  ACTION, out.encodeURL( out.getRequestURL())});
					out.emptyTag( INPUT, new String[] { 
						TYPE, HIDDEN, 
						NAME, RequestEvent.TARGET, 
						VALUE, this.getIdString()});
			

					out.tag( TD, CONTROL_TD_ATTRS);
						super.onDrawYearSelect( out);
						out.emptyTag( INPUT, GO_INPUT_ATTRS);
					out.closeTag();
				out.closeTag();
			out.closeTag();
		*/
			
		out.closeTag( TABLE);		
/*	
		out.tag( TABLE);

			int   firstDayOfWeek = this.calendar.getActualMinimum( Calendar.DAY_OF_WEEK);
			int  firstDayOfMonth = this.calendar.getActualMinimum( Calendar.DAY_OF_MONTH);
			int   lastDayOfMonth = this.calendar.getActualMaximum( Calendar.DAY_OF_MONTH);
			int firstWeekInMonth = this.calendar.getActualMinimum( Calendar.WEEK_OF_MONTH);
			int  lastWeekInMonth = this.calendar.getActualMaximum( Calendar.WEEK_OF_MONTH);

			int thisDay = firstDayOfMonth;
			int today = this.calendar.get( Calendar.DAY_OF_MONTH);

			int week = this.calendar.getActualMinimum( Calendar.WEEK_OF_MONTH);

			while ( week <= lastWeekInMonth)
			{
				out.tag( TR);
					int dayOfWeek = Calendar.SUNDAY;
					if ( week == firstWeekInMonth) // special case for filling the first week
						;
					
					while ( dayOfWeek <= WEEKDAYS.length)
					{
						out.tag( TD);
							out.print( thisDay++);
						out.closeTag();
						dayOfWeek++;
					}
				out.closeTag();
				week++;
			}			
		out.closeTag( TABLE);
*/
	}
}
