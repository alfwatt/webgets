package org.webgets.webtop;

import org.w3c.dom.*;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

public class BorderTreeListener extends List implements DOMTree.DOMTreeListener 
{
	public BorderTreeListener()
	{
		super();
	}
	
	public BorderTreeListener( String label)
	{
		super( label);
	}
	
	public void onNodeSelect( Node selected)
	{
		this.removeAllComponents();
		try
		{
			Editor editor = Editor.createEditor( selected);
			editor.init( this.getServletConfig());
			this.addComponent( editor);
		}
		catch ( Exception display)
		{
			this.addComponent( new Warning( display));
		}
	}
}
