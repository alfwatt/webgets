package org.webgets.webtop;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.text.DateFormat;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

A Note is a snippit of HTML.

*/
public class Note extends DelegatorAdapter
{
	private String text = "";
	private TextArea input = null;
	
	public Note()
	{
		super();
	}
	
	public Note( String label)
	{
		super( label);
		this.setText( label);
	}
	
	public Note( String label, String text)
	{
		super( label);
		this.setText( text);
	}
	
	public String getLabel()
	{
		if ( super.getLabel() == null)
			return TagWriter.trimString( this.getText());
		else return super.getLabel();
	}
	
	public String getText()
	{
		return this.text;
	}
	
	public void setText( String text)
	{
		Check.isNotNull( text);
		this.text = text;
		if ( this.input != null) // it may be on the constructor call
			this.input.setValue( text);
	}
	
	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		this.input = new TextArea( "text", this.getText());
		
		List inputs = new List();
		inputs.addComponent( this.input);
		inputs.addComponent( Input.createSubmitInput( "Save"));
		this.setDelegate( new Form( inputs, new TextListener()));
		super.init( config);
	}

	class TextListener extends RequestListener.TracedRequestListener
	{
		public void onRequest( RequestEvent request)
		{
			super.onRequest( request);
			
			if ( request.hasParameter( "text"))
			{
				String text = request.getParameter( "text");
				setText( text);
			}
		}
	}
}
