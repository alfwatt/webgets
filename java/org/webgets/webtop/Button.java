package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

Button creates buttons which either link to a specified URL or notify a
RequestListener. Constructors are provided to create a button, setting
the URL automatically removes the RequestListener and vice versa.

Buttons may be local, in which case any URL used will be rewritten with
the sessionid. By default Buttons created with RequestListeners are
local and those created with URLs are not.

*/
public class Button extends ComponentAdapter
{

	//
	// private members

	private RequestListener listener = null;
	private String url = null;
	private boolean local = true;
	private boolean enabled = true;
	
	//
	// Protected Constructors

	/**
	
	Create a new Button with the specified label.
	
	@param label the label of the new Button
	
	*/	
	protected Button( String label)
	{
		super( label);
	}
	
	/**
	
	Create a new Button with a RequestLIstener and all other params.
	
	@param label the label of the Button
	@param listener the RequestListener of the button
	@param local is true by default
	@param enabled is true by default
	
	*/
	protected Button( String label, RequestListener listener, boolean local, boolean enabled)
	{
		this( label);
		this.setListener( listener);
		this.setLocal( local);
		this.setEnabled( enabled);
	}

	/**
	
	Create a new Button with a UrL and all other params.
	
	@param label is the label of the Button
	@param url is the URL the Button links to
	@param local is false by default
	@param enabled is true by default
	
	*/
	protected Button( String label, String url, boolean local, boolean enabled)
	{
		this( label);
		this.setURL( url);
		this.setLocal( local);
		this.setEnabled( enabled);
	}

	//
	// Public Constructors	

	public Button()
	{
		super();
	}

	/**
	
	Create a new Button with the specified label and listener.
	
	*/
	public Button( String label, RequestListener listener)
	{
		this( label);
		this.setListener( listener);
		this.setLocal( true);
	}
	
	/**
	
	Creates a new Button witht the specified label and URL.
	
	*/
	public Button( String label, String url)
	{
		this( label);
		this.setURL( url);
		this.setLocal( false);
	}

	//
	// Instance Methods	

	/** @returns the RequestListener of this Button */
	public RequestListener getListener()
	{
		return this.listener;
	}
	
	/** @param the new RequestListener of this Button, will set the URL to null */
	public void setListener( RequestListener listener)
	{
		Check.isNotNull( listener);
		this.listener = listener;
		this.url = null;
	}
	
	/** @returns true if the Button has a listener */
	public boolean hasListener()
	{
		return this.listener != null;
	}
	
	/** @returns the url of this button, when the button is clicked the
	user sould be sent to this URL */
	public String getURL()
	{
		return this.url;
	}

	/** @param url is the new url of this Button */
	public void setURL( String url)
	{
		Check.isNotNull( url);
		this.url = url;
		this.listener = null;
	}
	
	/** @returns true if the Button has a non null url */
	public boolean hasURL()
	{
		return this.url != null;
	}

	/** @returns true if the Button is a local link, will rewrite the
	URL with the sessionid */
	public boolean isLocal()
	{
		return this.local;
	}
	
	/** @param local is true if the Button should rewrite it's URL with
	the seesionid */
	public void setLocal( boolean local)
	{
		this.local = local;
	}
	
	/** @returns true if the Button is enabled */
	public boolean isEnabled()
	{
		return this.enabled;
	}
	
	/** @param enabled will enable the button is true */
	public void setEnabled( boolean enabled)
	{
		this.enabled = enabled;
	}
	
	/** conveniance for: <codde>setEnabled( false)</code> */
	public void disable()
	{
		this.setEnabled( false);
	}
	
	/** conveniance for: <code>setEnabled( true)</code> */
	public void enable()
	{
		this.setEnabled( true);
	}
	
	//
	// Interface Methods
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		/* disabled buttons cannot be clicked, even from a previous draw */
		if ( this.hasListener() && this.isEnabled() && request.isTargeted( this))
			this.getListener().onRequest( request);
	}

	/*
	
	<form method="get" action="/page.jsp">
		<input type="submit" name="Prev" value="Prev">
		<input type="submit" name="Next" value="Next" disabled>
	</form> 
	
	*/
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		String actionURL = this.hasListener() ? out.getRequestURL() : this.getURL();
		out.tag( HTML.FORM, new String[] { HTTP.METHOD, HTTP.GET, 
									 	   HTML.ACTION, this.isLocal() 
									 	   		? out.encodeURL( actionURL): actionURL});
			out.emptyTag( HTML.INPUT, new String[] { HTML.TYPE, HTML.HIDDEN,
													 HTML.NAME, RequestEvent.TARGET,
													 HTML.VALUE, this.getIdString()});
			if ( this.isEnabled())
				out.emptyTag( HTML.INPUT, new String[] { HTML.TYPE, HTML.SUBMIT,
														 HTML.NAME, this.getLabel(),
														 HTML.VALUE, this.getLabel()});
			else
				out.emptyTag( HTML.INPUT, new String[] { HTML.TYPE, HTML.SUBMIT,
														 HTML.NAME, this.getLabel(),
														 HTML.VALUE, this.getLabel(),
														 HTML.DISABLED, null});
		out.closeTag( HTML.FORM);
	}
}
