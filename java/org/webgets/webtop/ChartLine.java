package org.webgets.webtop;

import java.util.*;
import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

A single line in a chart

*/
public class ChartLine extends ComponentAdapter implements HTML
{
	private Thermometer therm = null;

	protected ChartLine( String label)
	{
		super( label);
		this.therm = new Thermometer( 0.5f);
	}

	public ChartLine( String label, float percent)
	{
		super( label);
		this.therm = new Thermometer( percent);
	}

	public ChartLine( String label, float percent, String color)
	{
		super( label);
		this.therm = new Thermometer( percent, color);
	}

	public void setPercent( float percent)
	{
		this.therm.setPercent( percent);
	}
	
	public float getPercent()
	{
		return this.therm.getPercent();
	}
	
	public void setTodoColor( String todo)
	{
		this.therm.setTodoColor( todo);
	}
	
	public void setDoneColor( String done)
	{
		this.therm.setDoneColor( done);
	}

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( TABLE);
			out.tag( TR);
				this.onDrawLabel( out);
				this.onDrawPercent( out);
				this.onDrawSummary( out);
			out.closeTag();
		out.closeTag();
	}
	
	public void onDrawLabel( org.webgets.util.TagWriter out)
	{
		out.tag( TD);
			out.write( this.getLabel());
		out.closeTag();
	}

	public void onDrawPercent( org.webgets.util.TagWriter out)
	{
		out.tag( TD);
			this.therm.onDraw( out);
		out.closeTag();
	}

	public void onDrawSummary( org.webgets.util.TagWriter out)
	{
		out.tag( TD);
			out.write( this.therm.getLabel());
		out.closeTag();
	}
}
