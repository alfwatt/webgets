package org.webgets.webtop;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

/**

A toggle displayes one of two linked images, when a request is recieved the toggle
becomes 'selected' or 'unselected' and shows the other image.

*/
public class Toggle extends ComponentAdapter
{
	private boolean selected = true;

	private Image selectedImage  = Image.createLocalImage( "un-select", "down.gif", 15, 15);
	private Image unselectedImage  = Image.createLocalImage( "select", "right.gif", 15, 15);
	private String[] targetAttrs = new String[] { RequestEvent.TARGET, this.getIdString()};

	public Toggle()
	{
		super();
	}
	
	public Toggle( String label)
	{
		super( label);
	}
	
	public Toggle( String label, boolean slected)
	{
		super( label);
		this.setSelected( selected);
	}

	public void setSelectedImage( Image selected)
	{
		if ( selected == null)
			throw new IllegalArgumentException( "Toggle.setSelectedImage() selected is null");
		this.selectedImage = selectedImage;
	}
	
	public Image getSelectedImage()
	{
		return this.selectedImage;
	}
	
	public void setUnselectedImage( Image unselected)
	{
		if ( unselected == null)
			throw new IllegalArgumentException( "Toggle.setUnselectedImage() unselected is null");
		this.unselectedImage = unselectedImage;
	}

	public void setSelected( boolean selected)
	{
		this.selected = selected;
	}
	
	public boolean isSelected()
	{
		return this.selected;
	}

	public void toggle()
	{
		this.selected = ! this.selected;
	}

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( HTML.A, HTML.HREF, out.getRequestURL(), targetAttrs, true);
		if ( this.isSelected())
			this.selectedImage.onDraw( out);
		else
			this.unselectedImage.onDraw( out);
		out.closeTag( HTML.A);
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
	
		if ( request.isTargeted( this))
		{
			this.toggle();
			request.stopProcessing( this);
		}
	}
}
