package org.webgets.webtop;

import java.text.*;
import java.util.*;
import java.beans.*;
import java.lang.reflect.*;

import org.webgets.*;
import org.webgets.html.*;
import org.webgets.util.*;
import org.webgets.text.*;
import org.webgets.util.beans.*;
import org.webgets.store.*;

import org.webgets.html.List; // to override java.util.LIst

class BeanEditor extends Editor
{
	private static final String HELP     = "help";
	private static final String HELP_ON  = "on";
	private static final String HELP_OFF = "off";

    private static final String ACTION        = "action";
    private static final String ACTION_UPDATE = "update";
    private static final String ACTION_DELETE = "delete";
    
    private static final String NULL_STRING = "<font color=\"darkgray\">null</font>";

	private BeanInfo info = null;
	private Form form = null;
	private String action = null;
	private HashMap warnings = new HashMap();

	BeanEditor()
	{
		super();
	}

	BeanEditor( Object object) throws IntrospectionException
	{
		super( object);
		this.setBeanInfo( Introspector.getBeanInfo( object.getClass()));
	}
	
	BeanEditor( Object object, ObjectStore store) throws IntrospectionException
	{
		super( object, store);
		this.setBeanInfo( Introspector.getBeanInfo( object.getClass()));
	}

	BeanEditor( Object object, BeanInfo info)
	{
		super( object);
		this.setBeanInfo( info);
	}

	BeanEditor( Object object, BeanInfo info, ObjectStore store)
	{
		super( object, store);
		this.setBeanInfo( info);
	}
	
	public BeanInfo getBeanInfo()
	{
		try
		{
			if ( this.info == null)
				this.info = Introspector.getBeanInfo( this.getObject().getClass());
		}
		catch ( IntrospectionException warn)
		{
			Log.printWarn( warn.toString());
		}

		return this.info;
	}
	
	public void setBeanInfo( BeanInfo info)
	{
		this.info = info;
	}

	private void buildForm() // XXX throws javax.servlet.ServletException
	{
	    if ( this.getObject() == null )
	    {
		    this.form = new Form( new Warning( "Deleted successfully"));
		    return;
	    }
	    
		List body = new MenuList( this.getLabel() + ( this.isModified() ? "  *" : ""));
		
		if ( this.isHelping())
			body.addComponent( new Block( this.info.getBeanDescriptor().getShortDescription()));

		if ( this.hasStoreException())
			body.addComponent( new Align( 
				new Warning( this.getStoreException().getDisplayMessage())));

		LabeledList propInputs = new LabeledList( this.info.getBeanDescriptor().getDisplayName());
		body.addComponent( propInputs);
		
		// get the property descriptors
		PropertyDescriptor[] props = this.info.getPropertyDescriptors();

		int index = 0;
		int limit = props.length;
		while ( index < limit)
		{
			PropertyDescriptor prop = props[index++];
			Class type = prop.getPropertyType();
			Format format = BeanTools.lookupFormat( prop);
			String name = prop.getDisplayName();
			Object value = null;

			try
			{
				value = BeanTools.getPropertyValueAsObject( prop, this.getObject());
			}
			catch ( Exception cantRead)
			{
				Log.printWarn( "BeanEditor.buildForm() attempting to read property nane=" + name 
					+ " value=" + value, cantRead);
					
				if ( Debug.DEBUG)
					value = cantRead.toString();
				else
					value = "";
			}
			
			/* TODO make several inputs in the array case, including
			some controls to manipulate order,
			TODO make three inputs in the date case, year month and day,
			including some controls to pick dates */
			try
			{
				/* is this a read only property */
				if ( type.isArray())
				{
					BulletList array = new BulletList();
					Object[] values = BeanTools.getPropertyValueAsArray( prop, this.getObject());
					int arrayIndex = 0;
					while ( arrayIndex < values.length)
						array.addComponent( new StringComp( values[arrayIndex++]));
					propInputs.addComponent( new Label( name, array));
				}
				else if ( prop.getWriteMethod() != null)
					propInputs.addComponent( 
						new Label( name, Input.createInput( name, value, format)));
				else if ( format != null) /* read only, formatted property */
					propInputs.addComponent( 
						new Label( name, new Block( TagWriter.trimString( format.format( value)))));
				else if ( value != null) /* read only, unformatted property */
					propInputs.addComponent( 
						new Label( name, new Block( TagWriter.trimString( value.toString()))));
				else /* read only, unformatted, null property */
					propInputs.addComponent( 
						new Label( name, new Block( NULL_STRING)));
			}
			catch ( Exception report)
			{
				propInputs.addComponent( new Warning( report.toString()));
			}

			if ( this.warnings.containsKey( name))
				if ( false)
					propInputs.addComponent( 
						new Label( "!", new Warning( (Exception) this.warnings.get( name))));
				else
					propInputs.addComponent( 
						new Label( "!", new Warning( "[WARNING] Property Was Not Updated!<br>"
							+ prop.getShortDescription())));

			if ( this.isHelping())
				propInputs.addComponent( new Label( "?", new Block( prop.getShortDescription())));

		}

		this.form = new Form( body, this.new EditorRequestListener());

		// create the method buttons
		Group methodButtons = new Group( "Methods");
			/* scan for the no-arg methods and create push buttons */
		body.addComponent( new Align( methodButtons, Align.CENTER));
		
		// create the form buttons
		Group formButtons = new Group( "Buttons");
		if ( ! this.isHelping())
			formButtons.addComponent( new Link( this.action, new String[] { 
				RequestEvent.TARGET, this.form.getIdString(), 
				HELP, HELP_ON}, new Block( "Help"), true));
		else
			formButtons.addComponent( new Link( this.action, new String[] { 
				RequestEvent.TARGET, this.form.getIdString(), 
				HELP, HELP_OFF}, new Block( "Hide Help"), true));

		formButtons.addComponent( new Link( this.action, new String[] { 
			RequestEvent.TARGET, this.form.getIdString(), 
			ACTION, ACTION_DELETE}, new Block( "Delete"), true));

		formButtons.addComponent( new Input( Input.RESET_TYPE,  "Reset") );
		formButtons.addComponent( new Input( Input.SUBMIT_TYPE, ACTION, ACTION_UPDATE ) );
		body.addComponent( new Align( formButtons, Align.RIGHT));
	
		this.warnings.clear();
		
		// XXX HACK stuffed exception
		try
		{
			this.form.init( this.getServletConfig());
		}
		catch ( javax.servlet.ServletException stuff)
		{
			Log.printWarn( "BeanEditor.buildForm()", stuff);
		}
	}

	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
	    if ( this.action == null )
	        this.action = request.getSourceRequest().getRequestURI();
	    
		if ( this.form == null) 
			this.buildForm();
			
		this.form.onRequest( request);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);

		if ( this.form == null)
			this.buildForm();

/*	    out.tag( HTML.A, new String[] { HTML.NAME, this.getIdString()});
	    out.closeTag(); */
			
		this.form.onDraw( out);
	}
	
	/**
	
	This member class implements the RequestListener interface for Editors, it's used to 
	gather data from the form.
	
	*/
	protected class EditorRequestListener implements RequestListener
	{
		public void onRequest( RequestEvent request)
		{
			String help   = request.getSourceRequest().getParameter( HELP);
			String action = request.getSourceRequest().getParameter( ACTION);

			if ( HELP_ON.equalsIgnoreCase( help))
				BeanEditor.this.setHelping( true);
			else if ( HELP_OFF.equalsIgnoreCase( help))
				BeanEditor.this.setHelping( false);
				
			Log.printWarn( "Editor.helping? " + help + " " + BeanEditor.this.isHelping());
		
		    if ( ACTION_UPDATE.equals( action ) )
		    {
				PropertyDescriptor[] props = BeanEditor.this.info.getPropertyDescriptors();
		
				int index = 0;
				int limit = props.length;
				boolean errors = false; /* set to true if there are any errors, we won't try to update */
				
				while ( index < limit)
				{
					PropertyDescriptor prop = props[index];
					String propName  = prop.getDisplayName();
					String formValue = request.getSourceRequest().getParameter( propName);
					
					/* conveniances */
					Class type = prop.getPropertyType();
					Format format = BeanTools.lookupFormat( prop);
					
					try
					{
						String beanValue = BeanTools.getPropertyValueAsString( 
							prop, BeanEditor.this.getObject());
								
						if ( format != null 
						  && ! type.isArray() 
						  && prop.getWriteMethod() != null 
						  && ! beanValue.equals( formValue))
						{
							Log.printDebug( "Editor.onRequest() " + propName 
								+ " value changing to " + formValue);
							BeanTools.setPropertyValueAsString( 
								prop, BeanEditor.this.getObject(), formValue);
							BeanEditor.this.setModified( true);
						}
					}
					catch ( InvocationTargetException unwrap)
					{
						BeanEditor.this.warnings.put( propName, unwrap.getTargetException());
						errors = true;
					}
					catch ( Exception badData)
					{
						BeanEditor.this.warnings.put( propName, badData);
						errors = true;
					}
		
					index++;
				}
				
				if ( BeanEditor.this.isModified() && BeanEditor.this.hasStore() && ! errors)
				{
					try
					{
						BeanEditor.this.getStore().update( BeanEditor.this.getObject());
						BeanEditor.this.clearStoreException();
					}
					catch ( StoreException report)
					{
						BeanEditor.this.setStoreException( report);
					}
				}
			}
			else if( ACTION_DELETE.equals( action))
			{
			    if ( BeanEditor.this.hasStore())
			    {
				    try
				    {
					    BeanEditor.this.getStore().delete( BeanEditor.this.getObject());
					    BeanEditor.this.clearStoreException();
					    BeanEditor.this.setObject( null);
				    }
				    catch ( StoreException report)
				    {
					    BeanEditor.this.setStoreException( report);
				    }
			    }
			}
    				
		    BeanEditor.this.buildForm();
		}
	}
	
	public String getLabel()
	{
		return this.getBeanInfo().getBeanDescriptor().getDisplayName() 
			+ ": " + this.getObject().toString();
	}
}
