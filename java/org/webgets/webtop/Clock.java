package org.webgets.webtop;

import java.util.Date;
import java.text.DateFormat;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.HTML;

public class Clock extends ComponentAdapter
{
	private DateFormat timeFormat = DateFormat.getTimeInstance( DateFormat.MEDIUM);
	private DateFormat dateFormat = DateFormat.getDateInstance( DateFormat.MEDIUM);

	public static final int COMMENT = 0; /* useful for page draw timing */
	public static final int SMALL = 1;
	public static final int MEDIUM = 2;
	public static final int LARGE = 3; 

	private int size = Clock.MEDIUM;

	public Clock()
	{
		super();
	}

	public Clock( int size)
	{
		this();
		this.setSize( size);
	}
	
	public void setSize( int size)
	{
		if ( size < COMMENT || size > LARGE)
			throw new IllegalArgumentException( "Clock.setSize() can't make a clock that size,"
				+ " try Clock.SMALL, Clock.MEDIUM or Clock.LARGE instead.");
		this.size = size;
	}

	public String getLabel()
	{
		return timeFormat.format( new Date());
	}

	private static final String[] TIME_SMALL_FONT_ATTRS 
		= new String[] { "size", "-1", "face", "sans-serif"};
	private static final String[] TIME_MEDIUM_FONT_ATTRS 
		= new String[] { "size", "+1", "face", "sans-serif"};
	private static final String[] TIME_LARGE_FONT_ATTRS 
		= new String[] { "size", "+2", "face", "sans-serif"};

	private static final String[] DATE_SMALL_FONT_ATTRS 
		= new String[] { "size", "-1", "face", "sans-serif"};
	private static final String[] DATE_MEDIUM_FONT_ATTRS 
		= new String[] { "size", "+1", "face", "sans-serif"};
	private static final String[] DATE_LARGE_FONT_ATTRS 
		= new String[] { "size", "+2", "face", "sans-serif"};

	private static final String[] CLOCK_TD_ATTRS = new String[] { "align", "center" };

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		if ( this.size == Clock.COMMENT)
		{
			out.comment( Long.toString( System.currentTimeMillis()));
			return;
		}

		Date now = new Date();
		out.tag( HTML.TABLE);
			out.tag( HTML.TR);	
				out.tag( HTML.TD, CLOCK_TD_ATTRS);
					switch ( this.size)
					{
						case Clock.SMALL:  out.tag( HTML.FONT, TIME_SMALL_FONT_ATTRS);  break;
						case Clock.MEDIUM: out.tag( HTML.FONT, TIME_MEDIUM_FONT_ATTRS); break;
						case Clock.LARGE:  out.tag( HTML.FONT, TIME_LARGE_FONT_ATTRS);  break;
					}
						out.write( timeFormat.format( now));
					out.closeTag();
				out.closeTag();
			out.closeTag();
			out.tag( HTML.TR);
				out.tag( HTML.TD, CLOCK_TD_ATTRS);
					switch ( this.size)
					{
						case Clock.SMALL:  out.tag( HTML.FONT, DATE_SMALL_FONT_ATTRS);  break;
						case Clock.MEDIUM: out.tag( HTML.FONT, DATE_MEDIUM_FONT_ATTRS); break;
						case Clock.LARGE:  out.tag( HTML.FONT, DATE_LARGE_FONT_ATTRS);  break;
					}
						out.write( dateFormat.format( now));
					out.closeTag();
				out.closeTag();
			out.closeTag();
		out.closeTag();
	}
}
