package org.webgets.webtop;

import java.util.*;

import org.w3c.dom.*;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

import org.w3c.dom.Document; /* incantation to hide org.webgets.html.Document */

public class DOMTree extends ContainerAdapter implements HTML
{
	private ArrayList listeners = new ArrayList();

	public DOMTree( Document document)
	{
		super( document.getDocumentElement().getNodeName());
		this.addComponent( new DOMTreeNode( document.getDocumentElement(), 0, this));
	}

	public DOMTree( Document document, DOMTreeListener listener)
	{
		this( document);
		this.addTreeListener( listener);
	}
	
	
	public void addTreeListener( DOMTreeListener listener)
	{
		synchronized ( this.listeners)
		{
			this.listeners.add( listener);
		}
	}
	
		
	public void removeTreeListener( DOMTreeListener listener)
	{
		synchronized( this.listeners)
		{
			this.listeners.remove( listener);
		}
	}
	
	public boolean hasTreeListeners()
	{
		return this.listeners.size() > 0;
	}

	void notifyTreeListenersOnNodeSelect( Node selected)
	{			
		Iterator li;
		synchronized (this.listeners)
		{
			li = this.listeners.iterator();
		
			while ( li.hasNext())
				((DOMTreeListener) li.next()).onNodeSelect( selected);
		}
	}

	// extends ContainerAdapter
	
	public void onDraw( TagWriter out)
	{
		out.tag( TABLE);
			super.onDraw( out);
		out.closeTag();
	}
	
	/** DOMTreeListeners will be notified when a DOMTreeNode is selected */ 
	interface DOMTreeListener
	{
		void onNodeSelect( Node selected);
	}
	
	static class DOMTreeNode extends ContainerAdapter
	{		
		private Image space = Image.createLocalImage( null, "trans.gif", 15, 15);
		private Toggle toggle = new Toggle();
		private Node node;
		private int depth = 0;
		private DOMTree root;
	
		DOMTreeNode( Node node, int depth, DOMTree root)
		{
			super( node.getNodeName());
			this.node = node;
			this.depth = depth;
			this.root = root;
			this.toggle.setSelected( false);
			this.init();
		}
		
		/**
		
		Creates DOMTreeNodes for all the children of this Node
		
		*/
		void init()
		{
			NodeList children = this.node.getChildNodes();
			if ( children != null)
			{
				int cindex = 0;
				while ( cindex < children.getLength())
					this.addComponent( new DOMTreeNode( children.item( cindex++), this.depth+1, this.root));
			}
		}

		public void onRequest( RequestEvent request)
		{
			super.onRequest( request);
			this.toggle.onRequest( request);

			if ( request.isTargeted( this))
				this.root.notifyTreeListenersOnNodeSelect( this.node);
		}

		public void onDraw( TagWriter out)
		{
			// special case for text nodes * comment nodes
			if ( this.node instanceof Comment)
			{
				out.comment( ((Comment) this.node).getData());
			}
			else if ( this.node instanceof CharacterData)
			{
				String cdata = ((CharacterData) this.node).getData();
				if ( cdata.length() > 0 && TagWriter.stripWhitespace( cdata).length() > 0)
				{
					int counter = this.depth;
					out.tag( TR);
						out.tag( TD);
							while ( counter-- > 0)
								this.space.onDraw( out);
	
							out.print( cdata);
						out.closeTag();
					out.closeTag(); 
				}
			}		
			else
			{
				int counter = this.depth;
				out.mark();
				out.tag( TR);
					out.tag( TD);
						out.tag( NOBR);
							while ( counter-- > 0)
								this.space.onDraw( out);
							this.toggle.onDraw( out);
							out.print( NBSP);
							if ( this.root.hasTreeListeners())
								out.tag( HTML.A, HTML.HREF, out.getRequestURL(), 
									new String[] { RequestEvent.TARGET, this.getIdString() });
							out.print( HTML.LT_ENT);
							out.print( this.node.getNodeName());
							NamedNodeMap attrs = this.node.getAttributes();
							if ( attrs != null)
							{
								int aindex = 0;
								while ( aindex < attrs.getLength())
								{
									if ( aindex == 0) // step away from the name
										out.print( NBSP);

									Node attr = attrs.item( aindex++);
									out.print( attr.getNodeName());
									out.print( "=");
									out.print( "\"");
									out.print( attr.getNodeValue());
									out.print( "\"");

									if ( aindex < attrs.getLength()) // keep the "<" close
										out.print( "  ");
								}
							}
							out.print( HTML.GT_ENT);
				out.closeToMark();
				
				if ( this.toggle.isSelected())
					super.onDraw( out);
			}
		}
	
	} // static class DOMTreeNode
}
