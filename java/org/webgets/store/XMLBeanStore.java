package org.webgets.store;

import java.beans.*;
import org.webgets.util.beans.*;

public class XMLBeanStore extends ObjectStoreAdapter 
{
	private Class type = null;
	private BeanInfo info = null;
	
	public XMLBeanStore( Class type)
	{
		super();
		this.type = type;
	}
	
	public XMLBeanStore( Class type, BeanInfo info)
	{
		super();
		this.type = type;
		this.info = info;
	}
	
}
