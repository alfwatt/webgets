package org.webgets.store;

import java.io.*;
import org.webgets.util.*;

/**

SerialBeanStore stores java objets in .ser files

<code>
path = /this.getStore()/object.getClass().getName()/Integer.toString( object.hashCode()).ser

key = object.getClass().getName() + "/" + Integer.toString( object.hashCode())
</code>

*/
public class SerialBeanStore extends ObjectStoreAdapter implements IndexedStore
{
	private static String DOT_SER = ".ser";
	
	private File store = null;
	
	//
	// Public Constructors
	
	public SerialBeanStore( File store)
	{
		super();
		this.setStore( store);
	}
	
	public SerialBeanStore( String storePath)
	{
		this( new File( storePath));
	}
	
	//
	// Public Methods
	
	public void setStore( File store)
	{
		Check.isNotNull( store);
		Check.isLegalArg( ! store.exists() || store.isDirectory(), 
			"store exists and is not a directory: " + store.toString());
			
		if ( ! store.exists())
			Check.isLegalArg( store.mkdirs(), "could not create store directory: " 
				+ store.toString());
			
		this.store = store;
	}
	
	public File getStore()
	{
		return this.store;
	}

	//
	// Interface Methods

	public String[] getIndexes()
	{
		File[] classDirs = this.listDirs();
		String[] classNames = new String[classDirs.length];
		int index = 0;
		while ( index < classNames.length)
			classNames[index] = classDirs[index++].getName();
		return classNames;
	}
	
	public String[] getIndexKeys( String indexName)
	{
		File[] files = this.listFiles( indexName);
		String[] keys = new String[files.length];
		int index = 0;
		while ( index < keys.length)
		{
			String fileName = files[index].getName();
			keys[index] = fileName.substring( 0, fileName.length()-DOT_SER.length());
			index++;
		}
		return keys;
	}
	
	public String[] getIndexKeys( String index, int offset, int count)
	{
		return null;
	}
	
	public Object select( String index, String value) throws StoreException
	{
		throw new StoreException( "not implemented");
	}

	public String insert( Object insert) throws StoreException
	{
		String key = this.calcKey( insert);
		
		try
		{
			new File( this.calcDir( insert)).mkdirs();
			File outFile = new File( this.calcPath( insert));
			if ( outFile.exists())
				outFile.delete();
			outFile.createNewFile();
			ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream( outFile));
			out.writeObject( insert);
			out.flush();
			out.close();
		}
		catch ( IOException wrap)
		{
			throw new StoreException( wrap);
		}
				
		return key;
	}
	
	public Object select( String key) throws StoreException
	{
		String path = this.calcPath( key);
		Object selected = null;
		
		try
		{
			ObjectInputStream in = new ObjectInputStream( new FileInputStream( path));
			selected = in.readObject();
			in.close();
		}
		catch ( Exception wrap)
		{
			throw new StoreException( wrap);
		}
		
		return selected;
	}
	
	public void update( Object update) throws StoreException
	{
		this.insert( update);
	}
	
	public Object revert( Object revert) throws StoreException
	{
		return this.select( this.calcKey( revert));	
	}
	
	public void delete( Object delete) throws StoreException
	{
		File path = new File( this.calcPath( delete));
		if ( path.exists() && path.isFile())	
			path.delete();
		else
			throw new StoreException( "path does not exist or is not a file: " + path);			
	}

	//
	// Private Methods
	
	private String calcDir( Object object)
	{
		return this.getStore().toString() + "/" + object.getClass().getName();
	}
	
	private File[] listDirs()
	{
		return this.getStore().listFiles( new FileFilter() 
		{ 
			public boolean accept( File file) { return file.isDirectory(); }
		});
	}

	private File[] listFiles( String className)
	{
		File storeDir = new File( this.getStore(), className);
		Log.printDebug( storeDir.toString());
		return storeDir.listFiles();
	}

	private String calcPath( Object object)
	{
		return this.calcDir( object) + File.separator // /
					+ Integer.toString( object.hashCode()) + DOT_SER;
	}

	private String calcPath( String key)
	{
		return this.getStore().toString() + File.separator + key + DOT_SER;
	}
	
	private String calcKey( Object object)
	{
		return object.getClass().getName() + File.separator // /
			+ Integer.toString( object.hashCode());
	}
}
