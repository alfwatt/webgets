package org.webgets.store;

/**

ObjectStoreAdapter implements all the methods, except create(), of
ObjectStore to throw UnsupportedOperationExceptions.

*/
public class ObjectStoreAdapter implements ObjectStore
{
	public Object create( Class type) throws StoreException
	{
		try
		{
			return type.newInstance();
		}
		catch ( Exception wrap)
		{
			throw new StoreException( wrap);
		}
	}
	
	public String insert( Object insert) throws StoreException
	{
		throw new UnsupportedOperationException( "StoreAdapter.insert() not implemented");
	}
	
	public Object select( String key) throws StoreException
	{
		throw new UnsupportedOperationException( "StoreAdapter.select() not implemented");
	}
	
	public void update( Object update) throws StoreException
	{
		throw new UnsupportedOperationException( "StoreAdapter.update() not implemented");
	}
	
	public Object revert( Object revert) throws StoreException
	{
		throw new UnsupportedOperationException( "StoreAdapter.revert() not implemented");
	}
	
	public void delete( Object delete) throws StoreException
	{
		throw new UnsupportedOperationException( "StoreAdapter.delete() not implemented");
	}
}
