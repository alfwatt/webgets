package org.webgets.store;

/**

ObjectStore defines methods for creating, inserting, selecting,
updating, reverting and deleting object from a storage medium. It is
used by the Editor package to perform these operations against an
arbitray store.

Typical implementations of this class will adapt to some other
persistance tools.

*/
public interface ObjectStore
{

	/** @return a new bean from the Store, if the Store must create an
	ID or perform other checks, this is the time */
	Object create( Class type) throws StoreException;
	
	/** @return a key String for the Object inserted into the Store */
	String insert( Object insert) throws StoreException;
	
	/** @return an Object from the Store for the key String provided */
	Object select( String key) throws StoreException;
	
	/** updates a bean in the store */
	void update( Object update) throws StoreException;
	
	/** @return the Object provided, reverted to it's last state in the
	store, without whatever local changes have been made */
	Object revert( Object revert) throws StoreException;
	
	/** deletes the bean provided from the store */
	void delete( Object delete) throws StoreException;
}
