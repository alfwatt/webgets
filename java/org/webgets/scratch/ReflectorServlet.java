package org.webgets.scratch;

/**

ReflectorService - reflection of a multi-part mime stream from a client to
one or more other clients.

A namespace of stream buffers is created to which a stream may be posted.
The ReflectorService will keep a posted stream in the buffer until it's 
cache-control headers suggest it's removal or it's LRU'd out of the cache. 

Stream registration is sent as parameters to the POST request:

	stream-name			-- name of the stream for index
	stream-type			-- mime type of the stream for index
	stream-length		-- length of the stream for index
	stream-author		-- authror of the stream for index
	stream-recipiants	-- intended recipiants of the stream for index
	stream-timestamp	-- timestamp of the stream
	stream-data			-- data for the stream

This information is saved along with the stream an used to browse the
cache and allow client connections.

When requesting a stream the following POST or GET parameters are processed:

	stream-name
	stream-type
	stream-author
	stream-recipiant
	stream-timestamp
	
If a stream from the cache matches all of the conditions provided, it it streamed
to the requesting user.

*/
public class ReflectorServlet implements javax.servlet.Servlet
{
	/** The protected class Stream holds the record for a Stream which has been posted.
	it's for internal use only so it's not a bean. */
	protected class Stream extends Object
	{
		final String name;
		final String type;
		final String length;
		final String author;
		final String[] recipiants;
		final String timestamp;
		final StringBuffer data;
		final Object writeLock = new Object();
		
		public Stream( String name, 
					   String type, 
					   String length, 
					   String author,
					   String[] recipiants, 
					   String timestamp, 
					   StringBuffer data)
		{
			this.name=name;
			this.type=type;
			this.length=length;
			this.author=author;
			this.recipiants=recipiants;
			this.timestamp=timestamp;
			this.data=data;	
		}
	}
	
	private static java.util.Map index = new java.util.HashMap();
	
	static void indexStream( Stream stream)
	{
		index.put( stream.name, stream);
	}
	
	static Stream streamNamed( String name)
	{
		return (Stream) index.get( name);
	}


	private javax.servlet.ServletConfig config = null;
	
	public String getServletInfo()
	{
		return this.getClass().toString();
	}
	
	public void init(javax.servlet.ServletConfig config)
	{
		this.config = config;
	}
	
	public  javax.servlet.ServletConfig getServletConfig()
	{
		return this.config;
	}
	
	public void service( javax.servlet.ServletRequest request, javax.servlet.ServletResponse response)
	{
		
	}
	
	public void destroy()
	{
		this.config = null;
	}
}
