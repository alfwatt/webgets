package org.webgets.scratch;

// import some.pcre.package;
// import some.xpath.package;
// import command.line.like.grep.package;
// import a.small.web.services.framework

/**

The ScraperService is a demo web service which provides screen scraping
of text documents accessble by URL. This includes HTML, XML and Text web
pages.

ScrapeService takes a URL to fetch and scrape, and a search pattern to
send to the regular expression engine.

*/
public class ScraperService // implements javax.servlet.http.HttpServlet
{
	/** Perl Compatable Regular Expressions, search patterns will
	conintain comma delimited sub-patters to be returned as a  String[]
	by ScraperService.scrape() */
	public static String PCRE_ENGINE = "pcre";

	/** X-Path selectors seperated by commas can be applied to XML
	documents and returned as an array. */
	public static String XPATH_ENGINE = "xpath";
	
	/** Command-line grep mode, returns $1, etc.. in the array */
	public static String GREP_ENGINE = "grep";
	
	/** The local grep or find services of the host OS, results will
	vary depending on the platform. a novelty method. */
	public static String SYSTEM_ENGINE = "system";
	
	public static String DEFAULT_ENGINE = GREP_ENGINE;
	
	private static String[] ENGINES = new String[] { 
		PCRE_ENGINE, XPATH_ENGINE, GREP_ENGINE};
	
	public static String[] engines()
	{
		return ENGINES;
	}

	/**
	
	Scrape with the default engine, grep
	perhaps via a shell script at first, but better with a java
	library which provides grep-like regular expressions
	
	*/
	public static String[] scrape( String url, String pattern)
	{
		return scrape( url, pattern, DEFAULT_ENGINE);
	}
	
	/**
	
	@param url fetch the document at the url provided,

	@param pattern send the search pattern with replacements marking
	strings to return

	@param engine indicate which engine should process the pattern:
	pcre, xpath or grep

	@returns an array of matches to the pattern supplied provides by the
	engine specified
	 
	*/
	public static String[] scrape( String url, String pattern, String engine)
	{
		return null; // TODO
	}
}
