package org.webgets.demos.map;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;
//import org.webgets.webtop.*;
import org.webgets.svg.*;
import org.webgets.svg.Grid;

public class MapController extends org.webgets.DelegatorAdapter
{
	private Map map = null;
	private LabeledList pinList = null;
	
	public MapController()
	{
		super();
		this.init();
	}
	
	public MapController( Map map)
	{
		super( map.getLabel());
		this.setMap( map);
		this.init();
	}

	public void init()
	{
		LabeledList tasks = new LabeledList();
		this.setDelegate( tasks);
		
		Container pinInputs = new LabeledList( "Pin");
		Form pinForm = new Form( pinInputs);
			 pinForm.setMethod( org.webgets.util.HTTP.GET); // here thar be dragons...
			 pinForm.addRequestListener( new PinListener());
		pinInputs.addComponent( Input.createTextInput( "label", "", 12, 12));
		pinInputs.addComponent( Input.createTextInput( "color", "", 12, 12));
		pinInputs.addComponent( Input.createTextInput( "x", "", 4, 4));
		pinInputs.addComponent( Input.createTextInput( "y", "", 4, 4));
		pinInputs.addComponent( new Label( "", Input.createSubmitInput( "Add")));
		tasks.addComponent( pinForm); 
		
		this.pinList = new LabeledList( "Pins");
		tasks.addComponent( pinList);
		
		Container lineInputs = new LabeledList( "Line");
		Form lineForm = new Form( lineInputs);
			 lineForm.setMethod( org.webgets.util.HTTP.GET);
			 lineForm.addRequestListener( new LineListener());
		lineInputs.addComponent( Input.createTextInput( "x1", "0", 4, 4));
		lineInputs.addComponent( Input.createTextInput( "y1", "0", 4, 4));
		lineInputs.addComponent( Input.createTextInput( "x2", "10", 4, 4));
		lineInputs.addComponent( Input.createTextInput( "y2", "10", 4, 4));
		lineInputs.addComponent( new Label( "", Input.createSubmitInput( "Draw")));
		tasks.addComponent( lineForm);
	}
	
	public void setMap( Map map)
	{
		Check.isNotNull( map);
		this.map = map;
	}

	public Map getMap()
	{
		return this.map;
	}

	private void buildPinList()
	{
 	   this.pinList.removeAllComponents();
		Component[] pins = this.getMap().getComponents();
		int index = 0;
		while ( index < pins.length)
			this.pinList.addComponent( new Label( pins[index].getLabel(), ((SVG) pins[index++]).getTransform()));
	}

	private class PinListener implements RequestListener
	{
		public void onRequest( RequestEvent event) // throws java.io.IOException
		{
			String label = event.getParameter( "label");
			String color = event.getParameter( "color");
			int xint = Integer.parseInt( event.getParameter( "x"));
			int yint = Integer.parseInt( event.getParameter( "y"));
			if ( label == null || label.length() == 0)
				label = event.getParameter( "x") + "," + event.getParameter( "y");
			if ( color != null)
				getMap().addPin( new Pin( label, color), xint, yint);
			else
				getMap().addPin( new Pin( label), xint, yint);
			buildPinList();
			
			event.redirectResponse( this, event.getEncodedRequestURL()); // prevents get replay on reload
		}
	}
	
	private class LineListener implements RequestListener
	{
		public void onRequest( RequestEvent event) 
		{
			String x1 = event.getParameter( "x1");
			String x2 = event.getParameter( "x2");
			String y2 = event.getParameter( "y2");
			String y1 = event.getParameter( "y1");
			
			if ( x1 != null && x2 != null && y1 != null && y2 != null)
				getMap().add( new Line( x1, y1, x2, y2));
			
			event.redirectResponse( this, event.getEncodedRequestURL()); // prevents get replay on reload
		}
	}
	
	private class GridListener implements RequestListener
	{
		public void onRequest( RequestEvent event)
		{
			String xcount = event.getParameter( "xcount");
			String ycount = event.getParameter( "ycount");
			String xspace = event.getParameter( "xspace");
			String yspace = event.getParameter( "yspace");
			
			if  ( getMap().getGrid() == null)
				getMap().setGrid( new Grid());

			if ( xcount != null)
				getMap().getGrid().setXCount( Integer.parseInt( xcount));

			if ( ycount != null)
				getMap().getGrid().setYCount( Integer.parseInt( ycount));

			if ( xspace != null) 
				getMap().getGrid().setXSpace( Integer.parseInt( xspace));

			if ( yspace != null)
				getMap().getGrid().setYSpace( Integer.parseInt( yspace));

			event.redirectResponse( this, event.getEncodedRequestURL()); // preverts get replay on reload
		}
	}
}
