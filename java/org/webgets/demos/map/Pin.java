package org.webgets.demos.map;

import org.webgets.svg.*;
import org.webgets.util.*;

/*

Pins are placed inside a Group with a specified transform by the MapController 
then addedadded to a MapView, and are drawn after (on top of) the Map Component.

Pins must have either a label or a graphic or both, the label will be drawn after
the graphic...

*/
public class Pin extends SVGComponent
{
	private static SVG DEFAULT_GRAPHIC = new Circle( "3", "3", "4", "darkgrey");
	
	private SVG graphic = DEFAULT_GRAPHIC;
	
	public Pin( String label)
	{
		super( label);
	}

	public Pin( String label, String color)
	{
		super( label);
		this.graphic = new Circle( "3", "3", "4", color);
	}
	
	public Pin( String label, SVG graphic)
	{
		this( label);
		this.graphic = graphic;
	}

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		if ( this.graphic != null)
			this.graphic.onDraw( out);
		if ( this.hasLabel())
		{
			out.disableIndent(); // keep it with the graphic...
			out.tag( TEXT, this.attrs);
				out.print( this.getLabel());
			out.closeTag();
			out.enableIndent();
		}	
	}
}
