package org.webgets.demos.map;

import org.webgets.svg.*;
import org.webgets.util.*;
import org.webgets.svg.Grid;

/*

A Map is an SVGContainer that holds Pins, it has an image and an optional scaling factor 
for 

*/
public class Map extends SVGContainer
{
	private float DEFAULT_X_SCALE = 1.0f;
	private float DEFAULT_Y_SCALE = 1.0f;
	
	private SVG image = null;
	private Grid grid = null;
	private float xscale = DEFAULT_X_SCALE;
	private float yscale = DEFAULT_Y_SCALE;
	
	
	public Map( SVG image)
	{
		super();
		this.setImage( image);
	}
	
	public Map( String label, SVG image)
	{
		super( label);
		this.setImage( image);
	}
	
	public Map( String label, SVG image, Grid grid)
	{
		this( label, image);
		this.setGrid( grid);
	}
	
	public Map( String label, SVG image, float scale)
	{
		this( label, image, scale, scale);
	}
		
	public Map( String label, SVG image, float xscale, float yscale)
	{
		super( label);
		this.setImage( image);
		this.setXScale( xscale);
		this.setYScale( yscale);
	}
	
	public void setImage( SVG image)
	{
		this.image = image;
	}
	
	public void setXScale( float xscale)
	{
		this.xscale = xscale;
	}
	
	public void setYScale( float yscale)
	{
		this.yscale = yscale;
	}
	
	public void addPin( Pin pin, int xlocation, int ylocation)
	{
		Group g = new Group( pin);
		g.addTranslateTransform( Integer.toString( xlocation), Integer.toString( ylocation));
		if ( this.xscale != DEFAULT_X_SCALE || this.yscale != DEFAULT_Y_SCALE)	
			g.addScaleTransform( Float.toString( this.xscale), Float.toString( this.yscale));
		this.addComponent( g);
	}
	
	public void setGrid( Grid grid)
	{
		this.grid = grid;
	}
	
	public Grid getGrid()
	{
		return this.grid;
	}
	
	public void onRequest( RequestEvent req)
	{
		this.image.onRequest( req);
		if ( this.grid != null)
			this.grid.onRequest( req);
		super.onRequest( req);
	}
	
	public void onDraw( TagWriter out)
	{
		this.image.onDraw( out);
		if ( this.grid != null)
			this.grid.onDraw( out);
		super.onDraw( out);
	}
}
