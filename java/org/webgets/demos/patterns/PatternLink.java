package org.webgets.demos.patterns;

import org.webgets.util.*;

/**

A PatternLink establishes a relationship between a super-pattern and a
sub-pattern withing a PatternLanguage.

PatternLink are created with the method 
<code>PatternLanguage.createLink( Pattern, Pattern)</code>

*/
public class PatternLink extends Object implements java.io.Serializable
{
	public static final PatternLinkBeanInfo BEAN_INFO = PatternLinkBeanInfo.INSTANCE;

	private Pattern superPattern = null;
	private Pattern subPattern = null;
	
	PatternLink( Pattern superPattern, Pattern subPattern)
	{
		super();
		this.setSuperPattern( superPattern);
		this.setSubPattern( subPattern);
	}
	
	public Pattern getSuperPattern()
	{
		return this.superPattern;
	}
	
	public void setSuperPattern( Pattern superPattern)
	{
		Check.isNotNull( superPattern);
		this.superPattern = superPattern;
	}
	
	public Pattern getSubPattern()
	{
		return this.subPattern;
	}
	
	public void setSubPattern( Pattern subPattern)
	{
		Check.isNotNull( subPattern);
		this.subPattern = subPattern;
	}
	
	public String toString()
	{
		return this.getSuperPattern().toString() + " <- " + this.getSubPattern().toString();
//		return this.getSubPattern().toString() + " -> " + this.getSuperPattern().toString();
	}
}
