package org.webgets.demos.patterns;

import org.webgets.*;
import org.webgets.util.*;
import org.webgets.html.*;

public class PatternBrowser extends ComponentAdapter
{
	private Pattern pattern = null;
	
	public PatternBrowser()
	{
		super();
		this.setPattern( new Pattern());
	}
	
	public PatternBrowser( Pattern pattern)
	{
		super();
		this.setPattern( pattern);
	}
	
	public Pattern getPattern()
	{
		return this.pattern;
	}
	
	public void setPattern( Pattern pattern)
	{
		Check.isNotNull( pattern);
		this.pattern = pattern;
	}
	
	public String getLabel()
	{
		return this.pattern.getName();
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		Font.PLUS_2.onDraw( out, "Pattern: " + this.pattern.getName());
		Block.HR.onDraw( out);
		
		Font.PLUS_1.onDraw( out, "Context: ");
		Block.BR.onDraw( out);
		out.tag( HTML.P);
		out.print( this.pattern.getContext());
		out.closeTag();
		
		Font.PLUS_1.onDraw( out, "Problem: ");
		Block.BR.onDraw( out);
		out.tag( HTML.P);
		out.print( this.pattern.getProblem());
		out.closeTag();
		
		Font.PLUS_1.onDraw( out, "Forces: ");
		Block.BR.onDraw( out);
		out.tag( HTML.UL);
			String[] forces = this.pattern.getForces();
			int index = 0;
			while( index < forces.length)
			{
				out.tag( HTML.LI);
					out.print( forces[index]);
				out.closeTag( HTML.LI);				
				index++;
			}
		out.closeTag();
		
		Font.PLUS_1.onDraw( out, "Solution: ");
		Block.BR.onDraw( out);
		out.tag( HTML.P);
			out.print( this.pattern.getSolution());
		out.closeTag();

		Block.HR.onDraw( out);		
	}
}
