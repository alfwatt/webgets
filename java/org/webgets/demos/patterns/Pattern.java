package org.webgets.demos.patterns;

import java.util.ArrayList;

/**

<p>Each pattern is a three-part rule, which expresses a relation between
a centain context, a problem, and a solution.</p>

<p>Christopher Alexander coined the term "PatternLanguage" to emphasize
his belief that people had an innate ability for design that paralleled
their ability to speak. </p>

<p>Chris's book A TimelessWayOfBuilding is the most instructive in
describing his notion of a pattern language and its application to
designing and building buildings and towns.</p>

<p>He defines a 'pattern' as a three part construct. First comes the
'context'; under what conditions does this pattern hold. Next are a
'system of forces'. In many ways it is natural to think of this as the
'problem' or 'goal'. The third part is the 'solution'; a configuration
that balances the system of forces or solves the problems presented.</p>

*/
public class Pattern extends Object implements java.io.Serializable
{
	public static final PatternBeanInfo BEAN_INFO = PatternBeanInfo.INSTANCE;
	
	public static final int      PATTERN_VALUE = 1;
	public static final int  NON_PATTERN_VALUE = 0;
	public static final int ANTI_PATTERN_VALUE = -1;

	// Primaries	
	private String name = null;
	private String context = null;
	private String problem = null;
	private ArrayList forces = new ArrayList();
	private String solution = null;

	// Secondaries
	private String author = null;
	private int value = 1;
	
	// Internals
	private transient String[] forces_cache = null;

	/** Creates a new Pattern with all null fields. */
	public Pattern()
	{
		super();
	}
	
	/**
	
	Creates a new Pattern with the specified name.
	
	@param name the name of the Pattern
	
	*/
	public Pattern( String name)
	{
		this();
		this.setName( name);
	}
	
	/** @returns the name of this Pattern */
	public String getName()
	{
		return this.name;
	}
	
	/** @param name the new name of this Pattern */
	public void setName( String name)
	{
		this.name = name;
	}
	
	/** @returns the context of this Pattern */
	public String getContext()
	{
		return this.context;
	}
	
	/** @param context the new context of this Pattern */
	public void setContext( String context)
	{
		this.context = context;
	}

	/** @returns the problem this Pattern solves */
	public String getProblem()
	{
		return this.problem;
	}
	
	/** @param problem the description of the problem this Pattern solves */
	public void setProblem( String problem)
	{
		this.problem = problem;
	}
	
	/** @param force a force which the Patterns aims to bring into balance */
	public synchronized void addForce( String force)
	{
		this.forces_cache = null;
		this.forces.add( force);
	}
	
	/** @param force a force which this Pattern does not bring into balance */
	public synchronized void removeForce( String force)
	{
		this.forces_cache = null;
		this.forces.remove( force);
	}
	
	/** @returns the array of forces this Pattern brings into balance */
	public synchronized String[] getForces()
	{
		if ( this.forces_cache == null)
		{
			String[] fc = new String[ this.forces.size()];
			this.forces.toArray( fc);
			this.forces_cache = fc;
		}
		
		return this.forces_cache;
	}
	
	/** @returns the solution to the Pattern, the configuration of forces which brings
		the pattern into balance */
	public String getSolution()
	{
		return this.solution;
	}

	/** @param solution the new solution to the Pattern */	
	public void setSolution( String solution)
	{
		this.solution = solution;
	}
	
	/** @returns the author of this pattern */
	public String getAuthor()
	{
		return this.author;
	}
	
	/** @param author the authro of this pattern */
	public void setAuthor( String author)
	{
		this.author = author;
	}
	
	/** @returns the value of this pattern. The default value of a
	Pattern is 1, indicating that the pattern is usefull and good.
	Anti-Patterns will have negative values and patters of no
	appreciable value will have a zero value. */
	public int getValue()
	{
		return this.value;
	}
	
	/** @param value the new value of this pattern */
	public void setValue( int value)
	{
		this.value = value;
	}
	
	public String toString()
	{
		return this.getName();
	}
}
