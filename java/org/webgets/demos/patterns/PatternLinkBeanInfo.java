package org.webgets.demos.patterns;

import java.beans.*;
import org.webgets.util.*;
import org.webgets.util.beans.*;

public class PatternLinkBeanInfo extends SimpleBeanInfo
{
	public static final PatternLinkBeanInfo INSTANCE = new PatternLinkBeanInfo();
	
	private static final BeanDescriptor BEAN_DESCRIPTOR;
	private static final PropertyDescriptor[] BEAN_PROPS;
	
	static
	{
		BeanDescriptor bean = null;
		PropertyDescriptor[] props = null;
		
		try
		{
			bean = new BeanDescriptor( PatternLink.class, Object.class);
			bean.setDisplayName( "Link");
			bean.setShortDescription( "Link");
		
			props = new PropertyDescriptor[]
			{
				BeanInfoHelper.createPropertyDescriptor( 
					"superPattern", "Super Pattern",
					"The more general pattern", PatternLink.class),
				BeanInfoHelper.createPropertyDescriptor(
					"subPattern", "Sub Pattern",
					"The more specific pattern", PatternLink.class)
			};
		}
		catch ( Exception log)
		{
			Log.printErr( "PatternLinkBeanInfo could not init!", log);
		}
		finally
		{
			BEAN_DESCRIPTOR = bean;
			BEAN_PROPS = props;
		}
	}
	
	public BeanDescriptor getBeanDescriptor()
	{
		return BEAN_DESCRIPTOR;
	}
	
	public PropertyDescriptor[] getPropertyDescriptors()
	{
		return BEAN_PROPS;
	}

}
