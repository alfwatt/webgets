package org.webgets.demos.patterns;

import java.util.*;
import java.beans.BeanInfo;
import org.webgets.util.*;

public class PatternLanguage extends Object implements java.io.Serializable
{
	public static final BeanInfo BEAN_INFO = PatternLanguageBeanInfo.INSTANCE;
	
	private String name = null;
	private String author = null;
	private List links = new ArrayList(0);
	
	private transient PatternLink[] links_cache = null;

	protected PatternLanguage()
	{
		super();
	}
	
	public PatternLanguage( String name)
	{
		this();
		this.setName( name);
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName( String name)
	{
		Check.isNotNull( name);
		this.name = name;
	}
	
	public String getAuthor()
	{
		return this.author;
	}
	
	public void setAuthor( String author)
	{
		this.author = author;
	} 
	
	public void addLink( PatternLink link)
	{
		synchronized ( this.links)
		{
			this.links_cache = null;
			this.links.add( link);
		}
	}
	
	public void removeLink( PatternLink link)
	{
		synchronized ( this.links)
		{
			this.links_cache = null;
			this.links.remove( link);
		}
	}
	
	public PatternLink[] getLinks()
	{
		synchronized ( this.links)
		{
			if ( this.links_cache == null)
			{
				PatternLink[] cache = new PatternLink[ this.links.size()];
				this.links.toArray( cache);
				this.links_cache = cache;
			}
		}
				
		return this.links_cache;
	}
	
	public void createLink( Pattern superPattern, Pattern subPattern)
	{
		this.links.add( new PatternLink( superPattern, subPattern));
	}
	
	public String toString()
	{
		return this.getName();
	}

} // PatternLanguage
