package org.webgets.demos.patterns;

import java.beans.*;
import org.webgets.util.*;
import org.webgets.util.beans.*;

public class PatternLanguageBeanInfo extends java.beans.SimpleBeanInfo
{
	public static final PatternLanguageBeanInfo INSTANCE = new PatternLanguageBeanInfo();
	
	private static final BeanDescriptor BEAN_DESCRIPTOR;
	private static final PropertyDescriptor[] BEAN_PROPS;
	
	static
	{
		BeanDescriptor bean = null;
		PropertyDescriptor[] props = null;
		
		try
		{
			bean = new BeanDescriptor( PatternLanguage.class, Object.class);
			bean.setDisplayName( "Pattern Language");
			bean.setShortDescription( "Pattern Language");
		
			props = new PropertyDescriptor[]
			{
				BeanInfoHelper.createPropertyDescriptor( 
					"name", "Name", 
					"The Name of this Pattern Language", PatternLanguage.class),
				BeanInfoHelper.createPropertyDescriptor(
					"author", "Author",
					"The author of this Pattern Language", PatternLanguage.class),
				BeanInfoHelper.createReadOnlyPropertyDescriptor(
					"links", "Links",
					"Links between Patterns in this Language", false,
					PatternLanguage.class.getDeclaredMethod( "getLinks", BeanInfoHelper.NO_ARGS))
			};
		}
		catch ( Exception log)
		{
			Log.printErr( "PatternLanguageBeanInfo could not init!", log);
		}
		finally
		{
			BEAN_DESCRIPTOR = bean;
			BEAN_PROPS = props;
		}
	}
	
	public BeanDescriptor getBeanDescriptor()
	{
		return BEAN_DESCRIPTOR;
	}
	
	public PropertyDescriptor[] getPropertyDescriptors()
	{
		return BEAN_PROPS;
	}

}
