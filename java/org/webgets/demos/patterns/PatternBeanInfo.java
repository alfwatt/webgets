package org.webgets.demos.patterns;

import java.beans.*;
import org.webgets.util.*;
import org.webgets.util.beans.*;

public class PatternBeanInfo extends SimpleBeanInfo
{
	public static final PatternBeanInfo INSTANCE = new PatternBeanInfo();
	
	private static final BeanDescriptor BEAN_DESCRIPTOR;
	private static final PropertyDescriptor[] BEAN_PROPS;
	
	static
	{
		BeanDescriptor bean = null;
		PropertyDescriptor[] props = null;
		
		try
		{
			bean = new BeanDescriptor( Pattern.class, Object.class);
			bean.setShortDescription( "Pattern");
		
			props = new PropertyDescriptor[]
			{
				BeanInfoHelper.createPropertyDescriptor( 
					"name", "Name", 
					"The Name of this Pattern", Pattern.class),
				BeanInfoHelper.createPropertyDescriptor(
					"context", "Context", 
					"The Context in which this Pattern operates", Pattern.class),
				BeanInfoHelper.createPropertyDescriptor(
					"problem", "Problem", 
					"The Problem which this Pattern solves", 
					"java.text.Format", new org.webgets.text.TextAreaFormat( 8, 40),
					Pattern.class),
				BeanInfoHelper.createPropertyDescriptor(
					"solution", "Solution", 
					"The Solution which this Pattern describes", 
					"java.text.Format", new org.webgets.text.TextAreaFormat( 8, 40),
					Pattern.class),
				BeanInfoHelper.createPropertyDescriptor(
					"author", "Author",
					"The author of this Pattern",
					Pattern.class),
				BeanInfoHelper.createPropertyDescriptor(
					"value", "Value",
					"The value of this Pattern",
// NEED Number Select Format					"java.text.Format", new org.webgets.text.
					Pattern.class)
			};
		}
		catch ( Exception log)
		{
			Log.printErr( "PatternBeanInfo could not initilize!", log);
		}
		finally
		{
			BEAN_DESCRIPTOR = bean;
			BEAN_PROPS = props;
		}
	}
	
	public BeanDescriptor getBeanDescriptor()
	{
		return BEAN_DESCRIPTOR;
	}
	
	public PropertyDescriptor[] getPropertyDescriptors()
	{
		return BEAN_PROPS;
	}

}
