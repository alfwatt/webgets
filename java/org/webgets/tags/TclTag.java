package org.webgets.tags;

import javax.servlet.http.*;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.*;

import tcl.lang.*;

public class TclTag extends BodyTagSupport
{
	private Interp interp = null;
	
	public TclTag()
	{
		super();
		interp = new Interp();
	}
	
	public int doAfterBody() throws JspException 
	{
		try
		{
			String tclScript = this.getBodyContent().getString();
			this.getBodyContent().clearBody();
			
			try
			{
				interp.eval( tclScript);
				this.getPreviousOut().println( interp.getResult());
			}
			catch ( TclException badScript)
			{
				this.getPreviousOut().println( tclScript);
				this.getPreviousOut().println( "<hr>");
				badScript.printStackTrace( new java.io.PrintWriter( this.getPreviousOut()));
			}
		}
		catch ( java.io.IOException fail)
		{
			throw new JspException( (Throwable) fail);
		}
		finally
		{
			return SKIP_BODY;
		}
	}

	public void release()
	{
		interp = null;
	}
}
