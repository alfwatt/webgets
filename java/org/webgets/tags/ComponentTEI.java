package org.webgets.tags;

import javax.servlet.jsp.tagext.*;

public class ComponentTEI extends TagExtraInfo 
{
	public VariableInfo[] getVariableInfo( TagData data) 
	{
		return new VariableInfo[]
		{
			new VariableInfo(
				data.getId(),
				data.getAttributeString( "classname"),
				true,
				VariableInfo.NESTED
			)
		};
	}
}
