package org.webgets.tags;

import org.webgets.util.*;
import org.webgets.html.Document;

import javax.servlet.http.*;
import javax.servlet.jsp.tagext.*;

public class DocumentTag extends TagSupport
{

	public static final String WEBGETS_DOCUMENT = "webgets:document:";

	private String id = "document";
	private String label = "Untitled Document";
	private String styleType = null;
	private String styleHref = null;
	private Document document = null;
	
	public DocumentTag()
	{
		super();
	}
	
	public String getId()
	{
		return this.id;
	}
	
	public void setId( String id)
	{
		Check.isNotNull( id);
		this.id = id;
	}
	
	public Document getDocument()
	{
		return this.document;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public void setLabel( String label)
	{
		Check.isNotNull( label);
		this.label = label;
	}
	
	public String getStyleType()
	{
		return this.styleType;
	}
	
	public void setStyleType( String type)
	{
		Check.isNotNull( type);
		this.styleType = type;
	}

	public String getStyleHref()
	{
		return this.styleHref;
	}
		
	public void setStyleHref( String href)
	{
		Check.isNotNull( href);
		this.styleHref = href;
	}
	
	public int doStartTag()
	{
		int todo = SKIP_BODY;
		
		HttpSession session = this.pageContext.getSession();
		Object sessionObject = session.getValue( WEBGETS_DOCUMENT+this.getId());
		if ( sessionObject == null)
		{
			document = new Document( this.getLabel());
			if ( this.styleHref != null)
				document.setStyleHref( this.styleHref);
			if ( this.styleType != null)
				document.setStyleType( this.styleType);
			
			session.putValue( WEBGETS_DOCUMENT+this.getId(), document);
			todo = EVAL_BODY_INCLUDE;
		}
		else if ( sessionObject instanceof Document)
		{
			document = (Document) sessionObject;
		}
		else 
			 Check.cantHappen( WEBGETS_DOCUMENT + " in your session is not a Document!"); 
		
		this.pageContext.setAttribute( this.getId(), document);
		return todo;
	}
	
	public int doEndTag() throws javax.servlet.jsp.JspException
	{
		try
		{
			document.init( this.pageContext.getServletConfig());
			document.service( this.pageContext.getRequest(), this.pageContext.getResponse());
		}
		catch ( Exception rethrow)
		{
			Log.printWarn( rethrow.toString(), rethrow);
			throw new javax.servlet.jsp.JspException( rethrow.toString(), rethrow);
		}
		document = null; // release the document
		return EVAL_PAGE;
	}
	
	public void release()
	{
		id = null;
		label = null;
		document = null;
	}
}
