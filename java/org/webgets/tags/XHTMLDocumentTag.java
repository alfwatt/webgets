package org.webgets.tags;

import org.webgets.util.*;
import org.webgets.xhtml.XHTMLDocument;

import javax.servlet.http.*;
import javax.servlet.jsp.tagext.*;

public class XHTMLDocumentTag extends TagSupport
{

	public static final String WEBGETS_XHTML_DOCUMENT = "webgets:xhtml:document:";

	private String id = "public";
	private String label = "Untitled XHTML Document";
	private XHTMLDocument document = null;
	
	public XHTMLDocumentTag()
	{
		super();
	}
	
	// XXX is this broken for multiple sessions ?? test it! XXX
	public String getId()
	{
		return this.id;
	}
	
	public void setId( String id)
	{
		Check.isNotNull( id);
		this.id = id;
	}
	
	public XHTMLDocument getDocument()
	{
		return this.document;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public void setLabel( String label)
	{
		Check.isNotNull( label);
		this.label = label;
	}
		
	public int doStartTag()
	{
		int todo = SKIP_BODY;
		
		HttpSession session = this.pageContext.getSession();
		Object sessionObject = session.getValue( WEBGETS_XHTML_DOCUMENT+this.getId());
		if ( sessionObject == null)
		{
			document = new XHTMLDocument( this.getLabel());
			session.putValue( WEBGETS_XHTML_DOCUMENT+this.getId(), document);
			todo = EVAL_BODY_INCLUDE;
		}
		else if ( sessionObject instanceof XHTMLDocument)
		{
			document = (XHTMLDocument) sessionObject;
		}
		else 
			 Check.cantHappen( WEBGETS_XHTML_DOCUMENT + " in your session is not a Document!"); 
		
		this.pageContext.setAttribute( this.getId(), document);
		return todo;
	}
	
	public int doEndTag() throws javax.servlet.jsp.JspException
	{
		try
		{
			document.init( this.pageContext.getServletConfig());
			document.service( this.pageContext.getRequest(), this.pageContext.getResponse());
		}
		catch ( Exception rethrow)
		{
			Log.printWarn( rethrow.toString(), rethrow);
			throw new javax.servlet.jsp.JspException( rethrow.toString(), rethrow);
		}
		document = null; // release the document
		return EVAL_PAGE;
	}
	
	public void release()
	{
		id = null;
		label = null;
		document = null;
	}
}
