package org.webgets.tags;

import org.webgets.util.*;
import org.webgets.Component;

import javax.servlet.http.*;
import javax.servlet.jsp.tagext.*;

public class ComponentTag extends TagSupport
{
	public static final String WEBGETS_COMPONENT = "webgets:component:";

	private String id = "component";
	private String className = null;
	private String label = "Unlabeled";
	private Component component = null;
	private boolean init = false;
	
	public ComponentTag()
	{
		super();
	}
	
	public String getId()
	{
		return this.id;
	}
	
	public void setId( String id)
	{
		Check.isNotNull( id);
		this.id = id;
	}
	
	public boolean isInit()
	{
		return this.init;
	}
	
	public void setInit( boolean init)
	{
		this.init = init;
	}
	
	public String getClassname()
	{
		return this.className;
	}
	
	public void setClassname( String className)
	{
		Check.isNotNull( className);
		this.className = className;
	}
	
	public Component getComponent()
	{
		return this.component;
	}
	
	public String getLabel()
	{
		return this.label;
	}
	
	public void setLabel( String label)
	{
		Check.isNotNull( label);
		this.label = label;
	}
	
	public int doStartTag() throws javax.servlet.jsp.JspException
	{
		int todo = SKIP_BODY;
		try
		{
			HttpSession session = this.pageContext.getSession();
			Object sessionObject = session.getValue( WEBGETS_COMPONENT + this.getId());
			if ( sessionObject == null)
			{
				component = (Component) Class.forName( this.getClassname()).newInstance();
				component.setLabel( this.getLabel());
				session.putValue( WEBGETS_COMPONENT + this.getId(), component);
				this.pageContext.setAttribute( this.getId(), component);
				todo = EVAL_BODY_INCLUDE;
			}
			else if ( sessionObject instanceof Component)
			{
				component = (Component) sessionObject;
				this.pageContext.setAttribute( this.getId(), component);
				this.setInit( true);
			}
			else 
				 Check.cantHappen( WEBGETS_COMPONENT + this.getId() 
				 	+ " in your session is not a Component!"); 
		}
		catch ( Exception rethrow)
		{
			Log.printWarn( rethrow.toString(), rethrow);
			throw new javax.servlet.jsp.JspException( rethrow.toString(), rethrow);
		}
		finally
		{
			return todo;
		}
	}
	
	public int doEndTag() throws javax.servlet.jsp.JspException
	{
		try
		{
			if ( ! this.isInit())
				component.init( this.pageContext.getServletConfig());
			component.service( this.pageContext.getRequest(), this.pageContext.getResponse());
		}
		catch ( Exception rethrow)
		{
			Log.printWarn( rethrow.toString(), rethrow);
			throw new javax.servlet.jsp.JspException( rethrow);
		}
		component = null; // release the document
		return EVAL_PAGE;
	}
	
	public void release()
	{
		id = null;
		label = null;
		component = null;
	}
}
