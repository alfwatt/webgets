package org.webgets.tags;

import javax.servlet.jsp.tagext.*;

public class WebtopDocumentTEI extends TagExtraInfo 
{
	public VariableInfo[] getVariableInfo( TagData data) 
	{
		return new VariableInfo[]
		{
			new VariableInfo(
				data.getId(),
				"org.webgets.webtop.WebtopDocument",
				true,
				VariableInfo.NESTED
			)
		};
	}
}
