package org.webgets.tags;

import javax.servlet.jsp.tagext.*;

public class XHTMLDocumentTEI extends TagExtraInfo 
{
	public VariableInfo[] getVariableInfo( TagData data) 
	{
		return new VariableInfo[]
		{
			new VariableInfo(
				data.getId(),
				"org.webgets.xhtml.XHTMLDocument",
				true,
				VariableInfo.NESTED
			)
		};
	}
}
