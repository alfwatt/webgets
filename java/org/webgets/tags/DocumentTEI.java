package org.webgets.tags;

import javax.servlet.jsp.tagext.*;

public class DocumentTEI extends TagExtraInfo 
{
	public VariableInfo[] getVariableInfo( TagData data) 
	{
		return new VariableInfo[]
		{
			new VariableInfo(
				data.getId(),
				"org.webgets.html.Document",
				true,
				VariableInfo.NESTED
			)
		};
	}
}
