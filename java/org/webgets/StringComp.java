package org.webgets;

/**

StringComp is the Component couterpart of java.lang.String, final, immutable and intern()albe
for performance when you need that sort of thing. It behaves exactly like a String with regards
to hashing, equivalance and String value. 

*/
public final class StringComp implements Component
{

	public static StringComp intern( String string)
	{
		return intern( new StringComp( string.intern()));
	}

	public static StringComp intern( StringComp intern)
	{
		return intern; // TODO
	}

	private final int componentId = ComponentAdapter.getNextComponentId();
	private final String componentIdString = Integer.toString( this.componentId);
	private final String string;

	public StringComp( String string)
	{
		this.string = string;
	}
	
	public StringComp( Object object)
	{
		this.string = object.toString();
	}
	
	public int hashCode()
	{
		return this.string.hashCode();
	}
	
	public boolean equals( Object o)
	{
		return this.string.equals( o);
	}
	
	public String toString()
	{
		return this.string;
	}

	//
	// implements Component
	
	public int getId()
	{
		return this.componentId;
	}
	
	public String getIdString()
	{
		return this.componentIdString;
	}
	
	public String getLabel()
	{
		return this.string;
	}

	public void setLabel( String label)
	{
		return; /* XXX HACK WARNING this call silently fails WARNING HACK XXX */
	}

	public void onRequest( org.webgets.util.RequestEvent request)
	{
		return;
	}
	
	public void onDraw( org.webgets.util.TagWriter out)
	{
		out.print( this.string);
	}
	
	public org.webgets.util.WebgetsConfig getWebgetsConfig()
	{
		return null;
	}
	
	public void setWebgetsConfig( org.webgets.util.WebgetsConfig config)
	{
		return;
	}
	
	//
	// Component extends Servlet
	
	public java.lang.String getServletInfo()
	{
		return this.string;
	}
	
	public void init( javax.servlet.ServletConfig config)
	{
		return; /* this call silently fails */
	}
	
	public javax.servlet.ServletConfig getServletConfig()
	{
		return null; /* this call silently returns null */
	}
	
	public void service( javax.servlet.ServletRequest request, javax.servlet.ServletResponse response)
		throws java.io.IOException, javax.servlet.ServletException
	{
		response.getWriter().print( this.string);
	}
	
	public void destroy()
	{
		return; /* this call does nothing */
	}
}
