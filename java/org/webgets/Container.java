package org.webgets;

import org.webgets.util.*;

/**

The Container interface defines methods for dealing with adding and
removing Components, as well as event delivery methods.

*/
public interface Container extends Component
{
	/**
	
	Add a component to this container. The added component will now recieve draw()
	calls from the Container, and if the Component implements the RequestListener
	interface it should be automatically registered.

	*/
	void addComponent( Component add);

	/**
	
	Add a component to this continer, as above, but in a particular location. 
	Containers may interpret the location integer an any way they wish.

	*/
	void addComponent( Component add, int where);

	/**
	
	Removes this component from the Container. If the Component implements the
	RequestListener interface it should be automatically deregistered.

	*/
	void removeComponent( Component remove);

	/**
	
	@return an array of Components in this Container
	
	*/
	Component[] getComponents();
}
