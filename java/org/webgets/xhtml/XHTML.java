package org.webgets.xhtml;

import org.webgets.Component;
import org.webgets.util.HTTP;

/** 

   Extensible HTML version 1.0 Strict DTD

   This is the same as HTML 4.0 Strict except for
   changes due to the differences between XML and SGML.

   Namespace = http://www.w3.org/1999/xhtml

   For further information, see: http://www.w3.org/TR/xhtml1

   Copyright (c) 1998-2000 W3C (MIT, INRIA, Keio),
   All Rights Reserved. 

   This DTD module is identified by the PUBLIC and SYSTEM identifiers:

   PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"

   Revision: 1.1
   Date: 2000/01/26 14:08:56s

*/
public interface XHTML extends HTTP, Component // All the XHTML components implement this type 
{
	String DOCTYPE = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" "
		+ "\"http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-strict.dtd\">";

	/** core attributes common to most elements
	   id       document-wide unique id
	   class    space separated list of classes
	   style    associated style info
	   title    advisory title/amplification
	   <!ENTITY % coreattrs ... >
	*/
	String ID = "id";       // id          ID             #IMPLIED
	String CLASS = "class"; // class       CDATA          #IMPLIED
	String STYLE = "style"; // style       %StyleSheet;   #IMPLIED
	String TITLE = "title"; // title       %Text;         #IMPLIED"

	interface CoreAttributes
	{
		String getDocId();
		void setDocId( String id);
		
		String getCSSClass();
		void setCSSClass( String cssClass);
		
		String getStyle();
		void setStyle( String style);
		
		String getTitle();
		void setTitle( String title);
	}
	
	/** internationalization attributes
	   lang        language code (backwards compatible)
	   xml:lang    language code (as per XML 1.0 spec)
	   dir         direction for weak/neutral text
	   <!ENTITY % i18n ... >
	*/
	String LANG = "lang";         // lang        %LanguageCode; #IMPLIED
	String XML_LANG = "xml:lang"; // xml:lang    %LanguageCode; #IMPLIED
  	String DIR = "dir";			  // dir         (ltr|rtl)      #IMPLIED
	String LTR = "ltr";
	String RTL = "rtl"; 

	interface I18nAttributes
	{
		String getLang();
		void setLang( String lang);
		
		String getXMLLang();
		void setXMLLang( String xmlLang);
		
		String getDir();
		void setDir( String dir);
	}

	/** attributes for common UI events
		onclick     a pointer button was clicked
		ondblclick  a pointer button was double clicked
		onmousedown a pointer button was pressed down
		onmouseup   a pointer button was released
		onmousemove a pointer was moved onto the element
		onmouseout  a pointer was moved away from the element
		onkeypress  a key was pressed and released
		onkeydown   a key was pressed down
		onkeyup     a key was released
		<!ENTITY % events ... >
	*/
	String ONCLICK = "onclick";	//%Script;       #IMPLIED
	String ONDBLCLICK = "ondblclick";	//%Script;       #IMPLIED
	String ONMOUSEDOWN = "onmousedown";	//%Script;       #IMPLIED
	String ONMOUSEUP = "onmouseup";	//%Script;       #IMPLIED
	String ONMOUSEOVER = "onmouseover";	//%Script;       #IMPLIED
	String ONMOUSEMOVE = "onmousemove";	//%Script;       #IMPLIED
	String ONMOUSEOUT = "onmouseout";	//%Script;       #IMPLIED
	String ONKEYPRESS = "onkeypress";	//%Script;       #IMPLIED
	String ONKEYDOWN = "onkeydown";	//%Script;       #IMPLIED
	String ONKEYUP = "onkeyup";	//%Script;       #IMPLIED"

	interface EventAttributes
	{
		String getOnClick();
		void setOnClick( String script);
		
		String getOnDblClick();
		void setOnDblClick( String script);
		
		String getOnMouseDown();
		void setOnMouseDown( String script);
		
		String getOnMouseUp();
		void setOnMouseUp( String script);
		
		String getOnMouseOver();
		void setOnMouseOver( String script);
		
		String getOnMouseMove();
		void setOnMouseMove( String script);
		
		String getOnMouseOut();
		void setOnMouseOut( String script);
		
		String getOnKeyPress();
		void dsetOnKeyPress( String script);
		
		String getOnKeyDown();
		void setOnKeyDown( String script);
		
		String getOnKeyUp();
		void setOnKeyUp( String script); 
	}

	/** <!ENTITY % attrs "%coreattrs; %i18n; %events;"> */
	interface CommonAttributes extends CoreAttributes, I18nAttributes // should implement EventAttributes as well
	{
		// marker interface
	}

	/** attributes for elements that can get the focus
		accesskey   accessibility key character
		tabindex    position in tabbing order
		onfocus     the element got the focus
		onblur      the element lost the focus
	  
	  <!ENTITY % focus ... >
 	*/
	String ACCESSKEY = "accesskey";	//%Character;    #IMPLIED
	String TABINDEX = "tabindex";	//%Number;       #IMPLIED
	String ONFOCUS = "onfocus";	//%Script;       #IMPLIED
	String ONBLUR = "onblur";	//%Script;       #IMPLIED"

	interface FocusAttributes
	{
		String getAccessKey();
		void setAccessKey( String key);

		// really a number...
		Number getTabIndex();
		void setTabIndex( Number index);
		
		String getOnFocus();
		void setOnFocus( String script);
		
		String getOnBlur();
		void setOnBlur( String script);
	}

	/**  Text Elements */

	/** <!ENTITY % special ... > */
    String BR = "br"; 
    String SPAN = "span"; 
    String BDO = "bdo"; 
    String OBJECT = "object";
    String IMG = "img";
    String MAP = "map";

	/** <!ENTITY % fontstyle ... > */
	String TT = "tt";
	String I = "i";
	String B = "b";
	String BIG = "big";
	String SMALL = "small";

	/** <!ENTITY % phrase ... > */
	String EM = "em";
	String STRONG = "strong";
	String SFN = "dfn";
	String CODE = "code";
	String Q = "q";
	String SUB = "sub";
	String SUP = "sup";
    String SAMP = "samp";
    String KBD = "kbd";
    String VAR = "var";
    String CITE = "cite";
    String ABBR = "abbr";
    String ACRONYM = "acronym";

	/** <!ENTITY % inline.forms ... > */
    String INPUT = "input" ;
    String SELECT = "select" ;
    String TEXTAREA = "textarea" ;
    String LABEL = "label" ;
    String BUTTON = "button";

	/** <!-- these can occur at block or inline level -->
		<!ENTITY % misc ... >
	*/
	String INS = "ins"; 
    String DEL = "del" ;
    String SCRIPT = "script"; 
    String NOSCRIPT = "noscript";

	/** <!ENTITY % inline ... %special; | %fontstyle; | %phrase; | %inline.forms; > */
    String A = "a";

	/** <!-- %Inline; covers inline or "text-level" elements -->
		<!ENTITY % Inline "(#PCDATA | %inline; | %misc;)*"> */

	/** Block level elements */
	
	/** <!ENTITY % heading .. > */
	String H1 = "h1";
	String H2 = "h2";
	String H3 = "h3";
	String H4 = "h4";
	String H5 = "h5";
	String H6 = "h6";
	
	/** <!ENTITY % lists ... > */
	String UL = "ul";
	String OL = "ol";
	String DL = "dl";
	
	/** <!ENTITY % blocktext ... > */
	String PRE = "pre";
	String HR = "hr";
	String BLOCKQUOTE = "blockquote";
	String ADDRESS = "address";
	
	/** <!ENTITY % block ... %heading; | %lists; | %blocktext; > */
	String P = "p";
	String DIV = "div";
	String FIELDSET = "fieldset";
	String TABLE = "table";

	String ALIGN = "align";
	String LEFT = "left";
	String CENTER = "center";
	String RIGHT = "right";
	String JUSTIFY = "justify";
	String CHAR = "char";
	String CHAROFF = "charoff";
	String VALIGN = "valign";
	String TOP = "top";
	String MIDDLE = "middle";
	String BOTTOM = "bottom";
	String BASELINE = "baseline";
	String CAPTION = "caption";
	String THEAD = "thead";
	String TFOOT = "tfoot";
	String TBODY = "tbody";
	String COLGROUP = "colgroup";
	String COL = "col";
	String TR = "tr";
	String TH = "th";
	String TD = "td";
	String SUMMARY = "summary";
	String WIDTH = "width";
	String BORDER = "border";
	String FRAME = "frame";
	String RULES = "rules";
	String CELLSPACING = "cellspacing";
	String CELLPADDING = "cellpadding";
	String ROW = "row";
	String ROWGROUP = "rowgroup";
	String AXIS = "axis";
	String HEADERS = "headers";
	String SCOPE = "scope";
	String ROWSPAN = "rowspan";
	String COLSPAN = "colspan";

	/** <!ENTITY % Block "(%block; | form | %misc;)*"> */
	
	/** <!-- %Flow; mixes Block and Inline and is used for list items etc. -->
		<!ENTITY % Flow "(#PCDATA | %block; | form | %inline; | %misc;)*"> */

	/*
	<!--================== Content models for exclusions =====================-->

	<!-- a elements use %Inline; excluding a -->
	
	<!ENTITY % a.content
	   "(#PCDATA | %special; | %fontstyle; | %phrase; | %inline.forms; | %misc;)*">
	
	<!-- pre uses %Inline excluding img, object, big, small, sup or sup -->
	
	<!ENTITY % pre.content
	   "(#PCDATA | a | br | span | bdo | map | tt | i | b |
		  %phrase; | %inline.forms;)*">
	
	<!-- form uses %Block; excluding form -->
	
	<!ENTITY % form.content "(%block; | %misc;)*">
	
	<!-- button uses %Flow; but excludes a, form and form controls -->
	
	<!ENTITY % button.content
	   "(#PCDATA | p | %heading; | div | %lists; | %blocktext; |
		table | %special; | %fontstyle; | %phrase; | %misc;)*">

	*/

	/** Document Structure 
		the namespace URI designates the document profile

		<!ELEMENT html (head, body)>
		<!ATTLIST html
		  %i18n;
		  xmlns       %URI;          #FIXED 'http://www.w3.org/1999/xhtml'
		  >
	*/
	String HTML = "html";
	String XMLNS = "xmlns";
	String XHTML_NS = "http://www.w3.org/1999/xhtml";

	/** Document Head

	<!ENTITY % head.misc "(script|style|meta|link|object)*">
	
	content model is %head.misc; combined with a single
	 title and an optional base element in any order
	
	<!ELEMENT head (%head.misc;,
		 ((title, %head.misc;, (base, %head.misc;)?) |
		  (base, %head.misc;, (title, %head.misc;))))>
	
	<!ATTLIST head
	  %i18n;
	  profile     %URI;          #IMPLIED
	  >

	The title element is not considered part of the flow of text.
       It should be displayed, for example as the page header or
       window title. Exactly one title is required per document.

	<!ELEMENT title (#PCDATA)>
	<!ATTLIST title %i18n;>

	document base URI
	
	<!ELEMENT base EMPTY>
	<!ATTLIST base href %URI; #IMPLIED >
	*/
	String HEAD = "head";
	String PROFILE = "profile";
//	String TITLE = "title";
//	String SCRIPT = "script";
//	String STYLE = "style";
	String META = "meta";
	String LINK = "link";
//	String OBJECT = "object";
	String BASE = "base";

	/** generic metainformation
		<!ELEMENT meta EMPTY>
		<!ATTLIST meta
		  %i18n;
		  http-equiv  CDATA          #IMPLIED
		  name        CDATA          #IMPLIED
		  content     CDATA          #REQUIRED
		  scheme      CDATA          #IMPLIED
		  >
	*/
//	String META = "meta";
	String HTTP = "http";
	String NAME = "name";
	String CONTENT = "content";
	String SCHEME = "scheme";


	/**
	  Relationship values can be used in principle:
	
	   a) for document specific toolbars/menus when used
		  with the link element in document head e.g.
			start, contents, previous, next, index, end, help
	   b) to link to a separate style sheet (rel="stylesheet")
	   c) to make a link to a script (rel="script")
	   d) by stylesheets to control how collections of
		  html nodes are rendered into printed documents
	   e) to make a link to a printable version of this document
		  e.g. a PostScript or PDF version (rel="alternate" media="print")

	  <!ELEMENT link EMPTY>
	  <!ATTLIST link
		%attrs;
		charset     %Charset;      #IMPLIED
		href        %URI;          #IMPLIED
		hreflang    %LanguageCode; #IMPLIED
		type        %ContentType;  #IMPLIED
		rel         %LinkTypes;    #IMPLIED
		rev         %LinkTypes;    #IMPLIED
		media       %MediaDesc;    #IMPLIED
		>

	*/
//	String LINK = "link";
	String CHARSET = "charset";
	String HREF = "href";
	String HREFLANG = "hreflang";
	String TYPE = "type";
	String REL = "rel";
	String REV = "rev";
	String MEDIA = "media";

	/**	style info, which may include CDATA sections -->

	<!ELEMENT style (#PCDATA)>
		<!ATTLIST style
		  %i18n;
		  type        %ContentType;  #REQUIRED
		  media       %MediaDesc;    #IMPLIED
		  title       %Text;         #IMPLIED
		  xml:space   (preserve)     #FIXED 'preserve'
		  >
	*/
//	String STYLE = "style";
//	String TYPE = "type";
//	String MEDIA = "media";
//	String TITLE = "title";
	String XML_SPACE = "xml:space";
	String PRESERVE = "preserve";

	/** script statements, which may include CDATA sections
	<!ELEMENT script (#PCDATA)>
	<!ATTLIST script
	  charset     %Charset;      #IMPLIED
	  type        %ContentType;  #REQUIRED
	  src         %URI;          #IMPLIED
	  defer       (defer)        #IMPLIED
	  xml:space   (preserve)     #FIXED 'preserve'
	  >
		*/
//	String SCRIPT = "script";
//	String CHARSET = "charset";
//	String TYPE = "type";
	String SRC = "src";
	String DEFER = "defer";

	
	/** alternate content container for non script-based rendering 

	<!ELEMENT noscript %Block;>
	<!ATTLIST noscript
	  %attrs;
	  >
	*/	
//	String NOSCRIPT = "noscript";



/** Document Body
<!ELEMENT body %Block;>
<!ATTLIST body
  %attrs;
  onload          %Script;   #IMPLIED
  onunload        %Script;   #IMPLIED
  >
 */
String BODY = "body";
String ONLOAD = "onload";
String ONUNLOAD = "onunload";

/** content is %Inline; excluding "img|object|big|small|sub|sup" 

<!ELEMENT pre %pre.content;>
<!ATTLIST pre
  %attrs;
  xml:space (preserve) #FIXED 'preserve'
  >
*/
//String PRE = "pre";
String XML_SPAE = "xml:space";
//String PRESERVE = "preserve";


}

/*

<!--=================== Block-like Quotes ================================-->

<!ELEMENT blockquote %Block;>
<!ATTLIST blockquote
  %attrs;
  cite        %URI;          #IMPLIED
  >

<!--=================== Inserted/Deleted Text ============================-->

<!--
  ins/del are allowed in block and inline content, but its
  inappropriate to include block content within an ins element
  occurring in inline content.
-->
<!ELEMENT ins %Flow;>
<!ATTLIST ins
  %attrs;
  cite        %URI;          #IMPLIED
  datetime    %Datetime;     #IMPLIED
  >

<!ELEMENT del %Flow;>
<!ATTLIST del
  %attrs;
  cite        %URI;          #IMPLIED
  datetime    %Datetime;     #IMPLIED
  >

<!--================== The Anchor Element ================================-->

<!-- content is %Inline; except that anchors shouldn't be nested -->

<!ELEMENT a %a.content;>
<!ATTLIST a
  %attrs;
  charset     %Charset;      #IMPLIED
  type        %ContentType;  #IMPLIED
  name        NMTOKEN        #IMPLIED
  href        %URI;          #IMPLIED
  hreflang    %LanguageCode; #IMPLIED
  rel         %LinkTypes;    #IMPLIED
  rev         %LinkTypes;    #IMPLIED
  accesskey   %Character;    #IMPLIED
  shape       %Shape;        "rect"
  coords      %Coords;       #IMPLIED
  tabindex    %Number;       #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  >

<!ELEMENT bdo %Inline;>  <!-- I18N BiDi over-ride -->
<!ATTLIST bdo
  %coreattrs;
  %events;
  lang        %LanguageCode; #IMPLIED
  xml:lang    %LanguageCode; #IMPLIED
  dir         (ltr|rtl)      #REQUIRED
  >

<!ATTLIST q
  %attrs;
  cite        %URI;          #IMPLIED
  >

<!--==================== Object ======================================-->
<!--
  object is used to embed objects as part of HTML pages.
  param elements should precede other content. Parameters
  can also be expressed as attribute/value pairs on the
  object element itself when brevity is desired.
-->

<!ELEMENT object (#PCDATA | param | %block; | form | %inline; | %misc;)*>
<!ATTLIST object
  %attrs;
  declare     (declare)      #IMPLIED
  classid     %URI;          #IMPLIED
  codebase    %URI;          #IMPLIED
  data        %URI;          #IMPLIED
  type        %ContentType;  #IMPLIED
  codetype    %ContentType;  #IMPLIED
  archive     %UriList;      #IMPLIED
  standby     %Text;         #IMPLIED
  height      %Length;       #IMPLIED
  width       %Length;       #IMPLIED
  usemap      %URI;          #IMPLIED
  name        NMTOKEN        #IMPLIED
  tabindex    %Number;       #IMPLIED
  >

<!--
  param is used to supply a named property value.
  In XML it would seem natural to follow RDF and support an
  abbreviated syntax where the param elements are replaced
  by attribute value pairs on the object start tag.
-->
<!ELEMENT param EMPTY>
<!ATTLIST param
  id          ID             #IMPLIED
  name        CDATA          #IMPLIED
  value       CDATA          #IMPLIED
  valuetype   (data|ref|object) "data"
  type        %ContentType;  #IMPLIED
  >

<!--=================== Images ===========================================-->

<!--
   To avoid accessibility problems for people who aren't
   able to see the image, you should provide a text
   description using the alt and longdesc attributes.
   In addition, avoid the use of server-side image maps.
   Note that in this DTD there is no name attribute. That
   is only available in the transitional and frameset DTD.
-->

<!ELEMENT img EMPTY>
<!ATTLIST img
  %attrs;
  src         %URI;          #REQUIRED
  alt         %Text;         #REQUIRED
  longdesc    %URI;          #IMPLIED
  height      %Length;       #IMPLIED
  width       %Length;       #IMPLIED
  usemap      %URI;          #IMPLIED
  ismap       (ismap)        #IMPLIED
  >

<!-- usemap points to a map element which may be in this document
  or an external document, although the latter is not widely supported -->

<!--================== Client-side image maps ============================-->

<!-- These can be placed in the same document or grouped in a
     separate document although this isn't yet widely supported -->

<!ELEMENT map ((%block; | form | %misc;)+ | area+)>
<!ATTLIST map
  %i18n;
  %events;
  id          ID             #REQUIRED
  class       CDATA          #IMPLIED
  style       %StyleSheet;   #IMPLIED
  title       %Text;         #IMPLIED
  name        NMTOKEN        #IMPLIED
  >

<!ELEMENT area EMPTY>
<!ATTLIST area
  %attrs;
  shape       %Shape;        "rect"
  coords      %Coords;       #IMPLIED
  href        %URI;          #IMPLIED
  nohref      (nohref)       #IMPLIED
  alt         %Text;         #REQUIRED
  tabindex    %Number;       #IMPLIED
  accesskey   %Character;    #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  >

<!--================ Forms ===============================================-->
<!ELEMENT form %form.content;>   <!-- forms shouldn't be nested -->

<!ATTLIST form
  %attrs;
  action      %URI;          #REQUIRED
  method      (get|post)     "get"
  enctype     %ContentType;  "application/x-www-form-urlencoded"
  onsubmit    %Script;       #IMPLIED
  onreset     %Script;       #IMPLIED
  accept      %ContentTypes; #IMPLIED
  accept-charset %Charsets;  #IMPLIED
  >

<!--
  Each label must not contain more than ONE field
  Label elements shouldn't be nested.
-->
<!ELEMENT label %Inline;>
<!ATTLIST label
  %attrs;
  for         IDREF          #IMPLIED
  accesskey   %Character;    #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  >

<!ENTITY % InputType
  "(text | password | checkbox |
    radio | submit | reset |
    file | hidden | image | button)"
   >

<!-- the name attribute is required for all but submit & reset -->

<!ELEMENT input EMPTY>     <!-- form control -->
<!ATTLIST input
  %attrs;
  type        %InputType;    "text"
  name        CDATA          #IMPLIED
  value       CDATA          #IMPLIED
  checked     (checked)      #IMPLIED
  disabled    (disabled)     #IMPLIED
  readonly    (readonly)     #IMPLIED
  size        CDATA          #IMPLIED
  maxlength   %Number;       #IMPLIED
  src         %URI;          #IMPLIED
  alt         CDATA          #IMPLIED
  usemap      %URI;          #IMPLIED
  tabindex    %Number;       #IMPLIED
  accesskey   %Character;    #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  onselect    %Script;       #IMPLIED
  onchange    %Script;       #IMPLIED
  accept      %ContentTypes; #IMPLIED
  >

<!ELEMENT select (optgroup|option)+>  <!-- option selector -->
<!ATTLIST select
  %attrs;
  name        CDATA          #IMPLIED
  size        %Number;       #IMPLIED
  multiple    (multiple)     #IMPLIED
  disabled    (disabled)     #IMPLIED
  tabindex    %Number;       #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  onchange    %Script;       #IMPLIED
  >

<!ELEMENT optgroup (option)+>   <!-- option group -->
<!ATTLIST optgroup
  %attrs;
  disabled    (disabled)     #IMPLIED
  label       %Text;         #REQUIRED
  >

<!ELEMENT option (#PCDATA)>     <!-- selectable choice -->
<!ATTLIST option
  %attrs;
  selected    (selected)     #IMPLIED
  disabled    (disabled)     #IMPLIED
  label       %Text;         #IMPLIED
  value       CDATA          #IMPLIED
  >

<!ELEMENT textarea (#PCDATA)>     <!-- multi-line text field -->
<!ATTLIST textarea
  %attrs;
  name        CDATA          #IMPLIED
  rows        %Number;       #REQUIRED
  cols        %Number;       #REQUIRED
  disabled    (disabled)     #IMPLIED
  readonly    (readonly)     #IMPLIED
  tabindex    %Number;       #IMPLIED
  accesskey   %Character;    #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  onselect    %Script;       #IMPLIED
  onchange    %Script;       #IMPLIED
  >

<!--
  The fieldset element is used to group form fields.
  Only one legend element should occur in the content
  and if present should only be preceded by whitespace.
-->
<!ELEMENT fieldset (#PCDATA | legend | %block; | form | %inline; | %misc;)*>
<!ATTLIST fieldset
  %attrs;
  >

<!ELEMENT legend %Inline;>     <!-- fieldset label -->
<!ATTLIST legend
  %attrs;
  accesskey   %Character;    #IMPLIED
  >

<!--
 Content is %Flow; excluding a, form and form controls
--> 
<!ELEMENT button %button.content;>  <!-- push button -->
<!ATTLIST button
  %attrs;
  name        CDATA          #IMPLIED
  value       CDATA          #IMPLIED
  type        (button|submit|reset) "submit"
  disabled    (disabled)     #IMPLIED
  tabindex    %Number;       #IMPLIED
  accesskey   %Character;    #IMPLIED
  onfocus     %Script;       #IMPLIED
  onblur      %Script;       #IMPLIED
  >

<!--======================= Tables =======================================-->

<!-- Derived from IETF HTML table standard, see [RFC1942] -->

<!--
 The border attribute sets the thickness of the frame around the
 table. The default units are screen pixels.

 The frame attribute specifies which parts of the frame around
 the table should be rendered. The values are not the same as
 CALS to avoid a name clash with the valign attribute.
-->
<!ENTITY % TFrame "(void|above|below|hsides|lhs|rhs|vsides|box|border)">

<!--
 The rules attribute defines which rules to draw between cells:

 If rules is absent then assume:
     "none" if border is absent or border="0" otherwise "all"
-->

<!ENTITY % TRules "(none | groups | rows | cols | all)">
  
<!-- horizontal placement of table relative to document -->
<!ENTITY % TAlign "(left|center|right)">

<!-- horizontal alignment attributes for cell contents

  char        alignment char, e.g. char=':'
  charoff     offset for alignment char
-->
<!ENTITY % cellhalign
  "align      (left|center|right|justify|char) #IMPLIED
   char       %Character;    #IMPLIED
   charoff    %Length;       #IMPLIED"
  >

<!-- vertical alignment attributes for cell contents -->
<!ENTITY % cellvalign
  "valign     (top|middle|bottom|baseline) #IMPLIED"
  >

<!ELEMENT table
     (caption?, (col*|colgroup*), thead?, tfoot?, (tbody+|tr+))>
<!ELEMENT caption  %Inline;>
<!ELEMENT thead    (tr)+>
<!ELEMENT tfoot    (tr)+>
<!ELEMENT tbody    (tr)+>
<!ELEMENT colgroup (col)*>
<!ELEMENT col      EMPTY>
<!ELEMENT tr       (th|td)+>
<!ELEMENT th       %Flow;>
<!ELEMENT td       %Flow;>

<!ATTLIST table
  %attrs;
  summary     %Text;         #IMPLIED
  width       %Length;       #IMPLIED
  border      %Pixels;       #IMPLIED
  frame       %TFrame;       #IMPLIED
  rules       %TRules;       #IMPLIED
  cellspacing %Length;       #IMPLIED
  cellpadding %Length;       #IMPLIED
  >

<!ENTITY % CAlign "(top|bottom|left|right)">

<!ATTLIST caption
  %attrs;
  >

<!--
colgroup groups a set of col elements. It allows you to group
several semantically related columns together.
-->
<!ATTLIST colgroup
  %attrs;
  span        %Number;       "1"
  width       %MultiLength;  #IMPLIED
  %cellhalign;
  %cellvalign;
  >

<!--
 col elements define the alignment properties for cells in
 one or more columns.

 The width attribute specifies the width of the columns, e.g.

     width=64        width in screen pixels
     width=0.5*      relative width of 0.5

 The span attribute causes the attributes of one
 col element to apply to more than one column.
-->
<!ATTLIST col
  %attrs;
  span        %Number;       "1"
  width       %MultiLength;  #IMPLIED
  %cellhalign;
  %cellvalign;
  >

<!--
    Use thead to duplicate headers when breaking table
    across page boundaries, or for static headers when
    tbody sections are rendered in scrolling panel.

    Use tfoot to duplicate footers when breaking table
    across page boundaries, or for static footers when
    tbody sections are rendered in scrolling panel.

    Use multiple tbody sections when rules are needed
    between groups of table rows.
-->
<!ATTLIST thead
  %attrs;
  %cellhalign;
  %cellvalign;
  >

<!ATTLIST tfoot
  %attrs;
  %cellhalign;
  %cellvalign;
  >

<!ATTLIST tbody
  %attrs;
  %cellhalign;
  %cellvalign;
  >

<!ATTLIST tr
  %attrs;
  %cellhalign;
  %cellvalign;
  >


<!-- Scope is simpler than headers attribute for common tables -->
<!ENTITY % Scope "(row|col|rowgroup|colgroup)">

<!-- th is for headers, td for data and for cells acting as both -->

<!ATTLIST th
  %attrs;
  abbr        %Text;         #IMPLIED
  axis        CDATA          #IMPLIED
  headers     IDREFS         #IMPLIED
  scope       %Scope;        #IMPLIED
  rowspan     %Number;       "1"
  colspan     %Number;       "1"
  %cellhalign;
  %cellvalign;
  >

<!ATTLIST td
  %attrs;
  abbr        %Text;         #IMPLIED
  axis        CDATA          #IMPLIED
  headers     IDREFS         #IMPLIED
  scope       %Scope;        #IMPLIED
  rowspan     %Number;       "1"
  colspan     %Number;       "1"
  %cellhalign;
  %cellvalign;
  >


*/
