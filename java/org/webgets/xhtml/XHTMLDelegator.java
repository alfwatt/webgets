package org.webgets.xhtml;

import org.webgets.*;
import org.webgets.util.*;

/**

The XHTMLDelegator extends DelegatorAdapter and implements the XHTML interface.

XHTMLDelegators have no native attributes, they are intened for event processing and
rely on their delegates for display properties.

*/
public class XHTMLDelegator extends DelegatorAdapter implements XHTML
{
	public XHTMLDelegator()
	{
		super();
	}
	
	public XHTMLDelegator( XHTML delegate)
	{
		super( delegate);
	}
	
	public void setDelegate( Component delegate)
	{	
		Check.isLegalArg( delegate instanceof XHTML);
		super.setDelegate( delegate);
	}
}
