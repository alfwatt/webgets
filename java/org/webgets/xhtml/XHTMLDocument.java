package org.webgets.xhtml;

import org.webgets.*;
import org.webgets.util.*;

public class XHTMLDocument extends XHTMLContainer
{
	private static String[] HTML_ATTRS = new String[] { XMLNS, XHTML_NS};

	public XHTMLDocument()
	{
		super();
	}
	
	public XHTMLDocument( String title)
	{
		super( title);
	}

	public void onDraw( TagWriter out)
	{
		out.enableXML();
		out.println( DOCTYPE);
		out.tag( HTML, HTML_ATTRS); // and the namespace attrs
			out.tag( HEAD);
				out.tag( TITLE);
					out.print( this.getLabel());
				out.closeTag();
			out.closeTag();
			out.tag( BODY, this.attrs);
				super.onDraw( out);
			out.closeTag();
		out.closeTag();	
	}
}
