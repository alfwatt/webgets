package org.webgets.xhtml;

import org.webgets.*;
import org.webgets.util.*;

public class XHTMLContainer extends ContainerAdapter implements XHTML, XHTML.CommonAttributes
{
	protected Attributes attrs = new Attributes();
	
	public XHTMLContainer()
	{
		super();
		this.setDocId( super.getIdString());
	}
	
	public XHTMLContainer( String title)
	{
		super( title);
		this.setDocId( super.getIdString());
		this.setTitle( title);
	}

	public void add( XHTML add)
	{
		super.addComponent( add);
	}
	
	// implements XHTML.Attributes
	
	// XML.Attributes extends XHTML.CoreAttributes
	public String getDocId()
	{
		return this.attrs.getAsString( ID);
	}
	
	public void setDocId( String id)
	{
	this.attrs.put( ID, id);
	}

	public String getCSSClass()
	{
		return this.attrs.getAsString( CLASS);
	}
	
	public void setCSSClass( String cssClass)
	{
		this.attrs.put( CLASS, cssClass);
	}

	public String getStyle()
	{
		return this.attrs.getAsString( STYLE);
	}
	
	public void setStyle( String style)
	{
		this.attrs.put( STYLE, style);
	}

	public String getTitle()
	{
		return this.attrs.getAsString( TITLE);
	}
	
	public void setTitle( String title)
	{
		this.attrs.put( TITLE, title);
	}

	// XML.Attributes extends XHTML.I18nAttributes
	public String getLang()
	{
		return this.attrs.getAsString( LANG);
	}
	
	public void setLang( String lang)
	{
		this.attrs.put( LANG, lang);
	}

	public String getXMLLang()
	{
		return this.attrs.getAsString( XML_LANG);
	}
	
	public void setXMLLang( String xmlLang)
	{
		this.attrs.put( XML_LANG, xmlLang);
	}
	
	public String getDir()
	{
		return this.attrs.getAsString( DIR);
	}
	
	public void setDir( String dir)
	{
		this.attrs.put( DIR, dir);
	}
}
