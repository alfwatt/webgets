package org.webgets.xhtml;

import org.webgets.*;
import org.webgets.util.*;

/** XHTML.TABLE 

<!-- Derived from IETF HTML table standard, see [RFC1942] -->

<!--
 The border attribute sets the thickness of the frame around the
 table. The default units are screen pixels.

 The frame attribute specifies which parts of the frame around
 the table should be rendered. The values are not the same as
 CALS to avoid a name clash with the valign attribute.
-->
<!ENTITY % TFrame "(void|above|below|hsides|lhs|rhs|vsides|box|border)">

<!--
 The rules attribute defines which rules to draw between cells:

 If rules is absent then assume:
     "none" if border is absent or border="0" otherwise "all"
-->

<!ENTITY % TRules "(none | groups | rows | cols | all)">
  
<!-- horizontal placement of table relative to document -->
<!ENTITY % TAlign "(left|center|right)">

<!-- horizontal alignment attributes for cell contents

  char        alignment char, e.g. char=':'
  charoff     offset for alignment char
-->
<!ENTITY % cellhalign
  "align      (left|center|right|justify|char) #IMPLIED
   char       %Character;    #IMPLIED
   charoff    %Length;       #IMPLIED"
  >


<!ELEMENT table (caption?, (col*|colgroup*), thead?, tfoot?, (tbody+|tr+))>
<!ATTLIST table
  %attrs;
  summary     %Text;         #IMPLIED
  width       %Length;       #IMPLIED
  border      %Pixels;       #IMPLIED
  frame       %TFrame;       #IMPLIED
  rules       %TRules;       #IMPLIED
  cellspacing %Length;       #IMPLIED
  cellpadding %Length;       #IMPLIED
  >

*/
public class Table extends XHTMLContainer
{

	public Table()
	{
		super();
	}
	
	public Table( String title)
	{
		super( title);
	}
	
	public void onDraw( TagWriter out)
	{
		out.tag( TABLE, this.attrs);
		super.onDraw( out);
		out.closeTag();
	}
	
	/** XHTML.CAPTION 

	<!ELEMENT caption  %Inline;>
	<!ATTLIST caption
	  %attrs;
	  >

	*/
	public static class Caption extends XHTMLContainer
	{
		public Caption()
		{
			super();
		}
		
		public Caption( String title)
		{
			super( title);
		}
		
		public void onDraw( TagWriter out)
		{
			out.tag( CAPTION);
			super.onDraw( out);
			out.closeTag();
		}
	}
	
	/**
	The protected abstract class TableElement is used to implement
	drawing behaviouf for all the Table elements. Making the source
	for this class smaller.   
	*/
	protected abstract static class TableElement extends XHTMLContainer
	{
		private String tag = null;
		
		public TableElement( String tag)
		{
			super();
			this.setTag( tag);
		}
		
		public TableElement( String tag, String title)
		{
			super( title);
			this.setTag( tag);
		}
		
		protected String getTag()
		{
			return this.tag;
		}
		
		protected void setTag( String tag)
		{
			Check.isNotNull( tag);
			this.tag = tag;
		}
		
		/**
		<!-- horizontal alignment attributes for cell contents

		-->
		<!ENTITY % cellhalign
		  "align      (left|center|right|justify|char) #IMPLIED
		  >

		*/
		public String getAlign()
		{
			return this.attrs.getAsString( ALIGN);
		}
		
		public void setAlign( String align)
		{
			this.attrs.put( ALIGN, align);
		}

		/** 
			char        alignment char, e.g. char=':' 
			char       %Character;    #IMPLIED
		*/
		public String getChar()
		{
			return this.attrs.getAsString( CHAR);
		}
		
		public void setChar( String alignChar)
		{
			this.attrs.set( CHAR, alignChar);
		}
		
		/**
			charoff     offset for alignment char
			charoff    %Length;       #IMPLIED"
		*/
		public Number getCharOff()
		{
			return this.attrs.getAsNumber( CHAROFF);
		}
		
		/**
		<!-- vertical alignment attributes for cell contents -->

		<!ENTITY % cellvalign
		  "valign     (top|middle|bottom|baseline) #IMPLIED"
		  >
	
		*/
		public String getVAlign()
		{
			return this.attrs.getAsString( VALIGN);
		}
		
		public void setVAlign( String valign)
		{
			this.attrs.put( VALIGN, valign);
		}
		
		public void onDraw( TagWriter out)
		{
			out.tag( this.tag, this.attrs);
			super.onDraw( out);
			out.closeTag();
		}
	}
	
	/** XHTML.TR 

	<!ELEMENT tr       (th|td)+>
	<!ATTLIST tr
	  %attrs;
	  %cellhalign;
	  %cellvalign;
	  >

	*/
	public static class Row extends TableElement
	{
		public Row()
		{
			super( TR);
		}
		
		public Row( String title)
		{
			super( TR, title);
		}
	}

	/** XHTMl.TD 

	<!-- th is for headers, td for data and for cells acting as both -->

	<!ELEMENT td       %Flow;>
	<!ATTLIST td
	  %attrs;
	  abbr        %Text;         #IMPLIED
	  axis        CDATA          #IMPLIED
	  headers     IDREFS         #IMPLIED
	  scope       %Scope;        #IMPLIED
	  rowspan     %Number;       "1"
	  colspan     %Number;       "1"
	  %cellhalign;
	  %cellvalign;
	  >
	
	*/
	public static class Data extends TableElement
	{
		public Data()
		{
			super( TD);
		}
			
		public Data( String title)
		{
			super( TD, title);
		}
		
		protected Data( String tag, String title)
		{
			super( tag, title);
		}
		
		public String getAbbr()
		{
			return this.attrs.getAsString( ABBR);
		}
		
		public void setAbbr( String abbr)
		{
			this.attrs.put( ABBR, abbr);
		}
		
		public String getAxis()
		{
			return this.attrs.getAsString( AXIS);
		}
		
		public void setAxis( String axis)
		{
			this.attrs.put( AXIS, axis);
		}
		
		public String setHeaders()
		{
			return this.attrs.getAsString( HEADERS);
		}
		
		public void setHeaders( String headers)
		{
			this.attrs.put( HEADERS, headers);
		}

		/**		
		<!-- Scope is simpler than headers attribute for common tables -->
		<!ENTITY % Scope "(row|col|rowgroup|colgroup)">
		*/
		public String getScope()
		{
			return this.attrs.getAsString( SCOPE);
		}
		
		public void setScope( String scope)
		{
			this.attrs.put( SCOPE, scope);
		}
		
		public Number getRowspan()
		{
			return this.attrs.getAsNumber( ROWSPAN);
		}
		
		public void setRowspan( Number span)
		{
			this.attrs.put( ROWSPAN, span);
		}
		
		public Number getColspan()
		{
			return this.attrs.getAsNumber( COLSPAN);
		}
		
		public void setColspan( Number span)
		{
			this.attrs.put( COLSPAN, span);
		}
	}

	/** XHTML.TH

	<!ELEMENT th       %Flow;>
	<!ATTLIST th
		%attrs;
		abbr        %Text;         #IMPLIED
		axis        CDATA          #IMPLIED
		headers     IDREFS         #IMPLIED
		scope       %Scope;        #IMPLIED
		rowspan     %Number;       "1"
		colspan     %Number;       "1"
		%cellhalign;
		%cellvalign;
		>
	 */
	public static class Head extends Data
	{
		public Head()
		{
			super();
			this.setTag( TH);
		}
		
		public Head( String title)
		{
			super( TH, title);
		}
	}
	
	/** XHTML.THEAD

	<!--
    Use thead to duplicate headers when breaking table
    across page boundaries, or for static headers when
    tbody sections are rendered in scrolling panel.

    Use tfoot to duplicate footers when breaking table
    across page boundaries, or for static footers when
    tbody sections are rendered in scrolling panel.

    Use multiple tbody sections when rules are needed
    between groups of table rows.
	-->
	<!ELEMENT thead    (tr)+>
	<!ATTLIST thead
	  %attrs;
	  %cellhalign;
	  %cellvalign;
	  >

	*/
	public static class TableHead extends TableElement
	{
		public TableHead()
		{
			super( THEAD);
		}
		
		public TableHead( String title)
		{
			super( THEAD, title);
		}
	}
	
	/** XHTML.TFOOT 

	<!ELEMENT tfoot    (tr)+>
	<!ATTLIST tfoot
	  %attrs;
	  %cellhalign;
	  %cellvalign;
	  >

	*/
	public static class TableFoot extends TableElement
	{
		public TableFoot()
		{
			super( TFOOT);
		}
		
		public TableFoot( String title)
		{
			super( TFOOT, title);
		}
	}
	
	/** XHTML.TBODY
	
	<!ELEMENT tbody    (tr)+>
	<!ATTLIST tbody
	  %attrs;
	  %cellhalign;
	  %cellvalign;
	  >

	 */
	public static class TableBody extends TableElement
	{
		public TableBody()
		{
			super( TBODY);
		}
		
		public TableBody( String title)
		{
			super( TBODY, title);
		}
	}
	
	/** XHTML.COL 
	
	 col elements define the alignment properties for cells in
	 one or more columns.
	
	 The width attribute specifies the width of the columns, e.g.
	
		 width=64        width in screen pixels
		 width=0.5*      relative width of 0.5
	
	 The span attribute causes the attributes of one
	 col element to apply to more than one column.

	<!ELEMENT col      EMPTY>
	<!ATTLIST col
	  %attrs;
	  span        %Number;       "1"
	  width       %MultiLength;  #IMPLIED
	  %cellhalign;
	  %cellvalign;
	  >
	
	*/
	public static class Column extends TableElement
	{
		public Column()
		{
			super( COL);
		}
		
		public Column( String title)
		{
			super( COL, title);
		}

		protected Column( String tag, String title)
		{
			super( tag, title);
		}
		
		public Number getSpan()
		{
			return this.attrs.getAsNumber( SPAN);
		}
		
		public void setSpan( Number span)
		{
			this.attrs.set( SPAN, span);
		}
		
		public String getWidth()
		{
			return this.attrs.getAsString( WIDTH);
		}
		
		public void setWidth( String width)
		{
			this.attrs.set( WIDTH, width);
		}
	}

	/** XHTML.COLGROUP

	<!--
	colgroup groups a set of col elements. It allows you to group
	several semantically related columns together.
	-->
	<!ELEMENT colgroup (col)*>
	<!ATTLIST colgroup
	  %attrs;
	  span        %Number;       "1"
	  width       %MultiLength;  #IMPLIED
	  %cellhalign;
	  %cellvalign;
	  >

	*/	
	public static class Colgroup extends Column
	{
		public Colgroup()
		{
			super();
			this.setTag( COLGROUP);
		}
		
		public Colgroup( String title)
		{
			super( COLGROUP, title);
		}
	}
}
