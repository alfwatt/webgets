package org.webgets.xhtml;

import org.webgets.*;
import org.webgets.util.*;

public final class Cdata extends ComponentAdapter implements XHTML
{
	String data = "";
	
	public Cdata()
	{
		super();
	}
	
	public Cdata( String data)
	{
		super( TagWriter.trimString( data));
		this.setData( data);
	}
	
	public String getData()
	{
		return this.data;
	}
	
	public void setData( String data)
	{
		this.data = data;
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.print( this.data);
	}
}
