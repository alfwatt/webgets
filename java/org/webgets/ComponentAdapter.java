package org.webgets;

import javax.servlet.*;
import javax.servlet.http.*;

import org.webgets.util.*;

/**

ComponentAdapter implements the org.webgets.Component interface. 

*/
public abstract class ComponentAdapter implements Component
{
	protected static int NEXT_COMPONENT_ID = (int) System.currentTimeMillis();

	public static synchronized int getNextComponentId()
	{
		return NEXT_COMPONENT_ID++;
	}

    private final int componentID = NEXT_COMPONENT_ID++;
    private final String componentIDString = Integer.toString( this.componentID);
    private String label = null;
    private transient WebgetsConfig config = null;
    
    /** The default, no-arg constructor should be provided by all
    Components as a matter of style and testability. All Components
    should be minimally usefull when created in the default manner. */
    protected ComponentAdapter()
    {
        super();
    }
    
    /** The single argument constructor creates a Component with the
    label specified */
    protected ComponentAdapter( String label)
    {
        this();
        this.setLabel( label);
    }
    
    /* implement from org.webgets.Component */
    
    public int getId() 
    {
    	return this.componentID; 
    }

	public String getIdString() 
	{
		return this.componentIDString;
	}

	public boolean hasLabel()
	{
		return this.label != null;
	}

	public String getLabel()
	{
        return this.label;
	}

	public void setLabel( String label)
	{
	    this.label = label;
	}

	public WebgetsConfig getWebgetsConfig()
	{
		return this.config;
	}

	public void setWebgetsConfig( WebgetsConfig config)
	{
		Check.isNotNull( config);
		this.config = config;
	}

	/* Component Adaper defines these methods to simplify component development */
	
	public void onDraw( TagWriter out)
	{
		this.traceDraw();
	}
	
	protected final void traceDraw()
	{
		if ( Debug.TRACE_DRAWS)
			Log.printMsg( "[onDraw] " + this.getClass().getName() + " " + this.getLabel());
	}
	
	public void onRequest( RequestEvent request) //throws javax.servlet.ServletException, java.io.IOException
	{
		this.traceRequest();
	}

	protected final void traceRequest()
	{
		if ( Debug.TRACE_REQUESTS)
			Log.printMsg( "[onRequest] " + this.getClass().getName() + " " + this.getLabel());
	}
	
	/* implemented from javax.servlet.Servlet */

	public void init( ServletConfig config) throws javax.servlet.ServletException
	{
		if ( Debug.TRACE_INIT) Log.printMsg( "[init] " + this + " with config: " + config);

		if ( config instanceof WebgetsConfig)
			this.config = (WebgetsConfig) config;
		else
			this.config = new WebgetsConfig( config);
	}
	
	public String getServletInfo()
	{
		return this.getClass().getName() + "#" + this.getIdString() + ": " + this.getLabel();
	}
	
	public ServletConfig getServletConfig()
	{
		return this.config;
	}
	
	public void service( ServletRequest request, ServletResponse response)
		throws ServletException, java.io.IOException
	{
		this.service( request, response, true);
	}
	
	public void service( ServletRequest request, ServletResponse response, boolean rewriteURLs)
		throws ServletException, java.io.IOException
	{
		HttpServletRequest httpRequest   = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
	
		/* temps for Debug.TIMERS, hopefully a good compiler will notice that these are only
		   used inside of Debug.TIMGER blocks and optimize them out. */
		long serviceStartTime = 0l;
		long requestStartTime = 0l;
		long  requestStopTime = 0l;
		long    drawStartTime = 0l;
		long     drawStopTime = 0l;
		long  serviceStopTime = 0l;

		if ( Debug.TIMERS)
			serviceStartTime = System.currentTimeMillis();

		/* tell the client not to cache this page because it's dynamic */
		httpResponse.setHeader( HTTP.CACHE_CONTROL, HTTP.NO_CACHE);
		httpResponse.addHeader( HTTP.CACHE_CONTROL, HTTP.NO_STORE);

		/* dispatch request events, TODO filter the request for applicable events */
		try
		{
			if ( Debug.TIMERS)
				requestStartTime = System.currentTimeMillis();
				
			RequestEvent event = new RequestEvent( httpRequest, httpResponse);
			
			Log.printDebug( "Component.service() " + event.getRequestURL());
			
			this.onRequest( event);
		}
		catch ( RedirectResponse redirect)
		{
			if ( Debug.TRACE_REQUESTS)
				Log.printMsg( redirect.getMessage());
				
			return; /* do not draw the page or perform timing checks */
		}
		catch ( StopRequestProcessing stop)
		{
			/* ignores the exception which is thrown to interrupt request processing due to a
			timeout or a consumed event */
			if ( Debug.TRACE_REQUESTS)
				Log.printMsg( stop.getMessage());
		}
		finally
		{
			if ( Debug.TIMERS)
				requestStopTime = System.currentTimeMillis();
		}
		
		/* draw the component */

		if ( Debug.TIMERS)
			drawStartTime = System.currentTimeMillis();

		TagWriter out = new TagWriter( httpRequest, httpResponse);
		this.onDraw( out);

		if ( Debug.TIMERS)
			drawStopTime = System.currentTimeMillis();

		if ( Debug.TIMERS)
		{
			serviceStopTime = System.currentTimeMillis();
		
			Log.printMsg( "[timer] serviceStartTime: " + Long.toString( serviceStartTime));
			Log.printMsg( "[timer]  requestElaspsed: " + 
				Long.toString( requestStopTime - requestStartTime));
			Log.printMsg( "[timer]      drawElapsed: " + 
				Long.toString( drawStopTime - drawStartTime));
			Log.printMsg( "[timer]  serviceStopTime: " + Long.toString( serviceStopTime));
			Log.printMsg( "[timer]   serviceElapsed: " + 
				Long.toString( serviceStopTime - serviceStartTime));
		}
	} // service()
	
	public void destroy()
	{
		this.config = null; /* release the config */
	}
}

