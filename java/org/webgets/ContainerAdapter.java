package org.webgets;

import java.util.*;
import org.webgets.util.*;

/**

ContainerAdapter is a trivial implementation fo the Container interface. 
Classes wishing to be Containers can sub-class ContainerAdaper to inherit
it's behavour.

Warning: ContainerAdapter is not thread safe, it is not intened to be used
in a multi-threaded environment!

*/
public abstract class ContainerAdapter extends ComponentAdapter 
	implements Container
{
	private transient Component[] componentArrayCache = null;
    protected ArrayList components = new ArrayList();

	//
	// Constructors

    public ContainerAdapter()
    {
        super();
    }
    
    public ContainerAdapter( String label) 
    {
        super( label);
    }
    
    /** Creates a new Container with the label of the Component provided, 
    then add the Component to the list of Components. */
    public ContainerAdapter( Component delegate)
    {
    	this( delegate.getLabel());
    	this.addComponent( delegate);
    }
    
    /** Creates a new Container with the label provided and adds the Component
    to the list of Components */
    public ContainerAdapter( String label, Component add)
    {
    	this( label);
    	this.addComponent( add);
    }
    
    /** Creates a new Container with the lable provided and adds the Components
    to the list of Components */
    public ContainerAdapter( String label, Component[] add)
    {
    	this( label);
    	int index = 0;
    	while ( index < add.length)
    		this.addComponent( add[index++]);
    }

	//
	// Instance Methods

	public void addComponent( Component add)
	{
	    this.components.add( add);
	    this.componentArrayCache = null;
	}

	public void addComponent( Component add, int where)
	{
	    this.components.add( where, add);
	    this.componentArrayCache = null;
	}

	public void removeComponent( Component remove)
	{
	    this.components.remove( remove);
	    this.componentArrayCache = null;

	}
	
	public void removeComponent( int index)
	{
		this.components.remove( index);
		this.componentArrayCache = null;
	}
	
	public void trimToIndex( int index)
	{
		while ( index < this.components.size())
			this.components.remove( index);
		this.componentArrayCache = null;
	}
	
	public Component[] getComponents() throws ClassCastException
	{
		if ( this.componentArrayCache == null)
		{
			Component[] components = new Component[ this.components.size()];
			this.componentArrayCache = (Component[]) this.components.toArray( components);
		}

		return this.componentArrayCache;
	}
	
	public void removeAllComponents()
	{
		this.components.clear();
	    this.componentArrayCache = null;
	}
	
	//
	// Interface Methods
	
	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		super.init( config);
		Component[] comps = this.getComponents();
		int index = 0;
		while ( index < comps.length)
			comps[index++].init( this.getWebgetsConfig());
	}

	public void destroy()
	{
		super.destroy();
		Component[] comps = this.getComponents();
		int index = 0;
		while ( index < comps.length)
			comps[index++].destroy();
	}
	
	
	/**

	The onDraw() method of ContainerAdapter marks the TagWriter, passes the 
	onDraw()  call to all Components which have been added to the Container, 
	catches any exceptions which may have occured in the drawing and writes 
	their messages into a comment, then closes all open tags to the mark.

	*/
    public void onDraw( TagWriter out)
    {
		super.onDraw( out);

		out.mark();

		Component[] comps = this.getComponents();

		int index = 0;
		while ( index < comps.length)
		{
			try
			{
				comps[index++].onDraw( out);
			}
			catch ( Exception drawingError)
			{
				this.reportChildDrawingError( out, drawingError);
			}
		}

		out.closeToMark();
		this.traceEndDraw();
	}
	
	protected final void traceEndDraw()
	{
		if ( Debug.TRACE_DRAWS)
			Log.printMsg( "[endDraw] " + this.getClass().getName() + " " + this.getLabel());
	}
	
	protected final void reportChildDrawingError( TagWriter out, Exception drawingError)
	{
		if ( Debug.DEBUG)
		{
			out.tag( "pre");
				out.print(  "ContainerAdapter.onDraw() child drawing error: ");
				out.println( drawingError.getMessage());
				drawingError.printStackTrace( out);
			out.closeTag();
		}
		else
		{
			out.comment( "ContainerAdapter.onDraw() child drawing error: " 
				+ drawingError.getMessage());
		}
	
		Log.printWarn( "ContainerAdapter.onDraw() child drawing error", drawingError);
	}
	
	/**
	
	The onRequest() method of ContainerAdapter passes the call on to all Components
	which have been added to the Container, it does not provide exception handling.
	
	*/
	public void onRequest( RequestEvent event)
	{
		super.onRequest( event);
	
		Component[] children = this.getComponents();
		int index = 0;
		while ( index < children.length)
			children[index++].onRequest( event);

	}
	
	protected final void traceEndRequest()
	{
		if ( Debug.TRACE_REQUESTS)
			Log.printMsg( "[endReqeust] " + this.getClass().getName() + " " + this.getLabel());
	}

} // public abstract class ComponentAdapter
