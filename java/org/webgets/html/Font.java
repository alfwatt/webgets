package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Font is a Decorator which draws <font></font> tags.

TODO Font instances should be interned().

*/
public class Font implements Decorator, HTML
{
	public static final Font SIZE_1 = new Font( "1");
	public static final Font SIZE_2 = new Font( "2");
	public static final Font SIZE_3 = new Font( "3");
	public static final Font SIZE_4 = new Font( "4");
	public static final Font SIZE_5 = new Font( "5");
	public static final Font SIZE_6 = new Font( "6");
	public static final Font SIZE_7 = new Font( "7");
	public static final Font PLUS_1 = new Font( "+1");
	public static final Font PLUS_2 = new Font( "+2");
	public static final Font PLUS_3 = new Font( "+3");
	public static final Font PLUS_4 = new Font( "+4");
	public static final Font MINUS_1 = new Font( "-1");
	public static final Font MINUS_2 = new Font( "-2");

	/* <font face="serif, sans-serif"></font> */
	public static final Font SANS_SIZE_1 = new Font( "sans-serif", "1");
	public static final Font SANS_SIZE_2 = new Font( "sans-serif", "2");
	public static final Font SANS_SIZE_3 = new Font( "sans-serif", "3");
	public static final Font SANS_SIZE_4 = new Font( "sans-serif", "4");
	public static final Font SANS_SIZE_5 = new Font( "sans-serif", "5");
	public static final Font SANS_SIZE_6 = new Font( "sans-serif", "6");
	public static final Font SANS_SIZE_7 = new Font( "sans-serif", "7");
	public static final Font SANS_PLUS_1 = new Font( "sans-serif", "+1");
	public static final Font SANS_PLUS_2 = new Font( "sans-serif", "+2");
	public static final Font SANS_PLUS_3 = new Font( "sans-serif", "+3");
	public static final Font SANS_PLUS_4 = new Font( "sans-serif", "+4");
	public static final Font SANS_MINUS_1 = new Font( "sans-serif", "-1");
	public static final Font SANS_MINUS_2 = new Font( "sans-serif", "-2");

	public static final Font SERIF_SIZE_1 = new Font( "serif", "1");
	public static final Font SERIF_SIZE_2 = new Font( "serif", "2");
	public static final Font SERIF_SIZE_3 = new Font( "serif", "3");
	public static final Font SERIF_SIZE_4 = new Font( "serif", "4");
	public static final Font SERIF_SIZE_5 = new Font( "serif", "5");
	public static final Font SERIF_SIZE_6 = new Font( "serif", "6");
	public static final Font SERIF_SIZE_7 = new Font( "serif", "7");
	public static final Font SERIF_PLUS_1 = new Font( "serif", "+1");
	public static final Font SERIF_PLUS_2 = new Font( "serif", "+2");
	public static final Font SERIF_PLUS_3 = new Font( "serif", "+3");
	public static final Font SERIF_PLUS_4 = new Font( "serif", "+4");
	public static final Font SERIF_MINUS_1 = new Font( "serif", "-1");
	public static final Font SERIF_MINUS_2 = new Font( "serif", "-2");


	private static Attributes DEFAULT_ATTRIBUTES = new Attributes();

	private Attributes fontAttrs = new Attributes();

	public Font( String size)
	{
		super();
		this.setSize( size);
	}
	
	public Font( String face, String size)
	{
		super();
		this.setFace( face);
		this.setSize( size);
	}

	public Font( String face, String color, String size)
	{
		super();
		this.setFace( face);
		this.setColor( color);
		this.setSize( size);
	}
	
	public void setFace( String face)
	{
		this.fontAttrs.put( FACE, face);
	}
	
	public void setColor( String color)
	{
		this.fontAttrs.put( COLOR, color);
	}
	
	public void setSize( String size)
	{
		this.fontAttrs.put( SIZE, size);
	}
	
	public void onDraw( TagWriter out, Component delegate)
	{
		out.tag( FONT, this.fontAttrs);
		out.disableIndent();
			delegate.onDraw( out);
		out.closeTag();	
		out.enableIndent();
	}

	public void onDraw( TagWriter out, String text)
	{
		out.tag( FONT, this.fontAttrs);
			out.print( text);
		out.closeTag();
	}

}
