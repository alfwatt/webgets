package org.webgets.html;

import java.io.*;
import java.net.*;

import org.webgets.*;
import org.webgets.util.*;

/**

Include grabs the contents of a File or URL and displays it when
the onDraw method is called. For efficency the content it not fetched
until the onRequest method is called for the first time. This also allows
relitive URL paths to be used.

*/
public class Include extends ComponentAdapter
{
	private String sourcePath = null;
	private URL source = null;
	private long fetched = 0;
	private long refresh = 10000; // check every ten seconds
	private String content = null;
	private Warning failed = null;

	public Include( String sourcePath)
	{
		super( sourcePath);
		this.sourcePath = sourcePath;		
	}

	public Include( File sourceFile)
		throws java.net.MalformedURLException
	{
		this( sourceFile.toURL());
	}
	
	public Include( URL source)
	{
		super( source.toString());
		this.source = source;
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		try
		{
			// check if we need to evaluate the sourcePath & generate the source
			if ( source == null)
			{
				File sourceFile = new File( request.getSourceRequest().getRealPath( sourcePath));

				if ( sourceFile.isFile())
					source = sourceFile.toURL();
				else /* backup to web fetch method */
					source = new URL( new URL(  request.getRequestURL()), sourcePath);
			}
			
			// check the fetched time and fetch the content
			long now = System.currentTimeMillis();
			if ( fetched == 0 || ( now - fetched) > refresh)
			{
				Log.printDebug( "Include.onRequest() loading " + source.toString());
				
				URLConnection connection = source.openConnection();
				InputStream inStream = connection.getInputStream();
				StringWriter outStream = new StringWriter( connection.getContentLength());
				
				byte[] buffer = new byte[1024];
				int count = 0;
				
				while ( ( count = inStream.read( buffer)) >= 0)
					outStream.write( new String( buffer, 0, count));
				
				content = outStream.toString();				
				fetched = System.currentTimeMillis();
			}
		}
		catch ( Exception fail)
		{
			this.failed = new Warning( fail);
		}
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		if ( this.failed != null)
			this.failed.onDraw( out);
		else
			out.print( this.content);	
	}
	
	public String toString()
	{
		return this.getLabel();
	}
}
