package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

A Flow combines a number of Flowable Components in a single Container. 

Elements are laied out as a continous flow, much like the body of a document. 
Flows are usefull for moving around chunks of text which should stay together.

*/
public class Flow extends ContainerAdapter
{
	public interface Flowable
	{
		/* flowable defines no methods, it is a marker interface */
	}
	
	public Flow()
	{
		super();
	}
	
	public Flow( String label)
	{
		super();
		this.setLabel( label);
	}
	
	public Flow( Component add)
	{
		super( add);
	}
	
	public Flow( String label, Component add)
	{
		super( label, add);
	}
	
	public Flow( String label, Component[] add)
	{
		super( label, add);
	}
	
	public void addComponent( Component flowable)
	{
		if ( ! ( flowable instanceof Flowable))
			throw new IllegalArgumentException( "Flow.addComponent() not a Flowable,"
				+ "try implementing Flow.Flowable");
		else
			super.addComponent( flowable);
	}
}

