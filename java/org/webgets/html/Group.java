package org.webgets.html;

import org.webgets.*;

/**

Group is a trivial extention of ContainerAdapter, it's good for grouping stuff together.

*/
public class Group extends ContainerAdapter
{
	public Group()
	{
		super();	
	}

	public Group( String label)
	{
		super( label);
	}
	
	public Group( Component add)
	{
		super( add);
	}
	
	public Group( String label, Component add)
	{
		super( label, add);
	}
	
	public Group( String label, Component[] add)
	{
		super( label, add);
	}
}
