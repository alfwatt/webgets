package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

P is used to draw a simple HTML paragraph, it's Linkable and Flowable because it extends Block

*/
public class P extends Block
{
    public P( String block)
    {
        super( block);
    }
    
    public P( String label, String block)
    {
    	super( label, block);
    }
    
    public void onDraw( TagWriter out)
    {
        out.tag( HTML.P);
    	out.disableIndent();
    		super.onDraw( out);
        out.closeTag();
        out.enableIndent();
    }
}
