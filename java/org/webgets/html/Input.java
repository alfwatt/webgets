package org.webgets.html;

import java.text.*;
import org.webgets.*;
import org.webgets.util.*;
import org.webgets.text.*;

/**

<p>Inputs are the visible and invisible Form elements which can send
data back to a server. Various static <tt>create</tt> methods are
provided to build Inputs of various types and values.</p>

<p><tt>createInput()</tt> will create inputs based on a few common types
and formatting classes provided in the org.webgets.text package.</p>

<p>The static method createIdInput() will create the hidden target input
for a Component.</p>

<p>By default Inputs listen to all Rquests, a Form instance can be
provided in the various constructors or set after creation to allows the
input to filter requests based on the Form's id.</p>

*/
public class Input extends ComponentAdapter
{
	public static final String   BUTTON_TYPE = HTML.BUTTON;
	public static final String CHECKBOX_TYPE = HTML.CHECKBOX;
	public static final String     FILE_TYPE = HTML.FILE;
	public static final String   HIDDEN_TYPE = HTML.HIDDEN;
	public static final String    IMAGE_TYPE = HTML.IMAGE;
	public static final String PASSWORD_TYPE = HTML.PASSWORD;
	public static final String    RADIO_TYPE = HTML.RADIO;
	public static final String    RESET_TYPE = HTML.RESET;
	public static final String   SUBMIT_TYPE = HTML.SUBMIT;
	public static final String     TEXT_TYPE = HTML.TEXT;
	
	public static final String    OTHER_TYPE = "";

	/** @returns an HIDDEN Input identifying the given Identifiable */
	public static Input createIdInput( Identifiable id)
	{
		return new Input( HIDDEN_TYPE, RequestEvent.TARGET, id.getIdString()); 
	}

	/** @returns an Input which can be used to present the given object */
	public static Component createInput( String name, Object value, Format format)
	{
		/* XXX HACK to deal with number format wrappers */		
		if ( format instanceof NumberFormatWrappers.Wrapper) 
			format = ((NumberFormatWrappers.Wrapper) format).getWrapped();
		
		String stringValue = format.format( value);

		if ( format instanceof TextAreaFormat)
		{
			TextAreaFormat areaFormat = (TextAreaFormat) format;
			return new TextArea( name, stringValue, areaFormat.getRows(), areaFormat.getCols());
		}
		else if ( format instanceof ChoiceFormat)
		{
			ChoiceFormat choice = (ChoiceFormat) format;
			return new Select( name, stringValue, choice.getFormats());
		}
		else if ( format instanceof StringChoiceFormat )
		{
			StringChoiceFormat choice = (StringChoiceFormat) format;
			return new Select( name, stringValue, choice.getFormats(), choice.isMultiple() );
		}
		else if ( format instanceof BooleanFormat) // XXX should be implemented as a checkbox
		{
			Group bool = new Group( name);
				bool.addComponent( new Block( "true"));
				bool.addComponent( createRadioInput( name, "true", value.equals( Boolean.TRUE)));
				bool.addComponent( new Block( "false"));
				bool.addComponent( createRadioInput( name, "false", value.equals( Boolean.FALSE)));
			return bool;
		}
		else if ( format instanceof CharacterFormat)
			return createTextInput( name, stringValue, 1, 1);
		else if ( format instanceof NumberFormat)
			return createTextInput( name, stringValue, 128, 8);
		else
			return createTextInput( name, stringValue, 128, 20);
	}
	
	public static Input createResetInput( String value) 
	{
		return new Input( RESET_TYPE, value);
	}
	
	public static Input createResetInput( String value, Form form) 
	{
		return new Input( RESET_TYPE, value, form);
	}
	
	public static Input createSubmitInput( String value) 
	{
		return new Input( SUBMIT_TYPE, value);
	}
	
	public static Input createSubmitInput( String value, Form form) 
	{
		return new Input( SUBMIT_TYPE, value, form);
	}
	
	public static Input createHiddenInput( String name, String value)
	{
		return new Input( HIDDEN_TYPE, name, value);
	}
	
	public static Input createHiddenInput( String name, String value, Form form)
	{
		return new Input( HIDDEN_TYPE, name, value, form);
	}
	
	public static Input createButtonInput( String name, String value)
	{
		return new Input( BUTTON_TYPE, name, value);
	}
	
	public static Input createButtonInput( String name, String value, Form form)
	{
		return new Input( BUTTON_TYPE, name, value, form);
	}
	
	/**
		@param name is the name attribute of the Input
		@param value is the display value of the Input
		@param checked is the selected attribute of the Input
	*/
	public static Input createRadioInput( String name, String value, boolean checked)
	{
		Input radio = new Input( RADIO_TYPE, name, value);
		radio.setChecked( checked);
		return radio;
	}
	
	/**
		@param name is the name attribute of the Input
		@param value is the display value of the Input
		@param checked is the selected attribute of the Input
	*/
	public static Input createRadioInput( String name, String value, boolean checked, Form form)
	{
		Input radio = new Input( RADIO_TYPE, name, value, form);
		radio.setChecked( checked);
		return radio;
	}
	
	/**
		@param name is the name attribute of the Input
		@param value is the display value of the Input
		@param checked is the selected attribute of the Input
	*/
	public static Input createCheckboxInput( String name, String value, boolean checked)
	{
		Input checkbox = new Input( CHECKBOX_TYPE, name, value);
		checkbox.setChecked( checked);
		return checkbox;
	}

	/**
		@param name is the name attribute of the Input
		@param value is the display value of the Input
		@param checked is the selected attribute of the Input
	*/
	public static Input createCheckboxInput( String name, String value, boolean checked, Form form)
	{
		Input checkbox = new Input( CHECKBOX_TYPE, name, value, form);
		checkbox.setChecked( checked);
		return checkbox;
	}

	public static Input createFileInput( String name, String value, String mimeType)
	{
		Input file = new Input( FILE_TYPE, name, value);
		file.setMimeType( mimeType);
		return file;
	}
	
	public static Input createFileInput( String name, String value, String mimeType, Form form)
	{
		Input file = new Input( FILE_TYPE, name, value, form);
		file.setMimeType( mimeType);
		return file;
	}
	
	public static Input createImageInput( String name, String align, String src)
	{
		Input image = new Input( IMAGE_TYPE, name, align);
		image.setSrc( src);
		return image;
	}
	
	public static Input createImageInput( String name, String align, String src, Form form)
	{
		Input image = new Input( IMAGE_TYPE, name, align, form);
		image.setSrc( src);
		return image;
	}
	
	public static Input createPasswordInput( String name, String value, int maxlength, int size)
	{
		Input password =  new Input( PASSWORD_TYPE, name, value);
		password.setMaxLength( maxlength);
		password.setSize( size);
		return password;
	}
	
	public static Input createPasswordInput( String name, String value,
	                                         int maxlength, int size, Form form)
	{
		Input password =  new Input( PASSWORD_TYPE, name, value, form);
		password.setMaxLength( maxlength);
		password.setSize( size);
		return password;
	}
	
	public static Input createTextInput( String name, String value, int maxlength, int size)
	{
		Input text = new Input( TEXT_TYPE, name, value);
		text.setMaxLength( maxlength);
		text.setSize( size);
		return text;
	}

	public static Input createTextInput( String name, String value, 
	                                     int maxlength, int size, Form form)
	{
		Input text = new Input( TEXT_TYPE, name, value, form);
		text.setMaxLength( maxlength);
		text.setSize( size);
		return text;
	}

	private Form form = null;
	private Attributes attrs = null;

	//
	// Custom Constructors

	protected Input( String name)
	{
		super( name);
		this.attrs = new Attributes( HTML.TYPE, OTHER_TYPE); // HACK prevents some null cases
	}
	
	protected Input( String name, Form form)
	{
		this( name);
		this.setForm( form);
	}

	/**
		@param type must be one of: RESET_TYPE SUBMIT_TYPE 
		@value is the display value of the Input
	*/
	public Input( String type, String value)
	{
		super( value);
		this.attrs = new Attributes( HTML.TYPE,  type, 
									 HTML.VALUE, value);
	}

	/**
		@param type must be one of: RESET_TYPE SUBMIT_TYPE 
		@param value is the display value of the Input
		@param form the Form which the Input will listen to for updates
	*/
	public Input( String type, String value, Form form)
	{
		this( type, value);
		this.setForm( form);
	}

	/**
		@param type must be one of: HIDDEN_TYPE BUTTON_TYPE 
		@param name is the name attribute of the Input
		@param value is the display value of the Input
	*/
	public Input( String type, String name, String value)
	{
		super( type == HIDDEN_TYPE ? "" : name); // hidden inputs have no label
		this.attrs = new Attributes( HTML.TYPE,  type, 
									 HTML.NAME,  name, 
									 HTML.VALUE, value);
	}

	/**
		@param type must be one of: HIDDEN_TYPE BUTTON_TYPE 
		@param name is the name attribute of the Input
		@param value is the display value of the Input
		@param form the Form which the Input will listen to for updates
	*/
	public Input( String type, String name, String value, Form form)
	{
		this( type, name, value);
		this.setForm( form);
	}

	//
	// Instance Methods

	public String getType()
	{
		return this.attrs.getAsString( HTML.TYPE);
	}

	public void setForm( Form form)
	{
		this.form = form;
		this.form.addRequestListener( this);
	}

	public String getName()
	{
		return this.attrs.getAsString( HTML.NAME);
	}

	public void setName( String name)
	{
		this.attrs.put( HTML.NAME, name);
	}

	public String getValue()
	{
		return this.attrs.getAsString( HTML.VALUE);
	}
	
	public void setValue( String value)
	{
		this.attrs.put( HTML.VALUE, value);
	}
	
	public boolean isChecked()
	{
		return this.attrs.containsKey( HTML.CHECKED);
	}
	
	public void setChecked( boolean checked)
	{
		if ( checked)
		{
			Log.printDebug( "Input.setChecked( true);");
			this.attrs.put( HTML.CHECKED, null);
		}
		else
		{
			Log.printDebug( "Input.setChecked( false);");
			this.attrs.remove( HTML.CHECKED);
		}
	}

	public void setMimeType( String mimeType)
	{
		this.attrs.put( HTML.ACCEPT, mimeType);
	}
	
	public String getMimeType()
	{
		return this.attrs.getAsString( HTML.ACCEPT);
	}

	public void setAlign( String align)
	{
		this.attrs.put( HTML.ALIGN, align);
	}

	public String getAlign()
	{
		return this.attrs.getAsString( HTML.ALIGN);
	}
	
	public void setSrc( String src)
	{
		this.attrs.put( HTML.SRC, src);
	}

	public String getSrc()
	{
		return this.attrs.getAsString( HTML.SRC);
	}
	
	public void setMaxLength( int maxLength)
	{
		this.attrs.put( HTML.MAXLENGTH, new IntValue( maxLength));
	}

	public int getMaxLength()
	{
		return this.attrs.getAsNumberValue( HTML.MAXLENGTH).getIntValue();
	}
	
	public void setSize( int size)
	{
		this.attrs.put( HTML.SIZE, new IntValue( size));
	}

	public int getSize()
	{
		return this.attrs.getAsNumberValue( HTML.SIZE).getIntValue();
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
		if ( this.getName() != null && ( this.form == null || request.isTargeted( this.form)))
		{
			/* Radio and Checkbox inputs don't change value, they get checked or unchecked */
			if ( RADIO_TYPE.equals( this.getType()) || CHECKBOX_TYPE.equals( this.getType()))
			{
				String[] values = request.getSourceRequest().getParameterValues( this.getName());
				String value = this.getValue();
				if ( values != null && value != null)
				{
					boolean checked = false;
					int index = 0;
					while ( (! checked) && index < values.length)
					{
						Log.printDebug( value + "=" + values[index]);
						if ( value.equals( values[index++]))
							checked = true;
					}

					this.setChecked( checked);
				}
			}
			else
			{
				String value = request.getParameter( this.getName());
				if ( value != null && ! value.equals( this.getValue()))
					this.setValue( value);
			}
		}
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.emptyTag( HTML.INPUT, this.attrs);
	}
}

/* Love, Alf */
