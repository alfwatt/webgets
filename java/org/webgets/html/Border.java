package org.webgets.html;

import java.net.*;
import org.webgets.*;
import org.webgets.util.*;

/**

This class draws a simple HTML Table Border around any other component.

*/
public class Border extends ContainerAdapter implements HTML
{
	private Attributes rowAttrs = new Attributes( CLASS, CSS_LIGHT_BLUE);
	private Attributes tableAttrs = new Attributes( CLASS, CSS_BLUE,
			 										HTML.CELLSPACING, "2",
													HTML.CELLPADDING, "2");
	
	//
	//  Constructors from ContainerAdapter
	
	public Border()
	{
		super();
	}
	
	public Border( String label)
	{
		super( label);
	}
		
	public Border( Component body)
	{
		super( body);
	}
	
	public Border( String label, Component body)
	{
		super( label, body);
	}
	

	public Border( String label, Component[] body)
	{
		super( label, body);
	}
	
	//
	// Custom Constructor
	
	public Border( Component body, int width)
	{
		super( body);
		this.setWidth( width);
	}
	
	public Border( String label, Component body, int width)
	{
		super( label, body);
		this.setWidth( width);
	}
	
	//
	// Instance Methods
	
	public void setWidth( int width)
	{
		if ( width < 1)
			throw new IllegalArgumentException( "Border wdith must be > 1");
			
		this.tableAttrs.put( HTML.CELLSPACING, new IntValue( width));
	}
	
	public void setPadding( int padding)
	{
		if ( padding < 1)
			throw new IllegalArgumentException( "Padding width must be > 1");
		
		this.tableAttrs.put( HTML.CELLPADDING, new IntValue( padding));
	}


	public void onDraw( TagWriter out)
	{
		out.mark();
		out.tag( HTML.TABLE, this.tableAttrs);
			out.tag( HTML.TR, this.rowAttrs);
				out.tag( HTML.TD);
					super.onDraw( out);
		out.closeToMark();
	} 
}

