package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Insets draws a 3x3 table with the content in the center cell, the top, botton, 
left and right center cells are drawn with the appropritate attributes to provided
white space as defined in the insets.

*/
public class Insets extends ContainerAdapter implements HTML
{
	private static String[] TABLE_ATTRS = new String[] { CELLPADDING, "0", CELLSPACING, "0"};
	
	private Attributes topAttrs = new Attributes( new String[] { HEIGHT, "0"});
	private Attributes bottomAttrs = new Attributes( new String[] { HEIGHT, "0"});
	private Attributes leftAttrs = new Attributes( new String[] { WIDTH, "0"});
	private Attributes rightAttrs = new Attributes( new String[] { WIDTH, "0"});

	//
	// Constructors from ContainerAdapter

	public Insets()
	{
		super();
	}
	
	public Insets( String label)
	{
		super( label);
	}
	
	public Insets( Component add)
	{
		super( add);
	}
	
	public Insets( String label, Component add)
	{
		super( label, add);
	}
	
	public Insets( String label, Component[] add)
	{
		super( label, add);
	}
	
	//
	// Custom Constructors

	public Insets( String top, String botton, String left, String right)
	{
		this();
		this.setTop( top);
		this.setBottom( botton);
		this.setLeft( left);
		this.setRight( right);
	}
	
	public void setTop( String top)
	{
		this.topAttrs.put( HEIGHT, top);
	}
	
	public void setBottom( String botton)
	{
		this.bottomAttrs.put( HEIGHT, botton);
	}
	
	public void setLeft( String left)
	{
		this.leftAttrs.put( WIDTH, left);
	}
	
	public void setRight( String right)
	{
		this.rightAttrs.put( WIDTH, right);
	}
	
	public void onDraw( TagWriter out)
	{
		super.traceDraw();
		
		out.tag( TABLE, TABLE_ATTRS);

			out.tag( TR);
				out.tag( TD);
				out.closeTag();
				out.tag( TD, this.topAttrs);
				out.closeTag();
				out.tag( TD);
				out.closeTag();
			out.closeTag();

			out.tag( TR);
				out.tag( TD, this.leftAttrs);
				out.closeTag();
				out.tag( TD);
					super.onDraw( out);
				out.closeTag();
				out.tag( TD, this.rightAttrs);
				out.closeTag();
			out.closeTag();

			out.tag( TR);
				out.tag( TD);
				out.closeTag();
				out.tag( TD, this.bottomAttrs);
				out.closeTag();
				out.tag( TD);
				out.closeTag();
			out.closeTag();

		out.closeTag( TABLE);

		super.traceEndDraw();
	}
}

/* Love, Alf */
