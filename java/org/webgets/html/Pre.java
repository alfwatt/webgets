package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

*/
public class Pre extends ComponentAdapter 
{
    private String cdata = null;
    
    public Pre( String cdata)
    {
        super();
        this.setCdata( cdata);
    }

	public Pre( Object cdata)
	{
		this( cdata.toString());
	}
    
    public Pre( String label, String cdata)
    {
    	this( cdata);
    	this.setLabel( label);
    }

	public void setCdata( String cdata)
	{
		if ( cdata == null)
			throw new IllegalArgumentException( "Block cdata cannot be null");
		this.cdata = cdata;
	}

    public String getLabel()
    {
        String label = super.getLabel();
        if ( label == null && this.cdata != null)
            label = TagWriter.trimString( this.cdata);
        return label;
    }
    
    public void onDraw( TagWriter out)
    {
    	super.onDraw( out);
		out.tag( HTML.PRE);
			out.write( cdata);
		out.closeTag();
    }
}
