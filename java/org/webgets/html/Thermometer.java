package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

public class Thermometer extends ComponentAdapter implements HTML
{
	private static final String ZERO = "0";
	private static final String STUFF = "<font size=\"0\">&nbsp;</font>";

	private static final int WIDTH_INDEX = 3;
	private static final int HEIGHT_INDEX = 5;
	private static final int COLOR_INDEX = 1;

	private static String[] TABLE_ATTRS = new String[]
	{
		CELLSPACING, ZERO,
		CELLPADDING, ZERO,
		BORDER,      ZERO
	};	

	public static int floatToPercent( float percent)
	{
		return ( Math.round( percent*100));
	}
	
	public static String floatToPercentString( float percent)
	{
		return Integer.toString( floatToPercent( percent));
	}

	private float percent = 0.5f;
	private int width = 100;
	private int height = 10;
	
	private String[] doneAttrs = new String[] 
	{ 
		BGCOLOR, "black",
		WIDTH,  Integer.toString( 0 * this.width), 
		HEIGHT, Integer.toString( this.height),
	};
			
	private String[] todoAttrs = new String[] 
	{ 
		BGCOLOR, "silver",
		WIDTH,  Integer.toString( 1 * this.width),
		HEIGHT, Integer.toString( this.height),
	};
	
	public Thermometer( float percent)
	{
		super();
		this.setPercent( percent);
	}
	
	public Thermometer( float percent, int width, int height)
	{
		this( percent);
		this.setWidth( width);
		this.setHeight( height);
	}
	
	public Thermometer( float percent, String todoColor)
	{
		this( percent);
		this.setTodoColor( todoColor);
	}

	public Thermometer( float percent, int width, int height, String todoColor)
	{
		this( percent, width, height);
		this.setTodoColor( todoColor);
	}
	
	public Thermometer( float percent, int width, int height, String doneColor, String todoColor)
	{
		this( percent, width, height);
		this.setDoneColor( doneColor);
		this.setTodoColor( todoColor);
	}
	
	public void setPercent( float percent)
	{
		this.percent = percent;
		this.scale();
	}
	
	public float getPercent()
	{
		return this.percent;
	}
	
	public void setWidth( int width)
	{
		this.width = width;
		this.scale();
	}
	
	public void setHeight( int height)
	{
		this.height = height;
		this.doneAttrs[ HEIGHT_INDEX] = Integer.toString( height);
		this.todoAttrs[ HEIGHT_INDEX] = Integer.toString( height);
	}

	public void setTodoColor( String color)
	{
		this.todoAttrs[ COLOR_INDEX] = color;
	}
	
	public void setDoneColor( String color)
	{
		this.doneAttrs[ COLOR_INDEX] = color;
	}
	
	private void scale()
	{
		/* scale the percent to the width of the thermometer */
		this.doneAttrs[ WIDTH_INDEX] = Integer.toString( Math.round( this.percent * this.width));
		this.todoAttrs[ WIDTH_INDEX] = Integer.toString( this.width - Math.round( this.percent * this.width));
	}

	public String getLabel()
	{
		return Thermometer.floatToPercentString( this.getPercent()) + "%";
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( TABLE, TABLE_ATTRS);
			out.tag( TR);
			if ( this.percent > 0)
			{
				out.tag( TD, this.doneAttrs);
						out.print( STUFF);
				out.closeTag();
			}

			if ( this.percent < 1)
			{
				out.tag( TD, this.todoAttrs);
						out.print( STUFF);
				out.closeTag();
			}
			out.closeTag();
		out.closeTag();
	}

}
