package org.webgets.html;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.webgets.*;
import org.webgets.util.*;

/**

Form implements HTML form processing by priting the form tag, a hidden identity tag
and dispatching the RequestEvent to a specified RequestListener when the form is 
submitted. 

*/
public class Form extends ComponentAdapter
{
	public static final String INTERNET_MEDIA_TYPE = "application/x-www-form-urlencoded";
	public static final String     MULTI_PART_TYPE = "mulit-part/form-data";
	public static final String         POST_ACTION = "POST";
	public static final String          GET_ACTION = "GET";

	private Component delegate = null;
	private Input identity = null;
	private String action = null;
	private String method = null;
	private String encoding = null;
	private boolean local = true;
	private ArrayList listeners = new ArrayList(1);

	public Form()
	{
		super();
	}

	/**
	
	Create a lightweight Form that does a get back to this page.
	
	*/
	public Form( Component delegate)
	{
		super( delegate.getLabel());
		this.setDelegate( delegate);
		this.identity = Input.createIdInput( this);
	}
	
	public Form( Component delegate, RequestListener listener)
	{
		this( delegate);
		this.addRequestListener( listener);
	}

	/**
	
	Create a heavyweight Form that does either a post or get to some designated action URL.
	
	*/
	public Form( Component delegate, String action, String method)
	{
		this( delegate);
		this.setAction( action);
		this.setMethod( method);
	}
	
	
	/**
	
	Create a heavyweight Form that does either a post or get to some designated action URL.
	AND set the encoding, you dork.
	
	*/
	public Form( Component delegate, String action, String method, String encoding)
	{
		this( delegate, action, method);
		this.setEncoding( encoding);	
	}
	
	public Component getDelegate()
	{
		return this.delegate;
	}
	
	public void setDelegate( Component delegate)
	{
		if ( delegate == null)
			throw new IllegalArgumentException( "Form.setDelegate( delegate) cannot be null!");
			
		this.delegate = delegate;
	}
	
	public String getAction()
	{
		return this.action;
	}
	
	public void setAction( String action)
	{
		if ( action == null)
			throw new IllegalArgumentException( "Form.setAction( action) cannot be null!");
		
		this.action = action;
	}
	
	public String getMethod()
	{
		return this.method;
	}
	
	/**
	
	This set method only accepts Form.GET_METHOD or Form.POST_METHOD as values.
	
	*/
	public void setMethod( String method)
	{
		if ( method == null || ( method != GET_ACTION && method != POST_ACTION))
			throw new IllegalArgumentException( "Form.setMethod( method) must be one of " +
				" Form.GET_METHOD or Form.POST_METHOD");
		this.method = method;
	}
	
	public String getEncoding()
	{
		return this.encoding;
	}
	
	public void setEncoding( String encoding)
	{
		if ( encoding == null)
			throw new IllegalArgumentException( "Form.setEndocding( encoding) cannot be null!");
		
		this.encoding = encoding;
	}

	public void addRequestListener( RequestListener listener)
	{
		synchronized ( this.listeners)
		{
			this.listeners.add( listener);
		}
	}
	
	public void removeRequestListener( RequestListener listener)
	{
		synchronized( this.listeners)
		{
			this.listeners.remove( listener);
		}
	}

	public String getLabel()
	{
		return this.delegate.getLabel();
	}

	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		super.init( config);
		this.delegate.init( config);
	}

	/**
	
	Form modifies standard request handling as follows: it does not automatially pass
	request to it's delegate. Filtering events at the form layer is good for two reasons:
	1) it's not legal to nest forms in HTML, we don't want to encourage it, 2) haivng active
	elements such as toggles or novel input devices may seem like a good ideaa but Keep It
	Simple Stupid. As a side effect this can help improve request handling performance. This
	'feature' can be worked around by registreing the delegate as a RequestEvent listener
	with the addRequestListener() method, all webgets components are RequestListeners so
	this is easy.
	
	*/
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
		/* TODO check that request method matches the method we sent out (if any) */
		if ( request.isTargeted( this))
		{			
			Iterator li;
			synchronized (this.listeners)
			{
				li = this.listeners.iterator();
			}
			
			while ( li.hasNext())
				((RequestListener) li.next()).onRequest( request);
		}
				
		// this.delegate.onRequest( request);
	}

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		
		if ( this.local)
		{
		    String actionUrl = getAction();
		    if( actionUrl==null ) actionUrl = out.getRequestURL();

			/* draw an anchor so that clicks come back to the form to show updates on big pages */
			out.tag( HTML.A, new String[] { HTML.NAME, this.getIdString()});
			out.closeTag();
		    
			out.tag( HTML.FORM, new String[] { 
				HTML.ACTION, out.encodeURL( actionUrl) + "#" + this.getIdString(), 
				HTML.METHOD, POST_ACTION }); 
			this.identity.onDraw( out);
		}
		else
		{
			out.tag( HTML.FORM, HTML.ACTION, this.getAction(), null, this.local);
		}
			
		try
		{
			this.delegate.onDraw( out);
		}
		catch ( Exception report)
		{
			new Warning( report).onDraw( out);
		}
		out.closeTag();
	}
			
}

/* Love, Alf */
