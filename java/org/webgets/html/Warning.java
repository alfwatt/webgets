package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

A Warning is usedt o report an exception to the user, it draws in yellow with
red text to get their attention.

*/
public class Warning extends ComponentAdapter
{
	private Throwable source = null;
    private String text = null;
    
    public Warning( String text)
    {
        super();
        this.setText( text);
    }

	public Warning( Throwable source)
	{
		super();
		this.setSource( source);
	}

	public void setSource( Throwable source)
	{
		this.source = source;
		this.setText( source.toString());
	}

	public void setText( String text)
	{
		if ( text == null)
			throw new IllegalArgumentException( "Warning text cannot be null");
		this.text = text;
	}

    public String getLabel()
    {
        String label = super.getLabel();
        if ( label == null && this.text != null)
            label = TagWriter.trimString( text);
        return label;
    }
    
    private static String[] TABLE_ATTRS = new String[] { "bgcolor", "yellow", "cellpadding", "2"};
	private static String[]    TR_ATTRS = new String[] { "bgcolor", "white"};
	private static String[]  FONT_ATTRS = new String[] { "color", "red"};
	
    public void onDraw( TagWriter out)
    {
    	super.onDraw( out);
    	out.tag( HTML.TABLE, TABLE_ATTRS);
    		out.tag( HTML.TR, TR_ATTRS);
    			out.tag( HTML.TD);
    				out.tag( HTML.FONT, FONT_ATTRS);
    					out.print( this.text);
    				out.closeTag();
    			out.closeTag();
    		out.closeTag();

   		if ( Debug.DEBUG && this.source != null)
   		{
    		out.tag( HTML.TR);
    			out.tag( HTML.TD);
    				out.tag( HTML.PRE);
    					this.source.printStackTrace( out);
    					out.flush();
    				out.closeTag();
    			out.closeTag();
    		out.closeTag();
   		}
   		out.closeTag();
    }
    
    public static void main( String[] arguments)
    {
    	Warning w = new Warning( new Throwable());
    	System.err.println( w.getLabel());
    }
}
