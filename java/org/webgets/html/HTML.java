package org.webgets.html;

/** cheat interface you can implement to get the HTML constants */
public interface HTML
{
	//
	// Class Members

	/* misc chars */
	char EQ = '=';
	char EX = '!';
	char FS = '/';
	char GT = '>';
	char LT = '<';
	char NL = '\n';
	char QT = '"';
	char SP = ' ';
	char QUES = '?';
	char AND = '&';
	char NUM = '#';

	/* misc strings */
	String DD = "--";
	String DOT = ".";
	String INDENT = "  ";
	String NULL_STRING = "";
	String DISPLAY_NULL
		= "<font color=\"lightgrey\">null</font><br>";
	String DISPLAY_EMPTY
		= "<font color=\"lightgrey\">empty array</font><br>";
	String DISPLAY_ERROR
		= "<font color=\"red\">error!</font><br>";

	/* tags, indent level shows rough document outline */
	String DOCUMENT = "html";
		String COMMENT = "comment";
		String HEAD = "head";
			String BASE = "base";
			String BASEFONT = "basefont";
			String ISINDENT = "isindent";
			String LINK = "link";
			String META = "meta";
			String NEXTID = "nextid";
			String STYLE = "style";
			String TITLE = "title";
		String BODY = "body";
			String H1 = "h1";
			String H2 = "h2";
			String H3 = "h3";
			String H4 = "h4";
			String H5 = "h5";
			String H6 = "h6";
			String H7 = "h7";
			String H8 = "h8";
			String H9 = "h9";
			String P  = "p";
			String BLOCKQUOTE = "blockquote";
			String PRE = "pre";
			String CODE = "code";
			String EMP = "emp";
			String I = "i";
			String B = "b";
			String BIG = "big";
			String SMALL = "small";
			String STRIKE = "strike";
			String TT = "tt";
			String U = "u";
			String NOBR = "nobr";
			String OL = "ol";
			String UL = "ul";
			String LI = "li";
			String A = "a";
			String BR = "br";
			String WBR = "wbr";
			String HR = "hr";
			String IMG = "img";
			String FONT = "font";
			String TABLE = "table";
				String TH = "th";
				String TR = "tr";
				String TD = "td";
			String FORM = "form";
				String BUTTON = "button";
				String FIELDSET = "fieldset";
				String INPUT = "input";
				String KEYGEN = "keygen";
				String LABEL = "label";
				String LEGEND = "legend";
				String SELECT = "select";
				String OPTION = "option";
				String OPTGROUP = "optgroup";
				String TEXTAREA = "textarea";
					String ROWS = "rows";
					String COLS = "cols";

	/* attributes */
	String ACCEPT = "accept";
	String ACTION = "action";
	String ALIGN = "align";
	String ALT = "alt";
	String BGCOLOR = "bgcolor";
	String BACKGROUND = "background";
	String BORDER = "border";
	String BORDERCOLOR = "bordercolor";
	String CELLPADDING = "cellpadding";
	String CELLSPACING = "cellspacing";
	String COLOR = "color";
	String COLSPAN = "colspan";
	String CONTENT = "content";
	String FACE = "face";
	String FGCOLOR = "fgcolor";
	String HEIGHT = "height";
	String HREF = "href";
	String HSPACE = "hspace";
	String HTTP_EQUIV = "http-equiv";
	String METHOD = "method";
	String MULTIPLE = "multiple";
	String NAME = "name";
	String NOSHADE = "noshade";
	String NOWRAP = "nowrap";
	String REFRESH = "refresh";
	String SIZE = "size";
	String SRC = "src";
	String TYPE = "type";
	String VALIGN = "valign";
	String VALUE = "value";
	String VSPACE = "vspace";
	String WIDTH = "width";

	/* common attribute values */
	String BOTTOM = "bottom";
	String BASELINE = "baseline";
	String CENTER = "center";
	String LEFT = "left";
	String MIDDLE = "middle";
	String RIGHT = "right";
	String TOP = "top";
	String TRUE = "true";
	String FALSE = "false";
	String SAVE = "save";
	String CANCEL = "cancel";
	String EDIT = "edit";
	String NOW = "now";
	String NULL = "null";
	String ABSBOTTOM = "absbottom";

	/* font attribute values */
	String SERIF = "serif";
	String SANS_SERIF = "sans-serif";
	
	/* font size values */
	String ONE   = "1";
	String TWO   = "2";
	String THREE = "3";
	String FOUR  = "4";
	String FIVE  = "5";
	String SIX   = "6";
	String SEVEN = "7";

	String MINUS_ONE   = "-1";
	String MINUS_TWO   = "-2";
	String MINUS_THREE = "-3";

	String PLUS_ONE    = "+1";
	String PLUS_TWO    = "+2";
	String PLUS_THREE  = "+3";

	/* standard color names */
	String AQUA = "aqua";
	String BLACK = "black";
	String BLUE = "blue";
	String FUCHSIA = "fuchsia";
	String GRAY = "gray";
	String GREEN = "green";
	String LIME = "lime";
	String MAROON = "maroon";
	String NAVY = "navy";
	String OLIVE = "olive";
	String PURPLE = "purple";
	String RED = "red";
	String SILVER = "silver";
	String TEAL = "teal";
	String YELLOW = "yellow";
	String WHITE = "white";

	String[] COLORS = new String[]
	{
		BLACK, WHITE, GRAY, SILVER,
		RED, GREEN, BLUE, PURPLE, YELLOW,
		MAROON, TEAL, OLIVE, NAVY, 
		FUCHSIA, LIME, AQUA
	};
			
	/* input type attribute values */
	String CHECKBOX = "checkbox";
	String FILE = "file";
	String HIDDEN = "hidden";
	String IMAGE = "image";
	String PASSWORD = "password";
	String RADIO = "radio";
	String RESET = "reset";
	String SUBMIT = "submit";
	String TEXT = "text";
	String SELECTED = "selected";
	String CHECKED = "checked";
	String MAXLENGTH = "maxlength";
	String DISABLED = "disabled";

	/* Elements */
	String NBSP = "&nbsp;";
	String AMP = "&amp;";
	String LT_ENT = "&lt;";
	String GT_ENT = "&gt;";
	String COPY = "&copy;";
	String REG = "<sup>&reg;</sup>";
	String TM = "<font size=\"-1\"><sup>tm</sup></font>";
	String ACUTE = "&acute;";
	String LAQUO = "&laquo;";
	String RAQUO = "&raquo;";
	String IEXCL = "&iexcl;";
	String IQUEST = "&iquest;";
	String CAP_THORN = "&thorn;";
	String THORN = "&thorn;";
	String SZLIG = "&szlig;";
	String SECT = "&sect;";
	String PARA = "&para;";
	String MICRO = "&micro;";
	String BRVBAR = "&brvbar;";
	String PLUSMN = "&plusmn;";
	String MIDDOT = "&middot;";
	String ORDM = "&ordm;";
	String NOT = "&not;";
	String SHY = "&shy;";
	String MACR = "&macr;";
	String DEG = "&deg;";
	String SUP1 = "&sup1;";
	String SUP2 = "&sup2;";
	String SUP3 = "&sup3;";
	String FRAC14 = "&frac14;";
	String FRAC12 = "&frac12;";
	String FRAC34 = "&frac34;";
	String TIMES = "&times;";
	String DIVIDE = "&divide;";
	String CENT = "&cent;";
	String POUND = "&pound;";
	String CURREN = "&curren;";
	String YEN = "&yen;";
	
	/* CSS Values */
	// <link type="text/css" rel="stylesheet" href="webgets.css">

	String CLASS = "class";
	String REL = "rel";
	String TEXT_CSS = "text/css";
	String STYLESHEET = "stylesheet";
	
	/* Webgets standard CSS classes */
	String CSS_BLUE = "webgets-blue";
	String CSS_MEDIUM_BLUE = "webgets-medium-blue";
	String CSS_LIGHT_BLUE = "webgets-light-blue";
	String CSS_GREY = "webgets-grey";
	String CSS_MEDIUM_GREY = "webgets-meduium-grey";
	String CSS_LIGHT_GREY = "webgets-light-grey";
	String CSS_HILIGHT = "webgets-hilight";
	String CSS_SELECTED = "webgets-selected";
	String CSS_DISABLED = "webgets-disabled";
}
