package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Works just like superclass Label, except that the label text is trimmed using <tt>TagWriter.trimString()</tt>.

*/
public class ShortLabel extends Label
{
	public ShortLabel( String label, Component labeled)
	{
		super( TagWriter.trimString( label), labeled);
	}

	public ShortLabel( String label, String labeled)
	{
		super( TagWriter.trimString( label), labeled);
	}
}
