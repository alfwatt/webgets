package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

A List of Components. Each component in the list is drawn followed by a <br>.

For example:

List l = new LIst();
l.addComponent( new Block( "One"));
l.addComponent( new Block( "Two"));
l.addComponent( new Block( "Three"));

Draws as:

One<br>
Two<br>
Three<br>

List traps for child drawing exceptions (which must be RuntimeExceptions) and makes a comment of the exceptions message,
a warning is also printed to the log.

*/
public class List extends org.webgets.ContainerAdapter
{

	//
	// Constructors from ComponentAdatper
	
	public List()
	{
		super();
	}
	
    public List( String label)
    {
        super( label);
    }
    
    public List( Component delegate)
    {
    	super( delegate);
    }
    
    public List( String label, Component add)
    {
    	super( label, add);
    }
    
    public List( String label, Component[] add)
    {
    	super( label, add);
    }
    
    //
    // Instance Methods
    
    public void onDraw( TagWriter out)
    {
		super.traceDraw();
		
		int index = 0;
		Component[] components = this.getComponents();
		
		while ( index < components.length)
		{
			components[index++].onDraw( out);
			out.emptyTag( HTML.BR);
		}

		super.traceEndDraw();
    }
}
