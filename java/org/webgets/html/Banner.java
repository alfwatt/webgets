package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Banner is a Container that draws it's contents on a single table row.
The table backgound is printed in the dmColor except the first cell,
which shows the Banner's label.

*/
public class Banner extends ContainerAdapter
{
	//
	// Constructors from ContainerAdapter
	
	private Attributes tableAttrs = new Attributes( HTML.CLASS, HTML.CSS_BLUE);
	private Attributes cellAttrs = new Attributes( HTML.CLASS, HTML.CSS_LIGHT_BLUE);

	public Banner()
	{
		super();
	}
	
	public Banner( String label)
	{
		super( label);
	}
	
	public Banner( Component delegate)
	{
		super( delegate);
	}
	
	public Banner( String label, Component add)
	{
		super( label, add);
	}
	
	public Banner( String label, Component[] add)
	{
		super( label, add);
	}

	public void onDraw( TagWriter out)
	{
		super.traceDraw();
		out.tag( HTML.TABLE, this.tableAttrs);
			out.tag( HTML.TR);
				out.tag( HTML.TH);
					out.print( this.getLabel());
				out.closeTag();
			
				int index = 0;
				Component[] components = this.getComponents();
				while ( index < components.length)
				{
					out.tag( HTML.TD, this.cellAttrs);
						components[index++].onDraw( out);
					out.closeTag();
				}
			out.closeTag();
		out.closeTag();
		super.traceEndDraw();
	}
}
