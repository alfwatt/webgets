package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

A list which draws the label of each component on the left hand side
in a demoted font.

*/
public class LabeledList extends ContainerAdapter implements HTML
{
    private static String[]        TABLE_ATTRS = new String[] { CELLSPACING, "0", CELLPADDING, "2"};
	private static String[]     LABEL_TD_ATTRS = new String[] { ALIGN, RIGHT, VALIGN, TOP};
	private static String[] COMPONENT_TD_ATTRS = new String[] { ALIGN, LEFT,  VALIGN, TOP};

	private Decorator labelDecorator = Font.MINUS_1;

	//
	// Constructors from ContainerAdapter
	
	public LabeledList()
	{
		super();
	}
	
    public LabeledList( String label)
    {
        super( label);
    }

	public LabeledList( Component add)
	{
		super( add);
	}
	
	public LabeledList( String label, Component add)
	{
		super( label, add);
	}
	
	public LabeledList( String label, Component[] add)
	{
		super( label, add);
	}

	//
	// Custom Constructors
	
	public LabeledList( String label, Decorator deco)
	{
		this( label);
		this.setLabelDecorator( deco);
	}
	
	public void setLabelDecorator( Decorator deco)
	{
		if ( deco == null) 
			throw new IllegalArgumentException( "LabeledList.setLabelDecorator() cannot be null");
		this.labelDecorator = deco;
	}
	
    public void onDraw( TagWriter out)
    {
		super.traceDraw();		

		out.tag( HTML.TABLE, TABLE_ATTRS);
		
		int index = 0;
		Component[] components = this.getComponents();

		while ( index < components.length)
		{
			Component comp = components[index++];

			out.tag( HTML.TR);
				out.tag( HTML.TD, LABEL_TD_ATTRS);
					out.tag( HTML.B);
						this.labelDecorator.onDraw( out, comp.getLabel());
					out.closeTag();
				out.closeTag();

				out.tag( HTML.TD, COMPONENT_TD_ATTRS);
					comp.onDraw( out);
				out.closeTag();
			out.closeTag();
		}

        out.closeTag();

		super.traceEndDraw();
    }
}

/* Love, Alf */
