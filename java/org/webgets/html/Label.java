package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

public class Label extends DelegatorAdapter
{
	private static final String NULL_LABEL = "";

	private String label = null;

	public Label( String label, Component delegate)
	{
		super( delegate);
		this.setLabel( label);
	}

	public Label( String label, String delegate)
	{
		super( new StringComp( delegate));
		this.setLabel( label);
	}

	public void setLabel( String label)
	{	
		this.label = label;
	}

	public String getLabel()
	{
		return this.label;
	}
}
