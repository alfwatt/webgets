package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Align is a Container which draws a table with 100% width, the table has
a single cell which is aligned LEFT, CENTER or RIGHT, the cell can also
be valigned TOP, MIDDLE or BOTTOM.

By default an align will center it's aligned component inside the
container vertically and horizontally.

Align extends ContainerAdapter and will align any number of Components.
Align does not support any coloring operations.

*/
public class Align extends ContainerAdapter
{
	public static final String LEFT   = HTML.LEFT;
	public static final String CENTER = HTML.CENTER;
	public static final String RIGHT  = HTML.RIGHT;

	public static final String TOP    = HTML.TOP;
	public static final String MIDDLE = HTML.MIDDLE;
	public static final String BOTTOM = HTML.BOTTOM;

	private static final String[] MAX_SIZE = new String[] { HTML.WIDTH, "100%"};
	
	private Attributes alignAttrs = new Attributes( 
		new String[] { HTML.ALIGN, CENTER, HTML.VALIGN, MIDDLE});

	public Align()
	{
		super();
	}

	public Align( String label)
	{
		super( label);
	}

	public Align( Component aligned)
	{
		super( aligned);
	}
	
	public Align( String label, Component aligned)
	{
		super( label, aligned);
	}
	
	public Align( String label, Component[] aligned)
	{
		super( label, aligned);
	}

	public Align( Component aligned, String horzAlignment)
	{
		this( aligned);
		this.setHorzAlignment( horzAlignment);
	}

	public Align( Component aligned, String horzAlignment, String vertAlignment)
	{
		this( aligned);
		this.setHorzAlignment( horzAlignment);
		this.setVertAlignment( vertAlignment);
	}

	public void setHorzAlignment( String horzAlignment)
	{
		if ( horzAlignment != CENTER && horzAlignment != LEFT && horzAlignment != RIGHT)
			throw new IllegalArgumentException( "Align.setHorzAlignment() is not recognized, "
				+ "try Align.CENTER, Align.LEFT or Align,RIGHT");
				
		this.alignAttrs.put( HTML.ALIGN, horzAlignment);
	}

	public void setVertAlignment( String vertAlignment)
	{
		if ( vertAlignment != MIDDLE && vertAlignment != TOP && vertAlignment != BOTTOM)
			throw new IllegalArgumentException( "Align.setVertAlignment() is not recognized, "
				+ "try Align.MIDDLE, Align.TOP or Align.BOTTOM");
				
		this.alignAttrs.put( HTML.VALIGN, vertAlignment);
	}

	public void onDraw( TagWriter out)
	{
		out.tag( HTML.TABLE, MAX_SIZE);
			out.tag( HTML.TR);
				out.tag( HTML.TD, this.alignAttrs);
					super.onDraw( out);
		out.closeTags( 3);
	}
}
