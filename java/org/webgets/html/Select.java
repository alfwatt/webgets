package org.webgets.html;

import java.util.StringTokenizer;

import org.webgets.*;
import org.webgets.util.*;

public class Select extends ComponentAdapter
{
	private static String[] SELECTED_OPTION = new String[] { HTML.SELECTED, null};

    private boolean multiple;
	private String value;
	private Object[] options;
	private String[] labels = null;


	public Select( String name, String value, Object[] options)
	{
	    this( name, value, options, false );
    }

	public Select( String name, String value, Object[] options, boolean multiple )
	{
		super( name);
		
		this.multiple = multiple;
		this.value = value;		

		if ( options == null)
			throw new IllegalArgumentException( "Select() options must not be null!");
		this.options = options;
	}

	public Select( String name, String value, Object[] options, String[] labels)
	{
	    this( name, value, options, labels, false );
	}

	public Select( String name, String value, Object[] options, String[] labels, boolean multiple)
	{
		this( name, value, options, multiple );
		
		if ( labels == null || labels.length != options.length)
			throw new IllegalArgumentException( 
				"Select() labels must not be null and must have the same number as options");
		
		this.labels = labels;
	}

	public String getName()
	{
		return this.getLabel();
	}
	
	public void setName( String name)
	{
		this.setLabel( name);
	}
	
	public String getValue()
	{
		return this.value;
	}
	
	public void setValue( String value)
	{	
		this.value = value;
	}
		
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		
		// TODO listen for selection changes
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		
		if(!multiple) out.tag( HTML.SELECT, HTML.NAME, this.getName(), false);
		else          out.tag( HTML.SELECT, new String[]{
						 HTML.NAME, this.getName(), HTML.SIZE, "5", HTML.MULTIPLE, null } );
		
		int index = 0;
		while ( index < this.options.length)
		{
			String label  = this.labels != null ? this.labels[index] : this.options[index].toString();
			String option = this.options[index++].toString();

			if ( this.value != null && (( !multiple && option.equalsIgnoreCase(this.value)) 
			                           || (multiple && optionSelected(option))))
				out.tag( HTML.OPTION, new String[] { HTML.VALUE, option, HTML.SELECTED, null });
			else
				out.tag( HTML.OPTION, HTML.VALUE, option, false);
				
			out.print( label);	
			out.closeTag();
		}
		out.closeTag();
	}
	
	private boolean optionSelected( String option)
	{
	    boolean selected = false;
	    
	    StringTokenizer st = new StringTokenizer( value, "," );
	    
	    while( st.hasMoreTokens() )
	    {
	        String token = st.nextToken();
	        
	        if( token.startsWith( option ) )
	        {
	            selected = true;
	            break;
	        }
	    }
	    
	    return selected;
	}
}
