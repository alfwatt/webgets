package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

This is a raw Block of cdata, usefull for inlining hand-tooled html 
snippets or anything else you want, even simple text. 

*/
public class Block extends ComponentAdapter implements Link.Linkable, Flow.Flowable
{
	public static final Block BR = new Block( "", "<br>");
	public static final Block HR = new Block( "", "<hr>");

    private String cdata = null;
    
    public Block()
    {
    	super();
    }
    
    public Block( String cdata)
    {
        super();
        this.setCdata( cdata);
    }
    
    public Block( String label, String cdata)
    {
    	this( cdata);
    	this.setLabel( label);
    }

	public void setCdata( String cdata)
	{
		if ( cdata == null)
			throw new IllegalArgumentException( "Block cdata cannot be null");
		this.cdata = cdata;
	}

    public String getLabel()
    {
        String label = super.getLabel();
        if ( label == null && this.cdata != null)
            label = TagWriter.trimString( this.cdata);
        return label;
    }
    
    public void onDraw( TagWriter out)
    {
    	super.onDraw( out);
        out.write( cdata);
    }
}
