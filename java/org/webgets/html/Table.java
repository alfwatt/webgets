package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

An HTML Table can have any width and any number of components can be added to it.

*/
public class Table extends ContainerAdapter implements HTML
{
	private static final String[] TABLE_ATTRS = new String[] { 
		CELLSPACING, "2", 
		CELLPADDING, "2", 
		BORDER, "0"
	};
	
	private static final String[] TR_ATTRS = new String[] {
		VALIGN, "top"
	};

	private int width = 3;
	
	//
	// Constructors from ComponentAdapter
	
	public Table()
	{
		super();
	}
	
	public Table( String label)
	{
		super( label);
	}
	
	public Table( Component add)
	{
		super( add);
	}
	
	public Table( String label, Component add)
	{
		super( label, add);
	}
	
	public Table( String label, Component[] add)
	{
		super( label, add);
	}

	//
	// Custom Constructors
			
    public Table( int width)
    {
    	this();
    	this.width = width;
    }

	public Table( String label, int width)
	{
		super( label);
		this.width = width;
	}
	
    //
    // Instance Methods
    
    public void onDraw( TagWriter out)
    {
    	super.traceDraw();

		out.mark();
    	out.tag( TABLE, TABLE_ATTRS);
            int index = 0;
			Component[] components = this.getComponents();
			
            while ( index < components.length)
            {
            	if ( index % this.width == 0) out.tag( TR, TR_ATTRS);
				out.tag( TD);
					components[index++].onDraw( out);
				out.closeTag(); // TD
				if ( index % this.width == 0) out.closeTag(); // TR
            }
        out.closeToMark(); // little sloppy but it's much easier this way!

		super.traceEndDraw();
    }
}
