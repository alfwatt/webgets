package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Spacer draws an invisible spacer with the approximate pixel
dimensions requested.

This implementation uses an HTML table to do it's dirty work.

*/
public class Spacer extends ComponentAdapter implements HTML
{
	private static final String SPACER_LABEL = "&nbsp;";

	private Attributes spacerAttrs = new Attributes();
	
	public Spacer( int width, int height)
	{
		this.setWidth( width);
		this.setHeight( height);
	}

	public void setWidth( int width)
	{
		if ( width < 0)
			throw new IllegalArgumentException( "Spacer width cannot be < 0");
		 
		this.spacerAttrs.put( WIDTH, Integer.toString( width));
	}
	
	public void setHeight( int height)
	{
		if ( height < 0)
			throw new IllegalArgumentException( "Spacer heigth cannot be < 0");
			
		this.spacerAttrs.put( HEIGHT, Integer.toString( height));
	}

	public String getLabel()
	{
		return SPACER_LABEL;
	}

	private static final String[] TABLE_ATTRS = new String[] { CELLSPACING, "0", CELLPADDING, "0"};
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( HTML.TABLE, TABLE_ATTRS);
			out.disableIndent();
			out.tag( HTML.TR);
				out.tag( HTML.TD, this.spacerAttrs);
				out.closeTag();
			out.closeTag();				
		out.closeTag();				
		out.enableIndent();
	}
}

