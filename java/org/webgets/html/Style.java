package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Style extends ComponentAdpter and uses a Decorator to provide a shim for decorating
single components in an application. This is especially helpfull when converting from
the previous version of Font. You used to be able to write:

	Component c = new Font( "Foo", "+2");

But Font is not a Component any more it's a Decorator, so now you have to write:

	Compooent c = new Style( "Foo", new Font( "+2"));
	
or even better:

	Component c = new Style( "Foo", Font.PLUS_2);
	
or mabye even:

	Component c = new Style( "Foo", Font.SANS_SIZE_7);

See Font for other text styles.

Style also contains Decorator classes for the various HTML text styles:

	Style.B.onDraw( TagWriter, Component|String)		<b></b>
	Style.BIG.onDraw( TagWriter, Component|String)		<big></big>
	Style.I.onDraw( TagWriter, Component|String)		<i></i>
	Style.SMALL.onDraw( TagWriter, Component|String)	<small></small>
	Style.STRIKE.onDraw( TagWriter, Component|String)	<strike></strike>
	Style.TT.onDraw( TagWriter, Component|String)		<tt></tt>
	Style.U.onDraw( TagWriter, Component|String)		<u></u>
	Style.CENTER.onDraw( TagWriter, Component|String) 	<center></center>

*/
public final class Style extends ComponentAdapter implements Flow.Flowable, Link.Linkable
{

	public static Decorator B = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.B decorating non-Flowable & non-Linkable: " 
						 + delegate.toString());
					
			out.tag( HTML.B);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.B);
				out.print( text);
			out.closeTag();
		}
	};

	public static Decorator BIG = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.BIG decorating non-Flowable & non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.BIG);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.BIG);
				out.print( text);
			out.closeTag();
		}
	};

	public static Decorator I = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.I decorating non-Flowable, non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.I);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.I);
				out.print( text);
			out.closeTag();
		}
	};

	public static Decorator SMALL = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.SMALL decorating non-Flowable, non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.SMALL);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.SMALL);
				out.print( text);
			out.closeTag();
		}
	};
	
	public static Decorator STRIKE = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.BIG decorating non-Flowable, non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.STRIKE);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.STRIKE);
				out.print( text);
			out.closeTag();
		}
	};
	
	public static Decorator TT = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.TT decorating non-Flowable, non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.TT);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.TT);
				out.print( text);
			out.closeTag();
		}
	};

	public static Decorator U = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.U decorating non-Flowable, non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.U);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.U);
				out.print( text);
			out.closeTag();
		}
	};

	public static Decorator CENTER = new Decorator()
	{
		public void onDraw( TagWriter out, Component delegate)
		{
			if ( Debug.DEBUG)
				if ( ! (delegate instanceof Flow.Flowable && delegate instanceof Link.Linkable))
					Log.printWarn( "Style.U decorating non-Flowable, non-Linkable: " 
						+ delegate.toString());

			out.tag( HTML.CENTER);
				delegate.onDraw( out);
			out.closeTag();
		}
		
		public void onDraw( TagWriter out, String text)
		{
			out.tag( HTML.CENTER);
				out.print( text);
			out.closeTag();
		}
	};


	private Component delegate = null;
	private Decorator decorator = null;
    
    public Style( String text, Decorator decorator)
    {
    	this( new StringComp( text), decorator);
    }
    
    public Style( Component delegate, Decorator decorator)
    {
        super( delegate.getLabel());
        this.setDelegate( delegate);
        this.setDecorator( decorator);
    }

	public Component getDelegate()
	{
		return this.delegate;
	}
	
	public void setDelegate( Component delegate)
	{
		if ( delegate == null)
			throw new IllegalArgumentException( "DecoratorAdapter.setDelegate() must not be null");
		this.delegate = delegate;
	}

	public Decorator getDecorator()
	{
		return this.decorator;
	}

	public void setDecorator( Decorator decorator)
	{
		if ( decorator == null)
			throw new IllegalArgumentException( "DecoratorAdapter.setDecorator() must not be null");
		this.decorator = decorator;
	}

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		this.decorator.onDraw( out, this.delegate);
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		this.delegate.onRequest( request);
	}
}	
