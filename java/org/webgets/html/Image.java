package org.webgets.html;

import java.net.*;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.webgets.*;
import org.webgets.util.*;

/**

An HTML &lt;img ...&gt; tag. Supports width, height and alt attributes. The alt text
is mapped to the images Label.

*/
public class Image extends ComponentAdapter implements Link.Linkable, Flow.Flowable
{
	private static final String DEFAULT_LOCAL_PATH = "./images/";

	private static Map PATH_REGISTRY = new HashMap();

	public static void registerLocalPath( HttpServletRequest request, Object path)
	{
		String requestURL = javax.servlet.http.HttpUtils.getRequestURL( request).toString();
		Image.registerLocalPath( requestURL, path);
	}

	public static void registerLocalPath( String url, Object path)
	{
		PATH_REGISTRY.put( url, path);
	}
	
	public static void unregisterLocalPath( String url)
	{
		PATH_REGISTRY.remove( url);
	}
	
	public static String getLocalPath( String url)
	{
		Object pathObject = PATH_REGISTRY.get( url);
		return pathObject == null ? DEFAULT_LOCAL_PATH : pathObject.toString();
	}

	public static Image createLocalImage( String image)
	{
		Image local = new Image( image);
		local.setLocal( true);
		return local;
	}
	
	public static Image createLocalImage( String label, String image)
	{
		Image local = new Image( label, image);
		local.setLocal( true);
		return local;
	}
	
	public static Image createLocalImage( String label, String image, int width, int height)
	{
		Image local = new Image( label, image, width, height);
		local.setLocal( true);
		return  local;
	}

	private boolean local = false;
	private String localSrc = null;
	private Attributes attrs = new Attributes( new String[] { HTML.BORDER, "0"});

	//
	// 

	public Image( String url)
	{
		super( url);
		this.setLocalSrc( url);
	}

	public Image( String label, String url)
	{
		this( url);
		this.setAlt( label);
	}

	public Image( String label, String url, int width, int height)
	{
		this( label, url);
		this.setWidth( width);
		this.setHeight( height);
	}

	public Image( URL image)
	{
		this( image.toExternalForm());
	}

	private void setLocalSrc( String src)
	{
		this.localSrc = src;
		this.setSrc( src);
	}
	
	private String getLocalSrc()
	{
		return this.localSrc;
	}

	public void setSrc( String src)
	{
		this.attrs.put( HTML.SRC, src);
	}

	public String getSrc()
	{
		return this.attrs.getAsString( HTML.SRC);
	}

	/** @deparacated use Image.setSrc( String) */
	public void setImage( String image)
	{
		this.setSrc( image);
	}

	/** @deparacated use Image.getSrc() */
	public String getImage()
	{
		return this.getSrc();
	}

	public void setWidth( int width)
	{
		this.attrs.put( HTML.WIDTH, new IntValue( width));
	}
	
	public int getWidth()
	{
		Object width = this.attrs.get( HTML.WIDTH);
		if ( width != null)
			return ((IntValue) width).getIntValue();
		else
			return -1;
	}
	
	public void setHeight( int height)
	{
		this.attrs.put( HTML.HEIGHT, new IntValue( height));
	}

	public int getHeight()
	{
		Object height = this.attrs.get( HTML.HEIGHT);
		if ( height != null)
			return ((IntValue) height).getIntValue();
		else
			return -1;
	}

	public void setAlt( String alt)
	{
		this.setLabel( alt);
		this.attrs.put( HTML.ALT, alt);
	}
	
	public String getAlt()
	{
		return this.attrs.getAsString( HTML.ALT);
	}
	
	public boolean isLocal()
	{
		return this.local;
	}

	public void setLocal( boolean local)
	{
		this.local = local;
	}

	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		if ( this.isLocal())
			this.setSrc( Image.getLocalPath( out.getRequestURL()) + this.localSrc);
		out.emptyTag( HTML.IMG, this.attrs);
	}
}
