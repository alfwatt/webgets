package org.webgets.html;

import org.webgets.*;
import org.webgets.util.*;

/**

Draws the HTML, HEAD and BODY tags, using the label as the document
title and providing for a bgcolor and/or background attribute. Documents
can also have a refresh time which is implemented using the HTTP-EQUIV
Refresh attribute.

Documents are Containers and use ContainerAdapter's onDraw() method to
deal with drawing nested Components.

*/
public class Document extends org.webgets.ContainerAdapter
{
	private static String DOCTYPE_HTML_3_2 
		= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">";
	private static String DOCTYPE_HTML_4_01 
		= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" "
		+ "\"http://www.w3.org/TR/html4/loose.dtd\">";
	// TODO add XHTML 1.0 doctype and support it!
	private static final String WEBGETS_HTML_CSS = "webgets-html.css";
	private static final String UNTITLED = "Untitled Page";
	
	private static Component credit = new Block( 
		"This page built with <a href=\"http://webgets.org\">Webgets Components</a>");
	
	private int refresh = 0;
	private Attributes bodyAttrs = new Attributes();
	private Attributes styleAttrs = new Attributes( 
		HTML.REL,	HTML.STYLESHEET, 
		HTML.TYPE,	HTML.TEXT_CSS, 
		HTML.HREF,	WEBGETS_HTML_CSS);

	public Document()
	{
		super();
	}

	public Document( String label)
	{
		super( label);
	}

	public Document( Component add)
	{	
		super( add);
	}
	
	public Document( String label, Component add)
	{
		super( label, add);
	}
	
	public Document( String label, Component[] add)
	{
		super( label, add);
	}

	//
	// Custom Constructors

	/**
	
	Create a new Document with the specified label and refresh frequency in seconds.
	
	@param label the Document title
	@param refresh refresh time of the document in seconds

	*/
	public Document( String label, int refresh)
    {
        this( label);
        this.setRefresh( refresh);
    }

	//
	// Instance Methods
	
	public String getStyleType()
	{
		return this.styleAttrs.getAsString( HTML.TYPE);
	}
	
	public void setStyleType( String type)
	{
		Check.isNotNull( type);
		this.styleAttrs.put( HTML.TYPE, type);
	}

	public String getStyleHref()
	{
		return this.styleAttrs.getAsString( HTML.HREF);
	}
		
	public void setStyleHref( String href)
	{
		Check.isNotNull( href);
		this.styleAttrs.put( HTML.HREF, href);
	}
	
	/**

	The background color of the Document this is not related to the
	ColorSet shared by the rest of the components becuase it's often the
	case that the bgcolor of the document will be different than that of
	the components.
	
	@param bgcolor the background color of this document. 
	*/    
    public void setBgColor( String bgcolor)
    {
    	this.bodyAttrs.put( HTML.BGCOLOR, bgcolor);
    }
    
    /**
    
    Set the BACKGROUND of the Document.
    
    @param background BACKGROUND of the document
    
    */
    public void setBackground( String background)
    {
    	this.bodyAttrs.put( HTML.BACKGROUND, background);
    }
    
    /**
    
    Set the refresh frequency of the Document
    
    BUG: this will not work for session documents when using URL
    rewriting, the JSP session string uses a semicolon which is
    incompatable with the http-equiv tag which also uses a semicolon.
    
    @param refresh seconds between refreshes
    
    */
    public void setRefresh( int refresh)
    {
    	this.refresh = refresh;
    }
    
    public void onRequest( RequestEvent request)
    {
    	super.onRequest( request);
    	if ( this.refresh > 0)
    		request.getTargetResponse().setHeader( HTTP.REFRESH, Integer.toString( this.refresh));
    }
    
    public void onDraw( TagWriter out)
    {
    	out.getResponse().setHeader( HTTP.CACHE_CONTROL, HTTP.NO_CACHE);
		out.println( DOCTYPE_HTML_3_2);

        out.tag( HTML.DOCUMENT);
            out.tag( HTML.HEAD);

				/* sometimes all the fancy tag printing tricks in the world just don't help... 
				if ( this.refresh > 0)
				{
					out.println();
	            	out.print( "<meta http-equiv=\"Refresh\" content=\"");
	            	out.print( Integer.toString( this.refresh));
	            	out.print( "; url=");
	            	out.print( out.getRequestURL());
	            	out.println( "\">");
				}*/

                out.tag( HTML.TITLE);
                    out.write( this.getLabel());
                out.closeTag();
				out.emptyTag( HTML.LINK, this.styleAttrs);
            out.closeTag();
            out.tag( HTML.BODY, this.bodyAttrs);
	            super.onDraw( out);
	            
	            out.emptyTag( HTML.BR);
	            out.tag( HTML.CENTER);
	            out.tag( HTML.SMALL);
	            this.credit.onDraw( out);
	            if ( Debug.TIMERS)
	            {
	            	out.print( ",&nbsp;on&nbsp;");	     
	            	out.print( new java.util.Date());
	            }
	            if ( Debug.DEBUG)
	            {
	            	out.print( ",&nbsp;at&nbsp;");
	            	out.tag( HTML.A, HTML.HREF, out.getRequestURL(), false);
						out.print( out.getRequestURL());
	            	out.closeTag();
	            }
	            out.closeTag();
	            out.closeTag();
            out.closeTag();
        out.closeTag();   
    }
}
