package org.webgets.html;

import java.net.*;
import org.webgets.*;
import org.webgets.util.*;

/**

This class draws a simple HTML Link around some other component, that component
must implement the Link.Linkable interface to indicate that they can be
correctly linked to.

Links may be relative or absolute, local or remote. Unlike earlier versions of
this class, we do not require the use of a java.net.URL, though we support it 
for compatibility. Links take their alt text from the label of the Linkable you
provide. If the Linkable has no label no alt tag is written.

Note that the TagWriter that link passes to the Linkable will have indenting 
<i>turned off</i>! If you know that you want indenting turned back on you can
just enableIndent() first thing, please be sure to disableIndent() before you
exit. 

Links are also Flow.Flowable so they can be added to text flows.

*/
public class Link extends ComponentAdapter implements Flow.Flowable
{
	/**
	
	The linkable interface extends Component. Classes which can have an HTML 
	Anchor wrapped around them in the following fashion should implement this
	interface:
	
		<a href="http://www.webgets.org/"><!-- Linkable draws here --></a>
	
	*/
	public interface Linkable extends Component
	{
		/* Linkable defines no methods, it is a marker interface */
	}
	
	private String href = null;
	private String[] queryArgs = null;
	private Linkable body = null;
	private boolean local = true;

	public Link( String href, Linkable body)
	{
		super();
		this.setHref( href);
		this.setBody( body);
	}

	public Link( String href, String text)
	{
		this( href, new Block( text));
	}

	public Link( String href, String text, boolean local)
	{
		this( href, text);
		this.setLocal( local);
	}

	public Link( String href, Linkable body, boolean local)
	{
		this( href, body);
		this.setLocal( local);
	}

	public Link( String href, String[] queryArgs, Linkable body, boolean local)
	{
		this( href, body, local);
		this.setQueryArgs( queryArgs);
	}
	
	/** @deprecated */
	public Link( URL href, Linkable body)
	{
		this( href.toExternalForm(), body);
	}

	/** @deprecated */
	public Link( URL href, String body)
	{
		this( href, new Block( body));
	}

	/** @deprecated */
	public Link( URL href, Object body)
	{
		this( href, body.toString());
	}

	/** @deprecated */
	public Link( URL href, Object body, boolean local)
	{
		this( href, body);
		this.setLocal( local);
	}

	public void setHref( String href)
	{
		if ( href == null)
			throw new IllegalArgumentException( "link href cannot be null");
		this.href = href;
	}
	
	public void setBody( Linkable body)
	{
		if ( body == null)
			throw new IllegalArgumentException( "Link body (Linkable) cannot be null");
		this.body = body;
	}
	
	public  void setLocal( boolean local)
	{
		this.local = local;
	}

	public void setQueryArgs( String[] queryArgs)
	{
		this.queryArgs = queryArgs;
	}
		
	public String getLabel()
	{
		String label = super.getLabel();
		if ( label == null)
			label = this.body.getLabel();
		if ( label == null)
			label = this.href;
		return label;
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.mark();

		if ( this.queryArgs != null)
			out.tag( HTML.A, HTML.HREF, this.href, this.queryArgs, this.local);
		else
			out.tag( HTML.A, HTML.HREF, this.href, this.local);

		out.disableIndent();
			body.onDraw( out); 
		out.closeToMark();
		out.enableIndent();
		
	} 
}

