package org.webgets;

import org.webgets.util.*;

/**

<p>DelegatorAdapter provides basic methods and constructors to implement
the Delegator interface. Classes extending DelegatorAdapter have access
to the delegate property and constructors which set the delegate.</p>

*/
public abstract class DelegatorAdapter extends ComponentAdapter implements Delegator
{
	private Component delegate = null;

	public DelegatorAdapter()
	{
		super();
	}

	public DelegatorAdapter( String label)
	{
		super( label);
	}

	public DelegatorAdapter( Component delegate)
	{
		super( delegate.getLabel()); // bit of an implied Check.isNotNull()...
		this.setDelegate( delegate);
	}
	
	public DelegatorAdapter( String label, Component delegate)
	{
		super( label);
		this.setDelegate( delegate);
	}

	protected void setDelegate( Component delegate)
	{
		Check.isNotNull( delegate);
		this.delegate = delegate;
	}

	public Component getDelegate()
	{
		return this.delegate;
	}

	public boolean hasDelegate()
	{
		return this.delegate != null;
	}
	
	public String getLabel()
	{
		if ( this.hasDelegate())
			return this.getDelegate().getLabel();
		else
			return null;
	}
	
	public void init( javax.servlet.ServletConfig config) throws javax.servlet.ServletException
	{
		super.init( config);
		this.getDelegate().init( config);
	}
	
	public void destroy()
	{
		super.destroy();
		this.getDelegate().destroy();
	}
	
	public void onRequest( RequestEvent request)
	{
		super.onRequest( request);
		this.getDelegate().onRequest( request);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out); 
		this.getDelegate().onDraw( out);
	}
}
