package org.webgets;

/**

<p><code>Delegator extends Component</code> to provide for containers
having only one Component. The <code>delegate</code> property of the
Delegator is a Component which will listen for requests, potentially
draw on the screen and otherwise act as the child of the Delegator.</p>

<p>The implementation class of DelegatorAdapter defines the contract
with respet to children for this interface. either subclass it and call
super.method() where appropriate or study it's comments and
implementation.</p>

*/
public interface Delegator extends Component
{
	Component getDelegate();
}
