package org.webgets.text;

import java.text.*;
import java.util.Date;

/**  

The format registry is a simple registry of java.text.Format objects, it is initilized
with foramts for all the java primitive types and provides storage for short medium and
long formats.

When registering the default is to store a short format, methods are provided to register
a specific size as well.

When returning formats the default is to return the short format, if none is avaliable
we attempt to return a medium then a long format. When returning formats for a specific
size the a heuristic search is performed for the best match if the specific size cannot
be found.

When looking up formats the registry performs narrowing type conversion for you using
Class.getSuperclass(). If no format is registered for Object then you may be returned
a null pointer.

*/
public class FormatRegistry extends Object
{
	public static final int  SHORT_FORMAT = 0;
	public static final int MEDIUM_FORMAT = 1;
	public static final int   LONG_FORMAT = 2;
	private static final int FORMAT_COUNT = 3;

	private static java.util.HashMap registry = new java.util.HashMap();

	static
	{
		/* register formats for the primitive types, short medium and long */
		registerFormat( Object.class,   ObjectFormat.getInstance());

		registerFormat( Boolean.class, 	BooleanFormat.getInstance());
		registerFormat( Boolean.TYPE,  	BooleanFormat.getInstance());

		registerFormat( String.class, 	StringFormat.getInstance());
		
		registerFormat( Character.class,CharacterFormat.getInstance());
		registerFormat( Character.TYPE,	CharacterFormat.getInstance());

		registerFormat( Number.class,  	NumberFormat.getInstance());

		registerFormat( Integer.TYPE,  	NumberFormatWrappers.createIntegerWrapper( NumberFormat.getInstance()));
		registerFormat( Float.TYPE,    	NumberFormatWrappers.createFloatWrapper(   NumberFormat.getInstance()));
		registerFormat( Short.TYPE,    	NumberFormatWrappers.createShortWrapper(   NumberFormat.getInstance()));
		registerFormat( Byte.TYPE,     	NumberFormatWrappers.createByteWrapper(    NumberFormat.getInstance()));
		registerFormat( Long.TYPE,     	NumberFormatWrappers.createLongWrapper(    NumberFormat.getInstance()));
 		registerFormat( Double.TYPE,   	NumberFormatWrappers.createDoubleWrapper(  NumberFormat.getInstance()));

		registerFormat( Integer.class,	NumberFormatWrappers.createIntegerWrapper( NumberFormat.getInstance()));
		registerFormat( Float.class,   	NumberFormatWrappers.createFloatWrapper(   NumberFormat.getInstance()));
		registerFormat( Short.class,   	NumberFormatWrappers.createShortWrapper(   NumberFormat.getInstance()));
		registerFormat( Byte.class,    	NumberFormatWrappers.createByteWrapper(    NumberFormat.getInstance()));
		registerFormat( Long.class,		NumberFormatWrappers.createLongWrapper(    NumberFormat.getInstance()));
 		registerFormat( Double.class,	NumberFormatWrappers.createDoubleWrapper(  NumberFormat.getInstance()));
 		
 		registerFormat( Date.class,     DateFormat.getDateTimeInstance( DateFormat.SHORT,  DateFormat.SHORT),	 SHORT_FORMAT);
 		registerFormat( Date.class,     DateFormat.getDateTimeInstance( DateFormat.MEDIUM, DateFormat.MEDIUM),	MEDIUM_FORMAT);
 		registerFormat( Date.class,     DateFormat.getDateTimeInstance( DateFormat.LONG,   DateFormat.LONG),	  LONG_FORMAT);
 	}
	

	/**  */
	public static void registerFormat( Class clzz, Format format)
	{
		Object o = registry.get( clzz);
		
		java.text.Format[] formats;
		
		if ( o != null)
		{
			formats = (java.text.Format[]) o;
			formats[SHORT_FORMAT] = format;
		}
		else
		{
			formats = new java.text.Format[FORMAT_COUNT];
			formats[SHORT_FORMAT] = format;
			registry.put( clzz, formats);
		}
	}
	
	/**  */
	public static void registerFormat( Class clzz, java.text.Format format, int size)
	{
		if ( size < SHORT_FORMAT || size > LONG_FORMAT)
			throw new IllegalArgumentException( "FormatRegistry.registerFormat() size is out of range: " + size);
	
		Object o = registry.get( clzz);
		
		java.text.Format[] formats;
		
		if ( o != null)
		{
			formats = (java.text.Format[]) o;
			formats[size] = format;
		}
		else
		{
			formats = new java.text.Format[FORMAT_COUNT];
			formats[size] = format;
			registry.put( clzz, formats);
		}
	}
	
	/**  */
	public static java.text.Format getFormat( Class clzz)
	{
		Object o = registry.get( clzz);
		
		/* perform narrowing type conversion */
		while ( o == null && clzz != null)
		{
			clzz = clzz.getSuperclass();
			o = registry.get( clzz);
		}	
		
		if ( o != null)
		{
			java.text.Format[] formats = (java.text.Format[]) o;
			java.text.Format format = formats[SHORT_FORMAT];
			if ( format == null)
				format = formats[MEDIUM_FORMAT];
			else if ( format == null)
				format = formats[LONG_FORMAT];
				
			return format;
		}
		else
			return null;
	}
	
	/**  */
	public static java.text.Format getFormat( Class clzz, int size)
	{
		if ( size < SHORT_FORMAT || size > LONG_FORMAT)
			throw new IllegalArgumentException( "FormatRegistry.getFormat() size is out of range: " + size);
	
		Object o = registry.get( clzz);	
		
		/* perform narrowing type conversion the hard way */
		while ( o == null && clzz != null)
		{
			clzz = clzz.getSuperclass();
			o = registry.get( clzz);
		}	
		
		if ( o != null)
		{
			java.text.Format[] formats = (java.text.Format[]) o;
			java.text.Format format = formats[size];

			if ( format == null) /* perform a heuristic search */
				switch ( size)
				{
					case SHORT_FORMAT: /* try a MEDIUM format, then a LONG */
						format = formats[MEDIUM_FORMAT];
						if (format == null)
							format = formats[LONG_FORMAT];
					break;
					
					case MEDIUM_FORMAT: /* try a SMALL format, then a LONG */
						format = formats[SHORT_FORMAT];
						if ( format == null)
							format = formats[LONG_FORMAT];
					break;
					
					case LONG_FORMAT: /* try a MEDIUM format, then a SMALL */
						format = formats[MEDIUM_FORMAT];
						if ( format == null)
							format = formats[SHORT_FORMAT];
					break;
					
					default:
						throw new IllegalArgumentException( "Can't Happen");
				}
			
			return format;
		}
		else
			return null;
	}
	
	private FormatRegistry()
	{
		super();
	}
	
	public static void main( String[] arguments)
	{
		System.err.println( "FormatRegistry.main() registry static bindings");
		System.err.println();
		System.err.print( "getFormat( Number.class) ");
		System.err.println( getFormat( Number.class));
		System.err.print( "getFormat( Object.class) ");
		System.err.println( getFormat( Object.class));
		System.err.print( "getFormat( String.class) ");
		System.err.println( getFormat( String.class));
		System.err.println();
		System.err.print( "getFormat( Boolean.TYPE) ");
		System.err.println( getFormat( Boolean.TYPE));
		System.err.print( "getFormat( Character.TYPE) ");
		System.err.println( getFormat( Character.TYPE));
		System.err.print( "getFormat( Integer.TYPE) ");
		System.err.println( getFormat( Integer.TYPE));
		System.err.print( "getFormat( Float.TYPE) ");
		System.err.println( getFormat( Float.TYPE));
		System.err.print( "getFormat( Short.TYPE) ");
		System.err.println( getFormat( Short.TYPE));
		System.err.print( "getFormat( Byte.TYPE) ");
		System.err.println( getFormat( Byte.TYPE));
		System.err.print( "getFormat( Long.TYPE) ");
		System.err.println( getFormat( Long.TYPE));
		System.err.print( "getFormat( Double.TYPE) ");
		System.err.println( getFormat( Double.TYPE));
		System.err.println();
		System.err.print( "getFormat( Boolean.class) ");
		System.err.println( getFormat( Boolean.class));
		System.err.print( "getFormat( Character.class) ");
		System.err.println( getFormat( Character.class));
		System.err.print( "getFormat( Integer.class) ");
		System.err.println( getFormat( Integer.class));
		System.err.print( "getFormat( Short.class) ");
		System.err.println( getFormat( Short.class));
		System.err.print( "getFormat( Float.class) ");
		System.err.println( getFormat( Float.class));
		System.err.print( "getFormat( Byte.class) ");
		System.err.println( getFormat( Byte.class));
		System.err.print( "getFormat( Long.class) ");
		System.err.println( getFormat( Long.class));
		System.err.print( "getFormat( Double.class) ");
		System.err.println( getFormat( Double.class));
		System.err.println();
		System.err.print( "getFormat( Date.class) ");
		System.err.println( getFormat( Date.class));

	}
}
