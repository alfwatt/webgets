package org.webgets.text;

public class CharacterFormat extends java.text.Format
{
	private static CharacterFormat SINGELTON = new CharacterFormat();
	
	public static CharacterFormat getInstance()
	{
		return SINGELTON;
	}

	private CharacterFormat()
	{
		super();
	}
	
	/**
	
	format() invokes the toString method of the object
	
	*/
	public StringBuffer format( Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos)
	{
		if ( obj != null)
			toAppendTo.append( obj.toString().charAt(0));
		
		return toAppendTo;
	}
	
	/**
	
	parseObject() simply returns the string after updading the ParsePosition
	
	*/
	public Object parseObject( String source, java.text.ParsePosition status)
	{
		status.setIndex( status.getIndex() + 1);
		return new Character( source.charAt(0));
	}
}
