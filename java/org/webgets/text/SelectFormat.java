package org.webgets.text;

/**

*/
public class SelectFormat extends StringFormat
{
	private String[] options;
	private int selected = 0;

	public SelectFormat getInstance( String[] options)
	{
		return new SelectFormat( options, 0);
	}

	public SelectFormat getDaysInstance()
	{
		return null;
	}

	public SelectFormat getWeekdayInstance()
	{
		return null;
	}
	
	public SelectFormat getMonthInstance()
	{
		return null;
	}

	public SelectFormat getYearsInstance()
	{
		return null;
	}
	
	public SelectFormat getYearsInstance( int startYear, int endYear)
	{
		return null;
	}
	
	public SelectFormat( String[] options)
	{
		super();
		this.setOptions( options);
	}
	
	public SelectFormat( String[] options, int selected)
	{
		super();
		this.setOptions( options);
		this.setSelected( selected);
	}
	
    public StringBuffer format( Object object, StringBuffer appendTo, java.text.FieldPosition status)
    {
    	/* TODO check the Object to make sure: it's a string and it's one of the options specified, update selected */
    	return super.format( object, appendTo, status);
    }
    
    public Object parseObject( String string, java.text.ParsePosition status)
    {
    	Object object = super.parseObject( string, status);

    	/* TODO, check object to make sure: it's a string and it's one of the options specified, update selected */

    	return object;
    }
    
    public String[] getOptions()
    {
    	return this.options;
    }
    
    public void setOptions( String[] options)
    {
    	this.options = options;
    }
    
    public int getSelected()
    {
    	return this.selected;
   	}
  	
  	public void setSelected( int selected)
  	{
  		if ( selected < 0 || selected > options.length)
  			throw new IllegalArgumentException( "SelectFormat.setSelected() selected out of range: " + selected);
  		
  		this.selected = selected;
  	}
  	
  	public boolean isSelected( int index)
  	{
  		return index == this.selected;
  	}
}
