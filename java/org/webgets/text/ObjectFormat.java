package org.webgets.text;

public class ObjectFormat extends java.text.Format
{
	private static ObjectFormat SINGELTON = new ObjectFormat();
	
	public static ObjectFormat getInstance()
	{
		return SINGELTON;
	}

	private ObjectFormat()
	{
		super();
	}
	
	/**
	
	format() invokes the toString method of the object
	
	*/
	public StringBuffer format( Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos)
	{
		if ( obj != null)
			toAppendTo.append( obj.toString());
		
		return toAppendTo;
	}
	
	/**
	
	parseObject() always results in a ParseExceptoin
	
	*/
	public Object parseObject( String source, java.text.ParsePosition status)
	{
		return null;
	}
}
