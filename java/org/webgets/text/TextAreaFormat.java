package org.webgets.text;

/**

TextAreaFormat has attributes relating to text areas, they can be set to alter the display
of a string to be a html <textarea> in a webgets editor rathern than an <input type="text">.
Method are given for getting the rows and cols as integers, the values can only be set in 
the constructor.

*/
public class TextAreaFormat extends StringFormat
{
	private int rows;
	private int cols;

	public TextAreaFormat( int rows, int cols)
	{
		super();
		this.rows = rows;
		this.cols = cols;
	}

	public int getRows()
	{
		return this.rows;
	}
	
	public int getCols()
	{
		return this.cols;
	}	
}
