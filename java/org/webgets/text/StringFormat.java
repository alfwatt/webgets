package org.webgets.text;

public class StringFormat extends java.text.Format
{
	private static StringFormat SINGELTON = new StringFormat();
	
	public static StringFormat getInstance()
	{
		return SINGELTON;
	}

	protected StringFormat()
	{
		super();
	}
	
	/**
	
	format() invokes the toString method of the object
	
	*/
	public StringBuffer format( Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos)
	{
		if ( obj != null)
			toAppendTo.append( obj.toString());
		
		return toAppendTo;
	}
	
	/**
	
	parseObject() simply returns the string after updading the ParsePosition
	
	*/
	public Object parseObject( String source, java.text.ParsePosition status)
	{
		if ( source.length() > 0)
			status.setIndex( status.getIndex() + source.length());
		else
			status.setIndex( status.getIndex() + 1);
		return source;
	}
}
