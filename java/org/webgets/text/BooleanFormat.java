package org.webgets.text;

public class BooleanFormat extends java.text.Format
{
	private static final String  MICRO_FALSE_STRING = "";
	private static final String   SHORT_TRUE_STRING = "yes";
	private static final String  SHORT_FALSE_STRING = "no";
	private static final String  MEDIUM_TRUE_STRING = "true";
	private static final String MEDIUM_FALSE_STRING = "false";
	private static final String    LONG_TRUE_STRING = "is true";
	private static final String   LONG_FALSE_STRING = "is false";
 
 	private static BooleanFormat SINGLETON = new BooleanFormat();
 
	public static BooleanFormat getInstance()
	{
		return SINGLETON;
	}

	private BooleanFormat()
	{
		super();
	}
	
	public StringBuffer format( Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos)
	{
		/* TODO check the FieldPosition space pad to the right, minus one */
		return toAppendTo.append( ((Boolean) obj).booleanValue() ? MEDIUM_TRUE_STRING : MEDIUM_FALSE_STRING);
	}
	
	public Object parseObject( String source, java.text.ParsePosition status)
	{
		int index = status.getIndex();
		source = source.substring( index).trim();

		if ( source.equals( MICRO_FALSE_STRING))
		{
			status.setIndex( index + 1);
			return Boolean.FALSE;
		}	
		else if ( source.regionMatches( true, index, SHORT_TRUE_STRING, 0, SHORT_TRUE_STRING.length()))
		{
			status.setIndex( index + SHORT_TRUE_STRING.length());
			return Boolean.TRUE;
		}
		else if (  source.regionMatches( true, index, MEDIUM_TRUE_STRING, 0, MEDIUM_TRUE_STRING.length()))
		{
			status.setIndex( index + MEDIUM_TRUE_STRING.length());
			return Boolean.TRUE;
		}
		else if ( source.regionMatches( true, index, LONG_TRUE_STRING, 0, LONG_TRUE_STRING.length()))
		{
			status.setIndex( index + LONG_TRUE_STRING.length());
			return Boolean.TRUE;
		}
		else if ( source.regionMatches( true, index, SHORT_FALSE_STRING, 0, SHORT_FALSE_STRING.length()))
		{
			status.setIndex( index + SHORT_FALSE_STRING.length());
			return Boolean.FALSE;
		}
		else if ( source.regionMatches( true, index, MEDIUM_FALSE_STRING, 0, MEDIUM_FALSE_STRING.length()))
		{
			status.setIndex( index + MEDIUM_FALSE_STRING.length());
			return Boolean.FALSE;
		}
		else if ( source.regionMatches( true, index, LONG_FALSE_STRING, 0, LONG_FALSE_STRING.length()))
		{
			status.setIndex( index + LONG_FALSE_STRING.length());
			return Boolean.FALSE;
		}

		
		return null;
	}
	
	public static void main( String[] arguments) throws Exception
	{
		java.text.Format format = new BooleanFormat();
		
		// test some basic strings:
		
		System.out.println( "BooleanFormat.format( Boolean.TRUE):\t" + format.format( Boolean.TRUE));
		System.out.println( "BooleanFormat.format( Boolean.FALSE):\t" + format.format( Boolean.FALSE));
		System.out.println( "BooleanFormat.parseObject( \"true\").toString():\t" + format.parseObject( "true").toString());
		System.out.println( "BooleanFormat.parseObject( \"false\").toString():\t" + format.parseObject( "false").toString());	
		
		int index = 0;
		while ( index < arguments.length)
			System.out.println( "BooleanFormat.parseObject( \"" + arguments[index] + "\"):\t" 
				+ format.parseObject( arguments[index++]));
	}
}
