package org.webgets.text;

import java.text.Format;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.ParseException;

/**
 * 
 **/
public class StringChoiceFormat extends Format
{  
    private String[] choices;
    private boolean multiple;

	public StringChoiceFormat( String[] choices, boolean multiple )
	{
	    super();
	    
	    if( choices==null || choices.length==0 ) throw new IllegalArgumentException( "There must be some choices" );
	    
	    this.choices = choices;
	    this.multiple = multiple;
	}

	public StringChoiceFormat( String[] choices )
	{
        this( choices, false );
	}
	
	public StringBuffer format( Object obj, StringBuffer toAppendTo, FieldPosition pos )
    {
        if( obj!=null ) toAppendTo.append( obj.toString() );
        
        return toAppendTo;
    }
	
    /**
	
	parseObject() simply returns the string after updating the ParsePosition as long as
	the string is in the given set of choices
	
	*/
	public Object parseObject( String source, ParsePosition position )
	{
	    if( source == null ) return null;
	            	    
	    int start = position.getIndex();        	    
	            	    
	    if( start >= source.length() ) return null;     	    
	            	    
        String parsed = source.substring( start );	    
        	
        //TODO check that selections are valid here.
        		
		if( parsed.length()==0 ) position.setIndex( start + 1 );
		else position.setIndex( start + parsed.length() );
		
		return parsed;
	}    

    public Object[] getFormats() 
    {
        return choices;
    }	
    
    public boolean isMultiple()
    {
        return multiple;
    }
}
