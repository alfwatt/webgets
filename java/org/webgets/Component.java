package org.webgets;

/**

A webgets Component is a Servlet which provides some modular functionality for
building web applications. Component extends javax.servlet.Servlet and uses the
same lifecycle model as servlets.

@see Container

*/
public interface Component extends java.io.Serializable,
								   javax.servlet.Servlet, 
                                   org.webgets.util.Identifiable, 
                                   org.webgets.util.RequestListener
{	
	/** @return the label of this Component, for display to a user */
	String getLabel();

	/** @param label the new label of this Component */
	void setLabel( String label);

	/** @param out a TagWriter attached to the servlet output stream */
	void onDraw( org.webgets.util.TagWriter out);

	/** @returns the WebgetsConfig for this Component */
	org.webgets.util.WebgetsConfig getWebgetsConfig();

	/** @param config the new WebgetsConfig for this Component */
	void setWebgetsConfig( org.webgets.util.WebgetsConfig config);

	/* javax.servlet.Servlet Defines the following methods which you must also implement: 
	void init(ServletConfig);
	String getServletInfo();
	ServletConfig getServletConfig();
	void service(ServletRequest, ServletResponse);
	void destroy();
	*/
}

