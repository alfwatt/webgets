package org.webgets.util;

/**

IntValue defines a mutable integer value, so it can be used like 
java.lang.Integer and it's value can be changed. 

*/
public class IntValue extends NumberValue implements Comparable
{
	private int value;

    public IntValue( int value)
	{
		super();
		this.setIntValue( value);
	}

    /** @throws java.lang.NumberFormatException */
    public IntValue( java.lang.String value)
	{
		this( Integer.parseInt( value));
	}

	public IntValue( Number value)
	{
		super( value);
	}

	public byte getByteValue()
	{
		return (byte) this.value;
	}

	public double getDoubleValue()
	{
		return (double) this.value;
	}

	public float getFloatValue()
	{
		return (float) this.value;
	}

	public int getIntValue()
	{
		return this.value;
	}
	
	public long getLongValue()
	{
		return (long) this.value;
	}

	public short getShortValue()
	{
		return (short) this.value;
	}

	public void setByteValue( byte value)
	{
		this.value = (int) value;
	}

	public void setDoubleValue( double value)
	{
		this.value = (int) value;
	}

	public void setFloatValue( float value)
	{
		this.value = (int) value;
	}

	public void setIntValue( int value)
    {
        this.value = (int) value;
    }
		
	public void setLongValue( long value)
    {
        this.value = (int) value;
    }
    
	public void setShortValue( short value)
    {
        this.value = (int) value;
    }

	public Number getNumberValue()
	{
		return new Integer( this.value);
	}
	
	public void setNumberValue( Number value)
	{
		this.value = value.intValue();
	}

    public void increment()
    {
        this.value++;
    }

    public void decrement()
    {
        this.value--;
    }

    public boolean equals( java.lang.Object comp)
	{
		if ( comp instanceof IntValue)
			return ( this.getIntValue() == ((IntValue) comp).getIntValue());
		else if ( comp instanceof Integer)
			return ( this.getIntValue() == ((Integer) comp).intValue());
		else
			return false;
	}

    /* zero if they are the same,
       positive if this is greater than comp,
       negative if this is less than comp. */
    public int compareTo( int comp)
    {
        return this.value - comp;
    }      
           
    public int compareTo( Object comp) 
    {
        if ( comp instanceof NumberValue)
            return this.compareTo( ((NumberValue) comp).getIntValue());
        else if ( comp instanceof Number) 
            return this.compareTo( ((Number) comp).intValue());
        else 
            throw new ClassCastException( "IntValue cannot be compared to: " 
            	+ comp.getClass().getName());
    
    }

    public java.lang.String toString()
	{
		return Integer.toString( this.getIntValue());
	}
}

