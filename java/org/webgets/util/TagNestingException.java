package org.webgets.util;

/**

TagNestingException is thrown to indicate that a TagWriter.closeTag( String) call 
was made and the tag does not match the tag on the stack. This is helpfull when 
debugging component drawing code as it reports the code location of the mismatch.

*/
public class TagNestingException extends RuntimeException
{
	public TagNestingException( String provided, String needed)
	{
		super( "tag provided: " + provided + " does not match tag needed: " + needed);
	}
}
