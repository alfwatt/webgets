package org.webgets.util;

/**

DoubleValue defines a mutable double value, it can be used like java.lang.Double except that it's
value can be changed. DoubleValue owes it's odd name to the fact that the java.lang package is
always imported and I did not want to shadow any of the class names when org.webgets.util.*
is imported.

*/
public class DoubleValue extends NumberValue implements Comparable
{
	private double value;

    public DoubleValue( double value)
	{
		super();
		this.setDoubleValue( value);
	}

    /** @throws java.lang.NumberFormatException */
    public DoubleValue( java.lang.String value)
	{
		/* JDK 1.1.7 HACK
		this( Double.parseDouble( value)); */
		this( Double.valueOf( value).doubleValue());
	}
	
	public DoubleValue( Number value)
	{
		super( value);
	}

	public byte getByteValue()
	{
		return (byte) this.value;
	}

	public double getDoubleValue()
	{
		return this.value;
	}

	public float getFloatValue()
	{
		return (float) this.value;
	}

	public int getIntValue()
	{
		return (int) this.value;
	}
	
	public long getLongValue()
	{
		return (long) this.value;
	}

	public short getShortValue()
	{
		return (short) this.value;
	}

	public void setByteValue( byte value)
	{
		this.value = (double) value;
	}

	public void setDoubleValue( double value)
	{
		this.value = (double) value;
	}

	public void setFloatValue( float value)
	{
		this.value = (double) value;
	}

	public void setIntValue( int value)
    {
        this.value = (double) value;
    }
		
	public void setLongValue( long value)
    {
        this.value = (double) value;
    }
    
	public void setShortValue( short value)
    {
        this.value = (double) value;
    }

	public Number getNumberValue()
	{
		return new Double( this.value);
	}
	
	public void setNumberValue( Number value)
	{
		this.value = value.doubleValue();
	}

    public void increment()
    {
        this.value++;
    }

    public void decrement()
    {
        this.value--;
    }

    public boolean equals( java.lang.Object comp)
	{
		if ( comp instanceof DoubleValue)
			return ( this.value == ((DoubleValue) comp).value);
		else if ( comp instanceof Double)
			return ( this.value == ((Double) comp).doubleValue());
		else
			return false;
	}

    /* zero if they are the same,
       positive if this is greater than comp,
       negative if this is less than comp. */
    public int compareTo( double comp)
    {
        return (int) (this.value - comp);
    }      
           
    public int compareTo( Object comp) 
    {
        if ( comp instanceof NumberValue)
            return this.compareTo( ((NumberValue) comp).getDoubleValue());
        else if ( comp instanceof Number) 
            return this.compareTo( ((Number) comp).doubleValue());
        else 
            return this.compareTo( (double) comp.hashCode());
    }

    public java.lang.String toString()
	{
		return Double.toString( this.value);
	}
}

