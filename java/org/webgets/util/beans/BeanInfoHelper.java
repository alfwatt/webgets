package org.webgets.util.beans;

import java.beans.*;
import java.text.*;
import java.lang.reflect.*;

import org.webgets.util.*;
import org.webgets.text.*;

/**

BeanInfoHelper has conveniance methods for creating PropertyDescriptors
and MethodDescriptor objects for java beans.

*/
public final class BeanInfoHelper
{
	public static final Class[] NO_ARGS = new Class[] {};

	public static final Class[] ONE_OBJECT_ARGS = new Class[] { Object.class};
	public static final Class[] TWO_OBJECT_ARGS = new Class[] { Object.class, Object.class };

	public static final Class[] ONE_STRING_ARGS = new Class[] { String.class};
	public static final Class[] TWO_STRING_ARGS = new Class[] { String.class, String.class };

	/**

	Create a PropertyDescriptor for the class specified with the name
	and description provided.

	@param name the name of the Property
	@param desc the short description of the property
	@param propClass the Class to describe
	@returns a PropertyDescriptor

	*/
	public static PropertyDescriptor createPropertyDescriptor
	(
		String name,
		String desc,
		Class propClass
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = new PropertyDescriptor(name, propClass);
        pd.setShortDescription(desc);

        // firewall
        Check.isNotNull( pd.getPropertyType(), "PropertyDescriptor has null type!");

        return pd;
    }

	/**

	Create a PropertyDescriptor for the class specified with the name,
	displayName and description provided.

	@param name the name of the Property
	@param displayName the displayable name of the property
	@param desc the short description of the property
	@param propClass the Class to describe
	@returns a PropertyDescriptor

	*/
	public static PropertyDescriptor createPropertyDescriptor
	(
		String name,
		String displayName,
		String desc,
		Class propClass
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = createPropertyDescriptor( name, desc, propClass);
        pd.setDisplayName( displayName);

        return pd;
    }

	/**

	Create a PropertyDescriptor for the class specified with the name,
	displayName and description provided. Sets an attributes on the 
	PropertyDescriptor with the name and value provided.

	@param name the name of the Property
	@param displayName the displayable name of the property
	@param desc the short description of the property
	@param attrName the name of an attribute of the property
	@param attrValue the value of an attribute of the property	
	@param propClass the Class to describe
	@returns a PropertyDescriptor

	*/
	public static PropertyDescriptor createPropertyDescriptor
	(
		String name,
		String displayName,
		String desc,
		String attrName,
		Object attrValue,
		Class propClass
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = createPropertyDescriptor( name, displayName, desc, propClass);
		pd.setValue( attrName, attrValue);
		
        return pd;
    }

	/**

	Create a PropertyDescriptor with the paramters provided.

	Replaces propClass with getMethod and setMethod for non
	standard method signatures.

	*/
	public static PropertyDescriptor createPropertyDescriptor
	(
		String name,
		String displayName,
		String desc,
		boolean expert,
		Method getMethod,
		Method setMethod
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = new PropertyDescriptor( name, getMethod, setMethod);
        pd.setDisplayName( displayName);
        pd.setShortDescription(desc);
        pd.setExpert(expert);

        // firewall
        Check.isNotNull( pd.getPropertyType(), "PropertyDescriptor has null type!");

        return pd;
    }

	/**

	Create a PropertyDescriptor with the paramters provided.

	Replaces propClass with getMethod and setMethod for non
	standard method signatures.

	*/
	public static PropertyDescriptor createPropertyDescriptor
	(
		String name,
		String displayName,
		String desc,
		boolean expert,
		Method getMethod,
		Method setMethod,
		String attrName,
		Object attrValue
		
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = new PropertyDescriptor( name, getMethod, setMethod);
        pd.setDisplayName( displayName);
        pd.setShortDescription(desc);
        pd.setExpert(expert);
		pd.setValue( attrName, attrValue);

        // firewall
        Check.isNotNull( pd.getPropertyType(), "PropertyDescriptor has null type!");

        return pd;
    }

	public static PropertyDescriptor createReadOnlyPropertyDescriptor
	(
		String name,
		String displayName,
		String desc,
		boolean expert,
		Method getMethod
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = new PropertyDescriptor( name, getMethod, null);
        pd.setDisplayName( displayName);
        pd.setShortDescription(desc);
        pd.setExpert(expert);

        // firewall
        Check.isNotNull( pd.getPropertyType(), "PropertyDescriptor has null type!");

        return pd;
    }

	public static PropertyDescriptor createReadOnlyPropertyDescriptor
	(
		String name,
		String displayName,
		String desc,
		boolean expert,
		Method getMethod,
		String attrName,
		Object attrValue
	)
	throws IntrospectionException
    {
        PropertyDescriptor pd = new PropertyDescriptor( name, getMethod, null);
        pd.setDisplayName( displayName);
        pd.setShortDescription(desc);
        pd.setExpert(expert);
		pd.setValue( attrName, attrValue);
		
        // firewall
        Check.isNotNull( pd.getPropertyType(), "PropertyDescriptor has null type!");

        return pd;
    }



	/**

	Create a MethodDescriptor with the parameters provided.

	Performs method lookup via reflection.

    @throws SecurityException

	*/
    public static MethodDescriptor createMethodDescriptor
    (
    	String methodName,
    	String description,
    	Class methodClass,
    	Class[] args
    )
	throws NoSuchMethodException
	{
        Method m = methodClass.getMethod(methodName, args);
        MethodDescriptor md = new MethodDescriptor(m);
        md.setShortDescription(description);
        return md;
    }

	/**

	Create a MethodDescriptor with the parameters provided.

	Performs method lookup via reflection.

	Adds display name from above.

    @throws SecurityException

    */
    public static MethodDescriptor createMethodDescriptor
    (
    	String methodName,
    	String displayName,
    	String description,
    	Class methodClass,
    	Class[] args
    )
	throws NoSuchMethodException
	{
        Method m = methodClass.getMethod(methodName, args);
        MethodDescriptor md = new MethodDescriptor(m);
        md.setDisplayName( displayName);
        md.setShortDescription(description);
        return md;
    }

	/**

	Create a no_arg MethodDescriptor with the parameters provided.

	Performs method lookup via reflection.

	Adds display name from above.

    @throws SecurityException

	*/
    public static MethodDescriptor createMethodDescriptor
    (
    	String methodName,
    	String displayName,
    	String description,
    	Class methodClass
    )
	throws NoSuchMethodException
	{
        Method m = methodClass.getMethod(methodName, NO_ARGS);
        MethodDescriptor md = new MethodDescriptor(m);
        md.setDisplayName( displayName);
        md.setShortDescription(description);
        return md;
    }
    
    /* TODO
	public static Method findGetter( Class beanType, String propertyName, Class propertyType)
	{
		String methodName = "get" + propertyName;
		
		return null;
	}
	
	public static Method findSetter( Class beanType, String propertyName, Class propertyType)
	{
		String methodName = "set" + propertyName;
		
		return null;
	}
	*/
	
	/* no instances are allowed */
	private BeanInfoHelper()
	{
	}
}
