package org.webgets.util.beans;

import java.text.*;
import java.beans.*;
import java.lang.reflect.*;

import org.webgets.util.*;
import org.webgets.text.*;

/**

BeanTools is a collection of utility methods for dealing with the java
beans API. get/set/update property values as objects, strings or arrays

*/
public final class BeanTools extends Object
{
	
	public static java.text.Format lookupFormat( PropertyDescriptor prop)
	{
		/* first check for the named property "java.text.Format" of the PropertyDescriptor */
		Object customFormat = prop.getValue( "java.text.Format");

		if ( customFormat != null)
			return (java.text.Format) customFormat;
		else /* look it up in the registry, this should always result in at least an ObjectFormat */
			return FormatRegistry.getFormat( prop.getPropertyType());
	}

	public static Object getPropertyValueAsObject( PropertyDescriptor prop, Object bean)
		throws IllegalAccessException, InvocationTargetException
	{
		Method readMethod = prop.getReadMethod();
		if ( readMethod == null)
				throw new IllegalArgumentException( "BeanTools.getPropertyValueAsObject() no read method for property: " 
					+ prop.getDisplayName());
		return readMethod.invoke( bean, BeanInfoHelper.NO_ARGS);
	}
	
	public static void setPropertyValueAsObject( PropertyDescriptor prop, Object bean, Object value)
		throws IllegalAccessException, InvocationTargetException
	{
		Method writeMethod = prop.getWriteMethod();
		if ( writeMethod == null)
				throw new IllegalArgumentException( "BeanTools.setPropertyValueAsObject() no write method for property: " 
					+ prop.getDisplayName());
		writeMethod.invoke( bean, new Object[] {value});
	}
	
	public static boolean updatePropertyValueAsObject( PropertyDescriptor prop, Object bean, Object value)
		throws IllegalAccessException, InvocationTargetException
	{
		Object oldValue = getPropertyValueAsObject( prop, bean);

		if ( oldValue != value && value != null && ! value.equals( oldValue))
			setPropertyValueAsObject( prop, bean, value);
		else
			return false;
		
		return true;
	}
	
	public static String getPropertyValueAsString( PropertyDescriptor prop, Object bean)
		throws IllegalAccessException, InvocationTargetException
	{
		Object propValue = prop.getReadMethod().invoke( bean, BeanInfoHelper.NO_ARGS);
		Format propFormat = lookupFormat( prop);
		String value;

		if ( propFormat != null)
			value = propFormat.format( propValue);
		else if ( propValue != null)
			value = propValue.toString();
		else
			value = "";
			
		return value;
	}

	public static void setPropertyValueAsString( PropertyDescriptor prop, Object bean, String value)
		throws IllegalAccessException, InvocationTargetException, ParseException
	{
		Format propFormat = lookupFormat( prop);
		
		if ( propFormat != null && ! ( propFormat instanceof ObjectFormat))
		{
			Object valueObject = propFormat.parseObject( value);

			Method writeMethod = prop.getWriteMethod();
			if ( writeMethod == null)
				throw new IllegalArgumentException( "BeanTools.setPropertyValueAsString() no write method for property: " 
					+ prop.getDisplayName());
			writeMethod.invoke( bean, new Object[] { valueObject});
		}
		else
		{
			Log.printWarn( "BeanTools.setPropertyValueAsString() could not get Format for prop class=" 
				+ prop.getPropertyType().getName()  + " value=" + value);
		}
	}

	public static boolean updatePropertyValueAsString( PropertyDescriptor prop, Object bean, String value)
		throws IllegalAccessException, InvocationTargetException, ParseException
	{
		String oldValue = getPropertyValueAsString( prop, bean);
		
		if ( oldValue != value && value !=null && ! value.equals( oldValue))
			setPropertyValueAsString( prop, bean, value);
		else
			return false;
		
		return true;
	}

	public static Object[] getPropertyValueAsArray( PropertyDescriptor prop, Object bean)
		throws IllegalAccessException, InvocationTargetException
	{
		Check.isLegalArg( prop.getPropertyType().isArray(), "property must be array type");
		Method readMethod = prop.getReadMethod();
		Check.isNotNull( readMethod, "no read method for property: " + prop.getDisplayName());
		return (Object[]) readMethod.invoke( bean, BeanInfoHelper.NO_ARGS);
	}
	
	public static void setPropertyValueAsArray( PropertyDescriptor prop, Object bean, Object[] values)
		throws IllegalAccessException, InvocationTargetException
	{
		Method writeMethod = prop.getWriteMethod();
		Check.isNotNull( writeMethod, "no write method for property: " + prop.getDisplayName());
		writeMethod.invoke( bean, new Object[] {values});
	}

/*	TODO compare the arrays
	
	public static boolean updatePropertyValueAsArray( PropertyDescriptor prop, Object bean, Object[] values)
		throws IllegalAccessException, InvocationTargetException
	{
		Object[] oldValues = getPropertyValueAsArray( prop, bean);

		if ( value != null && oldValues.length != values.length)
			setPropertyValueAsArray( prop, bean, values);
		else
			return false;
		
		return true;
	}
*/

	private BeanTools()
	{
	}
}
