package org.webgets.util.beans;

import java.beans.*;

public class BeanCodeGenerator extends Object
{

	private static String  OPEN_BLOCK = "\n{\n";
	private static String CLOSE_BLOCK = "\n}\n";
	private static String DOT = ".";

	public static String generateCode( BeanInfo info)
	{
		StringBuffer code = new StringBuffer(128);
		code.append( genClass( info.getBeanDescriptor()));
		code.append( OPEN_BLOCK);
		
		code.append( getDefaultConstructor( info.getBeanDescriptor()));
		
		code.append( "\n    /* Bean Properties */\n\n");
		PropertyDescriptor[] props = info.getPropertyDescriptors();
		if ( props != null)
		{
			int propIndex = 0;
			while ( propIndex < props.length)
				code.append( genProperty( props[propIndex++]));
		}

		code.append( "\n    /* Bean Methods */\n\n");
		MethodDescriptor[] methods = info.getMethodDescriptors();
		if ( methods != null)
		{
			int methodIndex = 0;
			while ( methodIndex < methods.length)
				code.append( genMethod( methods[methodIndex++]));
		}
		code.append( "\n    /* Bean Event Sets */\n\n");

	

		code.append( CLOSE_BLOCK);		
		return code.toString();
	}
	
	/**

	package bean.package;
	public class BeanClassName extends BeanSuperClass implements Interface, AnotherInterface
	
	@param bean a BeanDescriptor 
	@returns the Class decleration for the Bean as a String
	
	*/
	private static String genClass( BeanDescriptor bean)
	{
		String beanClassName = bean.getBeanClass().getName();
		String beanPackageName = beanClassName.substring( 0, beanClassName.lastIndexOf( DOT));
		String shortClassName = beanClassName.substring( beanClassName.lastIndexOf( DOT)+1);
		String superclassName = bean.getBeanClass().getSuperclass().getName();
		
		StringBuffer decl = new StringBuffer();
		decl.append( "package ");
		decl.append( beanPackageName);
		decl.append( ";\n\n");
		decl.append( "public class ");
		decl.append( shortClassName);
		decl.append( " extends ");
		decl.append( superclassName);
		
		Class[] beanInterfaces = bean.getBeanClass().getInterfaces();
		if ( beanInterfaces != null && beanInterfaces.length > 0)
		{
			decl.append( "\n    implements ");
			int index = 0;
			do
			{
				decl.append( beanInterfaces[index].getName());
				if ( index + 1 < beanInterfaces.length)
					decl.append( ", ");
			}
			while ( ++index < beanInterfaces.length);
		}
				
		return decl.toString();
	}
	
	private static String getDefaultConstructor( BeanDescriptor bean)
	{
		String beanClassName = bean.getBeanClass().getName();
		String shortClassName = beanClassName.substring( beanClassName.lastIndexOf( DOT)+1);
		
		StringBuffer decl = new StringBuffer();
		decl.append( "    public ");
		decl.append( shortClassName);
		decl.append( "()\n    {\n        super();\n    }\n");
		return decl.toString();
	}
	
	private static String genProperty( PropertyDescriptor property)
	{
		StringBuffer decl = new StringBuffer( 128);
				
		decl.append( "    private ");
		if ( property.getPropertyType().isArray())
			decl.append( "Object[]"); // XXX HACK TODO: Mangle the array name into a proper declaration 
		else
			decl.append( property.getPropertyType().getName());
		decl.append( " ");
		decl.append( property.getName());
		decl.append( " = ");
		decl.append( genDefaultValue( property.getPropertyType()));
		decl.append( ";\n");
		
		return decl.toString();
	}
	
	private static String genMethod( MethodDescriptor method)
	{
		return "    /* " + method.getDisplayName() + " */\n" ;
	}
	
	private static String genEvent( EventSetDescriptor event)
	{
		return "    /* " + event.getDisplayName() + " */\n";
	}
	
	private static String genDefaultValue( Class type)
	{
		if ( type == Integer.TYPE
		  || type == Short.TYPE
		  || type == Byte.TYPE)
			return "0";
		else if ( type == Float.TYPE)
			return "0.0f";
		else if ( type == Long.TYPE)
			return "0.0l";
		else if ( type == Double.TYPE)
			return "0.0d";
		else if ( type == Boolean.TYPE)
			return "true";
		else if ( type == Character.TYPE)
			return "\'.\'";
		else if ( type == String.class)
			return "\"\"";
		else
			return "null";
	}
}
