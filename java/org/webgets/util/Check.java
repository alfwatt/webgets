package org.webgets.util;

/**

<p>Check provides static methods which mostly just throw exceptions, but they
make it really easy to check for null objects, legal arguments, legal states
and conditions that "can't happen". 

*/
public final class Check
{

	/** thrown to indicate a condition that "can't happen", such as
	reaching the default block of a case statement or branching past a
	conditional which should always be true.  */
	static class CantHappenException extends RuntimeException
	{
		CantHappenException()
		{
			super();
		}
		
		CantHappenException( String message)
		{
			super( message);
		}
	}

	/* the lock-out constructor */
	private Check()
	{
		super();
	}
	
	public static void isNotNull( Object test)
	{
		if ( test == null) throw new NullPointerException();
	}
	
	public static void isNotNull( Object test, String message)
	{
		if ( test == null) throw new NullPointerException( message);
	}
	
	public static void isLegalArg( boolean test)
	{
		if ( ! test) throw new IllegalArgumentException();
	}
	
	public static void isLegalArg( boolean test, String message)
	{
		if ( ! test) throw new IllegalArgumentException( message);
	}
	
	public static void isLegalState( boolean test)
	{
		if ( ! test) throw new IllegalStateException();
	}
	
	public static void isLegalState( boolean test, String message)
	{
		if ( ! test) throw new IllegalStateException( message);
	}
	
	public static void cantHappen()
	{
		throw new CantHappenException();
	}
	
	public static void cantHappen( String message)
	{
		throw new CantHappenException( message);
	}
	
} // class Check
