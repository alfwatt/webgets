package org.webgets.util;

/**

The RedirectResponse RuntimeException is throws by RequestEvent to short circut 
the rest of the request passing chain once the event has been consumed. It also
keeps the drawing code from executing. Response redirection is first come first
serve, so be carefully how you structure you application. Code which starts 
request processing must be ready to catch this exception because it will be thrown
if the event is consumed.

*/
public class RedirectResponse extends StopRequestProcessing
{
	private String redirectedTo;

	public RedirectResponse( Object stoppedBy, String redirectedTo)
	{
		super( stoppedBy);
		this.redirectedTo = redirectedTo;
	}
	
	public String redirectedTo()
	{
		return this.redirectedTo;
	}
	
	public String getMessage()
	{
		return super.getMessage() + " redirectedTo=" + redirectedTo;
	}

}
