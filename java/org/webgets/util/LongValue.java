package org.webgets.util;

/**

LongValue defines a mutable long value, it can be used like java.lang.Long except that it's
value can be changed. 

*/
public class LongValue extends NumberValue implements Comparable
{
	private long value;

    public LongValue( float value)
	{
		super();
		this.setFloatValue( value);
	}

    /** @throws java.lang.NumberFormatException */
    public LongValue( java.lang.String value)
	{
		/* JDK 1.1.7 HACK 
		this( Long.parseLong( value)); */
		this( Long.valueOf( value).longValue());
	}
	
	public LongValue( Number value)
	{
		super( value);
	}

	public byte getByteValue()
	{
		return (byte) this.value;
	}

	public double getDoubleValue()
	{
		return (double) this.value;
	}

	public float getFloatValue()
	{
		return (float) this.value;
	}

	public int getIntValue()
	{
		return (int) this.value;
	}
	
	public long getLongValue()
	{
		return (long) this.value;
	}

	public short getShortValue()
	{
		return (short) this.value;
	}

	public void setByteValue( byte value)
	{
		this.value = (long) value;
	}

	public void setDoubleValue( double value)
	{
		this.value = (long) value;
	}

	public void setFloatValue( float value)
	{
		this.value = (long) value;
	}

	public void setIntValue( int value)
    {
        this.value = (long) value;
    }
		
	public void setLongValue( long value)
    {
        this.value = value;
    }
    
	public void setShortValue( short value)
    {
        this.value = (long) value;
    }

	public Number getNumberValue()
	{
		return new Long( this.value);
	}
	
	public void setNumberValue( Number value)
	{
		this.value = value.longValue();
	}

    public void increment()
    {
        this.value++;
    }

    public void decrement()
    {
        this.value--;
    }

    public boolean equals( java.lang.Object comp)
	{
		if ( comp instanceof LongValue)
			return ( this.value == ((LongValue) comp).value);
		else if ( comp instanceof Float)
			return ( this.value == ((Long) comp).longValue());
		else
			return false;
	}

    /* zero if they are the same,
       positive if this is greater than comp,
       negative if this is less than comp. */
    public int compareTo( long comp)
    {
        return (int) (this.value - comp);
    }      
           
    public int compareTo( Object comp) 
    {
        if ( comp instanceof NumberValue)
            return this.compareTo( ((NumberValue) comp).getLongValue());
        else if ( comp instanceof Number) 
            return this.compareTo( ((Number) comp).longValue());
        else 
            return this.compareTo( (long) comp.hashCode());
    }


    public java.lang.String toString()
	{
		return Float.toString( this.value);
	}
}

