package org.webgets.util;

/**

FloatValue defines a mutable float value, it can be used like java.lang.Float except that it's
value can be changed. 

*/
public class FloatValue extends NumberValue implements Comparable
{
	private float value;

    public FloatValue( float value)
	{
		super();
		this.setFloatValue( value);
	}

    /** @throws java.lang.NumberFormatException */
    public FloatValue( java.lang.String value)
	{
		/* JDK 1.1.7 HACK
		this( Float.parseFloat( value)); */
		this( Float.valueOf( value).floatValue());
	}
	
	public FloatValue( Number value)
	{
		super( value);
	}

	public byte getByteValue()
	{
		return (byte) this.value;
	}

	public double getDoubleValue()
	{
		return (double) this.value;
	}

	public float getFloatValue()
	{
		return this.value;
	}

	public int getIntValue()
	{
		return (int) this.value;
	}
	
	public long getLongValue()
	{
		return (long) this.value;
	}

	public short getShortValue()
	{
		return (short) this.value;
	}

	public void setByteValue( byte value)
	{
		this.value = (float) value;
	}

	public void setDoubleValue( double value)
	{
		this.value = (float) value;
	}

	public void setFloatValue( float value)
	{
		this.value = value;
	}

	public void setIntValue( int value)
    {
        this.value = (float) value;
    }
		
	public void setLongValue( long value)
    {
        this.value = (float) value;
    }
    
	public void setShortValue( short value)
    {
        this.value = (float) value;
    }

	public Number getNumberValue()
	{
		return new Float( this.value);
	}
	
	public void setNumberValue( Number value)
	{
		this.value = value.floatValue();
	}

    public void increment()
    {
        this.value++;
    }

    public void decrement()
    {
        this.value--;
    }

    public boolean equals( java.lang.Object comp)
	{
		if ( comp instanceof FloatValue)
			return ( this.value == ((FloatValue) comp).value);
		else if ( comp instanceof Float)
			return ( this.value == ((Float) comp).floatValue());
		else
			return false;
	}

    /* zero if they are the same,
       positive if this is greater than comp,
       negative if this is less than comp. */
    public int compareTo( float comp)
    {
        return (int) (this.value - comp);
    }      
    
    /**
    
    Compare this FloatValue to another object, either a org.webgets.util.NumberValue 
    or java.lang.Number.
    
    */
    public int compareTo( Object comp)
    {
        if ( comp instanceof NumberValue)
            return this.compareTo( ((NumberValue) comp).getFloatValue());
        else if ( comp instanceof Number) 
            return this.compareTo( ((Number) comp).floatValue());
        else 
            return this.compareTo( (float) comp.hashCode());
    }

    public java.lang.String toString()
	{
		return Float.toString( this.value);
	}
}

