package org.webgets.util.test;

public class TestDriver extends Object implements Testable
{
	public static final String TEST_CASES = ".TestCases";
	
	public static Testable lookupTestable( String name)
	{
		try
		{
			Class clzz = Class.forName( name);
			return (Testable) clzz.newInstance();
		}
		catch ( Exception likley)
		{
			org.webgets.util.Log.printDebug( likley.getMessage());
			
			try
			{
				String casesName = name + TEST_CASES;
				Class clzz = Class.forName( casesName);
				return (Testable) clzz.newInstance();
			}
			catch ( Exception veryLikley)
			{
				org.webgets.util.Log.printWarn( "TestDriver.lookupTestable( \"" + name + "\") failed");
			}
		}
		
		return null;
	}

	private TestSession session = null;

	public TestDriver()
	{
		super();
	}

	public void setSession( TestSession session)
	{
		if ( session == null) throw new IllegalArgumentException( "setSession( TestSession session) must not be null!");
		this.session = session;
	}

	public void runTests( String[] names)
	{
		int index = 0;
		while ( index < names.length)
			this.runTest( lookupTestable( names[index++]));
	}
	
	public void runTests( Testable[] tests)
	{
		int index = 0;
		while ( index < tests.length)
			this.runTest( tests[index++]);
		return;
	} 
	
	public void runTest( Testable test)
	{
		if ( test != null)
			test.test( this.session);
	}
	
	public void test( TestSession session)
	{
		session.pass( "TestDriver just works.");
	}
	
	public static void main( String[] names)
	{
		if ( names.length == 0)
		{
			System.err.println( "usage: TestDriver name[s] 	# just add java class or package names");
			System.exit( -1);
		}
		
		TestDriver driver = new TestDriver();
		driver.setSession( new TestSession());
		driver.runTests( names);
		System.exit( 0);
	}

}
