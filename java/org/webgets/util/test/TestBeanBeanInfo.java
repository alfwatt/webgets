package org.webgets.util.test;

import java.beans.*;

import org.webgets.util.Log;
import org.webgets.text.*;
import org.webgets.util.beans.BeanInfoHelper;

/**

TestBeanBeanInfo provides extended bean info for TestBean. It provides bean descriptors
for every property in TestBean with extended information on how it should be displayed by
editors and browsers. 

*/
public class TestBeanBeanInfo extends SimpleBeanInfo
{
	private static final BeanDescriptor TEST_BEAN_DESCRIPTOR;
	private static final PropertyDescriptor[] TEST_BEAN_PROPS;

	static
	{
		Log.printDebug( "TestBeanBeanInfo.static{}");
		TEST_BEAN_DESCRIPTOR = new BeanDescriptor( TestBean.class, Object.class);
		TEST_BEAN_DESCRIPTOR.setShortDescription(
			"TestBean is a simple Java Bean class which implements geters and seters for all"
		  + " the primitive Java types plus an event source and test method." 
		);

		PropertyDescriptor[] testBeanProps = null;

		try
		{
			testBeanProps = new PropertyDescriptor[]
			{
				BeanInfoHelper.createPropertyDescriptor
				(
					"cash",
					"Ca$h",
					"It's Ca$h money baby!",
					false, /* cuz every whitey knows about ca$h */
					TestBean.class.getDeclaredMethod( "getTestFloat", new Class[] {}),
					TestBean.class.getDeclaredMethod( "setTestFloat", new Class[] { Float.TYPE}),
					"java.text.Format", NumberFormatWrappers.createFloatWrapper( java.text.NumberFormat.getCurrencyInstance())
				),

				BeanInfoHelper.createPropertyDescriptor
				(
					"size",
					"Size",
					"How big do you want it?",
					false,
					TestBean.class.getDeclaredMethod( "getTestInt", new Class[] {}),
					TestBean.class.getDeclaredMethod( "setTestInt", new Class[] { Integer.TYPE}),
					"java.text.Format", NumberFormatWrappers.createIntegerWrapper( new java.text.ChoiceFormat(
						new double[] { 0.0, 1.0, 2.0},
						new String[] { "Small", "Medium", "Large"}
					))
				),

				BeanInfoHelper.createPropertyDescriptor
				(
					"numSize",
					"Numeric Size",
					"How percise do you want it?",
					false,
					TestBean.class.getDeclaredMethod( "getTestInt", new Class[] {}),
					null,
					"java.text.Format", NumberFormatWrappers.createIntegerWrapper( java.text.DecimalFormat.getInstance())
				),

				BeanInfoHelper.createPropertyDescriptor
				(
					"bool",
					"Alive",
					"Are You Alive?",
					false,
					TestBean.class.getDeclaredMethod( "getTestBoolean", new Class[] {}),
					TestBean.class.getDeclaredMethod( "setTestBoolean", new Class[] { Boolean.TYPE}),
					"java.text.Format", BooleanFormat.getInstance()
				),

				BeanInfoHelper.createPropertyDescriptor
				(
					"char",
					"Initial",
					"What's your middle initial?",
					false,
					TestBean.class.getDeclaredMethod( "getTestChar", new Class[] {}),
					TestBean.class.getDeclaredMethod( "setTestChar", new Class[] { Character.TYPE}),
					"java.text.Format", CharacterFormat.getInstance()
				),

				BeanInfoHelper.createPropertyDescriptor
				(
					"date",
					"The Date",
					"Do you have the time?",
					false,
					TestBean.class.getDeclaredMethod( "getTestDate", new Class[] {}),
					TestBean.class.getDeclaredMethod( "setTestDate", new Class[] { java.util.Date.class}),
					"java.text.Format", java.text.DateFormat.getDateInstance()
				),

				BeanInfoHelper.createPropertyDescriptor
				(
					"notes",
					"Notes",
					"Keep Some Notes Here.",
					false,
					TestBean.class.getDeclaredMethod( "getTestString", new Class[] {}),
					TestBean.class.getDeclaredMethod( "setTestString", new Class[] { String.class}),
					"java.text.Format", new TextAreaFormat( 4, 25)
				),
				
				new IndexedPropertyDescriptor( "testObjectArray", TestBean.class)
			};
		}
		catch ( Exception log)
		{
			Log.printWarn( "TestBeanBeanInfo.static{}", log);
		}
		finally
		{
			TEST_BEAN_PROPS = testBeanProps;
			
			if ( TEST_BEAN_PROPS == null)
				Log.printWarn( "TestBeanBeanInfo.static{} failed, TEST_BEAN_PROPS is null!");
		}
	}
	
	public BeanDescriptor getBeanDescriptor()
	{
		return TEST_BEAN_DESCRIPTOR;
	}
	
	public PropertyDescriptor[] getPropertyDescriptors()
	{
		PropertyDescriptor[] introspected = super.getPropertyDescriptors();
		if ( introspected != null)
		{
			PropertyDescriptor[] combined = new PropertyDescriptor[ introspected.length + TEST_BEAN_PROPS.length];
			System.arraycopy( TEST_BEAN_PROPS, 0, combined, 0, TEST_BEAN_PROPS.length);
			System.arraycopy( introspected, 0, combined, TEST_BEAN_PROPS.length, introspected.length);
			return combined;
		}
		else
			return TEST_BEAN_PROPS;
	}
}
