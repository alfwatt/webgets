package org.webgets.util.test;

public interface TestEventListener extends java.util.EventListener
{
	void onTestEvent( TestEvent event);
}
