package org.webgets.util.test;

public class TestSession extends Object implements Testable
{
	private java.io.PrintStream report = System.out;

	public TestSession()
	{
		super();
	}

	public TestSession( java.io.PrintStream report)
	{
		if ( report != null) throw new IllegalArgumentException( "PrintStream report must not be null!");
		this.report = report;
	}

	public void note( String note)
	{
		report.print("[note] ");
		report.println( note);
	}
	
	public void test( String test, boolean passes)
	{
		if ( passes)
			report.print( "[pass] ");
		else
			report.print( "[fail] ");
		report.println( test);
	}

	public void pass( String test)
	{
		this.test( test, true);
	}
	
	public void fail( String test)
	{
		this.test( test, false);
	}
	
	public void test( String id, String test, boolean passes)
	{
		report.print( "[");
		report.print( id);
		report.print( " ");
		report.print( passes ? "pass] " : "fail] ");
		report.println( test);
	}
	
	public void test( String id, String test, Throwable reason)
	{
		this.test( id, test, false);
		reason.printStackTrace( report);
	}
	
	public void test( TestSession session)
	{
		session.note( "Welcome to TestSessions self test. TestSessions are of course Testable!");
		session.test( "printing a string", true);
		session.test( "failing", false);
		session.pass( "pass()ing");
		session.fail( "fail()ing");
		session.test( "one", "printing a test with an id", true);
		session.test( "1", "printing a failed test with an id", false);
		session.test( "foo", "printing a failed test with a reason", 
			new Exception( "Yikes! Good thing this Exception never gets thrown!"));
	}
}
