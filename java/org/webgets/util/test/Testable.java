package org.webgets.util.test;

/**

The Testable interface is inteded to be loaded by a test driver. The
driver will create a new instance of the Testable using
class.newInstance(), so the Testable must have a public no-arg
constructor which works! It's fair to say that most Testables are java
beans, so consider this the litmus.

It may be inconveniant to make all classes Testable. If you are writing
tests for an existing package it's best to externilze your tests for an
entire package in a seprate class. The test driver should encourage this
usage pattern by assuming that for the package org.webgets.test the
package level tests are in a class named org.webgets.test.TestCases

*/
public interface Testable
{


	/**

	Testable defines only the single test() method which takes a TextSession.
	All reporting is done via the TestSession object which defines a method
	to report a test.
	
	@param session will be used for reporting, by calling it's test( String, boolean),
	pass( String) and fail( String) methods.

	*/
	void test( TestSession session);
}
