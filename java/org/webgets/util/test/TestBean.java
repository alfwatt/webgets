package org.webgets.util.test;

import java.util.Date;

public final class TestBean extends Object implements Cloneable, java.io.Serializable
{
	public static final java.beans.BeanInfo BEAN_INFO = new TestBeanBeanInfo();

	private String testString = null;
	private int testInt = Integer.MAX_VALUE;
	private boolean testBoolean = false;
	private long testLong = Long.MAX_VALUE;
	private byte testByte = Byte.MAX_VALUE;
	private char testChar = 'c';
	private float testFloat = 0.5f;
	private double stuntDouble = Double.MAX_VALUE;
	private short testShort = Short.MAX_VALUE;
	private Void testVoid = null;
	private Object[] testObjectArray = new Object[] { new Object(), new Object(), new Object()};
	private TestBean testBean = null;
	private Date testDate = new Date();
	
	public TestBean()
	{
		super();	
	}
	
	public TestBean( String test)
	{
		this();
		this.setTestString( test);
	}
	
	/* primitive type properties */
	public String getTestString()
	{
		return this.testString;
	}
	
	public void setTestString( String test)
	{
		this.testString = test;
	}
	
	public int getTestInt()
	{
		return this.testInt;
	}
	
	public void setTestInt( int test)
	{
		this.testInt = test;
	}
	
	public boolean getTestBoolean()
	{
		return this.testBoolean;
	}
	
	public void setTestBoolean( boolean test)
	{
		this.testBoolean = test;
	}
	
	public long getTestLong()
	{
		return this.testLong;
	}
	
	public void setTestLong( long test)
	{
		this.testLong = test;
	}
	
	public byte getTestByte()
	{
		return this.testByte;
	}
	
	public void setTestByte( byte test)
	{
		this.testByte = test;
	}
	
	public char getTestChar()
	{
		return this.testChar;
	}
	
	public void setTestChar( char test)
	{
		this.testChar = test;
	}
	
	public float getTestFloat()
	{
		return this.testFloat;
	}
	
	public void setTestFloat( float test)
	{
		this.testFloat = test;
	}
	
	public double getStuntDouble()
	{
		return this.stuntDouble;
	}
	
	public void setStuntDouble( double stuntDouble)
	{
		this.stuntDouble = stuntDouble;
	}
	
	public short getTestShort()
	{
		return this.testShort;
	}
	
	public void setTestShort( short test)
	{	
		this.testShort = test;
	}
	
	public Void getTestVoid()
	{
		return this.testVoid;
	}
	
	public void setTestVoid( Void test)
	{
		this.testVoid = test;
	}
	
	/* Indexed Property */
	public Object[] getTestObjectArray()
	{
		return this.testObjectArray;
	}
	
	public Object getTestObjectArray( int index)
	{
		return this.testObjectArray[index];
	}	
	
	public void setTestObjectArray( Object[] test )
	{
		this.testObjectArray = test;
	}
	
	public void setTestObjectArray( Object test, int index)
	{
		this.testObjectArray[index] = test;
	}

	/* complex property (another TestBean) */
	public TestBean getTestBean()
	{
		if ( this.testBean == null) /* lazy create the test bean */
			this.testBean = new TestBean();
		return this.testBean;
	}
	
	public void setTestBean( TestBean test)
	{
		this.testBean = test;
	}
	
	public Date getTestDate()
	{
		return this.testDate;
	}
	
	public void setTestDate( Date test)
	{
		this.testDate = test;
	}
		
	/* TestBean deals with TestEvents (or pretends to) */
	public void addTestEventListener( TestEventListener listener)
	{
	
	}
	
	public void removeTestEventListener( TestEventListener listener)
	{
	
	}
}
