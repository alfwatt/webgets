package org.webgets.util.test;

public class TestCases extends Object implements Testable
{
	public TestCases()
	{
		super();
	}

	public void test( TestSession session)
	{
		session.pass( "package TestCases are loaded and run!");
	}
}
