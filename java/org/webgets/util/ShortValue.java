package org.webgets.util;

/**

ShortValue defines a mutable short value, it can be used like java.lang.Short except that it's
value can be changed. 

*/
public class ShortValue extends NumberValue implements Comparable
{
	private short value;

    public ShortValue( short value)
	{
		super();
		this.setShortValue( value);
	}

    /** @throws java.lang.NumberFormatException */
    public ShortValue( java.lang.String value)
	{
		this( Short.parseShort( value));
	}
	
	public ShortValue( Number value)
	{
		super( value);
	}

	public byte getByteValue()
	{
		return (byte) this.value;
	}

	public double getDoubleValue()
	{
		return (double) this.value;
	}

	public float getFloatValue()
	{
		return (float) this.value;
	}

	public int getIntValue()
	{
		return (int) this.value;
	}
	
	public long getLongValue()
	{
		return (long) this.value;
	}

	public short getShortValue()
	{
		return (short) this.value;
	}

	public void setByteValue( byte value)
	{
		this.value = (short) value;
	}

	public void setDoubleValue( double value)
	{
		this.value = (short) value;
	}

	public void setFloatValue( float value)
	{
		this.value = (short) value;
	}

	public void setIntValue( int value)
    {
        this.value = (short) value;
    }
		
	public void setLongValue( long value)
    {
        this.value = (short) value;
    }
    
	public void setShortValue( short value)
    {
        this.value = (short) value;
    }

	public Number getNumberValue()
	{
		return new Short( this.value);
	}
	
	public void setNumberValue( Number value)
	{
		this.value = value.shortValue();
	}

    public void increment()
    {
        this.value++;
    }

    public void decrement()
    {
        this.value--;
    }

    public boolean equals( java.lang.Object comp)
	{
		if ( comp instanceof ShortValue)
			return ( this.value == ((ShortValue) comp).value);
		else if ( comp instanceof Short)
			return ( this.value == ((Short) comp).shortValue());
		else
			return false;
	}

    /* zero if they are the same,
       positive if this is greater than comp,
       negative if this is less than comp. */
    public int compareTo( short comp)
    {
        return this.value - comp;
    }      
           
    public int compareTo( Object comp) 
    {
        if ( comp instanceof NumberValue)
            return this.compareTo( ((NumberValue) comp).getShortValue());
        else if ( comp instanceof Number) 
            return this.compareTo( ((Number) comp).shortValue());
        else 
            return this.compareTo( (short) comp.hashCode());
    }


    public java.lang.String toString()
	{
		return Short.toString( this.value);
	}
}

