package org.webgets.util;

/*

5 ��������CSS1 properties
5.1 ��������Notation for property values

*/
public interface CSS
{
	/** CSS.Style converts from String[]s to formatted CSS styles */
	class Style extends Object
	{
		public static String propertiesAsString( String[] properties)
		{
			Check.isLegalArg( properties.length % 2 != 0, "String[] properties must have even length");
			StringBuffer buffer = new StringBuffer( 30);
			int index = 0;
			while ( index < properties.length)
			{
				buffer.append( properties[index++]);
				buffer.append( ": ");
				buffer.append( properties[index++]);
				if ( index < properties.length)
					buffer.append( "; ");
			}
			return buffer.toString();
		}
	
		public Style( String selector, String[] properties)
		{
			super();
			this.setSelector( selector);
			this.setProperties( properties);
		}
		
		private String selector = null;
		private String[] properties = null;
		
		public void setSelector( String selector)
		{
			Check.isNotNull( selector);
			this.selector = selector;
		}
		
		public String getSelector()
		{
			return this.selector;
		}
		
		public void setProperties( String[] properties)
		{
			Check.isNotNull( properties);
			Check.isLegalArg( properties.length % 2 != 0, "String[] properties must have even length");
			this.properties = properties;
		}
		
		public String[] getProperties()
		{
			return this.properties;
		}
		
		public String getPropertiesAsString()
		{
			return Style.propertiesAsString( this.getProperties());
		} 
		
		public String toString()
		{
			return this.getSelector() + " { " + this.getPropertiesAsString() + " } ";
		}
	}

	/*	5.2 ��������Font properties */
	
	/*	5.2.1 ��������Font matching */
	
	/** CSS Section 5.2.2 : font-family */
	String FONT_FAMILY = "font-family";
	
	/*	'serif' (e.g. Times) */
	String SERIF = "serif";

	/*	'sans-serif' (e.g. Helvetica) */
	String SANS_SERIF = "sans-serif";	
	
	/*	'cursive' (e.g. Zapf-Chancery) */
	String CURSIVE = "cursive";	
	
	/*	'fantasy' (e.g. Western) */
	String FANTASY = "fantasy";	
	
	/*	'monospace' (e.g. Courier) */
	String MONSPACE = "monospace";	

	/** CSS Section 5.2.3 : font-style */
	String FONT_STYLE = "font-style";
	
	/** CSS Section 5.2.4 : font-variant */
	String FONT_VARIANT = "font-variant";
	
	/** CSS Section 5.2.5 : font-weight */
	String FONT_WEIGHT = "font-weight";
	
	/** CSS Section 5.2.6 : font-size */
	String FONT_SIZE = "font-size";
	
	/** CSS Section 5.2.7 : font */
	String FONT = "font";
	
	
	/* 5.3 ��������Color and background properties */
	
	/** CSS Section 5.3.1 : color */
	String COLOR = "color";
	
	/** CSS Section 5.3.2 : background-color */
	String BACKGROUND_COLOR = "background-color";
	
	/** CSS Section 5.3.3 : background-image */
	String BACKGROUND_IMAGE = "background-image";
	
	/** CSS Section 5.3.4 : background-repeat */
	String BACKGROUND_REPEAT = "background-repeat";
	
	/** CSS Section 5.3.5 : background-attachment */
	String BACKGROUND_ATTACHMENT = "background-attachment";
	
	/** CSS Section 5.3.6 : background-position */
	String BACKGROUND_POSITION = "background-position";
	
	/** CSS Section 5.3.7 : background */
	String BACKGROUND = "background";
	
	
	/* 5.4 ��������Text properties */
	
	/** CSS Section 5.4.1 : word-spacing */
	String WORD_SPACING = "word-spacing";
	
	/** CSS Section 5.4.2 : letter-spacing */
	String LETTER_SPACING = "letter-spacing";
	
	/** CSS Section 5.4.3 : text-decoration */
	String TEXT_DECORATION = "text-decoration";
	
	/** CSS Section 5.4.4 : vertical-align */
	String VERTICAL_ALIGN = "vertical-align";
	
	/** CSS Section 5.4.5 : text-transform */
	String TEXT_TRANSFORM = "text-transform";
	
	/** CSS Section 5.4.6 : text-align */
	String TEXT_ALIGN = "text-align";
	
	/** CSS Section 5.4.7 : text-indent */
	String TEXT_INDENT = "text-indent";
	
	/** CSS Section 5.4.8 : line-height */
	String LINE_HEIGHT = "line-height";
	
	
	/* 5.5 ��������Box properties */
	
	/** CSS Section 5.5.1 : margin-top */
	String MARGIN_TOP = "margin-top";
	
	/** CSS Section 5.5.2 : margin-right */
	String MARGIN_RIGHT = "margin-right";
	
	/** CSS Section 5.5.3 : margin-bottom */
	String MARGIN_BOTTOM = "margin-bottom";
	
	/** CSS Section 5.5.4 : margin-left */
	String MARGIN_LEFT = "margin-left";
	
	/** CSS Section 5.5.5 : margin */
	String MARGIN = "margin";
	
	/** CSS Section 5.5.6 : padding-top */
	String PADDING_TOP = "padding-top";
	
	/** CSS Section 5.5.7 : padding-right */
	String PADDING_RIGHT = "padding-right";
	
	/** CSS Section 5.5.8 : padding-bottom */
	String PADDING_BOTTOM = "padding-bottom";
	
	/** CSS Section 5.5.9 : padding-left */
	String PADDING_LEFT = "padding-left";
	
	/** CSS Section 5.5.10 : padding */
	String PADDING = "padding";
	
	/** CSS Section 5.5.11 : border-top-width */
	String BORDER_TOP_WIDTH = "border-top-width";
	
	/** CSS Section 5.5.12 : border-right-width */
	String BORDER_RIGHT_WIDTH = "border-right-width";
	
	/** CSS Section 5.5.13 : border-bottom-width */
	String BORDER_BOTTOM_WIDTH = "border-bottom-width";
	
	/** CSS Section 5.5.14 : border-left-width */
	String BORDER_LEFT_WIDTH = "border-left-width";
	
	/** CSS Section 5.5.15 : border-width */
	String BORDER_WIDTH = "border-width";
	
	/** CSS Section 5.5.16 : border-color */
	String BORDER_COLOR = "border-color";
	
	/** CSS Section 5.5.17 : border-style */
	String BORDER_STYLE = "border-style";
	
	/** CSS Section 5.5.18 : border-top */
	String BORDER_TOP = "border-top";
	
	/** CSS Section 5.5.19 : border-right */
	String BORDER_RIGHT = "border-right";
	
	/** CSS Section 5.5.20 : border-bottom */
	String BORDER_BOTTOM = "border-bottom";
	
	/** CSS Section 5.5.21 : border-left */
	String BORDER_LEFT = "border-left";
	
	/** CSS Section 5.5.22 : border */
	String BORDER = "border";
	
	/** CSS Section 5.5.23 : width */
	String WIDTH = "width";
	
	/** CSS Section 5.5.24 : height */
	String HEIGHT = "height";
	
	/** CSS Section 5.5.25 : float */
	String FLOAT = "float";
	
	/** CSS Section 5.5.26 : clear */
	String CLEAR = "clear";
	
	
	/*  5.6 ��������Classification properties */
	
	/** CSS Section 5.6.1 : display */
	String DISPLAY = "display";
	
	/** CSS Section 5.6.2 : white-space */
	String WHITE_SPACE = "white-space";
	
	/** CSS Section 5.6.3 : list-style-type */
	String LIST_STYLE_TYPE = "list-style-type";
	
	/** CSS Section 5.6.4 : list-style-image */
	String LIST_STYLE_IMAGE = "list-style-image";
	
	/** CSS Section 5.6.5 : list-style-position */
	String LIST_STYLE_POSITION = "list-style-position";
	
	/** CSS Section 5.6.6 : list-style */
	String LIST_STYLE = "list-style";
	
} 
