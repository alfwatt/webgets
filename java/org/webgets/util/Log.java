package org.webgets.util;

import java.io.PrintStream;

/**

Log is used to print formatted messages to the error log. 

Log provides static methods msg() which simplify printing to a servlets log stream.

Log provides static methods printMsg(), printDebug(), printWarn() and printError() for
printing so System.out or another PrintStream by calling setErr().

*/
public final class Log extends Object
{
	private static PrintStream err = System.err;

	private static final String DEBUG_BANNER = "[debug] ";
	private static final String  WARN_BANNER = "[WARNING] ";
	private static final String ERROR_BANNER = "[ERROR] ";

	/** Logs a message to this Servlet's log stream */
	public static void msg( javax.servlet.Servlet servlet, String message)
	{
		servlet.getServletConfig().getServletContext().log( message);
	}

	/** Logs a message and a Throwable this Servlet's log stream */
	public static void msg( javax.servlet.Servlet servlet, String message, Throwable throwable)
	{
		servlet.getServletConfig().getServletContext().log( message, throwable);
	}

	/** Set the error stream for message printing */
	public void setErr( PrintStream err)
	{
		this.err = err;
	}

	/** Prints a message to the Log.err stream (System.err) */
	public static final void printMsg( String msg)
	{
		err.println( msg);
	}

	/** Print a debug message to the Log.err stream (System.err) 
		depending on the value of Debug.DEBUG */
	public static final void printDebug( String debugMsg)
	{
		if ( Debug.DEBUG)
		{
			err.print( DEBUG_BANNER);
			err.println( debugMsg);
		}
	}

	/** Print a warning message to the Log.err stream (System.err) 
		depending on the value of Debug.WARN */
	public static final void printWarn( String warnMsg)
	{
		if ( Debug.WARN)
		{
			err.print( WARN_BANNER);
			err.println( warnMsg);
		}
	}

	public static final void printWarn( String warnMsg, Throwable printTrace)
	{
		if ( Debug.WARN)
		{
			err.print( WARN_BANNER);
			err.println( warnMsg);
			printTrace.printStackTrace( err);
		}
	}

	/** Print an error message to the Log.err. stream (System.err)
		depending on the value of Debug.ERROR */
	public static final void printErr( String errMsg)
	{
		if ( Debug.ERROR)
		{
			err.print( ERROR_BANNER);
			err.println( errMsg);
		}
	}

	public static final void printErr( String errMsg, Throwable printTrace)
	{
		if ( Debug.ERROR)
		{
			err.print( ERROR_BANNER);
			err.println( errMsg);
			printTrace.printStackTrace( err);
		}
	}
}
