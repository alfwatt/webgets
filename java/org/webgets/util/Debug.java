package org.webgets.util;

/**

Debug defines static final values for conditional compilation of java code. The java
compiler will optimize out code inside a block like:

<pre>
	if ( Debug.DEBUG)
	{
		// code here does not make it into the class files if DEBUG is false
	}
</pre>

This can be useful for developer builds where you may want to insert debugging statements
to trace events in the webgets code but do not want to impact the production performance of
the components.

*/
public final class Debug extends Object
{
	/** Guards debugging statements like: "foo=bar" */
	public static boolean DEBUG  = true;

	/** Guards WARNing statements like: "Some Value is null" */
	public static boolean WARN   = true;

	/** Guards ERROR statements like: "This Can't Happen" */
	public static boolean ERROR  = true;

	/** Guards TIMING statements like: "Some Operation: 12345ms" */
	public static boolean TIMERS = true;

	/** Guards TRACE_INIT statements line: "init: componentLabel" */
	public static boolean TRACE_INIT = true;

	/** Guards TRACE_DRAWS statements like: "onDraw: componentLabel" */
	public static boolean TRACE_DRAWS = true;
	
	/** Guards TRACE_REQUESTS statements like: "onRequest: coponentLabel" */
	public static boolean TRACE_REQUESTS = true;

	private Debug()
	{
		super();
	}
}
