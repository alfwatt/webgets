package org.webgets.util;

import java.io.*;

public final class Streams
{

	public static void copy( InputStream in, OutputStream out)
		throws IOException
	{
		Streams.copy( in, out, 1024);
	}

	public static void copy( InputStream in, OutputStream out, int bufferSize)
		throws IOException
	{
		byte[] buffer = new byte[bufferSize];
		int count = 0;
		
		while ( ( count = in.read( buffer)) >= 0)
			out.write( buffer, 0, count);

		out.flush();
	}

	public static void tee( InputStream in, OutputStream out, OutputStream dup)
		throws IOException
	{
		Streams.tee( in, out, dup, 1024);
	}

	public static void tee( InputStream in, OutputStream out, OutputStream dup, int bufferSize)
		throws IOException
	{
		byte[] buffer = new byte[bufferSize];
		int count = 0;
		
		while ( ( count = in.read( buffer)) >= 0)
		{
			out.write( buffer, 0, count);
			dup.write( buffer, 0, count);
		
		}

		out.flush();
		dup.flush();
	}
}
