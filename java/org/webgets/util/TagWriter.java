package org.webgets.util;

import javax.servlet.http.*;

/**

	<p>TagWriter extends PrintWriter and adds methods for writing HTML/XML tags, 
	quickly, with minimal irritation.</p>
	
	<p>TagWriters are be created around a ServletRequest and ServletResponse pair
	which provide the output stream as well as information for encoding URLs
	written to web pages.</p>
	
	<p>TagWriters maintian a stack representing the tags which have allready been
	written, this stack is used to make closing tags automatic:</p>
	
	<pre>
	TagWriter out = new TagWriter( request, response);
	out.tag( "p");
		out.print( "The text of the paragraph");
	out.closeTag(); // refers to the last call to tag()
	</pre>
	
	<p>The output of this code will be:</p>
	
	<p><tt>&lt;p&gt;The text of the paragraph&lt;/p&gt;</tt></p>

	<p>TagWriter also maintains a stack of marks, so many tags can be opened
	and then closed all at once:</p>
	
	<pre>
	TagWriter out = new TagWriter( request, response);
	out.mark();
	out.tag( "html");
		out.tag( "body");
			out.tag( "h1");
				out.print( "Hello Tag Writer");
	out.closeToMark();
	</pre>
	
	<p>The output being:</p>
	
	<pre>
	&lt;html&gt;
	  &lt;body&gt;
	  	&lt;h1&gt;Hello Tag Writer&lt;/h1&gt;
	  &lt;/body&gt;
	 &lt;/html&gt;
	</pre>

*/
public class TagWriter extends java.io.PrintWriter
{
	//
	// Class Members

	/* misc chars */
	public static final char LT = '<';
	public static final char GT = '>';
	public static final char FS = '/';
	public static final char QT = '"';
	public static final char EQ = '=';
	public static final char SP = ' ';
	public static final char EX = '!';
	public static final char NL = '\n';
	public static final char QUES = '?';
	public static final char AMP = '&';
	public static final char COL = ':';

	/* misc strings */
	public static final String DD = "--";
	public static final String INDENT = "\t";
	public static final String NULL = "null";
	public static final String EXCEPTION = "exception";

	//
	// Class Methods

    /** 

	Trims a string to 25 chars, taking the excess out of the middle and replacing
	it with three periods "..." to simulate an elipsis.

	sssssssssss...sssssssssss

	*/
	public static String trimString( String longString)
	{
		if ( longString.length() >= 25)
		{
			StringBuffer trimBuffer = new StringBuffer( 25);
			trimBuffer.append( longString.substring( 0, 10));
			trimBuffer.append( "...");
			trimBuffer.append( longString.substring( longString.length() - 10));
			return trimBuffer.toString();
		}
		else
			return longString;
	}

    /** 
    
    Trims a string to the specified length, taking the excess off the end
    and appending three periods "..." to simulate an elipsis. 
    
    sssssssss...
        
    */
	public static String trimString( String longString, int length)
	{
		if ( longString.length() <= length)
		{
			StringBuffer trimBuffer = new StringBuffer( length);
			trimBuffer.append( longString.substring( 0, length-3));
			trimBuffer.append( "...");
			return trimBuffer.toString();
		}
		else
			return longString;
	}

	/** @return a version of the string sutable for putting in URLs */
	public static String URLencode( String escape)
	{
		StringBuffer workBuffer = new StringBuffer( escape);

		int index = 0;

		/* has to re-eval each time, since the buffer can change size */
		while ( index < workBuffer.length()) 
		{
			if ( workBuffer.charAt( index) == ' ') /* spaces go to plusses */
				workBuffer.setCharAt( index, '+');

			index++;
			// TODO, other usefull cases...
		}
		
		return workBuffer.toString();
	}
	
	/** @return a version of the provided String with URL encoded chars converted to normal */
	public static String URLdecode( String decode)
	{
		return decode;
	}

	public static String stripWhitespace( String strip)
	{
		StringBuffer bed = new StringBuffer( strip.length());
		int index = 0;
		while ( index < strip.length())
		{
			switch ( strip.charAt( index))
			{
				case '\n': break;
				case '\t': break;
				case ' ':  break;
				default: bed.append( strip.charAt( index)); break;
			}
			index++;
		}
		return bed.toString().intern();
	}
	
	//
	// Members

	private java.util.Stack context = new java.util.Stack(); // tag context stack
	private java.util.Stack marks = new java.util.Stack();   // mark stack
	private boolean clean = true; // is the indent level clean?
	private boolean indent = true; // should I draw indents?
	private int indentDisables = 0; // how deep is the disable indent stack?
	private boolean xml = false; // draw XML style tags?
	private String requestURL = null; // the URL of this request
	private HttpServletRequest request = null; // the request we are servicing
	private HttpServletResponse response = null; // the response that we are writing 
	private boolean encodeURLs = true; // should we encode URL's for session tracking as they are printed?

	// namespae support
	private java.util.Stack namespaces = new java.util.Stack(); // the stack of nested namespaces
	private String namespace = null; // the current namespace

	//
	// Constructors
	
	/**
	
	Create a new TagWriter with the reuqest URL bound to the response output stream.
	
	*/
	public TagWriter( HttpServletRequest request, HttpServletResponse response)
		throws java.io.IOException
	{
		super( response.getWriter());
		this.request = request;
		this.response = response;
	}

	/**
	
	Create a new TagWriter with the reuqest URL bound to the response output stream.
	
	*/
	public TagWriter( HttpServletRequest request, HttpServletResponse response, boolean encodeURLs)
		throws java.io.IOException
	{
		this( request, response);
		this.encodeURLs = encodeURLs;
	}


	//
	// Public Methods

	/* tag output methods */
	public void tag( String tagName)
	{
		if ( this.indent) this.printIndent();

		this.pushContextStack( tagName);

		synchronized ( this) // keep any other threads from munging our tags
		{
			this.write( LT);
			if ( this.namespace != null)
			{
				this.write( this.namespace);
				this.write( COL);
			}
			this.write( tagName);
			this.write( GT);
		}
	}

	public void tag( String tagName, String[] tagAttrs)
	{
		if ( tagAttrs == null)
			throw new IllegalArgumentException( "tagAttrs must not be null");
		else if ( tagAttrs.length % 2 != 0)
			throw new IllegalArgumentException(
				"tagAttrs must have an even number of elements");

		if ( this.indent) this.printIndent();

		this.pushContextStack( tagName);

		synchronized ( this)
		{
			this.write( LT); // <
			if ( this.namespace != null)
			{
				this.write( this.namespace);
				this.write( COL);
			}
			this.write( tagName);
			this.writeAttributes( tagAttrs);
			this.write( GT); // >
		}
	}

	public void tag( String tagName, Attributes tagAttrs)
	{
		this.tag( tagName, tagAttrs.toStringArray());
	}

	/**

	Prints tags like: <tagname attrName="attrValue?query=Args"> ... </tagname>
	'er I mean like: <a href="http://host/path"> ... </a>
	'er I mean line: <form action="http://host/path"> ... </form>

	*/	
	public void tag( String tagName, String attrName, String attrValue)
	{
		this.tag( tagName, attrName, attrValue, null, true);
	}
	
	public void tag( String tagName, String attrName, String attrValue, boolean local)
	{
		this.tag( tagName, attrName, attrValue, null, local);
	}
	
	/**
	
	Prints tags like tag( String, String, String, String[], boolean) with default
	local value of true.
	
	*/
	public void tag( String tagName, String attrName, String attrValue, String[] queryArgs)
	{
		this.tag( tagName, attrName, attrValue, queryArgs, true);
	}
	
	
	/**
	
	Prints tags like: <tagname attrName="attrValue?query=Args"> ... </tagname>
	'er I mean like: <a href="http://host/path?target=xyzzy&action=ACK"> ... </a>

	So it's a special purpose method that optimises away one more case, it's really
	a bit of chromoe, but I like it, it shows how tight a ship I want to run here.
	
	*/
	public void tag( String tagName, String attrName, String attrValue, String[] queryArgs, boolean local)
	{
		if ( tagName == null || attrName == null || attrValue == null)
			throw new IllegalArgumentException( "tagName, attrName, and attrValue nust not be null");
		else if ( queryArgs != null && queryArgs.length % 2 != 0)
			throw new IllegalArgumentException( "queryArgs must have an even number of elements");
			
		if ( this.indent) this.printIndent();
		
		this.pushContextStack( tagName);
		
		synchronized (this)
		{
			this.write( LT);
			if ( this.namespace != null)
			{
				this.write( this.namespace);
				this.write( COL);
			}
			this.write( tagName);
			this.write( SP);
			this.write( attrName);
			this.write( EQ);
			this.write( QT);
			if ( local)
				this.write( this.encodeURL( attrValue));
			else
				this.write( attrValue);
			if ( queryArgs != null && queryArgs.length != 0)
				this.writeQueryString( queryArgs);
			this.write( QT);
			this.write( GT);
		}
	}

	/** print an HTML style empty tag with no attributes and a minmum of fuss.
	 Great for <p>, <br>, <td> and the like. */
	public void emptyTag( String tagName)
	{
//		if ( this.indent) this.printIndent();

		/* draw the tag */
		synchronized ( this) // see note above
		{
			this.write( LT); // <
			if ( this.namespace != null)
			{
				this.write( this.namespace);
				this.write( COL);
			}
			this.write( tagName);
			if ( xml)
				this.write( FS); // /
			this.write( GT); // >
		}
	}

	public void emptyTag( String tagName, Attributes attrs)
	{
		this.emptyTag( tagName, attrs.toStringArray());
	}

	public void emptyTag( String tagName, String[] tagAttrs)
	{
		if ( tagAttrs == null)
			throw new IllegalArgumentException( "tagAttrs must be null");
		else if ( tagAttrs.length % 2 != 0)
			throw new IllegalArgumentException(
				"tagAttrs must have an even number of elements");

		if ( this.indent) this.printIndent();

		// dirty the context so that the next tag gets an indent
		this.clean = false;

		synchronized ( this)
		{
			this.write( LT);
			if ( this.namespace != null)
			{
				this.write( this.namespace);
				this.write( COL);
			}
			this.write( tagName);

			this.writeAttributes( tagAttrs);

			if ( xml)
				this.write( FS); // /
			this.write( GT); // >
		}
	}

	//
	// Public Methods -- Close Up Tags

	/**	Print the close tag for the last tag on the stack. */
	public void closeTag()
	{
		if ( ! this.clean && this.indent)
			this.printCloseIndent();

		// dirty the context so that the next close tag breaks line
		this.clean = false;

		synchronized (this) // see note above
		{
			this.write( LT); // <
			this.write( FS); // /
			if ( this.namespace != null)
			{
				this.write( this.namespace);
				this.write( COL);
			}
			this.write( this.popContextStack());
			this.write( GT); // >
		}
	}
	
	/**
	
	Closes a tag and checks to make sure that the right tag was closed relative to the stack.
	This serves as an assert when drawing components and is especially usefull for drawing
	HTML.TABLEs and other complex elements as they tend to have nesting problems.
	
	out.tag( HTML.P);
		out.print( "Hello World");
	out.closeTag( HTML.B); // Tag NestingException is thrown here
	
	@throws TagNestingException if the provided tag does not match the last tag on the stack
	
	*/
	public void closeTag( String provided) // throws TagNestingException
	{
		String needed = this.peekContextStack();

		/* first check for same object (think HTML.*), then check for stringwise eq */
		if ( needed != provided || ! needed.equals( provided))
			throw new TagNestingException( provided, needed);

		this.closeTag();
	}

	/** Print the close tag for the last tag on the stack. */
	public void closeTags( int count)
	{
		while ( count > 0)
		{
			this.closeTag(); // XXX inline
			count--;
		}
	}

	/** Print close tags for all tags on the stack, good for Page to run last thing. */
	public void closeAllTags()
	{
		while ( ! this.context.empty())
		{
			this.closeTag();
		}
	}

	public void comment( String comment)
	{
		/* just draw the comment, indent level be damned */
		synchronized ( this)
		{
			this.write( SP);
			this.write( LT); // <
			this.write( EX); // !
			this.write( DD); // --
			this.write( SP);
			this.write( comment != null ? comment : NULL);
			this.write( SP);
			this.write( DD); // --
			this.write( GT); // >
			this.write( SP);
		}
	}

	public void write( String string)
	{
		if ( string != null)
			super.write( string);
	}

	public void write( Object object)
	{
		if ( object != null)
			super.write( object.toString());
	}

	/**
	
	@return the URL provided, encoded for session tracking. If the TagWriter was created with URL encoding turned
	off (by calling the three-arg constructor) the URL will not be encoded. This manitains compatability with older
	serlvet engines that do not support encodeURL (they're out there!)
	
	*/	
	public String encodeURL( String url)
	{
		if ( this.encodeURLs)
			return this.response.encodeUrl( url);
		else
			return url;
	}
	
	/**
	
	@return the URL of the request that we are responding to, encoded for session tracking if appropriate.
	
	*/
	public String getRequestURL()
	{
		if ( this.requestURL == null)
			this.requestURL = HttpUtils.getRequestURL( request).toString();
		
		return this.requestURL;
	}
	
	public HttpServletRequest getRequest()
	{
		return this.request;
	}
	
	public HttpServletResponse getResponse()
	{
		return this.response;
	}
	
	/** 
	
	Turns off auto-indent of the tag stream for best rendering control.
	
	Calls to disableIndent can nest (like locks), so calls must appear in pairs.
	If you ever call disableIndent() you should be sure to turn it back on somewhere
	else to mantain correct formatting.  
	
	*/
	public void disableIndent() 
	{
		if ( ++this.indentDisables > 0)
			this.indent = false;
	}

    /** 
    
    Turns on auto-indent of the tag stream for best human readability. 
    
    Calls to enableIndent() on a stream you know to have indents disabled may or may not
    enable indenting. If enough disables have been called you may not be near the top of
    the stack. It is important to keep these calls in balance with disableIndent() 
    calls in your code. 
    
    */
	public void enableIndent() 
	{ 
		if ( --this.indentDisables <= 0)
			this.indent = true;
	}	

    /** turn on drawing of XML style empty tags */
	public void enableXML() { this.xml = true; }
	
	/** turn off drawing of XML style empty tags */
	public void disableXML() { this.xml = false; }

	/** mark the stack so we can closeToMark() */
	public void mark()
	{
		marks.push( new Integer( this.getStackSize()));
	}

	public void clearMark()
	{
		marks.pop();
	}

	/** Close open tags until we hit the next mark point. */
	public void closeToMark()
	{
		this.closeTags(
			this.getStackSize() - ( (Integer) marks.pop()).intValue());
	}

	/** Enter an XML namespace */
	public void namespace( String prefix)
	{
		Check.isLegalArg( this.xml, "Cannot enter a namespace unless XML is enabled!");
		this.namespaces.push( prefix);
		this.namespace = prefix;
	}
	
	/** Exit an XML namespace */
	public void closeNamespace()
	{
		this.namespaces.pop();
		this.namespace = this.namespaces.size() > 0 ? (String) this.namespaces.peek() : null;
	}

	//
	// Private Methods

	/* indent methods */
	private final void printIndent()
	{
		int depth = this.getStackSize();

		synchronized ( this) // see above
		{
			/* start a new line for the indented string */
			this.write( NL);

			for ( int i = 0; i < depth; i++)
				this.write( INDENT);
		}
		/* context is clean untill the next println() or close tag */
		this.clean = true;
	}

	private final void printCloseIndent()
	{
		int depth = this.getStackSize();

		synchronized ( this) // see above
		{
			/* start a new line for the indented string */
			this.write( NL);

			for ( int i = 1; i < depth; i++) // one less indent level
				this.write( INDENT);
		}
		/* context is clean untill the next println() or close tag */
		this.clean = true;
	}

	/* context stack methods */
	private void pushContextStack( String tagName) 
	{
		this.context.push( tagName); 
	}

	private String popContextStack()
	{
		return (String) this.context.pop();
	}
	
	private String peekContextStack()
	{
		return (String) this.context.peek();
	}
	
	private int getStackSize()
	{
		return this.context.size();
	}

	/* utilty method for printing tag attributes: foo="bar" */
	private void writeAttributes( String[] attributes)
	{
		int index = 0; // set index to first key
		String temp = null;

		while ( index < attributes.length)
		{
			if ( attributes[index] != null) // ignore nulls
			{
				this.write( SP); // _
				this.write( attributes[ index]);

				index++; // increment to the value

				temp = attributes[index];

				if ( temp!=null || xml)
				{
					this.write( EQ); // =
					this.write( QT); // "
					this.write( ( temp==null ? "" : temp));
					this.write( QT); // "
				}
				index++; // increment to next key
			}
			else
			{
				index += 2; // skip this key and value
			}
		}
	}

	/* utility method for priting query string arguments: ?foo=bar&baz=bla */
	private void writeQueryString( String[] queryString)
	{
		int index = 0; // set index to first key
		String temp = null;

		this.write( QUES);

		while ( index < queryString.length)
		{
			if( index > 0)
				this.write( AMP);
			if ( queryString[index] != null) // ignore nulls
			{
				// TDOO Encode!
				this.write( queryString[ index]);

				index++; // increment to the value

				temp = queryString[index];

				if ( temp!=null)
				{
					this.write( EQ); // =
					// TODO Encode!
					this.write( temp);
				}
				index++; // increment to next key
			}
			else
			{
				index += 2; // skip this key and value
			}
		}
	}

	//
	// Overriden Methods

	/* override the println methods for null, string and object to mark the context as dirty */
	public final void println()
	{
		this.clean = false;
		super.println();
	}

	public final void println( String s)
	{
		this.clean = false;
		super.println( s);
	}

	public final void println( Object o)
	{
		this.clean = false;
		super.println( o);
	}

	/** flush and close our printwriter before the DrawingContext is reclaimed by the system */
	public void finalize() throws Throwable
	{
		if ( true /* this.out != null */) // HACK
		{
			if ( Debug.DEBUG) System.err.println( "[debug] TagWriter finalize()ed at: "
				+ System.currentTimeMillis());

			/* in case the context stack still has entries */
			this.closeAllTags();
		}

		super.finalize();
	}
}

/* Love, Alf */
