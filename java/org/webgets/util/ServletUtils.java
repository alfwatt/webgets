package org.webgets.util;

/**

ServletUtils contains conveniance methods for coverting request path information. Methods
are provided to get the file system path of a request and the base path of a request (minus
the Servlet or JSP name).

*/
public final class ServletUtils extends Object
{
	/** @return the base  pat of the request URI (minus the servlet path) */
	public static String getBasePathOfRequest( javax.servlet.http.HttpServletRequest request)
	{
		/* servlet.jsp for example */
		String servletPath = request.getServletPath();

		/* /utils/servlet.jsp for example */
		String requestURI = request.getRequestURI();

		return requestURI.substring( 0, requestURI.indexOf( servletPath));
	}
}
