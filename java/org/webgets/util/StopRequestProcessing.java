package org.webgets.util;

/**

The StopRequestProcessing RuntimeException is throws by RequestEvent to short circut 
the rest of the request passing chain once the event has been consumed. Code which
starts request processing must be ready to catch this exception because it will be
thrown if the event is consumed.

*/
public class StopRequestProcessing extends RuntimeException
{
	private static final String STOPPED = "Request Processing Stopped By: ";
	private Object stoppedBy = null;

	public StopRequestProcessing( Object stoppedBy)
	{
		super( STOPPED + stoppedBy.toString());
		this.stoppedBy = stoppedBy;
	}

	public Object stoppedBy()
	{
		return this.stoppedBy;
	}
}
