package org.webgets.util;

import javax.servlet.ServletConfig;

/**

WebgetsConfig extends ServletConfig

*/
public class WebgetsConfig implements javax.servlet.ServletConfig, java.io.Serializable
{	
	private ServletConfig config = null;

	public WebgetsConfig( ServletConfig config)
	{	
		super();
		this.setServletConfig( config);
	}
	
	public ServletConfig getServletConfig()
	{
		return this.config;
	}
	
	public void setServletConfig( ServletConfig config)
	{
		Check.isNotNull( config);
		this.config = config;
	}
	
	public String getInitParameter( String name)
	{
		return this.config.getInitParameter( name);
	}
                    
	public java.util.Enumeration getInitParameterNames()
	{
		return this.config.getInitParameterNames();
	}

	public javax.servlet.ServletContext getServletContext()
	{
		return this.config.getServletContext();
	}

	public String getServletName()
	{
		return this.config.getServletName();
	}
		
}
