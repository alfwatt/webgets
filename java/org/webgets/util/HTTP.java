package org.webgets.util;

/**

The HTTP interface proides constants for various HTTP protocol strings.

*/
public interface HTTP 
{
	/* method attribute values */
	String METHOD = "METHOD";
		String GET    = "GET";
		String POST   = "POST";
		String HEAD   = "HEAD";
		String LINK   = "LINK";	
		String UNLINK = "UNLINK";	
		String PUT    = "PUT";
		String DELETE = "DELETE";
		String TRACE  = "TRACE";
		String CONNECT= "CONNECT";

	/* content type */
	String CONTENT_TYPE = "Content-Type";

	/* general headers */
	String CACHE_CONTROL = "Cache-Control";

		/* cache control values */
		String NO_CACHE = "no-cache";
		String NO_STORE = "no-store";
		String MAX_AGE = "max-age";
		
		/* cache control request values */
		String MAX_STATE = "max-stale";
		String MIN_FRESH = "min-fresh";
		String ONLY_IF_CACHED = "only-if-cached";
		
		/* cache control response values */
		String PUBLIC = "public";
		String PRIVATE = "private";
		String NO_TRANSFORM = "no-transform";
		String MUST_REVALIDATE = "must-revalidate";
		String PROXY_REVALIDATE = "proxy-revalidate";

	/* refresh header */
	String REFRESH = "Refresh";
		String MINUTE = "60";
		String HOUR = "1200";

	/* client headers */
	String HOST = "Host";
	String USER_AGENT = "User-Agent";
	
	/* server headers */
	String WARNING = "Warning";
}
