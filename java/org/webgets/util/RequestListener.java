package org.webgets.util;

public interface RequestListener extends java.util.EventListener
{
	void onRequest( RequestEvent request); // throws javax.servlet.ServletException, java.io.IOException;

	abstract class TracedRequestListener implements RequestListener
	{
		public void onRequest( RequestEvent request)
		{
			if ( Debug.TRACE_REQUESTS)
				Log.printMsg( "[onRequest] " + this.toString());
		}
	}
}
