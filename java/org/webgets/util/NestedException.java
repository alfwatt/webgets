package org.webgets.util;

public class NestedException extends Exception
{
	protected final Throwable nested;
	
	public NestedException( Throwable nested)
	{
		super( nested.getMessage());
		this.nested = nested;
	}
	
	public NestedException( String message, Throwable nested)
	{
		super( message);
		this.nested = nested;
	}
	
	public Throwable getNested()
	{
		return this.nested;
	}
	
	public String toString()
	{
		return super.toString() + "\n\tnested: " + this.nested.toString();
	}
	
	public void printStackTrace( java.io.PrintWriter out)
	{
		super.printStackTrace( out);
		out.println( "\n\tnested stack trace:");
		this.nested.printStackTrace( out);
	}
}
