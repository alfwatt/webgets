package org.webgets.util;

/**

The Identifiable interface defines method for identifying webgets components
using a unique int value. These method used to be defined on Component, but I
got picky one Cristmas Eve and decided to remove a cross-package depandancy.

*/
public interface Identifiable
{
	/** @return the ID of this component as an int. The ID is assigned at creation
	    time and should be unique per VM */
	int getId();
	
	/** @return the ID of this component as a String. */
	String getIdString();
	
}
