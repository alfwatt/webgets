package org.webgets.util;

/**

NumberValue is an abstract class which defines methods for conversion
between the various numeric types, it is the base class for a set of
mutable replacements for the java.lang.Number classes.

Subclasses of NumberValue define the actual storage format and therefore
the precision of the value. Method names have been imporved from
java.lang.NumberValueber on which this class is based to make the
classes java beans. The classes are not declaired final, so further
enhancements could be made such as bound properties or better hashing
(which may be poor for the floating point values).

*/
public abstract class NumberValue extends java.lang.Number 
	implements java.io.Serializable, java.lang.Cloneable
{
	
	/**
	
	Create a number value for the Class provided.
	
	@param clzz is the class of Number you want a NumberValue for, 
	can be Integer.class or Integer.TYPE
	@return a NumberValue with native storage for the type you requested, 
	null if an appropriate NumberValue cannot be found.
	
	*/
	public static NumberValue createNumberValue( java.lang.Class clzz)
	{
		if     ( clzz == Byte.class 
    		  || clzz == Byte.TYPE)
			return new ByteValue( (byte) 0);

		else if ( clzz == Double.class 
		       || clzz == Double.TYPE)
			return new DoubleValue( 0.0);

		else if ( clzz == Float.class 
		       || clzz == Float.TYPE)
			return new FloatValue( 0.0f);

		else if ( clzz == Integer.class 
		       || clzz == Integer.TYPE)
			return new IntValue( 0);

		else if ( clzz == Long.class 
		       || clzz == Long.TYPE)
			return new LongValue( 0l);

		else if ( clzz == Short.class 
		        || clzz == Short.TYPE)
			return new ShortValue( (short) 0);

		else 
			return null;
	}
	
	/**
	
	Creates a new NumberValue equal to the Number provided
	
	@param value a Number to take the storage type and initial value from
	@return the number value best suited to represent the numeric type
	you provide. Please pass a subtype of java.lang.Number or one of the
	primitive TYPE fields.
	
	*/
	public static NumberValue createNumberValue( java.lang.Number value)
	{
		if ( value instanceof Byte)
			return new ByteValue( value);

		else if ( value instanceof Double)	
			return new DoubleValue( value);

		else if ( value instanceof Float)
			return new FloatValue( value);

		else if ( value instanceof Integer)
			return new IntValue( value);

		else if ( value instanceof Long)
			return new LongValue( value);

		else if ( value instanceof Short)
			return new ShortValue( value);

		else 
			throw new IllegalArgumentException( "NumberValue.createNumberValue() value"
			+ "provided is a number but we don't recognize the type, which"
			+ "can't happen!");
	}

	/* package protected constructor so that no foregn classes can extend */
	NumberValue()
	{
		super();
	}
	
	NumberValue( java.lang.Number value)
	{
		this();
		this.add( value);
	}

	/** @return the value of this NumberValue as a byte. */
    public abstract byte getByteValue();
	
	/** @param value the new byte value of this NumberValue. */
    public abstract void setByteValue( byte value);

	/** @return the value of this NumberValue as a double. */
    public abstract double getDoubleValue();

	/** @param value the new double value of this NumberValue. */
    public abstract void setDoubleValue( double value);

	/** @return the value of this NumberValue as a float. */
    public abstract float getFloatValue();

	/** @param value the new float value of this NumberValue. */
    public abstract void setFloatValue( float value);

	/** @return the value of this NumberValue as an int. */
    public abstract int getIntValue();

	/** @param value the new int value of this NumberValue. */
    public abstract void setIntValue( int value);

	/** @return the value of this NumberValue as a long. */
    public abstract long getLongValue();

	/** @param value the new long value of this NumberValue. */
    public abstract void setLongValue( long value);

	/** @return the value of this NumberValue as a short. */
    public abstract short getShortValue();

	/** @param value the new short value of this NumberValue. */
    public abstract void setShortValue( short value);

	/** @return the value as a Number */
	public abstract Number getNumberValue();
	
	/** @param value the new value of this NumberValue as a Number */
	public abstract void setNumberValue( Number value);

	/** increment this NumberValue by 1 */
    public abstract void increment();

	/** decrement this NumberValue by 1 */
    public abstract void decrement();
    
    public void add( Number number)
    {
    	this.setDoubleValue( this.getDoubleValue() + number.doubleValue());
    }
    
    public void add( NumberValue number)
    {
    	this.setDoubleValue( this.getDoubleValue() + number.getDoubleValue());
    }

	public void subtract( Number number)
	{
		this.setDoubleValue( this.getDoubleValue() - number.doubleValue());
	}
	
	public void subtract( NumberValue number)
	{
		this.setDoubleValue( this.getDoubleValue() - number.getDoubleValue());
	}
	
	public void multiply( Number number)
	{
		this.setDoubleValue( this.getDoubleValue() * number.doubleValue());
	}
	
	public void multiply( NumberValue number)
	{
		this.setDoubleValue( this.getDoubleValue() * number.getDoubleValue());
	}
	
	public void divideBy( Number number)
	{
		this.setDoubleValue( this.getDoubleValue() / number.doubleValue());
	}
	
	public void divideBy( NumberValue number)
	{
		this.setDoubleValue( this.getDoubleValue() / number.getDoubleValue());
	}
	
	public void divideInto( Number number)
	{
		this.setDoubleValue( number.doubleValue() / this.getDoubleValue());
	}
	
	public void divideInto( NumberValue number)
	{
		this.setDoubleValue( number.getDoubleValue() / this.getDoubleValue());
	}
	
	// TODO other math opts: power, exp, min, max, round, etc

	public int hashCode()
	{
		return this.getIntValue();
	}
	
	public Object clone() throws java.lang.CloneNotSupportedException
	{
		return super.clone();
	}
	
	// extends java.lang.Number
	
    /**
     * Returns the value of the specified number as an <code>int</code>.
     * This may involve rounding.
     *
     * @return  the numeric value represented by this object after conversion
     *          to type <code>int</code>.
     */
    public int intValue()
    {
    	return this.getIntValue();
    }

    /**
     * Returns the value of the specified number as a <code>long</code>.
     * This may involve rounding.
     *
     * @return  the numeric value represented by this object after conversion
     *          to type <code>long</code>.
     */
    public long longValue()
    {
    	return this.getLongValue();
    }

    /**
     * Returns the value of the specified number as a <code>float</code>.
     * This may involve rounding.
     *
     * @return  the numeric value represented by this object after conversion
     *          to type <code>float</code>.
     */
    public float floatValue()
    {
    	return this.getFloatValue();
    }

    /**
     * Returns the value of the specified number as a <code>double</code>.
     * This may involve rounding.
     *
     * @return  the numeric value represented by this object after conversion
     *          to type <code>double</code>.
     */
    public double doubleValue()
    {
    	return this.getDoubleValue();
    }

}
