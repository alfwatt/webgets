package org.webgets.util;

import java.util.*;

/**

Attributes is a HashMap which can be converted to and from a String[]
for use with a TagWriter.

*/
public class Attributes extends java.util.HashMap
{
	private String[] cached = null;

	public Attributes()
	{
		super();
	}

	public Attributes( String[] strings)
	{
		super();
		this.fromStringArray( strings);		
	}
	
	public Attributes( Object[] objects)
	{
		super();
		this.fromObjectArray( objects);
	}
	
	public Attributes( String key, String value)
	{
		super();
		this.put( key, value);
	}
	
	public Attributes( String key_1, String value_1, String key_2, String value_2)
	{
		super();
		this.put( key_1, value_1);
		this.put( key_2, value_2);
	}
	
	public Attributes( String k1, String v1, String k2, String v2, String k3, String v3)
	{
		super();
		this.put( k1, v1);
		this.put( k2, v2);
		this.put( k3, v3);	
	}
	
	/**
	
	Get's the value for key as a String, checking for nulls and
	non-String Objects and calling toString() if required.
	
	@param key the attribute name to fetch
	@return the value as a String, or null if there is no value
	
	*/
	public String getAsString( Object key)
	{
		Object value = this.get( key);

		if ( value instanceof String) 
			return (String) value;
		else if ( value == null)
			return null;
		else
			return value.toString();
	}
	
	/**
	
	Get's the value for the key as a NumberValue, otherwise returns null.
	
	@param key the attribute name to fetch
	@returns a clone of the NumberValue we have, you have to set( key, number) to make changes
	
	*/
	public NumberValue getAsNumberValue( Object key)
	{
		Object value = this.get( key);
		
		try
		{
			if ( value instanceof NumberValue)
				return (NumberValue) ((NumberValue) value).clone();
		}
		catch ( CloneNotSupportedException canthappen)
		{
			Check.cantHappen( canthappen.toString());
		}
		finally
		{
			return null;
		}
	}
	
	public Number getAsNumber( Object key)
	{
		return this.getAsNumberValue( key);
	}

	public void setInt( Object key, int value)
	{
		NumberValue number = this.getAsNumberValue( key);
		this.cached = null;
		if ( number == null)
			this.set( key, new IntValue( value));
		else
			number.setIntValue( value);
	}

	public void set( Object key, Object value)
	{
		this.put( key, value);
	}

	public Object put( Object key, Object value)
	{
		this.cached = null;
		return super.put( key, value);
	}
	
	public Object remove( Object key)
	{
		this.cached = null;
		return super.remove( key);
	}
	
	public String[] toStringArray()
	{
		if ( this.cached != null)
			return this.cached;
			
		/* build the string Array[] */
		Set keySet = this.keySet();
		Iterator keys = keySet.iterator();
		
		String[] attrArray = new String[keySet.size() * 2];

		int index = 0;
		while ( keys.hasNext())
		{
			String key = (String) keys.next();
			attrArray[index++] = key;
			attrArray[index++] = this.getAsString( key);
		}
		
		this.cached = attrArray;
		
		return attrArray;
	}
	
	public void fromStringArray( String[] init)
	{
		this.clear();
		
		int index = 0;
		
		while ( index < init.length)
			this.put( init[index++], init[index++]);

		this.cached = init;
	} 

	/**
	@throws a ClassCastException if any of the keys (even indexes) are not Strings
	*/
	public void fromObjectArray( Object[] objects)
	{
		this.clear();
		
		int index = 0;
		
		while ( index < objects.length)
			this.put( (String) objects[index++], objects[index++]);

		this.cached = this.toStringArray();
	} 

}
