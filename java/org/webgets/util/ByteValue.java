package org.webgets.util;

/**

ByteValuedefines a mutable byte value, it can be used like java.lang.Byte except that it's
value can be changed. ByteValueowes it's odd name to the fact that the java.lang package is
always imported and I did not want to shadow any of the class names when org.webgets.util.*
is imported.

*/
public class ByteValue extends NumberValue implements Comparable
{
	private byte value;

    public ByteValue( byte value)
	{
		super();
		this.setByteValue( value);
	}

    /** @throws java.lang.NumberFormatException */
    public ByteValue( java.lang.String value)
	{
		this( Byte.parseByte( value));
	}
	
	public ByteValue( Number value)
	{
		super( value);
	}

	public byte getByteValue()
	{
		return this.value;
	}

	public double getDoubleValue()
	{
		return (double) this.value;
	}

	public float getFloatValue()
	{
		return (float) this.value;
	}

	public int getIntValue()
	{
		return (int) this.value;
	}
	
	public long getLongValue()
	{
		return (long) this.value;
	}

	public short getShortValue()
	{
		return (short) this.value;
	}

	public void setByteValue( byte value)
	{
		this.value = value;
	}

	public void setDoubleValue( double value)
	{
		this.value = (byte) value;
	}

	public void setFloatValue( float value)
	{
		this.value = (byte) value;
	}

	public void setIntValue( int value)
    {
        this.value = (byte) value;
    }
		
	public void setLongValue( long value)
    {
        this.value = (byte) value;
    }
    
	public void setShortValue( short value)
    {
        this.value = (byte) value;
    }

	public Number getNumberValue()
	{
		return new Byte( this.value);
	}
	
	public void setNumberValue( Number value)
	{
		this.value = value.byteValue();
	}

	public void increment()
	{
		this.value++;
	}

	public void decrement()
	{
		this.value--;
	}

    public boolean equals( java.lang.Object comp)
	{
		if ( comp instanceof ByteValue)
			return ( this.value == ((ByteValue) comp).value);
		else if ( comp instanceof Byte)
			return ( this.value == ((Integer) comp).byteValue());
		else
			return false;
	}

	/* zero if they are the same, 
	   positive if this is greater than comp, 
	   negative if this is less than comp. */
	public int compareTo( byte comp)
	{
		return this.value - comp;
	}

	public int compareTo( Object comp)
	{
		if ( comp instanceof NumberValue)
			return this.compareTo( ((NumberValue) comp).getByteValue());
		else if ( comp instanceof Number)
			return this.compareTo( ((Number) comp).byteValue());
		else
			return this.compareTo( (byte) comp.hashCode());
	}

    public java.lang.String toString()
	{
		return Byte.toString( this.value);
	}
}

