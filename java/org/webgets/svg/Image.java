package org.webgets.svg;

import org.webgets.util.*;

/**

Attribute definitions:

x = "<coordinate>"
The x-axis coordinate of one corner of the rectangular region into which the referenced document is placed.
If the attribute is not specified, the effect is as if a value of "0" were specified.
Animatable: yes.
y = "<coordinate>"
The y-axis coordinate of one corner of the rectangular region into which the referenced document is placed.
If the attribute is not specified, the effect is as if a value of "0" were specified.
Animatable: yes.
width = "<length>"
The width of the rectangular region into which the referenced document is placed.
A negative value is an error (see Error processing). A value of zero disables rendering of the element.
Animatable: yes.
height = "<length>"
The height of the rectangular region into which the referenced document is placed.
A negative value is an error (see Error processing). A value of zero disables rendering of the element.
Animatable: yes.
xlink:href = "<uri>"
A URI reference.
Animatable: yes.

*/
public class Image extends SVGComponent
{

	public Image( String href, String width, String height)
	{
		super( href);
		this.setHref( href);
		this.setWidth( width);
		this.setHeight( height);
	}

	public void setX( String xc)
	{
		this.attrs.put( X, xc);
	}
	
	public void setY( String yc)
	{
		this.attrs.put( Y, yc);
	}
	
	public void setWidth( String width)
	{
		this.attrs.put( WIDTH, width);
	}
	
	public void setHeight( String height)
	{
		this.attrs.put( HEIGHT, height);
	}
	
	public void setHref( String href)
	{
		this.attrs.put( XLINK_HREF, href);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.emptyTag( IMAGE, this.attrs);
	}
}
