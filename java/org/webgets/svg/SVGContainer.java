package org.webgets.svg;

import org.webgets.*;
import org.webgets.util.*;

/**

SVGContainers accept only SVGs into their collection, for incentive an 
add( SVG component) method has been added which skips the type check performed
by overriding ComponentAdapter.addComponent( Component component).

Once they've been typechecked they are treated in every way as Components,
drawing order is especially important.

*/
abstract public class SVGContainer extends ContainerAdapter implements SVG
{
	protected Attributes attrs = new Attributes();

	public SVGContainer()
	{
		super();
	}
	
	public SVGContainer( String label)
	{
		super( label);
	}
	
	public SVGContainer( SVG delegate)
	{
		super( delegate);
	}
	
	public void setStyle( String style)
	{
		this.attrs.put( STYLE, style);
	}
	
	public String getStyle()
	{
		return this.attrs.getAsString( STYLE);
	}
	
	public void setStyleClass( String styleClass)
	{
		this.attrs.put( CLASS, styleClass);
	}
	
	public String getStyleClass()
	{
		return this.attrs.getAsString( CLASS);
	}
	
	public void setFill( String color)
	{
		this.attrs.put( FILL, color);
	}
	
	public String getFill()
	{
		return this.attrs.getAsString( FILL);
	}
	
	public void setStroke( String color)
	{
		this.attrs.put( STROKE, color);
	}
	
	public String getStroke()
	{
		return this.attrs.getAsString( STROKE);
	}
	
	public void setStrokeWidth( String width)
	{
		this.attrs.put( STROKE_WIDTH, width);
	}
	
	public String getStrokeWidth()
	{
		return this.attrs.getAsString( STROKE_WIDTH);
	}
	
	public void setTransform( String transform)
	{
		this.attrs.put( TRANSFORM, transform);
	}
	
	public void addTransform( String transform)
	{
		if ( this.getTransform() != null)
			this.setTransform( this.getTransform() + " " + transform);
		else
			this.setTransform( transform);
	}

	/*
	 *	translate(<tx> [<ty>]), which specifies a translation by tx and ty. 
	 *	If <ty> is not provided, it is assumed to be zero.
	 */
	public void addTranslateTransform( String tx, String ty)
	{
		this.addTransform( TRANSLATE + "(" + tx + " " + ty + ")");
	}

	/*�
	 *	scale(<sx> [<sy>]),
	 */
	public void addScaleTransform( String sx, String sy)
	{
		this.addTransform( SCALE + "(" + sx + " " + sy + ")");
	}

	public String getTransform()
	{
		return this.attrs.getAsString( TRANSFORM);
	}
	
	public void add( SVG component)
	{
		Check.isNotNull( component);
		super.addComponent( component);
	}
	
	public void addComponent( Component component)
	{
		Check.isNotNull( component);
		Check.isLegalArg( component instanceof SVG, "SVGContainer.addComponent() must be instanceof SVG");
		super.addComponent( component);
	}
}
