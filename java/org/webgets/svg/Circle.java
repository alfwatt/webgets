package org.webgets.svg;

import org.webgets.*;
import org.webgets.util.*;

/**

interface SVGCircleElement : 
                SVGElement,
                SVGTests,
                SVGLangSpace,
                SVGExternalResourcesRequired,
                SVGStylable,
                SVGTransformable,
                events::EventTarget { 

  readonly attribute SVGAnimatedLength cx;
  readonly attribute SVGAnimatedLength cy;
  readonly attribute SVGAnimatedLength r;
};

*/
public class Circle extends SVGComponent
{
	public static final String DEFAULT_CX = "100";
	public static final String DEFAULT_CY = "100";
	public static final String DEFAULT_R  = "50";

	public Circle()
	{
		super();
		this.setCX( DEFAULT_CX);
		this.setCY( DEFAULT_CY);
		this.setR( DEFAULT_R);
	}
	
	public Circle( String cx, String cy)
	{
		super();
		this.setCX( cx);
		this.setCY( cy);
		this.setR( DEFAULT_R);
	}
	
	public Circle( String cx, String cy, String radius)
	{
		super();
		this.setCX( cx);
		this.setCY( cy);
		this.setR( radius);
	}
	
	public Circle( String cx, String cy, String radius, String fill)
	{
		this( cx, cy, radius);
		this.setFill( fill);
	}
	
	public void setCY( String cy)
	{
		this.attrs.put( CY, cy);
	}
	
	public void setCX( String cx)
	{
		this.attrs.put( CX, cx);
	}
	
	public void setR( String radius)
	{
		this.attrs.put( R, radius);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		 out.emptyTag( CIRCLE, this.attrs);
	}
}
