package org.webgets.svg;

import java.beans.*;
import org.webgets.util.Log;
import org.webgets.util.beans.*;

public class GridBeanInfo extends SimpleBeanInfo
{
	public static final BeanInfo INSTANCE = new GridBeanInfo();
	
	private static final BeanDescriptor BEAN_DESCRIPTOR;
	private static final PropertyDescriptor[] BEAN_PROPS;
	
	static
	{
		BeanDescriptor bean = null;
		PropertyDescriptor[] props = null;
		
		try
		{
			bean = new BeanDescriptor( Grid.class, Object.class);
			bean.setShortDescription( "Grid");
		
			props = new PropertyDescriptor[]
			{
				BeanInfoHelper.createPropertyDescriptor( 
					"xSpace", "X Space", 
					"Vertical Space between gridlines", Grid.class),
				BeanInfoHelper.createPropertyDescriptor( 
					"ySpace", "Y Space", 
					"Horizontal Space between gridlines", Grid.class),
				BeanInfoHelper.createPropertyDescriptor( 
					"xCount", "X Count", 
					"Number of vertical gridlines", Grid.class),
				BeanInfoHelper.createPropertyDescriptor( 
					"yCount", "Y Count", 
					"Number of horizontal gridlines", Grid.class)
			};
		}
		catch ( Exception log)
		{
			Log.printErr( "GridBeanInfo could not initilize!", log);
		}
		finally
		{
			BEAN_DESCRIPTOR = bean;
			BEAN_PROPS = props;
		}
	}

	public BeanDescriptor getBeanDescriptor()
	{
		return BEAN_DESCRIPTOR;
	}
	
	public PropertyDescriptor[] getPropertyDescriptors()
	{
		return BEAN_PROPS;
	}
}
