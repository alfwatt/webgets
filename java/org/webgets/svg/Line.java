package org.webgets.svg;

import org.webgets.util.*;

public class Line extends SVGComponent
{
	public Line()
	{
		super();
		this.setX1( "25");
		this.setY1( "25");
		this.setX2( "75");
		this.setY2( "75");
	}
	
	public Line( String x2, String y2)
	{
		super();
		this.setX1( "0");
		this.setY1( "0");
	}
	
	public Line( String x1, String y1, String x2, String y2)
	{
		super();
		this.setX1( x1);
		this.setY1( y1);
		this.setX2( x2);
		this.setY2( y2);
	}
	
	public void setX1( String x1)
	{
		this.attrs.put( X1, x1);
	}
	
	public void setY1( String y1)
	{
		this.attrs.put( Y1, y1);
	}
	
	public void setX2( String x2)
	{
		this.attrs.put( X2, x2);
	}
	
	public void setY2( String y2)
	{
		this.attrs.put( Y2, y2);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.emptyTag( LINE, this.attrs);
	}
}
