package org.webgets.svg;

import org.webgets.util.*;

public class Text extends SVGComponent
{
	public Text()
	{	
		super();
	}
	
	public Text( String label)
	{
		super( label);
	}
	
	public Text( String text, String x, String y)
	{	
		this( text);
		this.setX( x);
		this.setY( y);
	}
	
	public void setX( String x)
	{
		this.attrs.put( X, x);
	}
	
	public void setY( String y)
	{
		this.attrs.put( Y, y);
	}
	
	public void setDX( String dx)
	{
		this.attrs.put( DX, dx);
	}
	
	public void setDY( String dy)
	{	
		this.attrs.put( DY, dy);
	}
	
	public void setFontFamily( String family)
	{
		this.attrs.put( FONT_FAMILY, family);
	}
	
	public void setFontSize( String size)
	{
		this.attrs.put( FONT_SIZE, size);
	}
	
	public void setRotate( String rotate)
	{
		this.attrs.put( ROTATE, rotate);
	}
	
	public void setTextLength( String tlen)
	{
		this.attrs.put( TEXT_LENGTH, tlen);
	}
	
	public void setLengthAdjust( String adjust)
	{
		this.attrs.put( LENGTH_ADJUST, adjust);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( TEXT, this.attrs);
		out.disableIndent();
			out.print( this.getLabel());
		out.closeTag();
		out.enableIndent();
	}
}
