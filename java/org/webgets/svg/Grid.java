package org.webgets.svg;

import org.webgets.util.*;

public class Grid extends SVGComponent
{
	public static final java.beans.BeanInfo BEAN_INFO = GridBeanInfo.INSTANCE;

	private IntValue xcount = new IntValue( 10);
	private IntValue ycount = new IntValue( 10);
	private IntValue xspace = new IntValue( 25);
	private IntValue yspace = new IntValue( 25);
	
	public Grid()
	{
		super();
	}
	
	public Grid( int xc, int yc)
	{
		super();
		this.setXCount( xc);
		this.setYCount( yc);
	}
	
	public Grid( int xc, int yc, int xs, int ys)
	{
		this( xc, yc);
		this.setXSpace( xs);
		this.setYSpace( ys);
	}
	
	public int getXCount()
	{
		return this.xcount.getIntValue();
	}
	
	public void setXCount( int xc)
	{
		this.xcount.setIntValue( xc);
	}
	
	public int getYCount()
	{
		return this.ycount.getIntValue();
	}
	
	public void setYCount( int yc)
	{
		this.ycount.setIntValue( yc);
	}

	public int getXSpace()
	{
		return this.xspace.getIntValue();
	}
	
	public void setXSpace(  int xs)	
	{
		this.xspace.setIntValue( xs);
	}
	
	public int getYSpace()
	{	
		return this.yspace.getIntValue();
	}
	
	public void setYSpace( int ys)
	{
		this.yspace.setIntValue( ys);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		
		Attributes lineAttrs = new Attributes();
		
		int xs = xspace.getIntValue();
		int ys = yspace.getIntValue();
		int xl = xcount.getIntValue() * xspace.getIntValue();
		int yl = ycount.getIntValue() * yspace.getIntValue();
		
		// first draw the vertical lines, Y1 and Y2 will be 0 and yl, X1 and X2 will step towards yl
		lineAttrs.setInt( Y1, 0);
		lineAttrs.setInt( Y2, yl);
		
		int xindex = xs;
		while ( xindex <= xl)
		{
			lineAttrs.setInt( X1, xindex);
			lineAttrs.setInt( X2, xindex);
			out.emptyTag( LINE, lineAttrs);
			xindex += xs;
		}		

		// now the horizontal lines, X1 and X2 will be 0 and xl, y1 and y2 will step towards yl
		lineAttrs.setInt( X1, 0);
		lineAttrs.setInt( X2, xl);
		
		int yindex = ys;
		while ( yindex <= yl)
		{
			lineAttrs.setInt( Y1, yindex);
			lineAttrs.setInt( Y2, yindex);
			out.emptyTag( LINE, lineAttrs);
			yindex += ys;
		}
	}
}
