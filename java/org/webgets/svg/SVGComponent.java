package org.webgets.svg;

import org.webgets.*;
import org.webgets.util.*;

abstract public class SVGComponent extends ComponentAdapter implements SVG
{
	protected Attributes attrs = new Attributes(); // FILL, "white", STROKE, "black", STROKE_WIDTH, "4");

	protected SVGComponent()
	{
		super();
	}
	
	protected SVGComponent( String label)
	{
		super( label);
	}
	
	// presentation attributes
	
	public void setStyle( String style)
	{
		this.attrs.put( STYLE, style);
	}
	
	public String getStyle()
	{
		return this.attrs.getAsString( STYLE);
	}
	
	public void setStyleClass( String styleClass)
	{
		this.attrs.put( CLASS, styleClass);
	}
	
	public String getStyleClass()
	{
		return this.attrs.getAsString( CLASS);
	}
	
	public void setFill( String color)
	{
		this.attrs.put( FILL, color);
	}
	
	public String getFill()
	{
		return this.attrs.getAsString( FILL);
	}
	
	public void setStroke( String color)
	{
		this.attrs.put( STROKE, color);
	}
	
	public String getStroke()
	{
		return this.attrs.getAsString( STROKE);
	}
	
	public void setStrokeWidth( String width)
	{
		this.attrs.put( STROKE_WIDTH, width);
	}
	
	public String getStrokeWidth()
	{
		return this.attrs.getAsString( STROKE_WIDTH);
	}
	
	public void setTransform( String transform)
	{
		this.attrs.put( TRANSFORM, transform);
	}
	
	public void addTransform( String transform)
	{
		if ( this.getTransform() != null)
			this.setTransform( this.getTransform() + " " + transform);
		else
			this.setTransform( transform);
	}
	
	/*
	 *	translate(<tx> [<ty>]), which specifies a translation by tx and ty. If <ty> is not provided, it is assumed to be zero.
	 */
	public void addTranslateTransform( String tx, String ty)
	{
		this.addTransform( TRANSLATE + "(" + tx + " " + ty + ")");
	}

	/*�
	 *	scale(<sx> [<sy>]),
	 */
	public void addScaleTransform( String sx, String sy)
	{
		this.addTransform( SCALE + "(" + sx + " " + sy + ")");
	}

	public String getTransform()
	{
		return this.attrs.getAsString( TRANSFORM);
	}
}
