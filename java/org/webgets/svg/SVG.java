package org.webgets.svg;

import org.webgets.util.HTTP;

/**

The SVG interface is implemented by all SVG Core objects, it extends Component
so all SVG objects respond to the standard onReqeust() and onDraw() methods and
may have a label, which a container could display.

*/
public interface SVG extends HTTP, org.webgets.Component // you can refer to all the SVG core objects using this interface
{
	String SVG_CONTENT_TYPE = "image/svg+xml"; // carefull these two will be identical Strings 
	String SVG_MIME_TYPE = "image/svg+xml";    // after intern(): SVG_CONTENT_TYPE == SVG_MIME_TYPE
	String XML_DOCUMENT = "<?xml version=\"1.0\" standalone=\"no\"?>";
	String SVG_DOCTYPE = "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\"\n"
		+ "  \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">";

	String XMLNS = "xmlns";
		String SVG_XMLNS = "http://www.w3.org/2000/svg";

	// Container Elements

	String SVG ="svg";
		String WIDTH = "width";
		String HEIGHT = "height";
		String VIEW_BOX = "viewBox";

	String G = "g";
		String DESC = "desc";
		String TITLE = "title";
	
	// Common Attributes
	
	String CLASS = "class";
	String STYLE = "style";
	String FILL = "fill";
	String STROKE = "stroke";
	String STROKE_WIDTH = "stroke-width";
	String TRANSFORM = "transform";
	
	// Simple Shapes

	String RECT = "rect";
		String X = "x";
		String Y = "y";
	
	String CIRCLE = "circle";
		String CX = "cx";
		String CY = "cy";
		String R = "r";
		
	String ELLIPSE = "ellipse";
		String RX = "rx";
		String RY = "ry";
		
	String LINE = "line";
		String X1 = "x1";
		String X2 = "x2";
		String Y1 = "y1";
		String Y2 = "y2";
		
	String POLYLINE = "polyline";
	String POLYGON = "polygon";
		String POINTS = "points";
	
	// Text Elements	
	
	String TEXT = "text";
		String DX = "dx";
		String DY = "dy";
		String FONT_FAMILY = "font-family";
		String FONT_SIZE = "font-size";
		String ROTATE = "rotate";
		String TEXT_LENGTH = "textLength";
		String LENGTH_ADJUST = "lengthAdjust";

	String A = "a";
		String XLINK_HREF = "xlink:href";
		String TARGET = "target";
		
	String SCALE = "scale";
	String TRANSLATE = "translate";

	String IMAGE = "image";

	String getTransform();

}
