package org.webgets.svg;

import org.webgets.util.*;

/**

interface SVGRectElement : 
                SVGElement,
                SVGTests,
                SVGLangSpace,
                SVGExternalResourcesRequired,
                SVGStylable,
                SVGTransformable,
                events::EventTarget { 

  readonly attribute SVGAnimatedLength x;
  readonly attribute SVGAnimatedLength y;
  readonly attribute SVGAnimatedLength width;
  readonly attribute SVGAnimatedLength height;
  readonly attribute SVGAnimatedLength rx;
  readonly attribute SVGAnimatedLength ry;
};

*/
public class Rect extends SVGComponent
{
	public static final String DEFAULT_X = "10";
	public static final String DEFAULT_Y = "10";
	public static final String DEFAULT_WIDTH = "100";
	public static final String DEFAULT_HEIGHT = "100";

	public Rect()
	{
		super();
		this.setX( DEFAULT_X);
		this.setY( DEFAULT_Y);
		this.setWidth( DEFAULT_WIDTH);
		this.setHeight( DEFAULT_HEIGHT);
	}
	
	public Rect( String x, String y)
	{
		super();
		this.setX( x);
		this.setY( y);
		this.setWidth( DEFAULT_WIDTH);
		this.setHeight( DEFAULT_HEIGHT);
	}
	
	public Rect( String x, String y, String fill)
	{
		super();
		this.setX( x);
		this.setY( y);
		this.setFill( fill);
		this.setWidth( DEFAULT_WIDTH);
		this.setHeight( DEFAULT_HEIGHT);
	}
	
	public Rect( String x, String y, String width, String height)
	{
		super();
		this.setX( x);
		this.setY( y);
		this.setWidth( width);
		this.setHeight( height);
	}
	
	public void setX( String x)
	{
		this.attrs.put( X, x);
	}
	
	public void setY( String y)
	{
		this.attrs.put( Y, y);
	}
	
	public void setWidth( String width)
	{
		this.attrs.put( WIDTH, width);
	}
	
	public void setHeight( String height)
	{
		this.attrs.put( HEIGHT, height);
	}
	
	public void setRX( String rx)
	{
		this.attrs.put( "rx", rx);
	}
	
	public void setRY( String ry)
	{
		this.attrs.put( "ry", ry);
	}
	
	// easy as pie to draw...
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.emptyTag( RECT, this.attrs);
	}
}
