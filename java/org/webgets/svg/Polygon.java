package org.webgets.svg;

import org.webgets.util.*;

public class Polygon extends Polyline
{
	public Polygon()
	{
		super();
	}
	
	public Polygon( String label)
	{
		super( label);
	}

	public Polygon( String label, String points)
	{
		super( label, points);
	}
	
	public void onDraw( TagWriter out)
	{
		super.traceDraw();
		out.emptyTag( POLYGON, this.attrs);
	} 
	
}
