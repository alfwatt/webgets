package org.webgets.svg;

import org.webgets.util.*;

/**

Draws inline SVG in it's own svg: namespace. Must be used inside an XMl document such as an XHTMLDocument

*/
public class InlineSVG extends org.webgets.ContainerAdapter implements SVG
{
	public static final String DEFAULT_STYLE = "width:250px; height:250px";
	public static final String NS = "xmlns:svg";
	private Attributes attrs = new Attributes( NS, SVG_XMLNS, STYLE, DEFAULT_STYLE);
	
	public InlineSVG()
	{
		super();
	}
	
	public InlineSVG( String label)
	{
		super( label);
	}
	
	public InlineSVG( String label, String style)
	{
		this( label);
		this.setStyle( style);
	}
	
	public void setStyle( String style)
	{
		this.attrs.put( STYLE, style);
	}
	
	public String getStyle()
	{
		return this.attrs.getAsString( STYLE);
	}
	
	public String getTransform()
	{
		return "";
	}
	
	/**
	
	<svg:svg xmlns:svg="http://www.w3.org/2000/svg" style="width:200px; height:300px">
		<!-- SVG content goes here -->
	</svg:svg>

	*/
	public void onDraw( TagWriter out)
	{
		out.namespace( SVG);
		out.tag( SVG, this.attrs);
		super.onDraw( out);
		out.closeTag();
		out.closeNamespace();
	}
}
