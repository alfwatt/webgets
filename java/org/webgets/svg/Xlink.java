package org.webgets.svg;

import org.webgets.util.*;

/**

<a xlink:href="General.xml" target="Center"><text x="20" y="32" style="&st1;">General</text></a> 


<!ENTITY % aExt "" >
<!ELEMENT a       (#PCDATA|desc|title|metadata|defs|
                   path|text|rect|circle|ellipse|line|polyline|polygon|
                   use|image|svg|g|view|switch|a|altGlyphDef|
                   script|style|symbol|marker|clipPath|mask|
                   linearGradient|radialGradient|pattern|filter|cursor|font|
                   animate|set|animateMotion|animateColor|animateTransform|
                   color-profile|font-face
                   %ceExt;%aExt;)* >
<!ATTLIST a
  %stdAttrs;
  xmlns:xlink CDATA #FIXED "http://www.w3.org/1999/xlink"
  xlink:type (simple) #FIXED "simple" 
  xlink:role %URI; #IMPLIED
  xlink:arcrole %URI; #IMPLIED
  xlink:title CDATA #IMPLIED
  xlink:show (new|replace) 'replace'
  xlink:actuate (onRequest) #FIXED 'onRequest'
  xlink:href %URI; #REQUIRED
  %testAttrs;
  %langSpaceAttrs;
  externalResourcesRequired %Boolean; #IMPLIED
  class %ClassList; #IMPLIED
  style %StyleSheet; #IMPLIED
  %PresentationAttributes-All;
  transform %TransformList; #IMPLIED
  %graphicsElementEvents;
  target %LinkTarget; #IMPLIED >

*/
public class Xlink extends org.webgets.DelegatorAdapter implements SVG
{
	private Attributes attrs = new Attributes();
	
	public Xlink( String href)
	{
		super( href);
		this.setHref( href);
	}
	
	public Xlink( String href, String text)
	{
		super( href, new Text( text));
		this.setHref( href);
	}
	
	public Xlink( String href, SVG delegate)
	{
		super( href, delegate);
		this.setHref( href);
	}
	
	public void setHref( String href)
	{
		this.attrs.put( XLINK_HREF, href);
	}
	
	public void setTarget( String target)
	{
		this.attrs.put( TARGET, target);
	}
	
	public String getTransform()
	{
		return "";
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.tag( A, this.attrs);
			super.onDraw( out);
		out.closeTag();
	}
}
