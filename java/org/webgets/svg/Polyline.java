package org.webgets.svg;

import org.webgets.util.*;

public class Polyline extends SVGComponent
{
	public Polyline()
	{
		super();
	}
	
	public Polyline( String label)
	{	
		super( label);
	}
	
	public Polyline( String label, String points)
	{
		super( label);
		this.setPoints( points);
	}
	
	public void setPoints( String points)
	{
		this.attrs.put( POINTS, points);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.emptyTag( POLYLINE, this.attrs);
	}
}
