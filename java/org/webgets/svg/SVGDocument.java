package org.webgets.svg;

import org.webgets.*;
import org.webgets.util.*;

public class SVGDocument extends ContainerAdapter implements SVG
{
	
	public SVGDocument()
	{
		super();
	}
	
	public SVGDocument( String label)
	{
		super( label);
	}
	
	public SVGDocument( String width, String height)
	{
		super();
		this.setWidth( width);
		this.setHeight( height);
	}
	
	public SVGDocument( String width, String height, String viewbox)
	{
		super();
		this.setWidth( width);
		this.setHeight( height);
		this.setViewBox( viewbox);	
	}
	
	public SVGDocument( String label, String width, String height, String viewbox)
	{
		super( label);
		this.setWidth( width);
		this.setHeight( height);
		this.setViewBox( viewbox);
	}
	
	private Attributes svgAttrs = new Attributes( new String[] { WIDTH, "500", HEIGHT, "500", XMLNS, SVG_XMLNS});
	
	public void setWidth( String width)
	{
		this.svgAttrs.put( WIDTH, width);
	}
	
	public String getWidth()
	{
		return this.svgAttrs.getAsString( WIDTH);
	}
	
	public void setHeight( String height)
	{
		this.svgAttrs.put( HEIGHT, height);
	}
	
	public String getHeight()
	{
		return this.svgAttrs.getAsString( HEIGHT);
	}
	
	public void setViewBox( String viewbox)
	{
		this.svgAttrs.put( VIEW_BOX, viewbox);
	}
	
	public String getViewBox()
	{
		return this.svgAttrs.getAsString( VIEW_BOX);
	}
	
	public String getTransform()
	{
		return "";
	}
	
	/**
	

	<?xml version="1.0" standalone="no"?>
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" 
		"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
		
	<svg width="10cm" height="5cm" viewBox="0 0 1000 500" 
		xmlns="http://www.w3.org/2000/svg">
		
		<!-- draw the contents here -->
		
	</svg>
	
	*/
	public void onDraw( TagWriter out)
	{
		out.enableXML();
		out.getResponse().setHeader( HTTP.CONTENT_TYPE, "image/svg+xml");
		
		out.println( XML_DOCUMENT);
		out.println( SVG_DOCTYPE);
		out.tag( SVG, this.svgAttrs);
			out.tag( TITLE);
				out.print( this.getLabel());
			out.closeTag();
			super.onDraw( out);
		out.closeTag();
		out.flush();
	}
}
