package org.webgets.svg;

import org.webgets.util.*;

public class Group extends SVGContainer
{
	public Group()
	{	
		super();
	}
	
	public Group( String transform)
	{
		super();
		this.setTransform( transform);
	}
	
	public Group( SVG component)
	{
		super( component);
	}
	
	public Group( String transform, SVG component)
	{	
		super( component);
		this.setTransform( transform);
	}
		
	public void onDraw( TagWriter out)
	{
		out.tag( G, this.attrs);
			super.onDraw( out);
		out.closeTag();
	}
}
