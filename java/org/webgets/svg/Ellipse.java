package org.webgets.svg;

import org.webgets.util.*;

public class Ellipse extends SVGComponent
{
	public static final String DEFAULT_CY = "100";
	public static final String DEFAULT_CX = "100";
	public static final String DEFAULT_RX = "25";
	public static final String DEFAULT_RY = "75";
	
	public Ellipse()
	{
		super();
		this.setCY( DEFAULT_CY);
		this.setCX( DEFAULT_CX);
		this.setRX( DEFAULT_RX);
		this.setRY( DEFAULT_RY);
	}
		
	public Ellipse( String cx, String cy)
	{
		super();
		this.setCY( cx);
		this.setCX( cy);
		this.setRX( DEFAULT_RX);
		this.setRY( DEFAULT_RY);
	}
	
	public Ellipse( String cx, String cy, String rx, String ry)
	{
		super();
		this.setCX( cx);
		this.setCY( cy);
		this.setRX( rx);
		this.setRY( ry);
	}
	
	public void setCX( String cx)
	{
		this.attrs.put( CX, cx);
	}
	
	public String getCX()
	{
		return this.attrs.getAsString( CX);
	}
	
	public void setCY( String cy)
	{
		this.attrs.put( CY, cy);
	}
	
	public String getCY()
	{
		return this.attrs.getAsString( CY);
	}
	
	public void setRX( String rx)
	{
		this.attrs.put( RX, rx);
	}
	
	public String getRX()
	{
		return this.attrs.getAsString( RX);
	}
	
	public void setRY( String ry)
	{
		this.attrs.put( RY, ry);
	}
	
	public String getRY()
	{
		return this.attrs.getAsString( RY);
	}
	
	public void onDraw( TagWriter out)
	{
		super.onDraw( out);
		out.emptyTag( ELLIPSE, this.attrs);
	}	

}
