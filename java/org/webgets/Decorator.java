package org.webgets;

import org.webgets.util.*;

/**

Decorator defines two methods for decorating the output stream. Either a commponent or a stirng is provided
and should be decorated by the implementing class.

*/
public interface Decorator
{
	void onDraw( TagWriter out, Component delegate);
	void onDraw( TagWriter out, String text);
}	
