#!/usr/bin/python

#
#	rss.py -- rss feed objects for webgets
#

from webgets import *

DATE_FORMAT_RSS = "%a, %d %b %G %T %Z"

MIME_TYPE_RSS = 'application/rss+xml'
MIME_TYPE_XML = 'text/xml'
RSS = 'rss'
CHANNEL = 'channel'
VERSION = 'version'
VERSION_091 = '0.91'
TITLE = 'title'
DESCRIPTION = 'description'
LINK = 'link'
LANGUAGE = 'language'
RATING = 'rating'
ITEM = 'item'
IMAGE = 'image'
URL = 'url'
WIDTH = 'width'
HEIGHT = 'height'
TEXTINPUT = 'textinput'
NAME = 'name'
COPYRIGHT = 'copyright'
PUBDATE = 'pubDate'
LASTBUILDDATE = 'lastbuilddate'
DOCS = 'docs'
MANAGINGEDITOR = 'managingeditor'
WEBMASTER = 'webmaster'
HOUR = 'hour'
DAY = 'day'
SKIPHOURS = 'skiphours'
SKIPDAYS = 'skipdays'

class RssFeed(Container):

	def __init__(self, title=None, link=None, description=None, components=[], language="en-us"):
		Container.__init__(self, label=title, components=components)
		self.title = title
		self.link = link
		self.description = description
		self.language = language

	def service( self, req, res):
		res.outln('Content-Type: text/xml')
		res.outln()
		
		res.outln('<?xml version="1.0" ?>')
		res.outln()
		
		res.tagln( RSS, {VERSION:VERSION_091})
		res.tagln( CHANNEL)

		res.tout( TITLE, self.title)
		res.tout( LINK, self.link)
		res.tout( DESCRIPTION, self.description)
		res.outln()
		
		Container.service( self, req, res)

		res.end( CHANNEL)
		res.end( RSS)
		res.outln()

class Item(Component):
	
	def __init__( self, title=None, link=None, description=None, date=None):
		Component.__init__(self, label=title)
		self.title = title
		self.link = link
		self.description = description
		self.date = date
	
	def service( self, req, res):
		res.tagln( ITEM)
		
		res.tout( TITLE, self.title)
		res.tout( LINK, self.link)
		res.tout( PUBDATE, self.date)
		res.tout( DESCRIPTION, self.description)
		
		res.end( ITEM)
		res.outln()


if __name__ == '__main__':

	request = Request()
	response = Response()
	feed = RssFeed( title="Feed Title", link="http://foo.com/", description="...")

	feed.append( Item( title="Item Title", link="http://bar.com/", description="..."))
	feed.append( Item( title="Item Title", link="http://bar.com/", description="..."))

	feed.service( request, response)
