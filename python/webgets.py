#!/usr/bin/python

import os
import re
import cgi

import posixpath

true=1
false=0

DIV="div"
CLASS="class"

"""
webgets is an web drawing toolkit which supports xthml, rss, atom and may be
extended to other xml document types. the simplest way to extend webgets is to
extend the Component or Container classes and implement the service method.
this trivial subclass of component says 'Hello Web':

class Hello(Component):

	def service( self, request, response):
		response.out("Hello Web") 

As you can see the extention is small and concise. it can be included into a
webgets document and run by creating a simple cgi script

"""

class Request:

	"""
	models the request from the browser, including headers, the HTTP method
	and GET or POST parameters. right now this is accomplished using the
	existing cgi module. in the future the request should be parsed.
	
	TODO patch in cgi for headers 
	TODO support for POST bodies and uploads
	"""

	def __init__( self, site_config={}):
		self.site_config = site_config
		self.params = cgi.parse()
		
	def env( self, name):
		"returns the CGI environment variable named"
		try:
			return os.environ[name]
		except:
			return None

	def config( self, name):
		"returns the config value with the name provided"
		try:
			return self.site_config[name]
		except:
			return None

	def param( self, name):
		"returns the URL or POST parameter values by name"
		try:
			param = self.params[name]
			if len(param) == 1:
				return param[0]
			else:
				return param
		except:
			return None

	# these are conveniance methods for various common request properties

	def method( self):
		"returns the HTTP method of the Request: GET POST PROPFIND etc..."
		return self.env('REQUEST_METHOD')

	def host( self):
		"returns the host name that this request was sent to"
		return self.env('HTTP_HOST')
		
	def referrer( self):
		"returns the referring URL"
		return  self.env('HTTP_REFERRER')
	
	def root( self):
		"returns the filesytem path of the root of the application"
		return self.config('site_root')

	def url( self):
		"returns the full url this request was made to"
		return "http://%s" % posixpath.join(self.host(), self.env('SCRIPT_URL'))
		
	def scripturl( self, script):
		"returns the url of a script in the same location as us"
		base = posixpath.dirname( self.env('SCRIPT_URL'))
		path = posixpath.join( self.host(), base, script)
		return "http://%s" % path

	def path( self):
		"returns the file system portion of a path to the right of the site root"
		
		p = self.param('path')
		if p != None:
			return p
		else:
			return ""

	def selected( self):
		"returns the posixpath to the selected file"
		return posixpath.join(self.root(), self.path())


class Response:

	"""
	Provides methods for priting XML tags to the output stream.
	
	TODO tag stack with checked closes.
	
	Here's a component which uses the resopnse to draw a table with the x and y
	provided.
	
	class Table (Component):
		
		def service( self, req, res):

			x = 5
			y = 5
			n = 1

			res.tag( "table")
			for each row in 1 .. y:
				res.tag("tr")
				for each col in 1 .. x:
					res.tag("td")
					res.out(n++)
					res.end("td")
				res.end("tr)"
			res.end( "table")

	Which outputs the following xhtml:
	
		<table>
			<tr>
				<td>1</td>
				<td>2</td>
				<td>3</td>
				<td>4</td>
				<td>5</td>
			</tr>
			<tr>
				<td>6</td>
				....
			</tr>
			...
		</table>	

	"""

	def tag( self, tag, params=None):
		""" print a tag, optionally with parameters specified in an dictionary """
		if params == None:
			print "<%s>" % tag,
		else:
			print "<%s" % tag,
			for key in params.keys():
				print "%s=\"%s\"" % (key, params[key]),
			print ">",
	
	def tagln( self, tag, params=None):
		""" print a tag, as tag() and followed by a newline """"
		self.tag( tag, params)
		self.outln()
	
	def end( self, tag):
		""" print an end-tag """
		print "</%s>" % tag

	def out( self, string=''):
		""" print the string """
		print string,

	def outln( self, string=''):
		""" print the string followed by a newline """
		print string

	def etag( self, tag, params=None):
		""" print an empty tag with the optional params """
		if params == None:
			print "<%s />" % tag
		else:
			print "<%s" % tag,
			for key in params.keys():
				print "%s=\"%s\"" % (key, params[key]),
			print "/>"

	def tout( self, tag, string):
		""" small conveniance to quickly put a tag around a string """
		print "<%s>%s</%s>" % (tag, string, tag)


class Component:

	"""
	
	The base class of all visible webgets, Component defines the minimal constructor
	and interface for a simple component. 

	The component simply prints it's label text to the response. If a Component
	is given an html_class it will print a <div> tag with that class around it's
	label text:
	
	<div class="html_class">label</div>
	
	"""

	def __init__( self, label="-", html_class=None):
		self.label = label
		self.html_class = html_class

	def service( self, request, response):
		if self.html_class != None:
			response.tag( DIV, {CLASS: self.html_class})
		
		response.out( self.label)
		
		if self.html_class != None:
			response.end( DIV)


class Container (Component, list):

	"""
	a container is a list of components which are drawn when the
	container is drawn and which may process requests as well 
	"""
	
	def __init__( self, label="+", html_class=None, components=[]):
		Component.__init__( self, label, html_class)
		for item in components:
			self.append(item)
	
	def service( self, request, response):
		""" 
		Containers default service methos draws an optional <div>
		with the Containers html_class around a list of it's components 
		"""
		if self.html_class != None:
			response.tag( DIV, {CLASS: self.html_class})
		for component in self:
			component.service(request, response)
		if self.html_class != None:
			response.end( DIV)
