#!/usr/bin/python

#
# this module provides the xhtml elements
#

from webgets import *

DOCTYPE = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-strict.dtd\">"
ID = "id"  #       // id          ID             #IMPLIED   document-wide unique id
CLASS = "class"  # // class       CDATA          #IMPLIED   space separated list of classes
STYLE = "style"  # // style       %StyleSheet;   #IMPLIED   associated style info
TITLE = "title"  # // title       %Text;         #IMPLIED	advisory title/amplification

# 	internationalization attributes

LANG = "lang"  #         // lang        %LanguageCode; #IMPLIED	language code (backwards compatible)
XML_LANG = "xml:lang"  # // xml:lang    %LanguageCode; #IMPLIED	language code (as per XML 1.0 spec)
DIR = "dir"  #			  // dir         (ltr|rtl)      #IMPLIED	direction for weak/neutral text
LTR = "ltr"  
RTL = "rtl"

# 	attributes for common UI events

ONCLICK = "onclick"  #	//%Script;       #IMPLIED		a pointer button was clicked
ONDBLCLICK = "ondblclick"  #	//%Script;       #IMPLIED	a pointer button was double clicked
ONMOUSEDOWN = "onmousedown"  #	//%Script;       #IMPLIED	a pointer button was pressed down
ONMOUSEUP = "onmouseup"  #	//%Script;       #IMPLIED		a pointer button was released
ONMOUSEOVER = "onmouseover"  #	//%Script;       #IMPLIED	a pointer was moved onto the element
ONMOUSEMOVE = "onmousemove"  #	//%Script;       #IMPLIED	a pointer was moved
ONMOUSEOUT = "onmouseout"  #	//%Script;       #IMPLIED	a pointer was moved away 
ONKEYPRESS = "onkeypress"  #	//%Script;       #IMPLIEDa key was pressed and released
ONKEYDOWN = "onkeydown"  #	//%Script;       #IMPLIED	a key was pressed down
ONKEYUP = "onkeyup"  #	//%Script;       #IMPLIED a key was released

# 	attributes for elements that can get the focus

ACCESSKEY = "accesskey"  #	//%Character;    #IMPLIED	accessibility key character
TABINDEX = "tabindex"  #	//%Number;       #IMPLIED	 position in tabbing order
ONFOCUS = "onfocus"  #	//%Script;       #IMPLIED	the element got the focus
ONBLUR = "onblur"  #	//%Script;       #IMPLIED	 the element lost the focu

# Text Elements

# <!ENTITY % special ... >

BR = "br"  # 
SPAN = "span"  # 
BDO = "bdo"  # 
OBJECT = "object"  
IMG = "img"  
MAP = "map"  

# <!ENTITY % fontstyle ... >

TT = "tt"  
I = "i"  
B = "b"  
BIG = "big"  
SMALL = "small"  

# <!ENTITY % phrase ... >

EM = "em"  
STRONG = "strong"  
SFN = "dfn"  
CODE = "code"  
Q = "q"  
SUB = "sub"  
SUP = "sup"  
SAMP = "samp"  
KBD = "kbd"  
VAR = "var"  
CITE = "cite"  
ABBR = "abbr"  
ACRONYM = "acronym"  

# <!ENTITY % inline.forms ... >

FORM = "form"
ACTION = "action"
METHOD = "method"
INPUT = "input"
SELECT = "select"
OPTION = "option"
TEXTAREA = "textarea"
LABEL = "label"
BUTTON = "button"  
TEXT = "text"
VALUE = "value"
SUBMIT = "submit"
RESET = "reset"
SIZE = "size"
ROWS = "rows"
COLS = "cols"

# <!-- these can occur at block or inline level --> <!ENTITY % misc ... >

INS = "ins"  # 
DEL = "del" ;
SCRIPT = "script"  # 
NOSCRIPT = "noscript"  

# Block level elements

# <!ENTITY % heading .. >

H1 = "h1"  
H2 = "h2"  
H3 = "h3"  
H4 = "h4"  
H5 = "h5"  
H6 = "h6"  
	
# <!ENTITY % lists ... >

UL = "ul"  
OL = "ol"  
DL = "dl"  
LI = "li"
DT = "dt"
DFN = "dfn"
	
# <!ENTITY % blocktext ... >

PRE = "pre"  
HR = "hr"  
BLOCKQUOTE = "blockquote"  
ADDRESS = "address"  
	
# <!ENTITY % block ... %heading; | %lists; | %blocktext; >

PARAGRAPH = "p"  
DIV = "div"  
FIELDSET = "fieldset"
TABLE = "table"
ALIGN = "align"
LEFT = "left"
CENTER = "center"
RIGHT = "right"
JUSTIFY = "justify"
CHAR = "char"
CHAROFF = "charoff"
VALIGN = "valign"
TOP = "top"
MIDDLE = "middle"
BOTTOM = "bottom"
BASELINE = "baseline"
CAPTION = "caption"
THEAD = "thead"
TFOOT = "tfoot"
TBODY = "tbody"
COLGROUP = "colgroup"
COL = "col"
TR = "tr"
TH = "th"
TD = "td"
SUMMARY = "summary"
WIDTH = "width"
BORDER = "border"
FRAME = "frame"
RULES = "rules"
CELLSPACING = "cellspacing"
CELLPADDING = "cellpadding"
ROW = "row"
ROWGROUP = "rowgroup"
AXIS = "axis"
HEADERS = "headers"
SCOPE = "scope"
ROWSPAN = "rowspan"
COLSPAN = "colspan"

HTML = "html"
XMLNS = "xmlns"
XHTML_NS = "http://www.w3.org/1999/xhtml"

HEAD = "head"
PROFILE = "profile"
META = "meta"
LINK = "link"
BASE = "base"
STYLESHEET = "stylesheet"
TEXT_CSS = "text/css"
TEXT_XML = "text/xml"
TEXT_HTML = "text/html"
TEXT_PLAIN = "text/plain"

HTTP = "http"
NAME = "name"
CONTENT = "content"
SCHEME = "scheme"

CHARSET = "charset"
ANCHOR = "a"
HREF = "href"
HREFLANG = "hreflang"
TYPE = "type"
REL = "rel"
REV = "rev"
MEDIA = "media"

XML_SPACE = "xml:space"
PRESERVE = "preserve"

SRC = "src"
DEFER = "defer"

BODY = "body"
ONLOAD = "onload"
ONUNLOAD = "onunload"



class Document (Container):

	def __init__( self, title="Untitled", html_class=None, components=[], style=None):
		Container.__init__( self, title, html_class, components)
		self.style = style

	"""
	Document is the root container of a webgets page. it creates the xhtml doccument
	skeleton and draws it's contents as the body. the label is printed in the title.
	"""
	
	def service( self, req, res):
		print "Content-Type: text/html\n\n"

		res.tag(HTML)
		
		res.tag(HEAD)
		res.tag(TITLE)
		res.out( self.label)
		res.end(TITLE)
		if self.style != None:
			res.tag(STYLE)
			res.out( self.style)
			res.end(STYLE)
		res.end(HEAD)
		
		res.tag(BODY)
		Container.service( self, req, res)
		res.end(BODY)
		
		res.end(HTML)


class Html (Component):

	"""
	provides a method to insert inline HTML code
	"""
	
	def __init__( self, html):
		self.html = html

	def service( self, req, res):
		res.out( self.html);


class Br (Component):
	
	def service( self, req, res):
		res.etag( BR)



class P (Component):
	
	def __init__(self, body, html_class=None):
		Component.__init__(self, body, html_class)
		self.body = body
	
	def service( self, req, res):
		if self.html_class == None:
			res.tout( PARAGRAPH, self.body)
		else:
			res.tag( PARAGRAPH, {CLASS:self.html_class})
			res.out( self.body)
			res.end( PARAGRAPH)

class A (Component):

	def __init__(self, body, link, html_class=None):
		Component.__init__(self, body, html_class)
		self.body = body
		self.link = link
		
	def service( self, req, res):
		if self.html_class == None:
			res.tag( ANCHOR, {HREF:self.link})
		else:
			res.tag( ANCHOR, {HREF:self.link, CLASS:self.html_class})
		res.out( self.body)
		res.end( ANCHOR)


class Pre (Component):
	
	def __init__(self, body, html_class=None):
		Component.__init__(self, body, html_class)
		self.body = body
		
	def service( self, req, res):
		if self.html_class == None:
			res.tout( PRE, self.body)
		else:
			res.tag( PRE, {CLASS:self.html_class})
			res.out( self.body)
			res.end( PRE)


class Div (Container):
	""" simple subclass of Container for conveniance """

class List(Component):

	def __init__(self, list, html_class=None):
		Component.__init__(self, "list %i items" % len(list), html_class)
		self.list = list
		
	def service( self, req, res):
		if self.html_class == None:
			res.tag( UL)
		else:
			res.tag( UL, {CLASS:self.html_class})
			
		for item in list:
			res.tag( LI)
			res.out( item)
			res.cend( LI)
			
		res.end(UL)
		
class Dictionary(Component):

	def __init__(self, dict, html_class=None):
		Component.__init__(self, "dict %i keys" % len(dict.keys()), html_class)
		self.dict = dict
		
	def service( self, req, res):
		if self.html_class == None:
			res.tag( DL)
		else:
			res.tag( DL, {CLASS:self.html_class})
			
		for key in self.dict.keys():
			res.tag( DT)
			res.out( key)
			res.end( DT)
			res.tag( DFN)
			res.out( self.dict[key])
			res.end( DFN)
		
		res.end(DL)

#
#
# Forms and related elements

class Form (Container):

	def __init__(self, action="", method="get"):
		Component.__init__(self)
		self.action = action
		self.method = method

	def service( self, req, res):
		res.tag( FORM, {ACTION: self.action, METHOD: self.method})
		Container.service( self, req, res)
		res.end( FORM)

class Input (Component):

	def __init__(self, name=None, value=None, type=TEXT, size=42):
		Component.__init__(self, name)
		self.value = value
		self.type = type
		self.size = size
		
	def service( self, req, res):
		res.etag( INPUT, {TYPE: self.type, 
								NAME: self.label, 
								VALUE: self.value,
								SIZE:  self.size})

class Select (Component):

	def __init__(self, name=None, options=[]):
		Component.__init__(self, name)
		self.options = options
		
	def service( self, req, res):
		res.tag( SELECT, {NAME: self.label})
		for opt in self.options:
			res.tag( OPTION, {VALUE: opt})
			res.out( opt)
			res.end( OPTION)
		res.end( SELECT)
		
class Textarea (Input):

	def __init__(self, name=None, value=None, rows=10, cols=42):
		Input.__init__(self, name, value)
		self.rows = rows
		self.cols = cols

	def service( self, req, res):
		res.tag( TEXTAREA, {NAME: self.label,
								  ROWS: self.rows,
								  COLS: self.cols})
		res.out( self.value)
		res.end( TEXTAREA)


#
# test code
#

if __name__ == '__main__':
	"""test the webgets module by creating a document with
	one of each object and displaying it"""

	c = Document("Webgets Base Components Self Test")
	c.append( Component( "Webgets Component"))
	c.append( Container( components=(Component("Components"), Component("in Conatiner"))))
	c.append( P("Paragraph"))
	c.append( Html( "Inline <b>HTML</b> <hr />"))
	
	f = Form()
	f.append( Input( name="test", value="test", type=TEXT))
	f.append( Br())
	f.append( Textarea( name="text", value="text area"))
	f.append( Input( name="go", value="Go", type=SUBMIT))
	f.append( Input( name="undo", value="Undo", type=RESET))
	
	c.append( f)
	
	c.service(Request(), Response())
