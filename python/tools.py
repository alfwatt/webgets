#!/usr/bin/python

import os
import grp
import pwd
import time

from xhtml import *

true=1
false=0

class Clock (Component):

	def __init__(self):
		Component.__init__(self, self.__class__.__name__, self.__class__.__name__)
		
	def service( self, req, res):
		res.tag( DIV, {CLASS:self.html_class})
		res.out( time.strftime( "%a %r", time.localtime()))
		res.end( DIV)


class Calendar (Component):

	def __init__(self):
		Component.__init__(self, self.__class__.__name__, self.__class__.__name__)
		
	def service( self, req, res):
		res.tag( DIV, {CLASS:self.html_class})
		
		res.tag( TABLE)
		
# to draw the calendar we need to know:
#	what day of the week is the first of this month?
#	how many days in this month?
#	what day of the month is it now? (so we can make it bold)

		now = time.time()
		local = time.localtime(now)

		# Month and Year
		res.tag( TR)
		res.tag( TH, {COLSPAN: 7})
		res.out( time.strftime( "%B %Y", local))
		res.end( TH)
		res.end( TR)

		# Days of the Week
		res.tag( TR)
		for day in ("M", "T", "W", "T", "F", "S", "S"):
			res.tag( TH)
			res.out( day)
			res.end( TH)
		res.end( TR)

		d = 1

		# TODO how many weeks in the month?
		for week in range( 1, 6):
			res.tag( TR)
			for day in range( 1, 8):
				res.tag( TD)
				res.out( d)
				d = d+1
				res.end( TD)
			res.end( TR)

		res.tag( TR)
		res.tag( TH, {COLSPAN:7})
		res.out( time.strftime( "%Y.%m.%d", time.localtime()))
		res.end( TH)
		res.end( TR)
		res.end( TABLE)
		res.end( DIV)


#	this is the calenar code from webgets which draws a calendar correctly
# 	many of the functions of java.util.Calendar are not directly avaliable
#	in python so we have to modify the algorithym
#
# 			int week = 1;
# 			while ( week++ <= this.calendar.getActualMaximum( Calendar.WEEK_OF_MONTH))
# 			{
# 				out.tag( TR, CALENDAR_TR_ATTRS);
# 					int dayOfWeek = 1;
# 					while ( dayOfWeek++ <= 7)
# 					{
# 						day.add( Calendar.DAY_OF_MONTH, 1);
# 						if ( day.get( Calendar.MONTH) != this.calendar.get( Calendar.MONTH))
# 							out.tag( TD, this.disabledAttrs);
# 						else if ( day.equals( this.calendar))
# 							out.tag( TD, this.selectedAttrs);
# 						else
# 							out.tag( TD);
# 						
# 						out.tag( A, HREF, out.getRequestURL(), new String[] { 
# 							RequestEvent.TARGET, this.getIdString(),
# 							YEAR, Integer.toString( day.get( Calendar.YEAR)),
# 							MONTH, Integer.toString( day.get( Calendar.MONTH)),
# 							DAY, Integer.toString( day.get( Calendar.DAY_OF_MONTH))});
# 							out.print( day.get( Calendar.DAY_OF_MONTH));
# 						out.closeTag();
# 						out.closeTag();
# 					}
# 				out.closeTag();
# 			}
# 		
# 
# 		out.closeTag( TABLE);		
