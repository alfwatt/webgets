#!/usr/bin/python

#
# webgets_files module encapsulates presentation of the file system
#

import os
import stat

import posixpath

import grp
import pwd
import time

from xhtml import *

true=1
false=0


# fixed sort order by modify time
def cmp_stat_mtime(y,x):
	stat_x = os.stat( x)
	stat_y = os.stat( y)
	return cmp( stat_x[stat.ST_MTIME], stat_y[stat.ST_MTIME])


class Index (Component):
	
	def __init__(self, path=None):
		Component.__init__(self, path, "webgets.files.index")
		self.path = path
	
	def service( self, req, res):
		if self.path == None:
			self.path = req.selected()

		p = self.path

		name = None
		for seg in req.root().split(os.sep):
			if seg != '':
				name = seg

		res.tag( DIV, {CLASS:self.html_class})
		res.tag( ANCHOR, {HREF:req.url()})
		res.out( name)
		res.end( ANCHOR)
		res.etag( BR)

		if os.path.isdir( self.path):		
			for file in os.listdir(self.path):
				if os.path.isdir( os.path.join(self.path, file)):
					res.out('&nbsp;&nbsp;')
					res.tag( 'a', {HREF:"%s?path=%s" %
						(req.url(), posixpath.join( req.path(), file))})
					res.out( file)
					res.end( 'a')
					res.etag( BR)

		res.end( DIV)



class Summary(Container):
	""" summary displays a summary of the files at a path or  """

	def __init__(self, path=None):
		Container.__init__( self, path)
		self.path = path

	def service( self, req, res):
		list = []

		if self.path == None:
			self.path = os.path.join( req.root(), req.path())

		if os.path.isdir( self.path):
			for file in os.listdir(self.path):
				if file[0] == '.':
					continue
				list.append( os.path.join( self.path, file))

			list.sort(cmp_stat_mtime)

			for f in list:
				self.append( Preview( f))
		else:
			list.append( self.path)

		Container.service( self, req, res)


class Preview (Component):
	
	def __init__(self, path=None):
		Component.__init__(self, path, self.__class__.__name__)
		self.path = path
		
	def service( self, req, res):
		if self.path == None:
			self.path = req.selected()

		self.info = Info( self.path)
			
		res.tag( DIV, {CLASS:self.html_class})

		self.info.service( req, res)
		
		# directories
		if os.path.isdir( self.path):
			res.tag( UL)
			for file in os.listdir( self.path):
				if file[0] == '.':
					continue
				p = posixpath.join( self.path, file)
				res.tag( LI)
				res.out( '<a href="%s?path=%s">%s</a>' % (req.url(), p, file))
				res.end( LI)
			res.end( UL)
		else:
			mime_type = "application/unknown"
			base, ext = posixpath.splitext(self.path)

			if len( ext) > 0:
				site_types = req.config('site_types') 
				if ext in site_types.keys():
					mime_type = site_types[ext]
			
			# text/plain
			if mime_type.startswith( "text"):
				f = open( self.file)

				# TODO Make this smarter, read the first
				# paragraph or x chars whichever is less
	
				for i in range( 1, 5):
					line = f.readline()
					res.out( line)
					res.etag( BR)
			
			if mime_type.startswith( "image") :
				file = "http://%s%s%s" % (req.host(), req.config('site_files'), path)
				res.etag( 'img', {SRC:file})

		res.end( DIV)


class Viewer (Component):

	def __init__(self, path=None):
		Component.__init__(self, path)
		self.path = path
	
	def service( self, req, res):
		if self.path == None:
			self.path = req.selected()

		f = open( self.path)
		p = self.path[len(req.root()):]

		mime_type = "application/unknown"
		base, ext = posixpath.splitext(self.path)

		res.tag( DIV, {CLASS:self.__class__.__name__})

		if self.path.endswith( '.txt'):
			for line in f.readlines():
				res.out( line)
				res.etag( BR)

		if self.path.endswith( '.png'):
			file = "http://%s%s%s" % (req.host(), req.config('site_files'), p)
			res.etag( 'img', {SRC:file})

		if self.path.endswith( '.gif'):
			file = "http://%s%s%s" % (req.host(), req.config('site_files'), p)
			res.etag( 'img', {SRC:file})

		if self.path.endswith( '.jpg'):
			file = "http://%s%s%s" % (req.host(), req.config('site_files'), p)
			res.etag( 'img', {SRC:file})

		if self.path.endswith( '.pdf'):
			file = "http://%s%s%s" % (req.host(), req.config('site_files'), p)
			res.etag( 'img', {SRC:file})

		if self.path.endswith( '.tif'):
			file = "http://%s%s%s" % (req.host(), req.config('site_files'), p)
			res.etag( 'img', {SRC:file})

		if self.path.endswith( '.tiff'):
			file = "http://%s%s%s" % (req.host(), req.config('site_files'), p)
			res.etag( 'img', {SRC:file})

		res.end( DIV)


class Info (Component):

	def __init__(self, path=None, detail=false):
		Component.__init__(self, path)
		self.path = path
		self.detail = detail
	
	def service( self, req, res):
		if self.path == None:
			self.path = req.selected()

		p = self.path[len(req.root()):]
		name = posixpath.basename( self.path)
		info = os.stat( self.path)

		res.tag( DIV, {CLASS:self.__class__.__name__})

		res.tag( ANCHOR, {HREF:"%s?path=%s" % (req.url(), p)})
		res.out( p)
		res.end( ANCHOR)

		res.out( "<br />Updated: ")
		res.out( time.strftime( "%a, %d %b %r", time.localtime(os.path.getmtime(self.path))))
		if os.path.isdir(self.path):
			res.out( '- %i items' % len(os.listdir(self.path)))
		else:
			res.out( '- %i bytes' % os.path.getsize(self.path))

		if self.detail:
			res.tag( PRE)
			res.outln( "name: %s" % self.selected)
			res.outln( "mode: 0x%o" % info[stat.ST_MODE])
			res.outln( "inode: %i" % info[stat.ST_INO])
			res.outln( "device: %i" %  info[stat.ST_DEV])
			res.outln( "links: %i" % info[stat.ST_NLINK])
			res.outln( "user: %s" % pwd.getpwuid(info[stat.ST_UID])[0])
			res.outln( "group: %s" % grp.getgrgid(info[stat.ST_GID])[0])
			res.outln( "size: %i"   % os.path.getsize(self.path))
			res.outln( "access: %s" % os.path.getatime(self.path))
			res.outln( "modify: %s" % os.path.getmtime(self.path))
			res.outln( "create: %s" % os.path.getctime(self.path))
			res.end( PRE)
			
		res.end( DIV)

class Today (Component):
	""" shows the items in the current path which have been updated today """
	
	def service( self, req, res):
		res.tag( DIV, {CLASS:'today'})
		res.out( "Today")
		res.end( DIV)


