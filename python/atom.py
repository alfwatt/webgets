#!/usr/bin/python

#
# atom.py provides atom object for the webgets toookit
#

from webgets import *

MIME_TYPE_ATOM = 'application/atom+xml'
MIME_TYPE_XML ='text/xml'

FEED = 'feed'
VERSION = 'version'
XMLNS = 'xmlns'
VERSION_03 = '0.3'
VERSION_10 = '1.0'
XMLNS_ATOM = 'http://purl.org/atom/ns#'
TITLE = 'title'
LINK = 'link'
HREF = 'href'
MODIFIED = 'modified'
AUTHOR = 'author'
NAME = 'name'
ENTRY = 'entry'
REL = 'rel'
ALTERNATE = 'alternate'
TEXT_HTML = 'text/html'
TYPE = 'type'
HREF = 'href'
ID = 'id'
ISSUED = 'issued'
MODIFIED = 'modified'


class AtomFeed( Container):

	def __init__(self, title=None, link=None, author=None, components=[]):
		Container.__init__(self, label=title, components=components)
		self.link = link
		self.author = author
		self.date = 'TODO get the date and format it correctly'
	
	def service( self, req, res):
		res.outln('Content-Type: text/xml')
		res.outln()
		res.outln()

		res.outln('<?xml version="1.0" encoding="utf-8"?>')
		res.outln()

		res.tagln( FEED, {VERSION:VERSION_03, XMLNS:XMLNS_ATOM})
		res.tout( TITLE, self.label)
		res.etag( LINK, {REL:ALTERNATE, TYPE:TEXT_HTML, HREF:self.link})
		res.tout( MODIFIED, self.date)
		res.tag( AUTHOR)
		res.tout( NAME, self.author)
		res.end( AUTHOR)
		res.outln()
		
		Container.service( self, req, res)
		
		res.end( FEED)
		res.outln()


class Entry( Component):

	def __init__(self, title=None, link=None, id=None, issued=None, modified=None):
		Component.__init__(self, label=title)
		self.link = link
		self.id = id
		self.issued = issued
		self.modified = modified
		
	def service( self, req, res):
		res.tagln( ENTRY)
		res.tout( TITLE, self.label)
		res.etag( LINK, {REL:ALTERNATE, TYPE:TEXT_HTML, HREF:self.link})
		res.tout( ID, self.id)
		res.tout( ISSUED, self.issued)
		res.tout( MODIFIED, self.modified)
		res.end( ENTRY)
		res.outln()


if __name__ == '__main__':
	feed = AtomFeed( title="Test Feed", link="http://site.com/", author="Nobody")
	feed.append( Entry( title="Test Entry", link="http://site.com/#1", id="1"))
	feed.append( Entry( title="Test Entry", link="http://site.com/#1", id="2"))
	feed.append( Entry( title="Test Entry", link="http://site.com/#1", id="3"))
	feed.service( Request(), Response())
