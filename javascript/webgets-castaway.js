/*

castaway.js -- rss to html converter in ajax

requires utilities.js be included before this file
requires webgets.js be included before this file

*/

Castaway.prototype = new Component();

function Castaway(id, feed)
{
    this.label = feed;
    this.node = document.getElementById(id);
    this.feed = feed;
    
    // todo read in the feed url or path
    this.request = XMLRequest();
    this.request.onreadystatechange = this.update;
    this.request.open("GET", this.feed, true);
    this.request.send();
    
    // todo create a dom tree with the feed info
    
    // todo write dom tree into a cache for re-use
}

Castaway.prototype.update = function()
{
    this.node.innerHTML = "!";
}