/* webgets.js -- webgets components for javascript */

/*

Component is the simplest webget, it has a label and knows how to 
draw itself by setting the innerHTML of the node it's attached to.

*/
function Component(id, label)
{
    this.node = document.getElementById(id);
    this.label = label;
}

Component.prototype.draw = function() 
{
    this.node.innerHTML = this.label;
}

/*

Container is a collection of Components and other Containers,
it inherits all the properties and functions of Component and
includes an add(Component) method for assembling groups.

*/

Container.prototype = new Component();
Container.prototype.constructor = Container;

function Container(id, label, components)
{
    this.node = document.getElementById(id);
    this.label = label;
    if (components)
        this.components = components;
    else
        this.components = new Array()
}

Container.prototype.add = function(component)
{
    components[components.length] = component;
}
    
Container.prototype.draw = function()
{
    var index = 0;
    while ( index < components.length)
    {
        components[index++].draw();
    }
}
