

function XMLRequest(feed) 
{
    var httprequest=false

    if (window.XMLHttpRequest) // Mozilla, Safari
    {
        httprequest=new XMLHttpRequest()
        if (httprequest.overrideMimeType)
            httprequest.overrideMimeType('text/xml')
    }
    else if (window.ActiveXObject) // IE
    {
        try { httprequest=new ActiveXObject("Msxml2.XMLHTTP"); } 
        catch (e)
        {
            try { httprequest=new ActiveXObject("Microsoft.XMLHTTP"); }
            catch (e) {}
        }
    }
    
    // set the httprequest source url & content type
    
    
    return httprequest;
}

function fade(node, start, finish, duration)
{
    var current = start;
    var step = ((Math.abs(finish) - Math.abs(start)) / duration);
    while ( duration > 0)
    {
        // adjust the current transparency
        current = current + step;
        
        // set the alpha on the component to current
        
        // decrement the duration
        duration = duration - 1;
    }
}
