<?xml version="1.0" encoding="UTF-8"?>

<!--    
 -->

<pml>
    <language>
        <name>Webgets Patterns</name>
        <synopsis>The Webgets Pattern Language describes a set of software interfaces
        for creating modular web pages. These patterns have expressions in java,
        python, objective-c, javascript and other langugages which are adapted from
        these descriptions. Each implementation is differently adapted to it's native
        idion but all share these same fundamental concepts.</synopsis>

        <pattern>
            <name>Component</name>
            <synopsis>Webgets Applications are composed of cooperating
            page components.<synopsis>
            <problem>
                <note>Quickly build modular web pages</note>
                <condition>Web Envirnonment</condition>
            </problem>
            <solution>Define a simple interface for darwing an object on the page
            and sending input </solution>
            <consequence>page composition seperated from functional elements</consequence>
            <relationship>
                <related>Design Patterns: Composite</related>
                <forward>Superpattern</forward>
                <reverse>Subpattern</reverse>
                <note>Component describes the same concept as the Component in
                the standard Composite pattern.</note>
            </relationship>
        </pattern>
        
        <pattern>
            <name>Container</name>
            <note></note>
            <problem>
                <note>Pages need to be organized</note>
                <condition>Web Environment</condition>
                <condition>Webgets Components</condition>
            </problem>
            <solution>
            The Container pattern provides for the manageent and display of groups of
            Components. Containers are collecitons which have a particular layout and
            may provide functionality such a tabbed groups, image grids or other high-
            level functionality for page authors to use.
            </solution>
            <consequence>Hierarchy of Containers</consequence>
            <consequence>Message Forwarding</consequence>
            <consequence>Rendering System</consequence>
            <relationship>
                <related>Webgets: Component</related>
                <forward>Container</forward>
                <reverse>Contained</reverse>
                <note>Components are collected into Containers which manage
                the passing of event and drawing messages.</note>
            </relationship>
        </pattern>

        <pattern>
            <name>Decorator</name>
            <note>Decorators are Flyweight objects which provide styling
            services for outputting markup.</note>
            <problem>
                <note>Some components would have many identical instances
                and memory is precious.</note>
                <condition>Webgets Container needs to format Components</condition>
                <condition>Adding decoration to components adds complexity</condition>
            </problem>
            <solution>
            Combine the standard Decorator and Flyweight patterns to create a 
            Webgets: Decorator which allows you to decorate any Component by 
            proxying drawing responsability through a shared object which can
            write out a open tag, draw the component then draw the end tag.
            </solution>
            <consequence>Seperation of styling and active page elements</consequence>
            <consequence>Temptaion to put too much styling information into page insead
            of applying styles to the page.</consequence>
            <relationship>
                <related>Webgets: Component</related>
                <forward>Decorated</forward>
                <reverse>Decprator</reverse>
                <note></note>
            </relationship>
            <relationship>
                <related>Webgets: Flyweight</related>
                <forward></forward>
                <reverse></reverse>
                <note></note>
            </relationship>
            <relationship>
                <related>Webgets: Decorator</related>
                <forward></forward>
                <reverse></reverse>
                <note></note>
            </relationship>
        </pattern>    
    
    </language>
</pml>
        